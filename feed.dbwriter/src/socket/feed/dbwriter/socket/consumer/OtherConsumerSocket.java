package feed.dbwriter.socket.consumer;

import com.eqtrade.SocketInterface;

import feed.dbwriter.consumer.OtherConsumer;
import feed.dbwriter.socket.core.ProcessorConsumerThread;

public class OtherConsumerSocket extends OtherConsumer {
	private SocketInterface socketConsumer;
	private ProcessorConsumerThread processor;

	public OtherConsumerSocket(SocketInterface socket) throws Exception {
		super("");
		this.socketConsumer = socket;
		processor = new ProcessorConsumerThread(this);
		processor.start();		
	}
	
	@Override
	public boolean connect() throws Exception {
		this.socketConsumer.sendMessage(("subscribe " + types+" "+msg.getSeqno()).getBytes());
		return true;
	}

	@Override
	public void exit() throws Exception {
		this.socketConsumer.sendMessage(("unsubscribe " + types).getBytes());
		processor.setStop();

	}

	public void addMsg(String msg) {
		processor.addMsg(msg);
	}


}
