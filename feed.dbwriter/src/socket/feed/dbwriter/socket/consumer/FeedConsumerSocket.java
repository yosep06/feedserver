package feed.dbwriter.socket.consumer;

import org.jfree.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eqtrade.ClientSocket;
import com.eqtrade.Receiver;
import com.eqtrade.SocketFactory;
import com.eqtrade.SocketInterface;

import feed.builder.msg.StockSummary;
import feed.dbwriter.consumer.FeedConsumer;
import feed.dbwriter.consumer.HisTradeSummaryConsumer;
import feed.dbwriter.consumer.OtherConsumer;
import feed.dbwriter.consumer.QuoteConsumer;
import feed.dbwriter.consumer.StockSummaryConsumer;
import feed.dbwriter.consumer.TradeConsumer;

public class FeedConsumerSocket extends FeedConsumer implements Receiver {
	private SocketInterface socketConsumer;
	private OtherConsumerSocket otherConsumer;
	private TradeConsumerSocket tradeConsumer;
	private QuoteConsumerSocket quoteConsumer;
	private Logger log = LoggerFactory.getLogger(getClass());
	private StockSummmarySocket stockSummaryConsumer;
	private TradeSummarTSSocketConsumer tradeSummaryTS;
	private HisTradeSummaryConsumer histTradeSumm;

	public FeedConsumerSocket() throws Exception {
		super();
		socketConsumer = SocketFactory.createSocket(
				config.getProperty("socket.consumer"), this);
		socketConsumer.start();
		otherConsumer = new OtherConsumerSocket(socketConsumer);
		tradeConsumer = new TradeConsumerSocket(socketConsumer);
		quoteConsumer = new QuoteConsumerSocket(socketConsumer);
		stockSummaryConsumer = new StockSummmarySocket(socketConsumer);
		stockSummaryConsumer.start();
		
		tradeSummaryTS = new TradeSummarTSSocketConsumer(socketConsumer);
		tradeSummaryTS.start();
		
		histTradeSumm = new HisTradeSummarrySocket(socketConsumer);
		histTradeSumm.start();
	}

	@Override
	public void connected(ClientSocket sock) {
		log.info("connected socket");
		sock.validated(true);
	}

	@Override
	public void disconnect(ClientSocket arg0) {
		log.info("disconnected socket");
		stop();
	}

	public StockSummmarySocket getStockSummaryConsumer() {
		return stockSummaryConsumer;
	}

	@Override
	public void receive(ClientSocket sock, byte[] bt) {
		String msg = new String(bt);
		log.info("receive "+msg);

		String[] sp = msg.split("\\!");
		if (sp[0].equals("0")) {
			
			String[] data = sp[1].split("\\|");

			if (data[1].equals("5")) {
				stockSummaryConsumer.newMessage(msg);

			}else if (data[1].equals("TS")) {
				tradeSummaryTS.newMessage(msg);;
			} else if (data[1].equals("TSSI")) {
				histTradeSumm.newMessage(msg);;
			} else {

				otherConsumer.addMsg(sp[1]);
			}
		} else if (sp[0].equals("1")) {
//			 quoteConsumer.addMsg(sp[1]);
		} else if (sp[0].equals("2")) {
//			 tradeConsumer.addMsg(sp[1]);
		}

	}

	@Override
	public TradeConsumer getTradeConsumer() {
		// TODO Auto-generated method stub
		return tradeConsumer;
	}

	@Override
	public QuoteConsumer getQuoteConsumer() {
		// TODO Auto-generated method stub
		return quoteConsumer;
	}

	@Override
	public OtherConsumer getOtherConsumer() {
		// TODO Auto-generated method stub
		return otherConsumer;
	}

	@Override
	public void start() throws Exception {
		otherConsumer.connect();
	}

	@Override
	public void stop() {
		try {
			otherConsumer.exit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public TradeSummarTSSocketConsumer getTradeSummaryTS() {
		return tradeSummaryTS;
	}
	
	public HisTradeSummaryConsumer getHistTradeSumm(){
		return histTradeSumm;
	}

}
