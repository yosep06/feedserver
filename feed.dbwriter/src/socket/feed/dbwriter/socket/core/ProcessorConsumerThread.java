package feed.dbwriter.socket.core;

import java.rmi.RemoteException;
import java.util.Vector;

import feed.provider.core.MsgListener;

public class ProcessorConsumerThread extends Thread {

	private MsgListener listener;
	private Vector<String> vmsg = new Vector<String>();
	private boolean terminated = false;

	public ProcessorConsumerThread(MsgListener listener) {
		this.listener = listener;
	}

	@Override
	public void run() {
		while (!terminated) {
			if (vmsg.isEmpty()) {
				synchronized (vmsg) {
					try {
						vmsg.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else
				try {
					listener.newMessage(vmsg.remove(0));
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		
		while(!vmsg.isEmpty()){
			try {
				listener.newMessage(vmsg.remove(0));
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	public void addMsg(String msg) {
		synchronized (vmsg) {
			vmsg.add(msg);
			vmsg.notify();
		}
	}

	public void setStop() {
		terminated = true;
		synchronized (vmsg) {
			vmsg.notify();
		}
	}

}
