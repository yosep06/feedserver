package feed.dbwriter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.PropertyConfigurator;

import com.eqtrade.database.AccessQueryLocator;
import com.eqtrade.utils.properties.FEProperties;

import feed.dbwriter.consumer.FeedConsumer;
import feed.dbwriter.core.Database;
import feed.dbwriter.core.FileConfig;
import feed.dbwriter.socket.consumer.FeedConsumerSocket;


public class FeedDBWriterApp {
	private Console console;
	private FeedConsumerSocket consumer;
	private FileConfig config;
	private final Log log = LogFactory.getLog(getClass());	
	private Database database;
	AccessQueryLocator accesData;
	
	public void start() throws Exception{
		config = new FileConfig("feed.dbwriter.config");
		database = new Database();
		database.start(config.getProperty("database"));
		//FEProperties fep = new FEProperties("gtw.properties");
		accesData = new AccessQueryLocator(config.getProperty("database"),false);
		consumer = new FeedConsumerSocket();
		consumer.getTradeConsumer().setAccess(accesData);
		consumer.getTradeConsumer().setDb(database);
		consumer.getQuoteConsumer().setAccess(accesData);
		consumer.getQuoteConsumer().setDb(database);
		consumer.getOtherConsumer().setAccess(accesData);
		consumer.getOtherConsumer().setDb(database);
		
		consumer.getStockSummaryConsumer().setAccess(accesData);
		consumer.getStockSummaryConsumer().setDb(database);
		consumer.getTradeSummaryTS().setAccess(accesData);
		consumer.getTradeSummaryTS().setDb(database);
		consumer.getHistTradeSumm().setAccess(accesData);
		consumer.getHistTradeSumm().setDb(database);
		consumer.start();
		console = new Console(this);
		console.start();
		 Runtime.getRuntime().addShutdownHook(new Thread() {
			    public void run() { 
			    	try {
				    	log.info("shutingdown sapplication");
			    	} catch (Exception ex){}
			    }
		 });
		 for(int i=0;i<5;i++)
			 ping();
	}
	
    private Connection conn = null;
	public boolean ping(){
		boolean result = true;
		try {
			String query = "select 1 as correct from feed_currency";
			ResultSet rec = null;
			Statement st = null;		
			if (conn == null) conn = database.getConnection();
			st = conn.createStatement();
			rec = st.executeQuery(query);
			System.out.println("ping db OK!");
			rec.close();
			st.close();
		} catch (Exception ex){
			try {
				result = false;
				System.out.println("db ping error: "+ex.getMessage());
				System.out.println("trying to reconnect...");
				conn = null;
			    database.start(config.getProperty("database"));
			    System.out.println("db back to live");
			} catch (Exception exy){
				System.out.println(exy.getMessage());
				System.out.println("reconnecting failed, please ping again!");
			}
		}
		return result;
	}		
	
	public void stop(){
		consumer.stop();
		database.setStop();
	}
	
	public void print(){
		consumer.getTradeSummaryTS().print();
	}
	
	public static void main(String[] args) {
		try {
			PropertyConfigurator.configure("log.properties");
			new FeedDBWriterApp().start();
		} catch (Exception ex){
			ex.printStackTrace();
			System.exit(-1);
		}
	}

}
