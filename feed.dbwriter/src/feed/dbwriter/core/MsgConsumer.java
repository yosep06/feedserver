package feed.dbwriter.core;

import java.rmi.Naming;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.eqtrade.database.AccessQueryLocator;

import feed.builder.core.MsgManager;
import feed.provider.core.MsgProducer;

public class MsgConsumer extends MsgManager {
	protected MsgProducer provider;
	protected Database database;
	protected AccessQueryLocator accesData;
	protected Log log = LogFactory.getLog(getClass());	
	
	public MsgConsumer(String url, String type) throws Exception{
		super(url, type);
	}
	
	public void setDb(Database database){
		this.database = database;
	}
	
	public void setAccess(AccessQueryLocator accesData){
		this.accesData = accesData;
	}
		
	public boolean connect() throws Exception {
		provider = (MsgProducer) Naming.lookup(url);
		if (provider != null){
			provider.subscribe(this, "", "", (int)msg.getSeqno());
		}
		log.info("connection to: "+url+" ready");
		return provider!=null;
	}
	
	public void exit() throws Exception {
		provider.unsubscribe(this);
	}
}
