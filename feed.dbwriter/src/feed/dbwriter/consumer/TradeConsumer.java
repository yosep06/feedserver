package feed.dbwriter.consumer;

import java.rmi.RemoteException;

import feed.builder.msg.Trade;
import feed.dbwriter.core.MsgConsumer;


public class TradeConsumer extends MsgConsumer {
	public TradeConsumer(String url) throws Exception{
		super(url, "1");
	}
	
	public void newMessage(String message) throws RemoteException {
		try {
		    	String[] data = message.split("\\|");
		    	Trade tr = new Trade();
		    	tr.setContent(data);
		    	if (tr.getSeqno() > msg.getSeqno()){
			    		msg.setSeqno((long)tr.getSeqno());
			    		db.putMsg(msg);
		    	}
		} catch (Exception ex){
			System.out.println("error while processing: "+message);
			ex.printStackTrace();
		}
	}		
}
