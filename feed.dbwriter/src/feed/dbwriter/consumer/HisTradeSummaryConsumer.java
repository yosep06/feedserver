package feed.dbwriter.consumer;

import java.sql.Connection;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eqtrade.database.AccessQueryLocator;

import feed.builder.msg.Message;
import feed.builder.msg.TradeSummary;
import feed.dbwriter.core.Database;
import feed.dbwriter.core.FileConfig;
import feed.provider.data.FeedMsg;

public class HisTradeSummaryConsumer extends Thread {

	private boolean terminated = false;
	private Vector<String> vmsgTemp = new Vector<String>();
	private Database database;
	private Connection connTradeSumm = null;
	private final String C_TRADESUMM_INSERT_HIS="insert into FEED_TRADESUMMARY_HIS values('%s','%s','%s','%s','%s',%f,%f,%f,%f,%f,%f,%f,%f)";
	private final String C_TRADESUMM_UPDATE_HIS ="update FEED_TRADESUMMARY_HIS set buyavg=%f, sellavg=%f, buyvol=%f, buyval=%f, buyfreq=%f, sellvol=%f, sellval=%f, sellfreq=%f where " +
			" transdate='%s' and stock='%s' and board='%s' and broker='%s' and investor='%s'";
	protected FeedMsg msg;
	protected FileConfig config;

	protected final static SimpleDateFormat formatDate = new SimpleDateFormat(
			"yyyyMMdd");
	protected final static NumberFormat format = new DecimalFormat("###0");
	private final static SimpleDateFormat dateFormat = new SimpleDateFormat(
			"yyyy-MM-dd");
	private final static SimpleDateFormat timeFormat = new SimpleDateFormat(
			"HHmmss");

	private Logger log = LoggerFactory.getLogger(getClass());
	private long end = -1,lastupdate = 0;;
	AccessQueryLocator accesData;
	
	public void setAccess(AccessQueryLocator accesData){
		this.accesData = accesData;
	}
	
	public void setDb(Database database) {
		this.database = database;
		log.info(this.database.toString());
		initConnection();
	}
	
	protected void loadConfig() throws Exception {
		msg = new FeedMsg();
		msg.setType("SETTING");
		config = new FileConfig("feed.dbwriter.config");
		msg.setMsg(formatDate.format(new Date()));
		msg.setSeqno(0);
		log.info("seqno for type Trade Summary HTS " + msg.getSeqno());
	}

	private void initConnection() {
		try {
			connTradeSumm = database.getConnection();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
	
	@Override
	public void run() {
		while (!terminated) {
			if (vmsgTemp.isEmpty()) {
				synchronized (vmsgTemp) {
					try {
						vmsgTemp.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else
				try {
					processHTS(vmsgTemp.remove(0));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}

		while (!vmsgTemp.isEmpty()) {
			try {
				processHTS(vmsgTemp.remove(0));

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	private void processHTS(String sdata) {
		String[] data = sdata.split("\\|");
		Message dat = null;
		if(data[1].equals("TSSI")){
			
			dat = new TradeSummary();
    		dat.setContent(data);
    		
    			Object[] param = new Object[]{

						new Double(((TradeSummary)dat).getBuyavg()),
						new Double(((TradeSummary)dat).getSellavg()),
						new Double(((TradeSummary)dat).getBuyvol()),
						new Double(((TradeSummary)dat).getBuyval()),
						new Double(((TradeSummary)dat).getBuyfreq()),
						new Double(((TradeSummary)dat).getSellvol()),
						new Double(((TradeSummary)dat).getSellval()),
						new Double(((TradeSummary)dat).getSellfreq()),
						dateFormat.format(new Date()),
						((TradeSummary)dat).getStock(),
						((TradeSummary)dat).getBoard(),
						((TradeSummary)dat).getBroker(),
						((TradeSummary)dat).getInvestor()
				};
    			if (updateStatement(C_TRADESUMM_UPDATE_HIS, param, connTradeSumm)==0){
					
					insertStatement(C_TRADESUMM_INSERT_HIS, 		    					
							new Object[]{	
							
							dateFormat.format(new Date()),
							((TradeSummary)dat).getStock(),
							((TradeSummary)dat).getBoard(),
							((TradeSummary)dat).getBroker(),
							((TradeSummary)dat).getInvestor(),
							new Double(((TradeSummary)dat).getBuyavg()),
							new Double (((TradeSummary)dat).getSellavg()),
							new Double(((TradeSummary)dat).getBuyvol()),
							new Double(((TradeSummary)dat).getBuyval()),
							new Double(((TradeSummary)dat).getBuyfreq()),
							new Double(((TradeSummary)dat).getSellvol()),
							new Double(((TradeSummary)dat).getSellval()),
							new Double(((TradeSummary)dat).getSellfreq())
							
							}, connTradeSumm
					);		    			
				}
		}
	}

	private int updateStatement(String query, Object[] data, Connection conn) {
		Statement st = null;
		int result = 0;
		try {
			if (conn == null)
				conn = database.getConnection();
			st = conn.createStatement();
			result = st.executeUpdate(String.format(query, data));
		} catch (Exception sqle) {
//			log.info("update database error " + sqle.getMessage());
//			sqle.printStackTrace();
			// try { conn.close();} catch (Exception ex){}
			result = -1;
			database.restart();
			if (database == null) {
				database = new Database();
				try {
					database.start(config.getProperty("database"));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			initConnection();
		} finally {
			try {
				st.close();
			} catch (Exception ex) {
			}
		}
		return result;
	}

	private int insertStatement(String query, Object[] data, Connection conn) {
		Statement st = null;
		int result = 0;
		try {
			if (conn == null)
				conn = database.getConnection();
			st = conn.createStatement();
			result = st.executeUpdate(String.format(query, data));
		} catch (Exception sqle) {
			 log.info("insert database error "+sqle.getMessage());
			// sqle.printStackTrace();
			// try { conn.close();} catch (Exception ex){}
			if (database == null) {
				database = new Database();
				try {
					database.start(config.getProperty("database"));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			result = -1;
		} finally {
			try {
				st.close();
			} catch (Exception ex) {
			}
		}
		return result;
	}
	
	public void newMessage(String message) {
		synchronized (vmsgTemp) {
			vmsgTemp.add(message);
			vmsgTemp.notifyAll();
		}
	}
	

}
