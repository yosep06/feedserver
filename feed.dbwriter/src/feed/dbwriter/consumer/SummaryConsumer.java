package feed.dbwriter.consumer;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.LogFactory;

import com.db4o.query.Query;

import feed.builder.msg.Message;
import feed.builder.msg.StockSummary;
import feed.builder.msg.TradeSummary;
import feed.dbwriter.core.Database;
import feed.dbwriter.core.MsgConsumer;

public class SummaryConsumer extends MsgConsumer{
	
	private HashMap ssMap ;//key stock+board

	public SummaryConsumer(String url) throws Exception {
		super(url, "3");
		// TODO Auto-generated constructor stub
	}
	public void setDb(Database database){
		this.database = database;
		initConnection();
	}
	private void initConnection(){
		try {
			connStockSumm = database.getConnection();
		} catch (Exception ex){}		
	}
	private Connection connStockSumm = null;
	private Connection connTradeSumm = null;
		
	protected void loadConfig(){
		log = LogFactory.getLog(getClass());	
		super.loadConfig();
			
	}
	private final String C_STOCKSUMM_INSERT="insert into feed_stocksummary values ('%s', '%s', '%s', %f, %f, %f, %f, %f, %f, %f)";
	private final String C_STOCKSUMM_UPDATE="update feed_stocksummary set high=%f, low=%f, last=%f, prev=%f, vol=%f, freq=%f where transdate='%s' and stock='%s' and board='%s'";
	private final String FEED_STOCKSUMMARY_INV_INSERT = "insert into FEED_STOCKSUMMARY_INV values ('%s', '%s','%s',%f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, '%s')";
	private final String FEED_STOCKSUMMARY_INV_UPDATE = "update FEED_STOCKSUMMARY_INV set  t_remark='%s', prev=%f, high=%f, low=%f, t_close=%f, t_change=%f, tradevol=%f, tradeval=%f, tradefreq=%f, t_index=%f, t_foreign=%f, t_open=%f, bestbid=%f, bestbidvol=%f, bestoffer=%f, bestoffervol=%f where stock='%s' and transdate='%s' and board='%s' ";
	private final String C_TRADESUMM_INSERT="insert into feed_tradesummary values('%s','%s','%s','%s','%s',%f, %f, %f, %f, %f, %f, %f, %f, '%s')";
	private final String C_TRADESUMM_UPDATE ="update feed_tradesummary set buyavg=%f, sellavg=%f, buyvol=%f, buyval=%f, buyfreq=%f, sellvol=%f, sellval=%f, sellfreq=%f where " +
			" transdate='%s' and stock='%s' and board='%s' and broker='%s' and investor='%s'";

	
	public void newMessage(String message) throws RemoteException{
		
		String[] data = message.split("\\|");
    	Message dat = null;
    	//System.out.println("message : "+message);
    	
		if (data[1].equals("5")){
			dat = new StockSummary();
			
    		dat.setContent(data);
    		if (dat.getSeqno()>=msg.getSeqno() && ((StockSummary)dat).getTradefreq()>0){
						Object[] param = new Object[]{	
								new Double(((StockSummary)dat).getHigh()),
								new Double(((StockSummary)dat).getLow()), 
								new Double(((StockSummary)dat).getClose()), 
								new Double(((StockSummary)dat).getPrev()),
								new Double(((StockSummary)dat).getTradevol()),
								new Double(((StockSummary)dat).getTradefreq()),								
								dateFormat.format(new Date()),
								((StockSummary)dat).getStock(), 
								((StockSummary)dat).getBoard()
						};
						if (updateStatement(C_STOCKSUMM_UPDATE, param, connStockSumm)==0){
							insertStatement(C_STOCKSUMM_INSERT, 		    					
									new Object[]{	
									dateFormat.format(new Date()),
									((StockSummary)dat).getStock(), 
									((StockSummary)dat).getBoard(),
									((StockSummary)dat).getOpen() == 0 ? new Double(((StockSummary)dat).getClose()) :  new Double(((StockSummary)dat).getOpen()), 
									new Double(((StockSummary)dat).getHigh()),
									new Double(((StockSummary)dat).getLow()), 
									new Double(((StockSummary)dat).getClose()), 
									new Double(((StockSummary)dat).getPrev()),
									new Double(((StockSummary)dat).getTradevol()),
									new Double(((StockSummary)dat).getTradefreq())								
									}, connStockSumm
							);		    			
						}
    		}
    		if (dat.getSeqno()>=msg.getSeqno() && ((StockSummary)dat).getTradefreq()>0){
			Object [] param2 = new Object []{
				
					((StockSummary)dat).getRemark(),
					new Double(((StockSummary)dat).getPrev()),
					new Double(((StockSummary)dat).getHigh()),
					new Double(((StockSummary)dat).getLow()),
					new Double(((StockSummary)dat).getClose()),
					new Double(((StockSummary)dat).getChange()),
					new Double(((StockSummary)dat).getTradevol()),
					new Double(((StockSummary)dat).getTradeval()),
					new Double(((StockSummary)dat).getTradefreq()),
					new Double(((StockSummary)dat).getIndex()),
					new Double(((StockSummary)dat).getForeign()),
					new Double(((StockSummary)dat).getOpen()),
					new Double(((StockSummary)dat).getBestbid()),
					new Double(((StockSummary)dat).getBestbidvol()),
					new Double(((StockSummary)dat).getBestoffer()),
					new Double(((StockSummary)dat).getBestoffervol()),
					((StockSummary)dat).getStock(),
					dateFormat.format(new Date()),
					((StockSummary)dat).getBoard()
					//data lot;
					
			};
			if(updateStatement(FEED_STOCKSUMMARY_INV_UPDATE , param2, connStockSumm)== 0){
				insertStatement(FEED_STOCKSUMMARY_INV_INSERT,
						new Object [] {
						//STOCK,BOARD,T_REMARK,PREV,HIGH,LOW,T_CLOSE,T_CHANGE,TRADEVOL,TRADEVAL
						//TRADEFREQ,T_INDEX,T_FOREIGN
						//T_OPEN,BESTBID,BESTBIDVOL,BESTOFFER,BESTOFFERVOL,TRANSDATE
						((StockSummary)dat).getStock(),
						((StockSummary)dat).getBoard(),
						((StockSummary)dat).getRemark(),
						new Double(((StockSummary)dat).getPrev()),
						new Double(((StockSummary)dat).getHigh()),
						new Double(((StockSummary)dat).getLow()),
						new Double(((StockSummary)dat).getClose()),
						new Double(((StockSummary)dat).getChange()),
						new Double(((StockSummary)dat).getTradevol()),
						new Double(((StockSummary)dat).getTradeval()),
						new Double(((StockSummary)dat).getTradefreq()),
						new Double(((StockSummary)dat).getIndex()),
						new Double(((StockSummary)dat).getForeign()),
						new Double(((StockSummary)dat).getOpen()),
						new Double(((StockSummary)dat).getBestbid()),
						new Double(((StockSummary)dat).getBestbidvol()),
						new Double(((StockSummary)dat).getBestoffer()),
						new Double(((StockSummary)dat).getBestoffervol()),
						dateFormat.format(new Date())
						
				}
						, connStockSumm); // sampai di sini
				
			}
			
}
    	}
		
		/*else if (data[1].equals("TS"))
		{

    		dat = new TradeSummary();
    		
    		
    		dat.setContent(data);
    		if (dat.getSeqno()>=msg.getSeqno()){
    			System.out.println("dat seq no || msg get seqno  : "+dat.getSeqno()+" || "+msg.getSeqno());
				Object[] param = new Object[]{
						new Double(((TradeSummary)dat).getBuyavg()),
						new Double(((TradeSummary)dat).getSellavg()),
						new Double(((TradeSummary)dat).getBuyvol()),
						new Double(((TradeSummary)dat).getBuyval()),
						new Double(((TradeSummary)dat).getBuyfreq()),
						new Double(((TradeSummary)dat).getSellvol()),
						new Double(((TradeSummary)dat).getSellval()),
						new Double(((TradeSummary)dat).getSellfreq()),
						dateFormat.format(new Date()),
						((TradeSummary)dat).getStock(),
						((TradeSummary)dat).getBoard(),
						((TradeSummary)dat).getBroker(),
						((TradeSummary)dat).getInvestor()
				};
				System.out.println("buy  :: "+(((TradeSummary)dat).getBuyfreq()));
				System.out.println("sell :: "+(((TradeSummary)dat).getSellfreq()));

				if (updateStatement(C_TRADESUMM_UPDATE, param, connTradeSumm)==0){
					insertStatement(C_TRADESUMM_INSERT, 		    					
							new Object[]{	
							dateFormat.format(new Date()),
							((TradeSummary)dat).getStock(),
							((TradeSummary)dat).getBoard(),
							((TradeSummary)dat).getBroker(),
							((TradeSummary)dat).getInvestor(),
							new Double(((TradeSummary)dat).getBuyavg()),
							new Double(((TradeSummary)dat).getSellavg()),
							new Double(((TradeSummary)dat).getBuyvol()),
							new Double(((TradeSummary)dat).getBuyval()),
							new Double(((TradeSummary)dat).getBuyfreq()),
							new Double(((TradeSummary)dat).getSellvol()),
							new Double(((TradeSummary)dat).getSellval()),
							new Double(((TradeSummary)dat).getSellfreq()),
							"H"						
							}, connTradeSumm
					);		    			
				}

    		} 
    	
		}*/
	}
	
	private final static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private final static SimpleDateFormat timeFormat = new SimpleDateFormat("HHmmss");

	private int  insertStatement(String query, Object[] data, Connection conn){
		Statement st = null;
		int result = 0;
		//System.out.println("query ins : "+query);
		try {
			if (conn == null) conn = database.getConnection();
			st = conn.createStatement();
			result = st.executeUpdate(String.format(query, data)); 
		} catch (Exception sqle){
			sqle.printStackTrace();
			//try { conn.close();} catch (Exception ex){}			
			result = -1;
		} finally {
			try { st.close();} catch (Exception ex){}
		}
		return result;
	}
	
	private int  updateStatement(String query, Object[] data, Connection conn){
		Statement st = null;
		int result=0;
		//System.out.println("query upd : "+query);
		try {
			if (conn == null) conn = database.getConnection();
			//System.out.println("update : "+data);
			st = conn.createStatement();
			
			result = st.executeUpdate(String.format(query, data)); 
			
		} catch (Exception sqle){
			sqle.printStackTrace();
			//try { conn.close();} catch (Exception ex){}			
			result = -1;
			database.restart();
			initConnection();
		} finally {
			try { st.close();} catch (Exception ex){}
		}
		return result;
	}
	
}
