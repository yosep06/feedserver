package feed.dbwriter.consumer;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.db4o.query.Query;
import com.eqtrade.database.FOData;
import com.eqtrade.database.QueryDataFeed.SQUERY;

import feed.builder.msg.Broker;
import feed.builder.msg.CorpAction;
import feed.builder.msg.Indices;
import feed.builder.msg.Message;
import feed.builder.msg.News;
import feed.builder.msg.Stock;
import feed.builder.msg.StockSummary;
import feed.builder.msg.Trade;
import feed.builder.msg.TradePrice;
import feed.builder.msg.TradeSummary;
import feed.dbwriter.core.Database;
import feed.dbwriter.core.FileConfig;
import feed.dbwriter.core.MsgConsumer;

public class OtherConsumer extends MsgConsumer {
	
	private Logger logIndices = LoggerFactory.getLogger("log.feed.dbwriter.consumer.logIndices");
	protected FileConfig config;
	
	public OtherConsumer(String url) throws Exception{
		super(url, "0");
	}
	

	public void setDb(Database database){
		this.database = database;
		initConnection();
	}
	
	private void initConnection(){
		try {
			connIndices = database.getConnection();
			connTradeSumm = database.getConnection();
			connStockSumm = database.getConnection();
			connStockIntraday = database.getConnection();
			connIndicesIntraday = database.getConnection();
		} catch (Exception ex){}		
	}
	
	public List getSnapShot(Class c, int seqno){
		Query query = db.getDb().getInstance().query();
		query.constrain(c);
		query.descend("seqno").orderAscending().constrain(new Long(seqno)).greater();
		List o = query.execute();
		return o;
	}
		
	protected void loadConfig(){
		log = LogFactory.getLog(getClass());	
		try {
			config = new FileConfig("feed.dbwriter.config");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		super.loadConfig();
	}	
	
	private Connection connIndices = null;
	private Connection connTradeSumm = null;
	private Connection connStockSumm = null;
	private Connection connStockIntraday = null;
	private Connection connIndicesIntraday = null;
	
	private final String C_INDICES_INSERT ="insert into feed_sectoral_indices values ('%s', '%s', %f, %f, %f, %f, %f)";
	private final String C_INDICES_UPDATE ="update feed_sectoral_indices set open=%f, high=%f, low=%f, last=%f, prev=%f where code='%s' and transdate='%s'";
	private final String C_TRADESUMM_INSERT="insert into feed_tradesummary values('%s','%s','%s','%s','%s',%f, %f, %f, %f, %f, %f, %f, %f, '%s')";
	private final String C_TRADESUMM_UPDATE ="update feed_tradesummary set buyavg=%f, sellavg=%f, buyvol=%f, buyval=%f, buyfreq=%f, sellvol=%f, sellval=%f, sellfreq=%f where " +
			" transdate='%s' and stock='%s' and board='%s' and broker='%s' and investor='%s'";
	private final String C_STOCKSUMM_INSERT="insert into feed_stocksummary values ('%s', '%s', '%s', %f, %f, %f, %f, %f, %f, %f)";
	private final String C_STOCKSUMM_UPDATE="update feed_stocksummary set high=%f, low=%f, last=%f, prev=%f, vol=%f, freq=%f where transdate='%s' and stock='%s' and board='%s'";
	private final String C_STOCKINTRADAY_INSERT="insert into feed_stockintraday values ('%s', '%s', '%s', %f)";
	private final String C_STOCKINTRADAY_UPDATE="update feed_stockintraday set last=%f where stock='%s' and transdate='%s' and transtime='%s' ";
	private final String C_INDICESINTRADAY_INSERT="insert into feed_indicesintraday values ('%s', '%s', '%s', %f)";
	private final String C_INDICESINTRADAY_UPDATE="update feed_indicesintraday set last=%f where code='%s' and transdate='%s' and transtime='%s' ";
	
	private final String FEED_STOCKSUMMARY_INV_INSERT = "insert into FEED_STOCKSUMMARY_INV values ('%s', '%s','%s',%f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, '%s')";
	private final String FEED_STOCKSUMMARY_INV_UPDATE = "update FEED_STOCKSUMMARY_INV set  t_remark='%s', prev=%f, high=%f, low=%f, t_close=%f, t_change=%f, tradevol=%f, tradeval=%f, tradefreq=%f, t_index=%f, t_foreign=%f, t_open=%f, bestbid=%f, bestbidvol=%f, bestoffer=%f, bestoffervol=%f where stock='%s' and transdate='%s' and board='%s' ";
	private final String C_TRADESUMM_INSERT_HIS="insert into FEED_TRADESUMMARY_HIS values('%s','%s','%s','%s','%s',%f,%f,%f,%f,%f,%f,%f,%f)";
	private final String C_TRADESUMM_UPDATE_HIS ="update FEED_TRADESUMMARY_HIS set buyavg=%f, sellavg=%f, buyvol=%f, buyval=%f, buyfreq=%f, sellvol=%f, sellval=%f, sellfreq=%f where " +
			" transdate='%s' and stock='%s' and board='%s' and broker='%s' and investor='%s'";
	
	
	public void newMessage(String message) throws RemoteException {
		try {
		    	String[] data = message.split("\\|");
		    	Message dat = null;
//		    	log.info("getMsg:"+message);
		    	if (data[1].equals("4")){
		    		dat =  new Broker();
		    		dat.setContent(data);
		    		if (dat.getSeqno()>=msg.getSeqno()){
		    		}
		    	} else if (data[1].equals("A")){
		    		dat = new CorpAction();
		    		dat.setContent(data);
		    		if (dat.getSeqno()>=msg.getSeqno()){
		    		}
		    	} else if (data[1].equals("6")){
		    		dat = new Indices();
		    		dat.setContent(data);
		    		
		    		logIndices.info(dat.toString());

		    		
					Object[] param = new Object[]{	new Double(((Indices)dat).getOpen()), 
														new Double(((Indices)dat).getHigh()),
														new Double(((Indices)dat).getLow()), 
														new Double(((Indices)dat).getIndex()), 
														new Double(((Indices)dat).getPrevious()),
														((Indices)dat).getCode(), 
														dateFormat.format(new Date())};
					FOData fo = new FOData();
					String[] str = new String[]{
							((Indices)dat).getCode(), 
							dateFormat.format(new Date())
					};
					Double[] dbl = new Double[]{
							new Double(((Indices)dat).getOpen()), 
							new Double(((Indices)dat).getHigh()),
							new Double(((Indices)dat).getLow()), 
							new Double(((Indices)dat).getIndex()), 
							new Double(((Indices)dat).getPrevious()),
					};
					fo.set_string(str);
					fo.set_double(dbl);
					Vector v = accesData.getQueryDataFeed().set_insert(SQUERY.sectoralindices.val
							, fo);

		    		/*if (updateStatement(C_INDICES_UPDATE, param, connIndices)==0){
		    			insertStatement(C_INDICES_INSERT, 		    					
		    					new Object[]{	((Indices)dat).getCode(), dateFormat.format(new Date()),  
		    												new Double(((Indices)dat).getOpen()),  new Double(((Indices)dat).getHigh()), 
		    												new Double(((Indices)dat).getLow()),  new Double(((Indices)dat).getIndex()),  
		    												new Double(((Indices)dat).getPrevious()) }, connIndices
		    			);		    			
		    		}*/
		    		
		    		
		    		FOData foind = new FOData();
		    		foind.set_string(
		    				((Indices)dat).getCode(),
		    				dateFormat.format(new Date()),
		    				((Indices)dat).getTranstime());
		    		
					foind.set_double(new Double(((Indices)dat).getIndex()) );
					
					Vector vind = accesData
							.getQueryDataFeed()
							.set_insert(SQUERY.indicesintraday.val, foind);
					
					
					/*if (insertStatement(C_INDICESINTRADAY_INSERT, new Object[]{	
							   ((Indices)dat).getCode(), dateFormat.format(new Date()), ((Indices)dat).getTranstime(), 
							   new Double(((Indices)dat).getIndex())}, connIndicesIntraday)==-1){
								updateStatement(C_INDICESINTRADAY_UPDATE, new Object[]{	
										new Double(((Indices)dat).getIndex()),
										((Indices)dat).getCode(), 
										dateFormat.format(new Date()),
										((Indices)dat).getTranstime()
								}, connIndicesIntraday);
					}*/
		    		
		    	} else if (data[1].equals("9")){
		    		dat = new News();
		    		dat.setContent(data);
		    		if (dat.getSeqno()>=msg.getSeqno()){
		    		}
		    	} else if (data[1].equals("3")){
		    		dat = new Stock();
		    		dat.setContent(data);
		    		FOData fo = new FOData();
		    		
		    		fo.set_string(((Stock)dat).getCode(),
		    					((Stock)dat).getName(),
		    					((Stock)dat).getSector(),
		    					dateFormat.format(new Date()));
//		    		System.out.println("get msg 3"+((Stock)dat).getCode());
//		    		if (dat.getSeqno()>=msg.getSeqno()){
//		    			Vector v = accesData.getQueryDataFeed().set_insert(SQUERY.stock.val, fo);
//		    			System.out.println("input database 3"+((Stock)dat).getCode());
//		    		}
		    	} else if (data[1].equals("5")){
		    		dat = new StockSummary();
		    		dat.setContent(data);
		    		
		    		FOData fo = new FOData();
					fo.set_string(dateFormat.format(new Date()),
							((StockSummary)dat).getStock(), 
							((StockSummary)dat).getBoard());
					fo.set_double(((StockSummary) dat).getOpen() == 0 ? new Double(
							((StockSummary) dat).getClose())
							: new Double(
									((StockSummary) dat)
											.getOpen()),
							new Double(((StockSummary) dat).getHigh()),
							new Double(((StockSummary)dat).getLow()), 
							new Double(((StockSummary)dat).getClose()), 
							new Double(((StockSummary)dat).getPrev()),
							new Double(((StockSummary)dat).getTradevol()),
							new Double(((StockSummary)dat).getTradefreq()) );
							
		    		if (dat.getSeqno()>=msg.getSeqno() && ((StockSummary)dat).getTradefreq()>0){
		    			
							Vector v = accesData.getQueryDataFeed().set_insert(SQUERY.stocksummary.val, fo);
							Object[] param = new Object[]{	
										new Double(((StockSummary)dat).getHigh()),
										new Double(((StockSummary)dat).getLow()), 
										new Double(((StockSummary)dat).getClose()), 
										new Double(((StockSummary)dat).getPrev()),
										new Double(((StockSummary)dat).getTradevol()),
										new Double(((StockSummary)dat).getTradefreq()),								
										dateFormat.format(new Date()),
										((StockSummary)dat).getStock(), 
										((StockSummary)dat).getBoard()
								};
								if (updateStatement(C_STOCKSUMM_UPDATE, param, connStockSumm)==0){
									insertStatement(C_STOCKSUMM_INSERT, 		    					
											new Object[]{	
											dateFormat.format(new Date()),
											((StockSummary)dat).getStock(), 
											((StockSummary)dat).getBoard(),
											((StockSummary)dat).getOpen() == 0 ? new Double(((StockSummary)dat).getClose()) :  new Double(((StockSummary)dat).getOpen()), 
											new Double(((StockSummary)dat).getHigh()),
											new Double(((StockSummary)dat).getLow()), 
											new Double(((StockSummary)dat).getClose()), 
											new Double(((StockSummary)dat).getPrev()),
											new Double(((StockSummary)dat).getTradevol()),
											new Double(((StockSummary)dat).getTradefreq())								
											}, connStockSumm
									);		    			
								}
		    		}
		    		if (dat.getSeqno()>=msg.getSeqno() && ((StockSummary)dat).getTradefreq()>0){
		    			
		    			FOData foinv = new FOData();
		    			foinv.set_string(dateFormat.format(new Date()),
		    					((StockSummary)dat).getStock(),
								((StockSummary)dat).getBoard(),
								((StockSummary)dat).getRemark());
		    			foinv.set_double(new Double(((StockSummary)dat).getPrev()),
		    					new Double(((StockSummary)dat).getHigh()),
		    					new Double(((StockSummary)dat).getLow()),
		    					new Double(((StockSummary)dat).getClose()),
		    					new Double(((StockSummary)dat).getChange()),
		    					new Double(((StockSummary)dat).getTradevol()),
		    					new Double(((StockSummary)dat).getTradeval()),
		    					new Double(((StockSummary)dat).getTradefreq()),
		    					new Double(((StockSummary)dat).getIndex()),
		    					new Double(((StockSummary)dat).getForeign()),
		    					new Double(((StockSummary)dat).getOpen()),
		    					new Double(((StockSummary)dat).getBestbid()),
		    					new Double(((StockSummary)dat).getBestbidvol()),
		    					new Double(((StockSummary)dat).getBestoffer()),
		    					new Double(((StockSummary)dat).getBestoffervol()));
		    			
		    			Vector vinv = accesData.getQueryDataFeed().set_insert(SQUERY.stocksummaryinv.val, foinv);
		    			/*Object [] param2 = new Object []{
		    				
		    					((StockSummary)dat).getRemark(),
		    					new Double(((StockSummary)dat).getPrev()),
		    					new Double(((StockSummary)dat).getHigh()),
		    					new Double(((StockSummary)dat).getLow()),
		    					new Double(((StockSummary)dat).getClose()),
		    					new Double(((StockSummary)dat).getChange()),
		    					new Double(((StockSummary)dat).getTradevol()),
		    					new Double(((StockSummary)dat).getTradeval()),
		    					new Double(((StockSummary)dat).getTradefreq()),
		    					new Double(((StockSummary)dat).getIndex()),
		    					new Double(((StockSummary)dat).getForeign()),
		    					new Double(((StockSummary)dat).getOpen()),
		    					new Double(((StockSummary)dat).getBestbid()),
		    					new Double(((StockSummary)dat).getBestbidvol()),
		    					new Double(((StockSummary)dat).getBestoffer()),
		    					new Double(((StockSummary)dat).getBestoffervol()),
		    					((StockSummary)dat).getStock(),
		    					dateFormat.format(new Date()),
		    					((StockSummary)dat).getBoard()
		    					//data lot;
		    					
		    			};
		    		if(updateStatement(FEED_STOCKSUMMARY_INV_UPDATE , param2, connStockSumm)== 0){
						insertStatement(FEED_STOCKSUMMARY_INV_INSERT,
								new Object [] {
								//STOCK,BOARD,T_REMARK,PREV,HIGH,LOW,T_CLOSE,T_CHANGE,TRADEVOL,TRADEVAL
								//TRADEFREQ,T_INDEX,T_FOREIGN
								//T_OPEN,BESTBID,BESTBIDVOL,BESTOFFER,BESTOFFERVOL,TRANSDATE
								((StockSummary)dat).getStock(),
								((StockSummary)dat).getBoard(),
								((StockSummary)dat).getRemark(),
								new Double(((StockSummary)dat).getPrev()),
								new Double(((StockSummary)dat).getHigh()),
								new Double(((StockSummary)dat).getLow()),
								new Double(((StockSummary)dat).getClose()),
								new Double(((StockSummary)dat).getChange()),
								new Double(((StockSummary)dat).getTradevol()),
								new Double(((StockSummary)dat).getTradeval()),
								new Double(((StockSummary)dat).getTradefreq()),
								new Double(((StockSummary)dat).getIndex()),
								new Double(((StockSummary)dat).getForeign()),
								new Double(((StockSummary)dat).getOpen()),
								new Double(((StockSummary)dat).getBestbid()),
								new Double(((StockSummary)dat).getBestbidvol()),
								new Double(((StockSummary)dat).getBestoffer()),
								new Double(((StockSummary)dat).getBestoffervol()),
								dateFormat.format(new Date())
								
						}
								, connStockSumm); // sampai di sini
		    			}*/
					}
		    	} else if (data[1].equals("TP")){
		    		dat =new TradePrice();
		    		dat.setContent(data);
		    		if (dat.getSeqno()>=msg.getSeqno()){
		    		}
		    	}else if(data[1].equals("TSSI"))
		    	{
		    		dat = new TradeSummary();
		    		dat.setContent(data);
		    		FOData fo = new FOData();
		    		String[] fost = new String[]{
		    				dateFormat.format(new Date()),
							((TradeSummary)dat).getStock(),
							((TradeSummary)dat).getBoard(),
							((TradeSummary)dat).getBroker(),
							((TradeSummary)dat).getInvestor()
		    		};
		    		fo.set_string(fost);
		    		Double[] fodl = new Double[]{
							new Double(((TradeSummary)dat).getBuyavg()),
							new Double(((TradeSummary)dat).getSellavg()),
							new Double(((TradeSummary)dat).getBuyvol()),
							new Double(((TradeSummary)dat).getBuyval()),
							new Double(((TradeSummary)dat).getBuyfreq()),
							new Double(((TradeSummary)dat).getSellvol()),
							new Double(((TradeSummary)dat).getSellval()),
							new Double(((TradeSummary)dat).getSellfreq()),
		    		};
		    		fo.set_double(fodl);
		    		Vector v = accesData.getQueryDataFeed().set_insert(SQUERY.histradesummary.val,fo);
		    		
		    		/*
		    			Object[] param = new Object[]{

								new Double(((TradeSummary)dat).getBuyavg()),
								new Double(((TradeSummary)dat).getSellavg()),
								new Double(((TradeSummary)dat).getBuyvol()),
								new Double(((TradeSummary)dat).getBuyval()),
								new Double(((TradeSummary)dat).getBuyfreq()),
								new Double(((TradeSummary)dat).getSellvol()),
								new Double(((TradeSummary)dat).getSellval()),
								new Double(((TradeSummary)dat).getSellfreq()),
								dateFormat.format(new Date()),
								((TradeSummary)dat).getStock(),
								((TradeSummary)dat).getBoard(),
								((TradeSummary)dat).getBroker(),
								((TradeSummary)dat).getInvestor()
						};
		    			if (updateStatement(C_TRADESUMM_UPDATE_HIS, param, connTradeSumm)==0){
							
							insertStatement(C_TRADESUMM_INSERT_HIS, 		    					
									new Object[]{	
									
									dateFormat.format(new Date()),
									((TradeSummary)dat).getStock(),
									((TradeSummary)dat).getBoard(),
									((TradeSummary)dat).getBroker(),
									((TradeSummary)dat).getInvestor(),
									new Double(((TradeSummary)dat).getBuyavg()),
									new Double (((TradeSummary)dat).getSellavg()),
									new Double(((TradeSummary)dat).getBuyvol()),
									new Double(((TradeSummary)dat).getBuyval()),
									new Double(((TradeSummary)dat).getBuyfreq()),
									new Double(((TradeSummary)dat).getSellvol()),
									new Double(((TradeSummary)dat).getSellval()),
									new Double(((TradeSummary)dat).getSellfreq())
									
									}, connTradeSumm
							);		    			
						}
		    		*/
		    				    		
		    	} else if (data[1].equals("TSBB") || data[1].equals("TSBS") || data[1].equals("TSSB") || data[1].equals("TSI") || data[1].equals("TSSI")){
		    		dat = new TradeSummary();
		    		dat.setContent(data);
		    	} else if (data[1].equals("TS")){
		    		dat = new TradeSummary();
		    		dat.setContent(data);
		    		
		    		
		    		if (dat.getSeqno()>=msg.getSeqno()){
		    			FOData fo = new FOData();
		    			
							String[] str = new String[]{
			    				dateFormat.format(new Date()),
								((TradeSummary)dat).getStock(),
								((TradeSummary)dat).getBoard(),
								((TradeSummary)dat).getBroker(),
								((TradeSummary)dat).getInvestor(),
								"H"
			    			};
							Double[] dbl = new Double[]{
									new Double(((TradeSummary)dat).getBuyavg()),
									new Double(((TradeSummary)dat).getSellavg()),
									new Double(((TradeSummary)dat).getBuyvol()),
									new Double(((TradeSummary)dat).getBuyval()),
									new Double(((TradeSummary)dat).getBuyfreq()),
									new Double(((TradeSummary)dat).getSellvol()),
									new Double(((TradeSummary)dat).getSellval()),
									new Double(((TradeSummary)dat).getSellfreq()),
							};
							
						fo.set_string(str);
						fo.set_double(dbl);
						
						Vector v = accesData.getQueryDataFeed().set_insert(SQUERY.tradesummary.val , fo);
						
		    		}/*
			    				Object[] param = new Object[]{
								new Double(((TradeSummary)dat).getBuyavg()),
								new Double(((TradeSummary)dat).getSellavg()),
								new Double(((TradeSummary)dat).getBuyvol()),
								new Double(((TradeSummary)dat).getBuyval()),
								new Double(((TradeSummary)dat).getBuyfreq()),
								new Double(((TradeSummary)dat).getSellvol()),
								new Double(((TradeSummary)dat).getSellval()),
								new Double(((TradeSummary)dat).getSellfreq()),
								dateFormat.format(new Date()),
								((TradeSummary)dat).getStock(),
								((TradeSummary)dat).getBoard(),
								((TradeSummary)dat).getBroker(),
								((TradeSummary)dat).getInvestor()
						};

						if (updateStatement(C_TRADESUMM_UPDATE, param, connTradeSumm)==0){
							insertStatement(C_TRADESUMM_INSERT, 		    					
									new Object[]{	
									dateFormat.format(new Date()),
									((TradeSummary)dat).getStock(),
									((TradeSummary)dat).getBoard(),
									((TradeSummary)dat).getBroker(),
									((TradeSummary)dat).getInvestor(),
									new Double(((TradeSummary)dat).getBuyavg()),
									new Double(((TradeSummary)dat).getSellavg()),
									new Double(((TradeSummary)dat).getBuyvol()),
									new Double(((TradeSummary)dat).getBuyval()),
									new Double(((TradeSummary)dat).getBuyfreq()),
									new Double(((TradeSummary)dat).getSellvol()),
									new Double(((TradeSummary)dat).getSellval()),
									new Double(((TradeSummary)dat).getSellfreq()),
									"H"						
									}, connTradeSumm
							);		    			
						}

		    		} */
		    	} else if (data[1].equals("TH")){
	    			dat = new Trade();		    		
	    			dat.setContent(data);
	    			if (dat.getSeqno()>=msg.getSeqno() && ((Trade)dat).getBoard().equals("RG")){
	    				
	    				FOData fo = new FOData();
	    				String[] str = new String[]{((Trade)dat).getStock() ,
	    						dateFormat.format(new Date()),
	    						((Trade)dat).getTradetime()
	    						};
	    				Double[] dbl = new Double[]{new Double(((Trade)dat).getPrice())};
	    				
	    				fo.set_double(dbl);
	    				fo.set_string(((Trade)dat).getStock() ,
	    						dateFormat.format(new Date()),
	    						((Trade)dat).getTradetime()); 
	    				
	    				Vector v = accesData.getQueryDataFeed().set_insert(SQUERY.stockintraday.val, fo);
	    				
						/*if (insertStatement(C_STOCKINTRADAY_INSERT, new Object[]{	
						   ((Trade)dat).getStock(), dateFormat.format(new Date()), ((Trade)dat).getTradetime(), 
						   new Double(((Trade)dat).getPrice())}, connStockIntraday)==-1){
							updateStatement(C_STOCKINTRADAY_UPDATE, new Object[]{	
									new Double(((Trade)dat).getPrice()),
									((Trade)dat).getStock(), 
									dateFormat.format(new Date()),
									((Trade)dat).getTradetime()
							}, connStockIntraday);
						}*/
	    			}
	    		}
		    	if (dat.getSeqno() >= msg.getSeqno()){
			    		msg.setSeqno((long)dat.getSeqno());
			    		db.putMsg(msg);
		    	}
		} catch (Exception ex){
			//System.out.println("error while processing: "+message);
			log.error("error while processing: "+message+" "+ex.getMessage());
			//ex.printStackTrace();
		}
	}
		
	//--------------------------------------------------------------------------------------------------------------------------database function goes here
	private final static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private final static SimpleDateFormat timeFormat = new SimpleDateFormat("HHmmss");

	private int  insertStatement(String query, Object[] data, Connection conn){
		Statement st = null;
		int result = 0;
		try {
			if (conn == null) conn = database.getConnection();
			st = conn.createStatement();
			result = st.executeUpdate(String.format(query, data)); 
		} catch (Exception sqle){
			//log.info("insert database error "+sqle.getMessage());
			//sqle.printStackTrace();
			//try { conn.close();} catch (Exception ex){}			
			if(database == null) { database = new Database(); 
			try {
				database.start(config.getProperty("database"));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
			result = -1;
		} finally {
			try { st.close();} catch (Exception ex){}
		}
		return result;
	}
	
	private int  updateStatement(String query, Object[] data, Connection conn){
		Statement st = null;
		int result=0;
		try {
			if (conn == null) conn = database.getConnection();
			st = conn.createStatement();
			result = st.executeUpdate(String.format(query, data)); 
		} catch (Exception sqle){
			log.info("update database error "+sqle.getMessage());
			sqle.printStackTrace();
			//try { conn.close();} catch (Exception ex){}			
			result = -1;
			database.restart();
			if(database == null) { database = new Database(); 
				try {
					database.start(config.getProperty("database"));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			}
			initConnection();
		} finally {
			try { st.close();} catch (Exception ex){}
		}
		return result;
	}
	
	
}
