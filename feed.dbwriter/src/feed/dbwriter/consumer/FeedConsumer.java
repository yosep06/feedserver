package feed.dbwriter.consumer;

import org.jfree.util.Log;

import feed.dbwriter.core.FileConfig;

public class FeedConsumer {
	private TradeConsumer tradeConsumer;
	private QuoteConsumer quoteConsumer;
	private OtherConsumer otherConsumer;
	protected FileConfig config;
	private SummaryConsumer summConsumer;
	
	

	public FeedConsumer() throws Exception {
		config = new FileConfig("feed.dbwriter.config");
	}

	public FeedConsumer(boolean rmi) throws Exception {
		config = new FileConfig("feed.dbwriter.config");
		tradeConsumer = new TradeConsumer(config.getProperty("buildertrade"));
		quoteConsumer = new QuoteConsumer(config.getProperty("builderquote"));
		otherConsumer = new OtherConsumer(config.getProperty("builderother"));
		summConsumer = new SummaryConsumer(config.getProperty("buildersumm"));
	}

	public TradeConsumer getTradeConsumer() {
		return tradeConsumer;
	}

	public QuoteConsumer getQuoteConsumer() {
		return quoteConsumer;
	}
	
	public SummaryConsumer getSummaryConsumer(){
		return summConsumer;
	}
	
	public OtherConsumer getOtherConsumer() {
		return otherConsumer;
	}

	public void start() throws Exception {
		// tradeConsumer.connect();
		// quoteConsumer.connect();
		try {
			otherConsumer.connect();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void stop() {
		try {
			// tradeConsumer.exit();
			// quoteConsumer.exit();
			otherConsumer.exit();
		} catch (Exception ex) {
		}
	}
}
