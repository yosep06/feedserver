package feed.dbwriter.consumer;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eqtrade.database.AccessQueryLocator;
import com.eqtrade.database.FOData;

import feed.builder.msg.Message;
import feed.builder.msg.StockSummary;
import feed.dbwriter.core.Database;
import feed.dbwriter.core.FileConfig;
import feed.provider.data.FeedMsg;

public class StockSummaryConsumer extends Thread {

	private Vector<String> vmsgTemp = new Vector<String>();
	private Database database;
	private Connection connStockSumm = null;
	private boolean terminated = false;
	private Logger log = LoggerFactory.getLogger(getClass());
	protected FeedMsg msg;
	protected final static SimpleDateFormat formatDate = new SimpleDateFormat("yyyyMMdd");
	protected final static NumberFormat format = new DecimalFormat("###0");
	private final static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private final static SimpleDateFormat timeFormat = new SimpleDateFormat("HHmmss");
	private final String C_STOCKSUMM_INSERT = "insert into feed_stocksummary values ('%s', '%s', '%s', %f, %f, %f, %f, %f, %f, %f)";
	private final String C_STOCKSUMM_UPDATE = "update feed_stocksummary set high=%f, low=%f, last=%f, prev=%f, vol=%f, freq=%f where transdate='%s' and stock='%s' and board='%s'";
	private final String FEED_STOCKSUMMARY_INV_INSERT = "insert into FEED_STOCKSUMMARY_INV values ('%s', '%s','%s',%f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, '%s')";
	private final String FEED_STOCKSUMMARY_INV_UPDATE = "update FEED_STOCKSUMMARY_INV set  t_remark='%s', prev=%f, high=%f, low=%f, t_close=%f, t_change=%f, tradevol=%f, tradeval=%f, tradefreq=%f, t_index=%f, t_foreign=%f, t_open=%f, bestbid=%f, bestbidvol=%f, bestoffer=%f, bestoffervol=%f where stock='%s' and transdate='%s' and board='%s' ";
	AccessQueryLocator accesData;
	protected FileConfig config;
	private ThreadPoolExecutor poolThread;
	private Logger logfile = LoggerFactory.getLogger("feedfile.feed.dbwriter.consumer");

	public void setAccess(AccessQueryLocator accesData) {
		this.accesData = accesData;
	}

	public StockSummaryConsumer() {
		poolThread = new ThreadPoolExecutor(10, 20, 10, TimeUnit.SECONDS,
				(BlockingQueue<Runnable>) new java.util.concurrent.LinkedBlockingQueue<Runnable>());
	}

	protected void loadConfig() throws Exception {
		msg = new FeedMsg();
		msg.setType("SETTING");
		config = new FileConfig("feed.dbwriter.config");
		msg.setMsg(formatDate.format(new Date()));
		msg.setSeqno(0);
		log.info("seqno for type Stock Summary " + msg.getSeqno());
	}

	public void setDb(Database database) {
		this.database = database;
		log.info(this.database.toString());
		initConnection();
	}

	private void initConnection() {
		try {
			connStockSumm = database.getConnection();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void newMessage(final String smessage) {

		synchronized (vmsgTemp) {
			vmsgTemp.add(smessage);
			vmsgTemp.notifyAll();
		}

		
	}

	@Override
	public void run() {
		while (!terminated) {
			if (vmsgTemp.isEmpty()) {
				synchronized (vmsgTemp) {
					try {
						vmsgTemp.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else
				try {
					processThread(vmsgTemp.remove(0));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}

		while (!vmsgTemp.isEmpty()) {
			try {
				processSS(vmsgTemp.remove(0));

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void processThread(final String smessage) {
		log.info("stocksumm " + smessage);
		poolThread.execute(new Runnable() {

			@Override
			public void run() {
				try {
					logfile.info("process " + smessage);
					processSS(smessage);
					logfile.info("process done " + smessage);
				} catch (Exception e) {
					e.printStackTrace();
					logfile.error(logException(e));

				}

			}
		});
	}

	private void processSS(String message) throws Exception {
		try {
			String[] data = message.split("\\|");
			Message dat = null;

			if (data[1].equals("5")) {
				dat = new StockSummary();
				dat.setContent(data);
				if (dat.getSeqno() >= msg.getSeqno() && ((StockSummary) dat).getTradefreq() > 0) {
					logfile.info("update feed_stocksummary " + message);

					Object[] param = new Object[] { new Double(((StockSummary) dat).getHigh()),
							new Double(((StockSummary) dat).getLow()), new Double(((StockSummary) dat).getClose()),
							new Double(((StockSummary) dat).getPrev()), new Double(((StockSummary) dat).getTradevol()),
							new Double(((StockSummary) dat).getTradefreq()), dateFormat.format(new Date()),
							((StockSummary) dat).getStock(), ((StockSummary) dat).getBoard() };
					if (updateStatement(C_STOCKSUMM_UPDATE, param, connStockSumm) == 0) {
						insertStatement(C_STOCKSUMM_INSERT, new Object[] { dateFormat.format(new Date()),
								((StockSummary) dat).getStock(), ((StockSummary) dat).getBoard(),
								((StockSummary) dat).getOpen() == 0 ? new Double(((StockSummary) dat).getClose())
										: new Double(((StockSummary) dat).getOpen()),
								new Double(((StockSummary) dat).getHigh()), new Double(((StockSummary) dat).getLow()),
								new Double(((StockSummary) dat).getClose()), new Double(((StockSummary) dat).getPrev()),
								new Double(((StockSummary) dat).getTradevol()),
								new Double(((StockSummary) dat).getTradefreq()) }, connStockSumm);
					}
					logfile.info("update feed_stocksummary done " + message);

				}
			}
			if (dat.getSeqno() >= msg.getSeqno() && ((StockSummary) dat).getTradefreq() > 0) {
				logfile.info("update feed_stocksummary_inv " + message);

				Object[] param2 = new Object[] {

						((StockSummary) dat).getRemark(), new Double(((StockSummary) dat).getPrev()),
						new Double(((StockSummary) dat).getHigh()), new Double(((StockSummary) dat).getLow()),
						new Double(((StockSummary) dat).getClose()), new Double(((StockSummary) dat).getChange()),
						new Double(((StockSummary) dat).getTradevol()), new Double(((StockSummary) dat).getTradeval()),
						new Double(((StockSummary) dat).getTradefreq()), new Double(((StockSummary) dat).getIndex()),
						new Double(((StockSummary) dat).getForeign()), new Double(((StockSummary) dat).getOpen()),
						new Double(((StockSummary) dat).getBestbid()), new Double(((StockSummary) dat).getBestbidvol()),
						new Double(((StockSummary) dat).getBestoffer()),
						new Double(((StockSummary) dat).getBestoffervol()), ((StockSummary) dat).getStock(),
						dateFormat.format(new Date()), ((StockSummary) dat).getBoard()
						// data lot;

				};
				if (updateStatement(FEED_STOCKSUMMARY_INV_UPDATE, param2, connStockSumm) == 0) {
					insertStatement(FEED_STOCKSUMMARY_INV_INSERT, new Object[] {
							// STOCK,BOARD,T_REMARK,PREV,HIGH,LOW,T_CLOSE,T_CHANGE,TRADEVOL,TRADEVAL
							// TRADEFREQ,T_INDEX,T_FOREIGN
							// T_OPEN,BESTBID,BESTBIDVOL,BESTOFFER,BESTOFFERVOL,TRANSDATE
							((StockSummary) dat).getStock(), ((StockSummary) dat).getBoard(),
							((StockSummary) dat).getRemark(), new Double(((StockSummary) dat).getPrev()),
							new Double(((StockSummary) dat).getHigh()), new Double(((StockSummary) dat).getLow()),
							new Double(((StockSummary) dat).getClose()), new Double(((StockSummary) dat).getChange()),
							new Double(((StockSummary) dat).getTradevol()),
							new Double(((StockSummary) dat).getTradeval()),
							new Double(((StockSummary) dat).getTradefreq()),
							new Double(((StockSummary) dat).getIndex()), new Double(((StockSummary) dat).getForeign()),
							new Double(((StockSummary) dat).getOpen()), new Double(((StockSummary) dat).getBestbid()),
							new Double(((StockSummary) dat).getBestbidvol()),
							new Double(((StockSummary) dat).getBestoffer()),
							new Double(((StockSummary) dat).getBestoffervol()), dateFormat.format(new Date())

					}, connStockSumm); // sampai di sini
				}
				logfile.info("update feed_stocksummary_inv done " + message);

			}
		} catch (Exception exception) {
			exception.printStackTrace();
			log.error("error while processing: " + message + " " + exception.getMessage());

		}
	}

	private int insertStatement(String query, Object[] data, Connection connp) {
		Statement st = null;
		int result = 0;
		Connection conn = null;
		try {
			// if (conn == null)
			conn = accesData.getQueryDataFeed().getConnection();
			st = conn.createStatement();
			result = st.executeUpdate(String.format(query, data));
		} catch (Exception sqle) {
			// log.info("insert database error "+sqle.getMessage());
			// sqle.printStackTrace();
			// try { conn.close();} catch (Exception ex){}
			sqle.printStackTrace();
			logfile.error(logException(sqle));
			/*if (database == null) {
				database = new Database();
				try {
					database.start(config.getProperty("database"));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}*/

			result = -1;
		} finally {
			try {
				st.close();
				conn.close();
			} catch (Exception ex) {
				ex.printStackTrace();
				logfile.error(logException(ex));
			}
		}
		return result;
	}

	public static String logException(Exception ex) {
		StringBuffer strresult = new StringBuffer("");

		Object[] objarr = ex.getStackTrace();
		strresult.append("[Start:Error : ").append("\n");
		strresult.append(ex.toString()).append("\n");
		for (int i = 0; i < objarr.length; i++) {
			strresult.append("---->").append(objarr[i].toString()).append("\n");
		}
		strresult.append("End:Error]");

		return strresult.toString();
	}

	private int updateStatement(String query, Object[] data, Connection connp) {
		Statement st = null;
		int result = 0;
		Connection conn = null;
		try {
			// if (conn == null)
			conn = accesData.getQueryDataFeed().getConnection();
			st = conn.createStatement();
			result = st.executeUpdate(String.format(query, data));
		} catch (Exception sqle) {
			log.info("update database error " + sqle.getMessage());
			// sqle.printStackTrace();
			logfile.error(logException(sqle));

			// try { conn.close();} catch (Exception ex){}
			sqle.printStackTrace();
			result = -1;
			/*database.restart();
			if (database == null) {
				database = new Database();
				try {
					database.start(config.getProperty("database"));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			initConnection();*/
		} finally {
			try {
				st.close();
				conn.close();
			} catch (Exception ex) {
				ex.printStackTrace();
				logfile.error(logException(ex));
			}
		}
		return result;
	}

}
