package feed.dbwriter.consumer;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.LogFactory;

import com.db4o.query.Query;

import feed.builder.msg.Indices;
import feed.builder.msg.Quote;
import feed.dbwriter.core.Database;
import feed.dbwriter.core.MsgConsumer;

public class QuoteConsumer extends MsgConsumer {

	private Connection connQuote = null;
	private final static SimpleDateFormat dateFormat = new SimpleDateFormat(
			"yyyy-MM-dd");
	private final static SimpleDateFormat timeFormat = new SimpleDateFormat(
			"HHmmss");
	private final String C_QUOTE_INSERT = "insert into feed_quote values ('%s', '%s', '%s', '%s', '%s',%f)";
	private final String C_QUOTE_UPDATE = "update feed_quote set time='%s', data='%s',seqno=%f where stock='%s' and board='%s' and orderdate='%s'";

	public QuoteConsumer(String url) throws Exception {
		super(url, "2");
	}

	public void setDb(Database database) {
		this.database = database;
		initConnection();
	}

	private void initConnection() {
		try {
			connQuote = database.getConnection();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public List getSnapShot(int seqno) {
		Query query = db.getDb().getInstance().query();
		query.constrain(Quote.class);
		query.descend("seqno").orderAscending().constrain(new Long(seqno))
				.greater();
		List o = query.execute();
		return o;
	}

	protected void loadConfig() {
		log = LogFactory.getLog(getClass());
		super.loadConfig();
	}

	public void newMessage(String message) throws RemoteException {
		try {
			Quote quote = new Quote();
			quote.setContent(message);
			// if (quote.getSeqno() > msg.getSeqno()){
			// msg.setSeqno((long)quote.getSeqno());
			// db.putMsg(msg);
			// }
			//log.info("quote receive "+message);
			String stime = quote.getOrdertime().split("#")[0];
			Object[] param = new Object[] { stime, message,
					new Double(quote.getSeqno()), quote.getStock(),
					quote.getBoard(), dateFormat.format(new Date()) };

			if (updateStatement(C_QUOTE_UPDATE, param, connQuote) == 0) {
				insertStatement(
						C_QUOTE_INSERT,
						new Object[] { quote.getStock(), quote.getBoard(),
								dateFormat.format(new Date()),
								stime, message, quote.getSeqno() },
						connQuote);
			}

		} catch (Exception ex) {
			System.out.println("error while processing: " + message);
			ex.printStackTrace();
		}
	}

	private int insertStatement(String query, Object[] data, Connection conn) {
		Statement st = null;
		int result = 0;
		try {
			if (conn == null)
				conn = database.getConnection();
			st = conn.createStatement();
			//log.info(String.format(query, data));
			result = st.executeUpdate(String.format(query, data));
		} catch (Exception sqle) {
			sqle.printStackTrace();
			// try { conn.close();} catch (Exception ex){}
			result = -1;
		} finally {
			try {
				st.close();
			} catch (Exception ex) {
			}
		}
		return result;
	}

	private int updateStatement(String query, Object[] data, Connection conn) {
		Statement st = null;
		int result = 0;
		try {
			if (conn == null)
				conn = database.getConnection();
			st = conn.createStatement();
			//log.info("update "+String.format(query, data));
			result = st.executeUpdate(String.format(query, data));
		} catch (Exception sqle) {
			sqle.printStackTrace();
			// try { conn.close();} catch (Exception ex){}
			result = -1;
			//database.restart();
			//initConnection();
		} finally {
			try {
				st.close();
			} catch (Exception ex) {
			}
		}
		return result;
	}

}
