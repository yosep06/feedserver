package feed.builder.test.queue;

import java.text.SimpleDateFormat;
import java.util.Date;
public class TestMain {
	
	public static void main(String[] args){
		SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
		System.out.println(Integer.parseInt(sdf.format(new Date())));
	}

}
