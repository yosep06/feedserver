package feed.builder.thread;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import feed.provider.core.FileConfig;

public class PoolingExecutor implements ThreadPooling {
	private ThreadPoolExecutor poolworker;

	public PoolingExecutor(FileConfig config) {
		poolworker = new ThreadPoolExecutor(
				Integer.parseInt(config.get("corepoolsize").toString()),
				Integer.parseInt(config.get("maxpoolsize").toString()),
				Integer.parseInt(config.get("keepalivetime").toString()),
				TimeUnit.SECONDS,
				new java.util.concurrent.LinkedBlockingQueue<Runnable>());

		boolean isprerunning = Boolean.parseBoolean((String) config
				.get("isprerunning"));

		if (isprerunning)
			poolworker.prestartAllCoreThreads();

	}

	@Override
	public void execute(Runnable runnable) {
		poolworker.execute(runnable);
	}

}
