package feed.builder.builder;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TimerHold extends Thread {

	public QuoteBuilder quoteBuilder;
	private SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
	private boolean isterminate = false,ishold = false;
	private int timehold = 160000;
	private int timer = 0,counter = 0;
	private Logger log = LoggerFactory.getLogger(getClass());
	
	public TimerHold(QuoteBuilder qq){
		this.quoteBuilder = qq;
	}

	@Override
	public void run() {
		while (!isterminate) {
			synchronized (this) {
				try {
					this.wait(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			int time = Integer.parseInt(sdf.format(new Date()));
			if(time >= timehold && !ishold){
				//quoteBuilder.hold();
				ishold = true;
				log.info("hold time "+time+" "+timehold);
			}else if(ishold){
				if(timer >= 10){
					timer = 0;
					//quoteBuilder.sendTemp();
					counter++;
					log.info("send temp "+timer+" "+counter);
					if(counter == 4){
						//quoteBuilder.unhold();
						isterminate = true;
					}
				}else
					timer++;
			}

		}
		log.info("finished send temp ");
	}

}
