package feed.builder.msg;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class Queue implements Comparable {
	private String id;
	private double price;
	private double lot;
	private double match;
	private String time = "";
	private String type = "";
	private String stock = "";
	private String board = "";

	private static final String delimiter = "|";
	protected final static NumberFormat format = new DecimalFormat("###0");

	// public Queue(String id, double price, double lot,String time, String
	// type, String stock, String board){
	public Queue(String id, double price, double lot, String time) {
		// System.out.println("Queueu price= "+ price);
		this.id = id;
		this.price = price;
		this.lot = lot;
		this.match = 0;
		this.time = time;
		/*
		 * this.type = type; this.stock = stock; this.board = board;
		 */
	}
	
	public Queue(String stock,String board,String type,String id, double price, double lot, String time) {
		// System.out.println("Queueu price= "+ price);
		this.stock = stock;
		this.board = board;
		this.type = type;
		this.id = id;
		this.price = price;
		this.lot = lot;
		this.match = 0;
		this.time = time;
		/*
		 * this.type = type; this.stock = stock; this.board = board;
		 */
	}

	// public Queue(String id, double price, double lot, double match, String
	// time , String type, String stock, String board){
	public Queue(String id, double price, double lot, double match, String time) {
		this.id = id;
		this.price = price;
		this.lot = lot;
		this.match = match;
		this.time = time;
		/*
		 * this.type = type; this.stock = stock; this.board = board;
		 */

	}

	public Queue(String stock, String board, String id, double price,
			double lot, String time) {
		// System.out.println("Queueu price= "+ price);
		this.stock = stock;
		this.board = board;
		this.id = id;
		this.price = price;
		this.lot = lot;
		this.match = 0;
		this.time = time;
		/*
		 * this.type = type; this.stock = stock; this.board = board;
		 */
	}
	
	public Queue(String stock, String board,String type, String id, double price,
			double lot, String time,double match) {
		// System.out.println("Queueu price= "+ price);
		this.stock = stock;
		this.board = board;
		this.type = type;
		this.id = id;
		this.price = price;
		this.lot = lot;
		//this.match = 0;
		this.time = time;
		this.match = match;
		/*
		 * this.type = type; this.stock = stock; this.board = board;
		 */
	}

	@Override
	public String toString() {
		StringBuffer result = new StringBuffer(100);
		result.append(id).append(delimiter);
		result.append(format.format(price)).append(delimiter);
		result.append(format.format(lot)).append(delimiter);
		result.append(time).append(delimiter);
		result.append(format.format(match)).append(">");
		return result.toString();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getLot() {
		return lot;
	}

	public void setLot(double lot) {
		this.lot = lot;
	}

	public double getMatch() {
		return match;
	}

	public void setMatch(double match) {
		this.match = match;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	@Override
	public int compareTo(Object arg0) {
		return (this.id.equals(((Queue) arg0).getId())) ? 0 : -1;
	}

	@Override
	public boolean equals(Object obj) {
		return ((Queue) obj).getId().equals(getId());
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	// stock|board<B[stock|board|type|idx|orderid|price|lot>type|idx|orderid|price|lot>type|idx|orderid|price|lot]O[type|orderid|price|lot>type|orderid|price|lot]
	// type = 0 / 1 (add)
	// type = 2 / 3 delete
	public String newOrder(int idx) {

		StringBuffer result = new StringBuffer(100);
		result.append(stock).append(delimiter);
		result.append(board).append(delimiter);
		result.append(getType()).append(delimiter);
		result.append(idx).append(delimiter);
		result.append(id).append(delimiter);
		result.append(format.format(price)).append(delimiter);
		result.append(format.format(lot)).append(delimiter);
		return result.toString();

		// return getTime()+"|" + idx + "|" + getId() + "|" + getPrice() + "|" +
		// getLot();
	}
	
	public String changeOrder(int idx) {

		StringBuffer result = new StringBuffer(100);
		result.append(stock).append(delimiter);
		result.append(board).append(delimiter);
		result.append(getType().equals("0") ? "4" : "5").append(delimiter);
		result.append(idx).append(delimiter);
		result.append(id).append(delimiter);
		result.append(format.format(price)).append(delimiter);
		result.append(format.format(lot)).append(delimiter);
		return result.toString();

		// return getTime()+"|" + idx + "|" + getId() + "|" + getPrice() + "|" +
		// getLot();
	}

	public String deleteOrder(int idx) {

		StringBuffer result = new StringBuffer(100);
		result.append(stock).append(delimiter);
		result.append(board).append(delimiter);
		result.append(getType().equals("0") ? "2" : "3").append(delimiter);
		result.append(idx).append(delimiter);
		result.append(id).append(delimiter);
		result.append(format.format(price)).append(delimiter);
		result.append(format.format(lot)).append(delimiter);
		return result.toString();

		// return getTime()+"|" + idx + "|" + getId() + "|" + getPrice() + "|" +
		// getLot();
	}

	public String getStock() {
		return stock;
	}

	public String getBoard() {
		return board;
	}
	
	

}
