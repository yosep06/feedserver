package feed.builder.msg;

public class GlobalIndices extends Message{
	private String code;
	private double last;
	private double prev;
	private double change;
	private double percent;
	private double urutan;
	private double high;
	private double low;
	private double open;
	
	public GlobalIndices(){}
	
	@Override
	public String toString(){
		StringBuffer result = new StringBuffer(super.toString());
		result.append(delimiter).append(code);
		result.append(delimiter).append(format3.format(last));
		result.append(delimiter).append(format3.format(prev));
		result.append(delimiter).append(format3.format(high));
		result.append(delimiter).append(format3.format(low));
		result.append(delimiter).append(format3.format(open));
		result.append(delimiter).append(format3.format(change));
		result.append(delimiter).append(format3.format(percent));		
		result.append(delimiter).append(format3.format(urutan));
		return result.toString();
	}
	
	@Override
	public void setContent(String[] data){
		super.setContent(data);
		code = data[3].trim();
		last = Double.parseDouble(data[4]);
		prev = Double.parseDouble(data[5]);
		change = Double.parseDouble(data[6]);
		percent = Double.parseDouble(data[7]);		
		urutan = Double.parseDouble(data[8]);
		high = Double.parseDouble(data[9]);
		low = Double.parseDouble(data[10]);		
		open = Double.parseDouble(data[11]);
	}
	
	@Override
	public void fromProtocol(String[] data){
		super.fromProtocol(data);
		code = data[5].trim();
		last = Double.parseDouble(data[6]);
		prev = Double.parseDouble(data[7]);
		change = Double.parseDouble(data[8]);
		percent = Double.parseDouble(data[9]);		
		urutan = Double.parseDouble(data[10]);
		high = Double.parseDouble(data[11]);
		low = Double.parseDouble(data[12]);		
		open = Double.parseDouble(data[13]);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public double getLast() {
		return last;
	}

	public void setLast(double last) {
		this.last = last;
	}

	public double getPrev() {
		return prev;
	}

	public void setPrev(double prev) {
		this.prev = prev;
	}

	public double getChange() {
		return change;
	}

	public void setChange(double change) {
		this.change = change;
	}

	public double getPercent() {
		return percent;
	}

	public void setPercent(double percent) {
		this.percent = percent;
	}
	
	public double getUrutan() {
		return urutan;
	}

	public void setUrutan(double urutan) {
		this.urutan = urutan;
	}
	public double getHigh() {
		return high;
	}

	public void setHigh(double high) {
		this.high = high;
	}
	public double getLow() {
		return low;
	}

	public void setLow(double low) {
		this.low = low;
	}
	public double getOpen() {
		return open;
	}

	public void setOpen(double open) {
		this.open = open;
	}
}
