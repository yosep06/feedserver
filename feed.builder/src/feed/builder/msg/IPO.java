package feed.builder.msg;

public class IPO extends Message{
	private String name;
	private String stock;
	private double nominal;
	private double totalshare;
	private double percentshare;
	private String effdate;
	private String offerstart;
	private String offerend;
	private String allotment;
	private String refunddate;
	private String listingdate;
	
	public IPO(){}
	
	@Override
	public void setContent(String[] data){
		super.setContent(data);
		name = data[3].trim();
		stock = data[4].trim();
		nominal = Double.parseDouble(data[5]);
		totalshare = Double.parseDouble(data[6]);
		percentshare = Double.parseDouble(data[7]);
		effdate = data[8].trim();
		offerstart = data[9].trim();
		offerend = data[10].trim();
		allotment = data[11].trim();
		refunddate = data[12].trim();
		listingdate = data[13].trim();
	}
	
@Override
public void fromProtocol(String[] data){
	super.fromProtocol(data);
	name = data[5].trim();
	stock = data[6].trim();
	nominal = Double.parseDouble(data[7]);
	totalshare = Double.parseDouble(data[8]);
	percentshare = Double.parseDouble(data[9]);
	effdate = data[10].trim();
	offerstart = data[11].trim();
	offerend = data[12].trim();
	allotment = data[13].trim();
	refunddate = data[14].trim();
	listingdate = data[15].trim();	
}
	
	@Override
	public String toString(){
		StringBuffer result = new StringBuffer(super.toString()).append(delimiter).append(name).append(delimiter).append(stock);
		result.append(delimiter).append(format.format(nominal));
		result.append(delimiter).append(format.format(totalshare));
		result.append(delimiter).append(format2.format(percentshare));
		result.append(delimiter).append(effdate);
		result.append(delimiter).append(offerstart);
		result.append(delimiter).append(offerend);
		result.append(delimiter).append(allotment);
		result.append(delimiter).append(refunddate);
		result.append(delimiter).append(listingdate);
		return result.toString();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStock() {
		return stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}

	public double getNominal() {
		return nominal;
	}

	public void setNominal(double nominal) {
		this.nominal = nominal;
	}
	
	public double getTotalshare(){
		return totalshare;
	}
	
	public void setTotalshare(double totalshare){
		this.totalshare = totalshare;
	}

	public double getPercentshare(){
		return percentshare;
	}
	
	public void setPercentshare(double percentshare){
		this.percentshare = percentshare;
	}
	
	public String getEffdate() {
		return effdate;
	}

	public void setEffdate(String effdate) {
		this.effdate = effdate;
	}

	public String getOfferstart() {
		return offerstart;
	}

	public void setOfferstart(String offerstart) {
		this.offerstart = offerstart;
	}

	public String getOfferend() {
		return offerend;
	}

	public void setOfferend(String offerend) {
		this.offerend = offerend;
	}

	public String getAllotment() {
		return allotment;
	}

	public void setAllotment(String allotment) {
		this.allotment = allotment;
	}

	public String getRefunddate() {
		return refunddate;
	}

	public void setRefunddate(String refunddate) {
		this.refunddate = refunddate;
	}

	public String getListingdate() {
		return listingdate;
	}

	public void setListingdate(String listingdate) {
		this.listingdate = listingdate;
	}
}
