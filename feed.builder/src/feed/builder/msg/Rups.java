package feed.builder.msg;

public class Rups extends Message{
	private String stock;
	private String transdate;
	private String transtime;
	private String description;
	
	public Rups(){}
	
	@Override
	public String toString(){
		StringBuffer result = new StringBuffer(super.toString()).append(delimiter).append(stock).append(delimiter).append(transdate);
		result.append(delimiter).append(transtime);
		result.append(delimiter).append(description);
		return result.toString();
	}
	
	@Override
	public void setContent(String[] data){
		super.setContent(data);
		stock = data[3].trim();
		transdate = data[4].trim();
		transtime = data[5].trim();
		description = data[6].trim();
	}
	
	@Override
	public void fromProtocol(String[] data){
		super.fromProtocol(data);
		stock = data[5].trim();
		transdate = data[6].trim();
		transtime = data[7].trim();
		description = data[8].trim();		
	}

	public String getStock() {
		return stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}

	public String getTransdate() {
		return transdate;
	}

	public void setTransdate(String transdate) {
		this.transdate = transdate;
	}

	public String getTranstime() {
		return transtime;
	}

	public void setTranstime(String transtime) {
		this.transtime = transtime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
