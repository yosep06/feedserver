package feed.builder.msg;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class Message {
	protected String header;
	protected String date;
	protected String time;
	protected double seqno;
	protected String type;
	protected  double price;
	protected final static String delimiter= "|";
	protected final static 	 NumberFormat format= new DecimalFormat("###0");
	protected final static 	 NumberFormat format2= new DecimalFormat("###0.00");
	protected final static 	 NumberFormat format3= new DecimalFormat("###0.000");

	public Message(){		
	}
	
	public Message(String header, String date, String time, int seqno, String type){
		this.header = header;
		this.date = date;
		this.time = time;
		this.seqno = seqno;
		this.type = type;
		//this.price = price;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getSeqno() {
		return seqno;
	}

	public void setSeqno(double seqno) {
		this.seqno = seqno;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}
	
	public void setPriceQueue(double price) {
		this.price = price;
		
	}
	
	@Override
	public String toString(){
		/*return new StringBuffer()
		.append(header).append(delimiter)
		.append(type).append(delimiter)
		.append(format.format(seqno)).toString();	*/	
		return new StringBuffer()
		.append(header).append(delimiter)
		.append(type).append(delimiter)
		.append(format.format(seqno)).toString();
	}
	
	public void fromProtocol(String[] data){
		header = data[0].trim();
		date = data[1].trim();
		time = (data[2]).trim();
		seqno = Double.parseDouble(data[3]);
		//price = Double.parseDouble(data[4]);
		type = data[4].trim();				           		
		//type = data[5].trim();
	}
	
	public void setHeader(String[] data){
		header = data[0].trim();
		type = data[1].trim();
		seqno = Double.parseDouble(data[2]);
		//price = Double.parseDouble(data[3]);
		date = "";
		time = "";		
	}
	
	public void setContent(String[] data){
		setHeader(data);
	}
}
