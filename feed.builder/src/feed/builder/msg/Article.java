package feed.builder.msg;

public class Article extends Message {
	private String id;
	//private String source;
	private String date;
	private String subject;
//	private String category;
	private String title;
	private String content;
	//private String link; 

	public Article(){
	}
	
	@Override
	public String toString(){
		StringBuffer result = new StringBuffer(super.toString());
		result.append(delimiter).append(id);
	//	result.append(delimiter).append(source);
		result.append(delimiter).append(date);
		result.append(delimiter).append(title);
		result.append(delimiter).append(subject);
	//	result.append(delimiter).append(category);
		result.append(delimiter).append(content);
		//result.append(delimiter).append(link);
		return result.toString();
	}
	
	@Override
	public void setContent(String[] data){
		super.setContent(data);
		id = data[3].trim();
		//source = data[3].trim();
		date = data[4].trim();
		subject = data[5].trim();
	//	category = data[5].trim();
		title = data[6].trim();
		content = data[7].trim();
		//link = data[8].trim();
	}
	
	@Override
	public void fromProtocol(String[] data){
		super.fromProtocol(data);
		id = data[5].trim();
		//source=data[5].trim();
		date = data[6].trim();
		subject = data[7].trim();
	//	category = data[7].trim();
		title = data[8].trim();
		content = data[9].trim();
		//link = data[10].trim();
	}

	public String getID() {
		return id;
	}

	public void setID(String id) {
		this.id = id;
	}

	@Override
	public String getDate() {
		return date;
	}

	@Override
	public void setDate(String date) {
		this.date = date;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	}
