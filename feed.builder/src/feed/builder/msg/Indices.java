package feed.builder.msg;

public class Indices extends Message{
	private String code;
	private double basevalue;
	private double marketvalue;
	private double index;
	private double open;
	private double high;
	private double low;
	private double previous = 0;
	private String transtime;
	
	public Indices(){}
	
	@Override
	public String toString(){
		StringBuffer result = new StringBuffer(super.toString());
		result.append(delimiter).append(code);
		result.append(delimiter).append(format.format(basevalue));
		result.append(delimiter).append(format.format(marketvalue));
		result.append(delimiter).append(format3.format(index));
		result.append(delimiter).append(format3.format(open));
		result.append(delimiter).append(format3.format(high));
		result.append(delimiter).append(format3.format(low));
		result.append(delimiter).append(format3.format(previous));
		result.append(delimiter).append(transtime);
		return result.toString();
	}
	
	@Override
	public void setContent(String[] data){
		super.setContent(data);
		code = data[3].trim();
		basevalue = Double.parseDouble(data[4]);
		marketvalue = Double.parseDouble(data[5]);
		index = Double.parseDouble(data[6]);
		open = Double.parseDouble(data[7]);
		high = Double.parseDouble(data[8]);
		low = Double.parseDouble(data[9]);		
		previous = Double.parseDouble(data[10]);
		transtime = data[11].trim();
	}
	
	@Override
	public void fromProtocol(String[] data){
		super.fromProtocol(data);
		code = data[5].trim();
		basevalue = Double.parseDouble(data[6]);
		marketvalue = Double.parseDouble(data[7]);
		index = Double.parseDouble(data[8]);
		open = Double.parseDouble(data[9]);
		high = Double.parseDouble(data[10]);
		low = Double.parseDouble(data[11]);		
		if (previous == 0) previous = index;
		transtime = (data[2]).trim();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public String getTranstime() {
		return transtime;
	}

	public void setTranstime(String time) {
		this.transtime = time;
	}

	public double getBasevalue() {
		return basevalue;
	}

	public void setBasevalue(double basevalue) {
		this.basevalue = basevalue;
	}

	public double getMarketvalue() {
		return marketvalue;
	}

	public void setMarketvalue(double marketvalue) {
		this.marketvalue = marketvalue;
	}

	public double getIndex() {
		return index;
	}

	public void setIndex(double index) {
		this.index = index;
	}

	public double getOpen() {
		return open;
	}

	public void setOpen(double open) {
		this.open = open;
	}

	public double getHigh() {
		return high;
	}

	public void setHigh(double high) {
		this.high = high;
	}

	public double getLow() {
		return low;
	}

	public void setLow(double low) {
		this.low = low;
	}

	public double getPrevious() {
		return previous;
	}

	public void setPrevious(double previous) {
		this.previous = previous;
	}
	

}
