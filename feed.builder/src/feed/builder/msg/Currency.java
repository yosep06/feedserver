package feed.builder.msg;

import java.text.Format;
import java.text.SimpleDateFormat;

public class Currency extends Message {
	private String code;
	private double value;
	private double change;
	private double high;
	private double low;
	private String time;
	private String tipe;
	   
	 final Format formatter = new SimpleDateFormat("hh:mm:ss"); 
	public Currency(){
	}
	
	@Override
	public String toString(){
		StringBuffer result = new StringBuffer(super.toString());
		result.append(delimiter).append(code);
		result.append(delimiter).append(time);
		result.append(delimiter).append(format2.format(value));
		result.append(delimiter).append(format2.format(change));
		result.append(delimiter).append(format2.format(high));
		result.append(delimiter).append(format2.format(low));
		result.append(delimiter).append(tipe);
		return result.toString();
	}
	
	@Override
	public void setContent(String[] data){
		super.setContent(data);
		code = data[3].trim();
		value = Double.parseDouble(data[4]);
		change = Double.parseDouble(data[5]);
		high = Double.parseDouble(data[6]);
		low = Double.parseDouble(data[7]);
		time = data[8].trim();
		tipe = data[9].trim();
	}
	
	@Override
	public void fromProtocol(String[] data){
		super.fromProtocol(data);
		code = data[5].trim();
		value = Double.parseDouble(data[6]);
		change = Double.parseDouble(data[7]);
		high = Double.parseDouble(data[8]);
		low = Double.parseDouble(data[9]);
		time = data[10].trim();
		tipe = data[11].trim();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double val) {
		this.value = val;
	}

	public double getChange() {
		return change;
	}

	public void setChange(double change) {
		this.change = change;
	}
	public double getHigh() {
		return high;
	}

	public void setHigh(double high) {
		this.high = high;
	}
	public double getLow() {
		return low;
	}

	public void setLow(double low) {
		this.low = low;
	}

	
	public String getTime1() {
		return time;
	}
	
	public void setTime1(String time) {
		this.time = time;
	}
	public String getTipe() {
		return tipe;
	}
	
	public void setTipe(String tipe) {
		this.tipe = tipe;
	}

}

