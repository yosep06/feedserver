package feed.builder.msg;


public class TradePrice extends Message{
	private String stock;
	private String board;
	private double price;
	private double lot;
	private double freq;
	
	public TradePrice(){		
	}
	
	@Override
	public String toString(){
		StringBuffer result = new StringBuffer(super.toString()).append(delimiter).append(stock).append(delimiter).append(board).append(delimiter).append(format.format(price));
		result.append(delimiter).append(format.format(lot));
		result.append(delimiter).append(format.format(freq));
		return result.toString();
	}
	
	@Override
	public void setContent(String[] data){
		super.setContent(data);
		stock = data[3].trim();
		board = data[4].trim();
		price = Double.parseDouble(data[5]);
		lot = Double.parseDouble(data[6]);
		freq = Double.parseDouble(data[7]);
	}
	
	public String getStock() {
		return stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}

	public String getBoard() {
		return board;
	}

	public void setBoard(String board) {
		this.board = board;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getLot() {
		return lot;
	}

	public void setLot(double lot) {
		this.lot = lot;
	}
	
	public double getFreq() {
		return freq;
	}

	public void setFreq(double freq) {
		this.freq = freq;
	}
}
