package feed.builder.core;

import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import feed.builder.msg.Queue;
import feed.builder.msg.Quote;
import feed.provider.core.MsgListener;

public class QueueProcessClientHandler implements MsgListener {

	private Hashtable<String, Quote> hQueue = new Hashtable<String, Quote>();

	private Hashtable<String, Vector<SusbcriberQueue>> hClient = new Hashtable<String, Vector<SusbcriberQueue>>();
	private Logger log = LoggerFactory.getLogger(getClass());

	@Override
	public void close() throws RemoteException {
	}

	// stock|board<B[stock|board|type|idx|orderid|price|lot>type|idx|orderid|price|lot>type|idx|orderid|price|lot]O[type|orderid|price|lot>type|orderid|price|lot]
	// type = 0 / 1 (add)
	// type = 2 / 3 delete
	// type = 4 /5 change
	@Override
	public void newMessage(String msg) throws RemoteException {
		try {
			String[] sp = msg.split("<");
			String[] keys = sp[0].split("\\|");
			String key = keys[0] + "#" + keys[1];

			Quote q = hQueue.get(key);

			// log.info("new msg:"+msg);

			if (q == null) {
				q = new Quote();
				q.setStock(keys[0].trim());
				q.setBoard(keys[1].trim());
				hQueue.put(key, q);
			}

			StringBuffer sbf = new StringBuffer();

			String sbid = getString(msg, "B[");
			if (!sbid.isEmpty()) {
				// log.info("sbid "+sbid);
				String[] sdata = sbid.split(">");

				for (int i = 0; i < sdata.length; i++) {
					String[] qs = sdata[i].split("\\|");
					String type = qs[2];
					int idx = Integer.parseInt(qs[3]);
					Queue qd = new Queue(keys[0].trim(), keys[1].trim(),
							qs[4].trim(), Double.parseDouble(qs[5].trim()),
							Double.parseDouble(qs[6].trim()), "");

					// log.info("sp[0]" + sp[0] + ":" + sp[1] + ":" +
					// sdata[i]+":"+qd.newOrder(-1));

					qd.setType("0");

					Vector v = (Vector) q.getBidOrder().get(qd.getPrice());
					if (v == null) {
						v = new Vector();
						q.getBidOrder().put(qd.getPrice(), v);
					}

					if (v != null) {

						synchronized (v) {

							if (type.equals("0")) {
								if (idx >= 0 && idx <= v.size()) {
									v.add(idx, qd);
								} else
									v.addElement(qd);
							} else if (type.equals("2")) {

								v.remove(qd);

							} else if (type.equals("4")) {
								int idx2 = v.indexOf(qd);

								if (idx2 >= 0) {

									Queue qobj = (Queue) v.get(idx2);
									// log.info("before change order " +
									// v.size()
									// + "price " + key + "#" +
									// qd.getPrice()+"|"+qobj.toString()+"#comparator#"+qd.toString());
									// v.addElement(qd);
									v.remove(idx2);
									qobj = qd;

									v.add(idx2, qobj);
								} else
									v.addElement(qd);

								// Queue test =(Queue) v.get(v.indexOf(qobj));

								// log.info("after change order " + v.size()
								// + "price " + key + "#" +
								// qd.getPrice()+"|"+test.toString());

							}
						}
						// sbf.append(
						// type.equals("0") ? qd.newOrder(idx) : qd
						// .deleteOrder(idx)).append(">");

						/*
						 * if ((type.equals("2") || type.equals("4")) &&
						 * qd.getStock().equals("BUMI") && qd.getPrice() == 660)
						 * { log.info("sdata bid append " + sdata[i]); }
						 */
						sbf.append(sdata[i]).append(">");

					}
					// log.info("v size "+v.size());

				}

			}

			String soff = getString(msg, "O[");

			if (!soff.isEmpty()) {
				String[] sdata = soff.split(">");

				for (int i = 0; i < sdata.length; i++) {
					String[] qs = sdata[i].split("\\|");
					String type = qs[2];
					int idx = Integer.parseInt(qs[3]);
					Queue qd = new Queue(keys[0].trim(), keys[1].trim(),
							qs[4].trim(), Double.parseDouble(qs[5].trim()),
							Double.parseDouble(qs[6].trim()), "");

					qd.setType("1");

					Vector v = (Vector) q.getOffOrder().get(qd.getPrice());

					if (v == null) {
						v = new Vector();
						q.getOffOrder().put(qd.getPrice(), v);
					}

					if (v != null) {

						synchronized (v) {

							if (type.equals("1")) {
								if (idx >= 0 && idx <= v.size())
									v.add(idx, qd);
								else
									v.addElement(qd);
							} else if (type.equals("3"))
								v.remove(qd);
							else if (type.equals("5")) {
								// v.addElement(qd);
								int idx2 = v.indexOf(qd);

								if (idx2 >= 0) {

									Queue qobj = (Queue) v.get(idx2);
									// log.info("before change order " +
									// v.size()
									// + "price " + key + "#" +
									// qd.getPrice()+"|"+qobj.toString()+"#comparator#"+qd.toString());
									// v.addElement(qd);
									v.remove(idx2);
									qobj = qd;

									v.add(idx2, qobj);
								} else {
									v.addElement(qd);
								}

								// Queue test =(Queue) v.get(v.indexOf(qobj));

							}
						}

						// log.info("sdata offer append "+sdata[i]);

						sbf.append(sdata[i]).append(">");
					}

				}

			}

			// log.info("sbf "+sbf.toString());
			broadcast(sbf.toString());
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error("error msg " + msg);

		}

	}

	public void broadcast(String data) {

		if (data != null && !data.isEmpty()) {

			String[] sdata = data.split(">");
			for (String ss : sdata) {
				if (!ss.isEmpty()) {
					String[] key = ss.split("\\|");

					String type = key[2].equals("2") || key[2].equals("4") ? "0"
							: key[2].equals("3") || key[2].equals("5") ? "1"
									: key[2].equals("0") || key[2].equals("1") ? key[2]
											: "";
					if (type.isEmpty()) {
						log.info("cannot process msg " + ss);
					}

					Vector<SusbcriberQueue> v = hClient.get(key[0] + "#"
							+ key[1] + "#" + type + "#" + key[5]);
					if (v != null) {
						for (int i = 0; i < v.size(); i++) {
							// log.info("broadcast:" + ss + ":" + data);
							SusbcriberQueue sb = v.elementAt(i);
							boolean issuccess = sb.newMessage(ss);
							if (!issuccess) {
								v.remove(i);
							}

						}
					}

				}
			}
		}

	}

	public static String getString(String sfeed, String pattern) {
		String sdata = sfeed.split("<")[1];
		int idxbid = sdata.indexOf(pattern);
		int bidend = sdata.indexOf("]", idxbid + 1);

		// System.out.println(idxbid+" "+bidend);

		return sdata.substring(idxbid + 1, bidend).replace("[", "")
				.replace("]", "");
	}

	public static void main(String[] args) {
		String sdata = "INCO|RG<B[INCO|RG|0|-1|000427257495|2600|5|>]O[]";
		System.out.println(getString(sdata, "B["));
	}

	public Vector getQueue(String stock, String board, Double price) {
		String key = stock + "#" + board;

		if (hQueue.get(key) != null) {
			Vector v = (Vector) hQueue.get(key).getBidOrder().get(price);
			if (v.size() <= 0) {
				Iterator<String> keys = hQueue.keySet().iterator();
				while (keys.hasNext()) {
					String kk = keys.next();
					Quote q = hQueue.get(kk);
					log.info("key  stock " + kk);
					Iterator<Double> bidkey = q.getBidOrder().keySet()
							.iterator();
					while (bidkey.hasNext()) {
						Double dd = bidkey.next();
						v = (Vector) q.getBidOrder().get(dd);
						log.info("key bid " + dd + " " + v.size());
						for (Object obj : v) {
							Queue qb = (Queue) obj;
							log.info("queue " + qb.toString());
						}

					}

				}
			}
			return v;
		} else {
			Iterator<String> keys = hQueue.keySet().iterator();
			while (keys.hasNext()) {
				String kk = keys.next();
				Quote q = hQueue.get(kk);
				log.info("key  stock " + kk);
				Iterator<Double> bidkey = q.getBidOrder().keySet().iterator();
				while (bidkey.hasNext()) {
					Double dd = bidkey.next();
					Vector v = (Vector) q.getBidOrder().get(dd);
					log.info("key bid " + dd + " " + v.size());
					for (Object obj : v) {
						Queue qb = (Queue) obj;
						log.info("queue " + qb.toString());
					}

				}

			}

		}

		return null;
	}

	public void subscribe(MsgListener listener, String msg) {
		String[] sdata = msg.split("\\|");
		// stock|board|type|price
		String key = sdata[2] + "#" + sdata[3] + "#" + sdata[4] + "#"
				+ sdata[5];
		String keystock = sdata[2] + "#" + sdata[3];
		String type = sdata[4];
		Vector<SusbcriberQueue> v = hClient.get(key);
		if (v == null) {
			v = new Vector<SusbcriberQueue>();
			hClient.put(key, v);
		}

		SusbcriberQueue sq = new SusbcriberQueue(listener);

		
		Quote q = hQueue.get(keystock);

		log.info("susbcribe " + keystock + " " + "type " + type + "#" + key);

		Vector vd = null;
		if (q != null) {
			Double price = Double.parseDouble(sdata[5]);
			if (type.equals("0")) {
				vd = (Vector) q.getBidOrder().get(price);
			} else if (type.equals("1")) {
				vd = (Vector) q.getOffOrder().get(price);

			}

			log.info("subscribe data bid" + price + " with size "
					+ (vd != null ? vd.size() : 0) + " type " + type);
		}
		v.add(sq);


		if (vd != null) {
			
			synchronized (vd) {

				for (int i = 0; i < vd.size(); i++) {
					Queue qb = (Queue) vd.elementAt(i);
					//log.info("new order string " + qb.newOrder(-1));
					//log.info("tst " + qb.getStock() + ":" + qb.getBoard());
					StringBuffer sbf = new StringBuffer();
					sbf.append(qb.newOrder(i)).append(">");

					try {
						listener.newMessage(sbf.toString());
					} catch (RemoteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				sq.setIsstart(true);

			}

		}

	}

	public void unsubscribe(MsgListener listener, String msg) {
		String[] sdata = msg.split("\\|");
		String key = sdata[2] + "#" + sdata[3] + "#" + sdata[4] + "#"
				+ sdata[5];
		Vector<SusbcriberQueue> v = hClient.get(key);
		if (v != null) {
			log.info("remove subscriber queue "
					+ v.remove(new SusbcriberQueue(listener)));
		}
	}

}
