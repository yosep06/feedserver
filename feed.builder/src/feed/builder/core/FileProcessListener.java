package feed.builder.core;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import feed.provider.core.MsgListener;

public class FileProcessListener implements MsgListener  {
	private String sfname;
	private PrintWriter out;
	public static final SimpleDateFormat sdf = new SimpleDateFormat(
	"'result'yyMMdd'.txt'");
	private SimpleDateFormat  sdftime = new SimpleDateFormat("HH:mm:ss.SSSSSS");
	private Vector<String> vmsg = new Vector<String>();
	private boolean isstop = false;
	
	public FileProcessListener(String sfname) throws FileNotFoundException{
		this.sfname = sfname+"-"+sdf.format(new Date());
		out = new PrintWriter(sfname);
	}	

	@Override
	public void newMessage(String message) throws RemoteException {		
		process_msg("["+sdftime.format(new Date())+"]["+message+"]");
	}

	@Override
	public void close() throws RemoteException {
		out.flush();
		out.close();
	}


	/*@Override
	public void run() {
		while (!isstop) {
			if (this.vmsg.size() > 0) {
				String stemp = (String) this.vmsg.remove(0);
				process_msg(new String(stemp));
			} else {
				try {
					synchronized (this.vmsg) {
						this.vmsg.wait();
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
	}*/

	private void process_msg(String msg) {		
		
		boolean bvalid = true;

		if (bvalid) {
			this.out.println(msg);
			this.out.flush();
		}
	}
}
