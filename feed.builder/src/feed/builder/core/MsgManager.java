package feed.builder.core;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.db4o.query.Query;

import feed.provider.core.MsgListener;
import feed.provider.data.FeedDb;
import feed.provider.data.FeedMsg;

public abstract class MsgManager  implements MsgListener {
	private static final long serialVersionUID = 2683467732638580178L;
	protected String url;
	protected FeedDb db;
	protected Hashtable clientList;
	protected FeedMsg msg;
	protected final static SimpleDateFormat formatDate = new SimpleDateFormat("yyyyMMdd");	
	protected final static 	 NumberFormat format= new DecimalFormat("###0");
	protected  Logger log = LoggerFactory.getLogger(getClass());
	protected String types;

	public MsgManager(){
		
	}
	
	public MsgManager(String url,String type,boolean usingRMI)throws Exception{
		this.url = url;
		this.types = type;
		clientList = new Hashtable();
		db = new FeedDb(type);
		db.start();
		loadConfig();
		if(usingRMI)
			UnicastRemoteObject.exportObject(this);
	}
	
	public MsgManager(String url, String type) throws Exception{
		this.url = url;
		this.types = type;
		clientList = new Hashtable();
		db = new FeedDb(type);
		db.start();
		loadConfig();
		//UnicastRemoteObject.exportObject(this);
	}
	
	protected synchronized void broadcast(String msg){
        for(Iterator clients = clientList.keySet().iterator(); clients.hasNext();) {
            MsgListener listener = (MsgListener) clients.next();
        	Subscriber s = (Subscriber)clientList.get(listener);
        	s.addQueue(msg);
        }		
	}

	
	protected void loadConfig(){
		msg = new FeedMsg();
		msg.setType("SETTING");
		List o = db.getDb().getInstance().queryByExample(msg);
		if (o.size()>0){
			msg = (FeedMsg)o.get(0);
			msg.setSeqno(msg.getMsg().equals(formatDate.format(new Date()))? msg.getSeqno() : 0);
			msg.setMsg(formatDate.format(new Date()));
		} else {
			msg.setMsg(formatDate.format(new Date()));
			msg.setSeqno(0);
		}
		log.info("seqno for type "+this.types+" "+msg.getSeqno());
	}
	
	public List getSnapShot(int seqno){
		Query query = db.getDb().getInstance().query();
		query.constrain(FeedMsg.class);
		query.descend("seqno").orderAscending().constrain(new Long(seqno)).greater();
		List o = query.execute();
		log.info("request snapshot for type FeedMsg with seqno: "+seqno+" and result: "+(o!=null?o.size()+"" :"0")+ " record(s)");
		return o;
	}
	
	public void addSnapShot(Object message){
		db.putMsg(message);
		//db.getDb().getInstance().store(message);
	}
	
	public synchronized void printClient(){
		System.out.println("total client for Builder type "+types+" "+clientList.size());
	}
	
	public synchronized int subscribe(MsgListener listener, String userid, String pass, int seqno) throws RemoteException {		
		int sessionid = -1;
		if (!clientList.containsKey(listener)){
			log.info("new subscriber "+userid+" from seqno "+seqno);
			Subscriber s = new Subscriber(this, listener, seqno);
			sessionid = s.login();
			clientList.put(listener, s);
		} else sessionid = -2;
		return sessionid;
	}

	public synchronized void unsubscribe(MsgListener listener) throws RemoteException {
		Subscriber s = (Subscriber)clientList.get(listener);
		if (s!=null) s.setStop();
		Object obj = clientList.remove(listener);
		
		log.info("new unsubscriber "+(obj != null));

	}	
	
	public abstract boolean connect() throws Exception ;
	
	public abstract void exit() throws Exception;

	@Override
	public void newMessage(String message) throws RemoteException {
		try {
				//log.info(message);
		    	String[] data = message.split("\\|");
		    	FeedMsg feedMsg = new FeedMsg(data[4],message,Long.parseLong(data[3]));
		    	//System.out.println(feedMsg.getType()+" "+feedMsg.getSeqno()+" "+feedMsg.getMsg());
		    	if (feedMsg.getSeqno() > msg.getSeqno()){
			    		msg.setSeqno(feedMsg.getSeqno());
			    		db.putMsg(msg);
			        	addSnapShot(feedMsg);
			        	broadcast(message);
		    	}
		} catch (Exception ex){
			System.out.println("error while processing: "+message);
			ex.printStackTrace();
		}
	}
	
    @Override
	public synchronized void close(){
        for(Iterator clients = clientList.keySet().iterator(); clients.hasNext();) {
            MsgListener listener = (MsgListener) clients.next();
            try {
            	listener.close();
            } catch (RemoteException e){
            }
        }
        db.getDb().getInstance().commit();
        db.doStop();
    }
}
