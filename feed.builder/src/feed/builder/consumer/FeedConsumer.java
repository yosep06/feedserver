package feed.builder.consumer;

import feed.builder.core.MsgManager;
import feed.provider.core.FileConfig;

public class FeedConsumer {
	private TradeConsumer tradeConsumer;
	private OrderConsumer orderConsumer;
	private OtherConsumer otherConsumer;
	//private Double lot = new Double(500); // 2 Desember ubah ke 100
	private String getLot;
	private Double lot;
	protected FileConfig config;
	
	public FeedConsumer(boolean usingRMI) throws Exception{
		config = new FileConfig("feed.builder.config");
		tradeConsumer = new TradeConsumer(config.getProperty("tradeurl"));		
		orderConsumer = new OrderConsumer(config.getProperty("orderurl"));		
		otherConsumer = new OtherConsumer(config.getProperty("otherurl"));
	}
	
	public FeedConsumer() throws Exception{
	}
	
	public MsgManager getTradeConsumer(){
		return tradeConsumer;
	}
	
	public MsgManager getOrderConsumer(){
		return orderConsumer;
	}
	
	public MsgManager getOtherConsumer(){
		return otherConsumer;
	}
	
	public void start() throws Exception{
		otherConsumer.connect();
		tradeConsumer.connect();
		orderConsumer.connect();
	}
		
	public void stop(){
		try {
			tradeConsumer.exit();
			orderConsumer.exit();
			otherConsumer.exit();
		} catch (Exception ex ){}
	}

	public void setLot(Double  lot) {
		this.lot = lot;
	}

	public Double getLot() {
		getLot = config.getProperty("lotsize");
		lot = Double.parseDouble(getLot);
		//System.out.println("GET LOT :" + lot.toString());
		return lot;
	}
}
