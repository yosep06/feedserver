package feed.builder;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import feed.builder.msg.Message;
import feed.builder.msg.Quote;
import feed.provider.data.FeedMsg;

public final class Console extends Thread {
	private final Logger log = LoggerFactory.getLogger(getClass());
	private FeedBuilderApp apps;
	private static final String C_VERSION = "1.0.0.Release1";
	private boolean stop;

	public Console(FeedBuilderApp apps) {
		this.apps = apps;
		stop = false;
	}

	private void prompt() {
		System.out.println("");
		System.out.print("builder> ");
	}

	public void showVersion() {
		System.out.println("Version : " + C_VERSION);
	}

	private void showMenu() {
		welcomScreen();
		System.out.println("List of All Commands : ");
		System.out
				.println("\tCommands must appear first on line and end with enter ");
		System.out.println("");
		System.out.println("");
		System.out.println("help\t\tdisplay this help (menu).");
		System.out.println("version\t\tdisplay server  version.");
		System.out.println("client\t\tdisplay client list");
		System.out.println("exit\t\texit server application");
		System.out.println("");
	}

	public void setstop() {
		stop = true;
	}

	@Override
	public void run() {
		welcomScreen();
		prompt();
		BufferedReader bufferedreader = new BufferedReader(
				new InputStreamReader(System.in));
		do {
			try {
				String cmd = bufferedreader.readLine();
				if (cmd.toLowerCase().equals("start")) {
					// apps.run();
				} else if (cmd.toLowerCase().equals("version")) {
					showVersion();
				} else if (cmd.toLowerCase().equals("help")) {
					showMenu();
				} else if (cmd.toLowerCase().equals("client")) {
					apps.getBuilder().printClient();
				} else if (cmd.toLowerCase().startsWith("data.provider")) {
					String[] sp = cmd.split(" ");
					int type = Integer.parseInt(sp[1]);
					List l = null;
					if (type == 1) {
						l = apps.getConsumer().getOrderConsumer()
								.getSnapShot(0);
					} else if (type == 2)
						l = apps.getConsumer().getTradeConsumer()
								.getSnapShot(0);
					else if (type == 0)
						l = apps.getConsumer().getOtherConsumer()
								.getSnapShot(0);

					System.out.println("data consumer for type " + type + " "
							+ l.size());
					long bytemsg = 0;
					for (int i = 0; i < l.size(); i++)
						bytemsg += ((FeedMsg) l.get(i)).getMsg().getBytes().length;

					System.out.println("length of msg for type " + type + " "
							+ bytemsg);

				} else if (cmd.toLowerCase().startsWith("data.queue")) {
					// stock|board
					String[] dt = cmd.split(" ");
					String[] sp = dt[1].split("\\|");
					String sdata = apps.getBuilder().getQueueBuilder()
							.getQuote(sp[0], sp[1]);
					log.info("data.quote " + sdata);

				} else if (cmd.toLowerCase().startsWith("data.bid.queue")) {
					// stock|board
					String[] dt = cmd.split(" ");
					String[] sp = dt[1].split("\\|");
					Vector v = apps.getBuilder().getQueueBuilder()
							.getQueue(sp[0], sp[1], Double.parseDouble(sp[2]));
					log.info("data.quote " + v.size());
					if(v.size() > 0){
						log.info("vdata "+v.toString());
					}

				} else if (cmd.toLowerCase().startsWith("data.builder")) {
					String[] sp = cmd.split(" ");
					int type = Integer.parseInt(sp[1]);
					boolean show = Boolean.parseBoolean(sp[2]);
					List l = null;
					if (type == 0) {
						l = apps.getBuilder().getOtherBuilder().getSnapShot(0);
					} else if (type == 1)
						l = apps.getBuilder().getQuoteBuilder().getSnapShot(0);
					else if (type == 2)
						l = apps.getBuilder().getTradeBuilder()
								.getForceSnapShot(0);

					System.out.println("data consumer for type " + type + " "
							+ l.size() + " isshow " + show);
					long bytemsg = 0;
					for (int i = 0; i < l.size(); i++) {

						Object obj = l.get(i);
						if (obj instanceof Quote) {
							bytemsg += ((Quote) obj).toString().getBytes().length;
						} else if (obj instanceof Message) {
							bytemsg += ((Message) obj).toString().getBytes().length;
						}

						if (show) {
							if (obj instanceof Message)
								log.info("message "
										+ ((Message) obj).toString());
						}

						// bytemsg +=
						// ((FeedMsg)l.get(i)).getMsg().getBytes().length;
					}

					System.out.println("length of msg for type " + type + " "
							+ bytemsg);

				} else if (cmd.toLowerCase().startsWith("bench.print")) {
					apps.getConsumer().getTimerBench().printLatency();
				} else if (cmd.toLowerCase().startsWith("q|")) {
					String[] quote = cmd.split("\\|");
					System.out.println(apps.getBuilder().getQuote(
							quote[1].toUpperCase(), quote[2].toUpperCase()));
				} else if (cmd.toLowerCase().equals("exit")) {
					try {
						stop = true;
						apps.stop();
						System.exit(0);
					} catch (Exception ex) {
						ex.printStackTrace();
						System.out.println("error : stopping services");
						log.error("error stopping service", ex);
					}
				} else if (cmd.toLowerCase().startsWith("t|")) {
					String[] trade = cmd.split("\\|");
					System.out.println(apps.getBuilder().getTrade(
							trade[1].toUpperCase()));
				}
			} catch (Exception exception) {
				exception.printStackTrace();
				System.out.println("bad command, please try again..");
			}
			prompt();
		} while (!stop);
		System.exit(0);
	}

	public void welcomScreen() {
		StringBuffer stringbuffer = new StringBuffer(100);
		stringbuffer
				.append("\nWelcome to the Feed Builder. Commands end with enter ");
		stringbuffer.append("\nYour  Module id is FeedBuilder version : "
				+ C_VERSION);
		stringbuffer.append("\nCreated by vollux.team@05MAY10 ");
		stringbuffer.append("\n");
		stringbuffer.append("\n Type 'help' for help (menu). ");
		stringbuffer.append("\n");
		System.out.print(stringbuffer);
		System.out.println("");
	}
}