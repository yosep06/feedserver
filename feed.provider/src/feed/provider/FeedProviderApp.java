package feed.provider;

import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import feed.provider.core.Console;
import feed.provider.core.FeedManager;
import feed.provider.provider.FeedProvider;
import feed.provider.socket.FeedProviderSocket;
import feed.provider.source.FeedSource;
import feed.provider.source.FeedSourceManager;

public class FeedProviderApp implements FeedManager {
	private final Logger log = LoggerFactory.getLogger(getClass());

	private int sessionid = 0;
	// private FeedProvider feedProvider;
	private FeedProviderSocket feedProvider;

	// private FeedSource feedSource;

	private FeedSourceManager feedSource;

	// private FeedSourceSolace feedSource;

	public int login(String userid, String password) {
		// tambahkan validasi user & password kalau perlu
		// kembalikan nomor sessionid yang baru jika suskses dan -1 jika gagal.
		return sessionid++;
	}

	public FeedProvider getProvider() {
		return feedProvider;
	}

	public FeedSourceManager getSource() {
		return feedSource;
	}

	public void start() throws Exception {
		 feedSource = new FeedSource(this);
		//feedSource = new FeedSourceSolaceC3S(this);

		// feedProvider = new FeedProvider(this);
		// feedProvider.start();
		feedProvider = new FeedProviderSocket(this);
		feedProvider.start();
		// if (!feedSource.start()) throw new
		// Exception("cannot connect to data source");
		/*
		 * Runtime.getRuntime().addShutdownHook(new Thread() { public void run()
		 * { log.info("shuting down FeedProvider application");
		 * //feedSource.quit(); //feedProvider.close(); } });
		 */
	}

	public static void main(String[] args) {
		try {
			PropertyConfigurator.configure("log.properties");
			FeedProviderApp app = new FeedProviderApp();
			app.start();
			new Console(app).start();
		} catch (Exception ex) {
			ex.printStackTrace();
			System.exit(-1);
		}
	}

}
