package feed.provider.source;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.LineDelimiter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.filter.logging.LoggingFilter;
import org.apache.mina.transport.socket.nio.NioSocketConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import feed.provider.core.FeedManager;
import feed.provider.core.FileConfig;
import feed.provider.core.Utils;
import feed.provider.data.FeedDb;
import feed.provider.data.FeedMsg;
import feed.provider.provider.FileProvider;

public class FeedSource extends IoHandlerAdapter implements FeedSourceManager {
	private final Logger log = LoggerFactory.getLogger(getClass());

	private FeedManager app;
	private NioSocketConnector connector;
	private IoSession session;
	private SocketAddress address;
	private FeedDb db;
	private FeedMsg msg;
	private final static SimpleDateFormat format = new SimpleDateFormat(
			"yyyyMMdd");
	private boolean ready;
	private FileConfig config;
	private String userid = "";
	private String pass = "";
	private String server = "";
	private int port = 2222;
	private FileProvider fileProvider = null;

	public FeedSource(FeedManager manager) throws Exception {
		this.app = manager;
		this.connector = new NioSocketConnector();
		this.db = new FeedDb("master", "");
		db.start();
		loadConfig();
		ready = false;
	}

	protected void loadConfig() throws Exception {
		config = new FileConfig("feed.provider.config");
		userid = config.getProperty("userid", "demo");
		pass = config.getProperty("password", "demo");
		server = config.getProperty("beiserver", "localhost");
		port = Integer.parseInt(config.getProperty("beiport", "2222"));
		msg = new FeedMsg();
		msg.setType("SETTING");
		List o = db.getDb().getInstance().queryByExample(msg);
		if (o.size() > 0) {
			msg = (FeedMsg) o.get(0);
			msg.setSeqno(msg.getMsg().equals(format.format(new Date())) ? msg
					.getSeqno() : 0);
			msg.setMsg(format.format(new Date()));
		} else {
			msg.setMsg(format.format(new Date()));
			msg.setSeqno(0);
		}
		address = new InetSocketAddress(server, port);

		if (config.getProperty("datafeedsaved").equals("yes")) {
			fileProvider = new FileProvider();
			fileProvider.start();
		}
	}

	public boolean start() {
		if (session != null && session.isConnected()) {
			throw new IllegalStateException(
					"Already connected. Disconnect first.");
		}

		try {
			IoFilter LOGGING_FILTER = new LoggingFilter();
			TextLineCodecFactory f = new TextLineCodecFactory(
					Charset.forName("ISO-8859-1"), LineDelimiter.UNIX,
					LineDelimiter.AUTO);
			f.setDecoderMaxLineLength(1024 * 20);
			f.setEncoderMaxLineLength(1024 * 20);
			IoFilter CODEC_FILTER = new ProtocolCodecFilter(f);
			// IoFilter CODEC_FILTER = new ProtocolCodecFilter(new
			// TextLineCodecFactory());
			// connector.getFilterChain().addLast("mdc", new
			// MdcInjectionFilter());
			connector.getFilterChain().addLast("codec", CODEC_FILTER);
			connector.getFilterChain().addLast("logger", LOGGING_FILTER);

			connector.setHandler(this);
			ConnectFuture future1 = connector.connect(address);
			log.info("connecting to FeedSource at " + server + "@" + port);
			future1.awaitUninterruptibly();
			if (!future1.isConnected()) {
				return false;
			} else {
				session = future1.getSession();
				login();
				return true;
			}
		} catch (Exception e) {
			log.info("connection failed");
			log.error(Utils.logException(e));
			this.connector = null;
			this.connector = new NioSocketConnector();
			return false;
		}
	}

	public void login() {
		String cmd = userid + "|" + pass + "|"
				+ (msg.getSeqno() == 0 ? 1 : msg.getSeqno()) + "\r\n";
		log.info("request logon  to server: " + cmd);
		session.write(cmd);
	}

	public void quit() {
		ready = false;
		log.info("close connection from FeedSource");
		if (session != null) {
			if (session.isConnected()) {
				session.close(true);
			}
		}
		db.putMsg(msg);
		db.doStop();
	}
	
	public void changeConnection(String type) throws Exception {

		ready = false;

		log.info("change connection from FeedSource with ip " + server + "@" + port);

 		if (session != null) {

			if (session.isConnected()) {

				session.close(true);

			}

		}

		

		if(connector != null) {

			connector = null;

			this.connector = new NioSocketConnector();

			

			IoFilter LOGGING_FILTER = new LoggingFilter();

			TextLineCodecFactory f = new TextLineCodecFactory(Charset.forName("ISO-8859-1"), LineDelimiter.UNIX,

					LineDelimiter.AUTO);

			f.setDecoderMaxLineLength(1024 * 20);

			f.setEncoderMaxLineLength(1024 * 20);

			IoFilter CODEC_FILTER = new ProtocolCodecFilter(f);

			// IoFilter CODEC_FILTER = new ProtocolCodecFilter(new

			// TextLineCodecFactory());

			// connector.getFilterChain().addLast("mdc", new

			// MdcInjectionFilter());

			connector.getFilterChain().addLast("codec", CODEC_FILTER);

			connector.getFilterChain().addLast("logger", LOGGING_FILTER);



			connector.setHandler(this);

 			

		}



		config = new FileConfig("feed.provider.config");

		userid = config.getProperty(type + "userid", "demo");

		pass = config.getProperty(type + "password", "demo");

		server = config.getProperty(type + "beiserver", "localhost");

		port = Integer.parseInt(config.getProperty(type + "beiport", "2222"));



		address = new InetSocketAddress(server, port);



		// while (true) {

 		log.info("trying to connect FeedSource with " + server + "@" + port);

		Thread.sleep(500);

		ConnectFuture future1 = connector.connect(address);

		future1.awaitUninterruptibly();

		if (future1.isConnected()) {

			log.info("connection to FeedSource back to live");

			session = future1.getSession();

			ready = true;

			login();



			//break;

		}else {

			log.info("failed connect to FeedSource with ip "+ server + "@" + port);

			this.connector = null;

			this.connector = new NioSocketConnector();

		}

		// }

	}



	public void messageReceived(IoSession session, Object message) {
		// IDX|20090124|084648|0000001|0|1|Begin sending records |?w
		try {
			if (!ready) {

				log.info("reply logon from server: " + message.toString());
				log.info("application ready to receive data");
				log.info("first message will be ignored");
				ready = true;
				// if (message.equals("OK")) {
				// log.info("reply logon from server: "+message.toString());
				// log.info("application ready to receive data");
				// ready = true;
				// } else {
				// log.info("reply logon from server: "+message.toString());
				// log.info("invalid response from server. session will be closed");
				// quit();
				// }
			} else {
				String src = (String) message;
				String[] data = src.split("\\|");
				// System.out.println("receive msg:" + src);
				// header|date|time|seqo|type|DAT|checksum|CRLF
				/*
				 * if (data[4].trim().equals("5")) { StringBuffer sbf = new
				 * StringBuffer();
				 * 
				 * for (int i = 0, index = 0; i < (data.length + 1); i++,
				 * index++) { if (i == 7) { sbf.append("--U3---2"); index--; }
				 * else { sbf.append(data[index]); }
				 * 
				 * if (i != data.length) sbf.append("|"); } src =
				 * sbf.toString(); data = src.split("\\|"); }
				 */
				FeedMsg feedMsg = new FeedMsg(data[4].trim(), src,
						Long.parseLong(data[3]));
				feedMsg.setTime(Integer.parseInt(data[2].trim()));
				if (feedMsg.getSeqno() > msg.getSeqno()) {
					app.getProvider().newMsg(feedMsg);
					msg.setSeqno(feedMsg.getSeqno());
					db.putMsg(msg);
				}
				if (fileProvider != null)
					fileProvider.addMsg(src);
			}
		} catch (Exception ex) {
			log.error("cannot process this message (ignored): " + message);
			// log.error(Utils.logException(ex));
		}
	}

	public void exceptionCaught(IoSession session, Throwable cause) {
		log.error("error accured, " + cause.getMessage());
		cause.printStackTrace();
		session.close(true);
	}

	public void sessionClosed(IoSession session) throws Exception {
		if (ready)
			reconnect(session);
	}

	protected void reconnect(IoSession sess) throws Exception {
		while (true) {
			log.info("connection to FeedSource closed, trying to reconnect");
			Thread.sleep(500);
			ConnectFuture future1 = connector.connect(address);
			future1.awaitUninterruptibly();
			if (future1.isConnected()) {
				log.info("connection to FeedSource back to live");
				session = future1.getSession();
				login();
				break;
			}
		}
	}

	public void relogin() {
		if (session != null && session.isConnected()) {
			ready = false;
			login();
		} else
			try {
				reconnect(null);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

	public void restart() {
		ready = false;
		log.info("close connection from FeedSource");
		if (session != null) {
			if (session.isConnected()) {
				session.close(true);
			}
		}

	}

	public static void main(String[] args) {
		String added = "--U3---2";
		String message = "IDX|20100429|080503|0001889|5|TOTO      |TN  |00000008500.00|00000000000.00|00000000000.00|00000000000.00|+00000.00|000000000000|0000000000000000|0000000|000000000.0000|00000049536000|00000000000.00|00000000000.00|000000000000|00000000000.00|000000000000|Ñ";

		String[] sp = message.split("\\|");
		if (sp[4].trim().equals("5")) {
			StringBuffer sbf = new StringBuffer();

			for (int i = 0, index = 0; i < (sp.length + 1); i++, index++) {
				if (i == 7) {
					sbf.append(added);
					index--;
				} else {
					sbf.append(sp[index]);
				}

				if (i != sp.length)
					sbf.append("|");
			}
			System.out.println(sbf.toString());

		}

	}

	@Override
	public long getLastSeqno() {
		// TODO Auto-generated method stub
		return msg.getSeqno();
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub

	}
}
