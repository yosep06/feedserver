package feed.provider.source;

public interface FeedSourceManager {
	
	public void quit();
	
	public long getLastSeqno();
	
	public boolean start();
	
	public void login();
	
	public void  relogin();
	
	public void restart();
	
	public void stop();

	public void changeConnection(String type) throws Exception;

}
