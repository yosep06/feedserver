package feed.provider.provider;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

public class FileProvider extends Thread {
	private PrintWriter out;
	public static final SimpleDateFormat sdf = new SimpleDateFormat(
			"'DF'yyMMdd'.txt'");
	private Vector<String> vmsg = new Vector<String>();
	private boolean isstop = false;
	public static long nincomingmsg = 0L;

	public FileProvider() throws FileNotFoundException {
		initProperty();
	}

	private void initProperty() throws FileNotFoundException {
		String sfconfig = sdf.format(new Date());
		StringBuffer sb = new StringBuffer("data/");
		sb.append(sfconfig);
		out = new PrintWriter(sb.toString());
	}

	@Override
	public void run() {
		while (!isstop) {
			if (this.vmsg.size() > 0) {
				String stemp = (String) this.vmsg.remove(0);
				process_msg(new String(stemp));
			} else {
				try {
					synchronized (this.vmsg) {
						this.vmsg.wait();
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
	}

	private void process_msg(String msg) {
		boolean bvalid = false;
		if (msg.length() > 30) {
			//System.out.println("pf:" + msg.substring(0, 30));
		}

		String[] sdata = msg.split("\\|", -2);
		if (sdata.length > 3) {
			long ntemp = convertToLong(sdata[3], -1L);
			if (ntemp > -1L) {
				bvalid = true;
				nincomingmsg = ntemp;
			}
		}

		if (bvalid) {
			this.out.println(msg);
			this.out.flush();
		}
	}

	public void setStop() {
		isstop = true;
	}

	public void addMsg(String msg) {
		vmsg.add(msg);
		synchronized (vmsg) {
			vmsg.notifyAll();
		}
	}

	public static long convertToLong(String strInput, long defaultValue) {
		long nReturn = 0L;
		boolean bFormat = true;

		Long nTemp = new Long(defaultValue);
		Number nNumber = null;
		NumberFormat nf = NumberFormat.getInstance();

		nReturn = defaultValue;
		strInput = strInput.trim();
		if ((bFormat) && (strInput.length() > 0)) {
			ParsePosition ps = new ParsePosition(0);
			nNumber = nf.parse(strInput, ps);
			if (ps.getIndex() == strInput.length()) {
				nReturn = nNumber.longValue();
			}
		}

		return nReturn;
	}

}
