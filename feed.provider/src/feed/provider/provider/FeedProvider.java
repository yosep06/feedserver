package feed.provider.provider;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import feed.provider.core.FeedManager;
import feed.provider.core.FileConfig;
import feed.provider.core.MsgProvider;
import feed.provider.core.Utils;
import feed.provider.data.FeedMsg;

public class FeedProvider {
	private final Logger log = LoggerFactory.getLogger(getClass());

	protected FeedManager app;
	private OrderProvider orderProvider;
	private TradeProvider tradeProvider;
	private OtherProvider otherProvider;
	protected FileConfig config;

	public FeedProvider(FeedManager app) {
		this.app = app;
	}

	public void printClientlist() {
		orderProvider.printClientlist();
		tradeProvider.printClientlist();
		otherProvider.printClientlist();
	}

	public void start() throws Exception {
		config = new FileConfig("feed.provider.config");

		Utils.loadTime(config);
		orderProvider = new OrderProvider("1", app, Integer.parseInt(config
				.getProperty("providerport", "5000")));
		tradeProvider = new TradeProvider("2", app, Integer.parseInt(config
				.getProperty("providerport", "5000")));
		otherProvider = new OtherProvider("0", app, Integer.parseInt(config
				.getProperty("providerport", "5000")));
		Registry registry = LocateRegistry.createRegistry(Integer
				.parseInt(config.getProperty("providerport", "5000")));
		registry.rebind(config.getProperty("tradeprovider", "trade"),
				tradeProvider);
		registry.rebind(config.getProperty("orderprovider", "order"),
				orderProvider);
		registry.rebind(config.getProperty("otherprovider", "other"),
				otherProvider);
		log.info("FeedProvider ready on port: "
				+ config.getProperty("providerport"));
		log.info("and available services are: "
				+ config.getProperty("tradeprovider") + " "
				+ config.getProperty("orderprovider") + " "
				+ config.getProperty("otherprovider"));

		log.info("time first_preop=" + Utils.first_preop + ",end_preop="
				+ Utils.end_preop + ",first_ses1=" + Utils.first_ses1
				+ ",end_ses1=" + Utils.end_ses1 + ",end_ses1_friday="
				+ Utils.end_ses1_friday + ",first_ses2=" + Utils.first_ses2
				+ ",first_ses2_friday=" + Utils.first_ses2_friday
				+ ",end_ses2=" + Utils.end_ses2 + ",first_preclosing="
				+ Utils.first_preclosing + ",end_preclosing="
				+ Utils.end_preclosing + ",first_post_trading="
				+ Utils.first_post_trading + ",end_post_trading="
				+ Utils.end_post_trading);

	}

	private boolean preopening = true;

	public void newMsg(FeedMsg msg) {
		// IDX|20090124|084648|0000001|0|1|Begin sending records |?w
		char c = msg.getType().charAt(0);
		switch (c) {
		case '1':
			orderProvider.addMsg(msg);
			break;
		case '2':
			tradeProvider.addMsg(msg);
			// khusus untuk saham-saham preopening, quote awal dibentuk oleh
			// trade preop
			/*
			 * if (preopening) { if ((msg.getTime() < 93000) || ((msg.getTime()
			 * > 160000) && (msg.getTime() <161500)) ){
			 * orderProvider.addMsg(msg); }
			 * 
			 * // if (msg.getTime()<93000){ //orderProvider.addMsg(msg); //}
			 * else { //preopening = false; //} }
			 */

			// new datafeed with new time feed
			// if (msg.getTime() >= 155000 && msg.getTime() < 160500) {
			if (msg.getTime() >= Utils.first_preclosing.intValue()
					&& msg.getTime() < Utils.first_post_trading.intValue()) {

				orderProvider.addMsg(msg);

			}
			// end of change

			break;
		case '5':
			// log.info("stock summary "+msg.getMsg());
			otherProvider.addMsg(msg);
			break;
		case '3':
			// log.info("stock data "+msg.getMsg());
			otherProvider.addMsg(msg);

			break;
		default:
			otherProvider.addMsg(msg);
			break;
		}
	}

	public void close() {
		orderProvider.close();
		tradeProvider.close();
		otherProvider.close();
	}

	public MsgProvider getOrderProvider() {
		return null;
	}

	public MsgProvider getTradeProvider() {
		return null;
	}

	public MsgProvider getOtherProvider() {
		return null;
	}
}
