package feed.provider.data;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.db4o.Db4o;
import com.db4o.ObjectContainer;

public class Db {
	private ObjectContainer db = null;
	private final static SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
	private final Logger log = LoggerFactory.getLogger(getClass());
	String typedb;
	
	public Db(String prefix){
		String session = format.format(new Date());
		log.info("opening database : "+"data/"+prefix+"-feed."+session+".db");
		db=Db4o.openFile("data/"+prefix+"-feed."+session+".db");		
		typedb = prefix+"-feed."+session+".db";
		log.info("database opened");
	}
	
	public Db(String prefix, String session){
		log.info("opening database : "+"data/"+prefix+"-feed."+(session.equals("")? "db": session+".db"));
		db=Db4o.openFile("data/"+prefix+"-feed."+(session.equals("")? "db": session+".db"));
		typedb = prefix;
	}
	
	public ObjectContainer getInstance(){
		return db;
	}
	
	public void test(){
		FeedMsg fm = new FeedMsg("0", "ini adalah testing feed message", 0);
		db.store(fm);
	}
	
	public void close(){
		try {
			log.info("closing database "+typedb);
			db.close();
		} catch (Exception ex){
			ex.printStackTrace();
		}
	}
}
