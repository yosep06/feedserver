package feed.provider.data;

import java.util.List;
import java.util.Vector;

public class FeedDb extends Thread {
	protected Db db;
	protected Vector queue;
	private volatile boolean terminated = false;

	public FeedDb(String type) throws Exception {
		super();
		db = new Db(type);
		queue = new Vector(15, 5);
	}

	public FeedDb(String type, String session) throws Exception {
		super();
		db = new Db(type, session);
		queue = new Vector(15, 5);
	}

	public void putMsg(Object msg) {
		synchronized (queue) {
			queue.addElement(msg);
			queue.notify();
		}
	}

	protected Object getMsg() {
		synchronized (queue) {
			return (queue.size() > 0) ? queue.remove(0) : null;
		}
	}

	public List getSnapShot(int seqno) {
		return null;
	}

	public List query(Class param) {
		return db.getInstance().query(param);
	}

	public Db getDb() {
		return db;
	}

	public void run() {
		while (!terminated) {
			try {
				Object msg = getMsg();
				if (msg != null)
					process(msg);
				else
					synchronized (queue) {
						try {
							queue.wait();
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		while (queue.size() > 0) {
			Object msg = getMsg();
			if (msg != null)
				process(msg);
			else
				break;
		}
		try {
			db.close();
			// join();
		} catch (Exception ex) {

			ex.printStackTrace();

		}
	}

	public void doStop() {
		terminated = true;
		synchronized (queue) {
			queue.notify();
		}
	}

	protected void process(Object msg) {
		FeedMsg m = (FeedMsg) msg;
		//System.out.println("----process---- "+m.getMsg());
		db.getInstance().store(msg);
		Thread.yield();
	}
}
