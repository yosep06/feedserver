package feed.provider.core;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import feed.provider.data.FeedMsg;

public class Subscriber extends Thread {
	private String type;
	private FeedManager admin;
	private MsgProvider provider;
	private MsgListener listener;
	private boolean ready = false;
	private boolean needSnapShot = true;
	private String userid;
	private String password;
	private int seqno;
	private Vector qeueu;
	private int sessionid = -1;
	private boolean bstop = false;
	private Logger log = LoggerFactory.getLogger(getClass());

	public Subscriber(String type, FeedManager admin, MsgProvider provider,
			MsgListener listener, String userid, String password, int seqno) {
		this.type = type;
		this.admin = admin;
		this.provider = provider;
		this.listener = listener;
		this.userid = userid;
		this.password = password;
		this.seqno = seqno;
		this.qeueu = new Vector(20, 5);
		setPriority(NORM_PRIORITY + 3);
	}

	public void setStop() {
		if (!bstop) {
			bstop = true;
			synchronized (qeueu) {
				qeueu.notify();
			}
		}
	}

	public void run() {
		while (!bstop) {
			String msg = null;
			synchronized (qeueu) {
				if (qeueu.size() > 0) {
					msg = (String) qeueu.remove(0);
				}
			}
			if (msg != null) {
				newMessage(msg);
				Thread.yield();
			} else {
				synchronized (qeueu) {
					try {
						qeueu.wait();
					} catch (Exception ex) {
					}
				}
			}
		}
		// sent rest of queue
		while (qeueu.size() > 0) {
			String msg = (String) qeueu.remove(0);
			if (msg != null)
				newMessage(msg);
			Thread.yield();
		}
	}

	public int login() {
		sessionid = admin.login(userid, password);
		if (sessionid > -1)
			sendSnapShot();
		return sessionid;
	}

	private void sendSnapShot() {
		if (needSnapShot && !ready) {
			new Thread(new Runnable() {
				public void run() {
					List snapshot = provider.getSnapShot(type, seqno);
					for (int i = 0; i < snapshot.size(); i++) {
						FeedMsg msg = (FeedMsg) snapshot.get(i);
						newMessage(msg.getMsg());
						try {
							Thread.sleep(1);
						} catch (Exception ex) {
						}
					}
					setReady(true);
					Subscriber.this.start();
				}
			}).start();
		} else {
			this.start();
		}
	}

	public synchronized boolean isReady() {
		return ready;
	}

	public synchronized void setReady(boolean ready) {
		this.ready = ready;
	}

	public void addQueue(String message) {
		synchronized (qeueu) {
			qeueu.addElement(message);
			qeueu.notify();
		}
	}

	private void newMessage(String message) {
		try {
			//log.info("new nessage " + message + " size "
				//	+ message.getBytes().length);
			listener.newMessage(message);
			//log.info("new nessage finished" + message);

		} catch (RemoteException ex) {
			qeueu.clear();
			this.setStop();
			provider.clientList.remove(listener);
			ex.printStackTrace();
			System.out.println("error :" + ex.getMessage());
			log.error("error " + ex.getMessage());
		}
	}
}
