package feed.provider.core;

import java.util.List;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eqtrade.ClientSocket;

import feed.provider.data.FeedMsg;

public class SubscriberSocket {
	private String type;
	private FeedManager admin;
	private MsgProviderSocket provider;
	private boolean ready = false;
	private boolean needSnapShot = true;
	private String userid;
	private String password;
	private int seqno;
	private Vector qeueu;
	private int sessionid = -1;
	private boolean bstop = false;
	private Logger log = LoggerFactory.getLogger(getClass());
	private ClientSocket connector;
	private Vector<Object> vtemp = new Vector<Object>();

	public SubscriberSocket(String type, ClientSocket connector,
			FeedManager admin, MsgProviderSocket provider, String userid,
			String password, int seqno) {
		this.type = type;
		this.connector = connector;
		this.admin = admin;
		this.provider = provider;
		this.userid = userid;
		this.password = password;
		this.seqno = seqno;
	}

	public int login() {
		sessionid = admin.login(userid, password);
		return sessionid;
	}

	public void sendSnapShot() {
		if (needSnapShot && !ready) {
			new Thread(new Runnable() {
				public void run() {
					List snapshot = provider.getSnapShot(type, seqno);
					// Vector<Object> vmsg = new Vector<Object>();
					for (int i = 0; i < snapshot.size(); i++) {
						FeedMsg msg = (FeedMsg) snapshot.get(i);
						// vmsg.add(msg.getMsg().getBytes());
						if (connector.isConnected())
							connector.sendMessage((type + "#" + msg.getMsg())
									.getBytes());
						else
							break;

					}
					ready = true;
				}
			}).start();
		}
	}

	protected void newMessage(String msg) {
		try {
			msg = type + "#" + msg;
			if (!ready) {
				vtemp.add(msg.getBytes());
			} else if (isConnected()) {
				if (!vtemp.isEmpty()) {
					for (Object obj : vtemp)
						connector.sendMessage(obj);

				}

				connector.sendMessage(msg.getBytes());
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private boolean isConnected() {
		return connector.isConnected();
	}

}
