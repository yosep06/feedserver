package feed.provider.core;

import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.db4o.query.Query;
import com.eqtrade.ClientSocket;

import feed.provider.data.FeedDb;
import feed.provider.data.FeedMsg;

public class MsgProviderSocket extends MsgProvider implements Runnable {
	private final Logger log = LoggerFactory.getLogger(getClass());
	private static final long serialVersionUID = 6263004426413596696L;
	protected Hashtable clientList;
	protected FeedManager manager;
	protected String type;
	protected FeedDb db;
	private Vector<FeedMsg> vmsg = new Vector<FeedMsg>();
	private boolean terminated = false;
	private Thread th;

	public MsgProviderSocket(String type, FeedManager manager) throws Exception {
		this.manager = manager;
		this.type = type;
		clientList = new Hashtable();
		db = new FeedDb(type);
		db.start();
		th = new Thread(this);
		th.start();
		log.info("Msg Provider for " + type + " created");
	}
	
	

	@Override
	public void printClientlist() {
		synchronized (clientList) {

			System.out.println("total client for provider type " + type + " "
					+ clientList.size() + " record(s)");
		}
	}

	@Override
	public void run() {
		while (!terminated) {

			if (vmsg.isEmpty()) {
				synchronized (vmsg) {
					try {
						vmsg.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else {
				try {
					process(vmsg.remove(0));
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
	}

	public void addSnapShot(FeedMsg message) {
		db.putMsg(message);
	}

	private void process(FeedMsg message) {
		addSnapShot(message);
		synchronized (clientList) {

			for (Iterator clients = clientList.keySet().iterator(); clients
					.hasNext();) {
				ClientSocket listener = (ClientSocket) clients.next();
				SubscriberSocket s = (SubscriberSocket) clientList
						.get(listener);
				s.newMessage(message.getMsg());
			}
		}
	}

	public void close(ClientSocket sock) {
		synchronized (clientList) {

			Object obj = clientList.remove(sock);
			log.info("removed socket from provider " + type + " "
					+ (obj != null) + " " + clientList.size());
		}
	}

	public int subscribe(ClientSocket connector, String userid, String pass,
			int seqno) {
		int sessionid = -1;
		synchronized (clientList) {

			if (!clientList.containsKey(connector)) {
				log.info("new subscriber : " + userid + " from seqno " + seqno
						+ " " + connector.getClass());
				SubscriberSocket s = new SubscriberSocket(type, connector,
						manager, this, userid, pass, seqno);
				sessionid = s.login();
				if (sessionid != -1) {
					clientList.put(connector, s);
					s.sendSnapShot();
				}

			} else {
				sessionid = -2;
			}

			log.info("subscribe with " + sessionid + " " + clientList.size()
					+ " " + getClass());
		}
		return sessionid;
	}

	public void unsubscribe(ClientSocket connector) {
		synchronized (clientList) {

			SubscriberSocket s = (SubscriberSocket) clientList.get(connector);

			if (s != null) {
				clientList.remove(connector);
			}
			log.info("unsubscribe " + type + " " + clientList.size());
		}
	}

	public void addMsg(FeedMsg message) {
		synchronized (vmsg) {
			vmsg.add(message);
			vmsg.notify();
		}
	}

	public List getSnapShot(String type, int seqno) {
		Query query = db.getDb().getInstance().query();
		query.constrain(FeedMsg.class);
		query.descend("seqno").orderAscending().constrain(new Long(seqno))
				.greater();
		List o = query.execute();
		log.info("request snapshot for type: " + type + " with seqno: " + seqno
				+ " and result: " + (o != null ? o.size() + "" : "0")
				+ " record(s)");
		return o;
	}

	public void close() {
		synchronized (clientList) {

			log.info("closing db type " + type + " with client connected: "
					+ clientList.size());
			for (Iterator clients = clientList.keySet().iterator(); clients
					.hasNext();) {
				ClientSocket listener = (ClientSocket) clients.next();
				try {
					listener.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			db.doStop();
			log.info("closed");
		}
	}

}
