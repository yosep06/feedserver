package feed.gateway.socket.consumer;

import java.rmi.RemoteException;
import java.util.List;

import com.db4o.query.Query;
import com.eqtrade.SocketInterface;

import feed.builder.msg.Quote;
import feed.builder.msg.Trade;
import feed.gateway.core.MsgConsumer;
import feed.gateway.socket.core.ProcessorConsumerThread;

public class TradeConsumerSocket extends feed.gateway.consumer.TradeConsumer
		 {

	private SocketInterface socketConnector;
	private ProcessorConsumerThread processorManager;

	
	public TradeConsumerSocket(SocketInterface socket) throws Exception {
		super("","2",false);
		this.socketConnector = socket;
		processorManager = new ProcessorConsumerThread(this);
		processorManager.start();
	}


	@Override
	public boolean connect() throws Exception {
		this.socketConnector.sendMessage(("subscribe " + types+" "+msg.getSeqno()).getBytes());
		return true;
	}

	@Override
	public void exit() throws Exception {
		this.socketConnector.sendMessage(("unsubscribe " + types).getBytes());
		processorManager.setStop();

	}
	
	public void addMsg(String msg) {
		processorManager.addMsg(msg);
	}
	
	

	

}
