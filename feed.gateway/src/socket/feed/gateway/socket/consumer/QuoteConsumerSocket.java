package feed.gateway.socket.consumer;

import com.eqtrade.SocketInterface;

import feed.gateway.consumer.QuoteConsumer;
import feed.gateway.socket.core.ProcessorConsumerThread;

public class QuoteConsumerSocket extends QuoteConsumer {

	private SocketInterface socketConnector;
	private ProcessorConsumerThread processorManager;

	public QuoteConsumerSocket(SocketInterface socket)
			throws Exception {
		super("","1",false);
		processorManager = new ProcessorConsumerThread(this);
		this.socketConnector = socket;
		processorManager.start();
	}

	@Override
	public boolean connect() throws Exception {
		this.socketConnector.sendMessage(("subscribe " + types+" "+msg.getSeqno()).getBytes());
		return true;
	}

	@Override
	public void exit() throws Exception {
		this.socketConnector.sendMessage(("unsubscribe " + types).getBytes());
		processorManager.setStop();

	}

	public void addMsg(String msg) {
		processorManager.addMsg(msg);
	}

}
