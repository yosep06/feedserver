package feed.gateway.socket.broadcaster;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SchedulledProcess extends Thread {

	private int delay = 500, synchtimeDelayed = -1;
	private String type;
	private IProcess process;
	private SimpleDateFormat sdf = new SimpleDateFormat("SSS");
	private Logger log = LoggerFactory.getLogger(getClass());

	public SchedulledProcess(int delayed, String type, IProcess process) {
		this.delay = delayed;
		this.type = type;
		this.process = process;
	}

	@Override
	public void run() {
		try {
			synchtimeDelayed = 1000 - Integer.parseInt(sdf.format(new Date()));
			if (synchtimeDelayed >= 1000) {
				synchtimeDelayed = -1;
			}
		} catch (Exception ex) {
			synchtimeDelayed = -1;
		}

		while (true) {
			synchronized (this) {
				try {
					if (synchtimeDelayed > 0) {
						log.info("synchronez " + process + " " + synchtimeDelayed);

						this.wait(synchtimeDelayed);
						synchtimeDelayed = -1;
					} else {
						

						this.wait(delay);
					}
					process();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
	}

	private void process() {
		this.process.process(type);
	}

	public static void main(String[] args) {
		PropertyConfigurator.configure("log.properties");
		new SchedulledProcess(400, "PROC", new IProcess() {
			
			@Override
			public void process(String type) {
				// TODO Auto-generated method stub
				
			}
		}).start();;
	}

}
