package feed.gateway.socket.broadcaster;

public interface IProcess {
	
	public void process(String type);

}
