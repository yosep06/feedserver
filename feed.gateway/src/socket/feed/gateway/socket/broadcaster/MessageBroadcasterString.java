package feed.gateway.socket.broadcaster;

import java.util.Date;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import feed.provider.core.Utils;

public class MessageBroadcasterString extends Thread {

	protected FeedGatewayAppSocket feedApp;
	protected int delay = 0, sizeclient = 6, qidx = 0, sizeClientTemp = 0;
	protected boolean terminated = false;
	protected Logger log = LoggerFactory.getLogger(getClass());
	protected long diff = 0, delayClientTemp = 0, delayTime;

	// private ThreadPoolExecutor poolExecutor;

	// private Vector<SenderBroadcaster> vSenderBroadcasters = new
	// Vector<SenderBroadcaster>();

	public MessageBroadcasterString(FeedGatewayAppSocket feedApp) {
		this.feedApp = feedApp;
		delay = feedApp.getTimerMessageBroadcaster();
		/*
		 * int maxpool = Integer.parseInt((String) feedApp.getConfig().get(
		 * "maxthreadsenderhandler"));
		 * 
		 * //int maxthread = Integer.parseInt((String) feedApp.getConfig().get(
		 * // "maxthreadsenderhandler"));
		 * 
		 * //int maxCoreThread = Runtime.getRuntime().availableProcessors();
		 * 
		 * System.out.println(maxpool);
		 * 
		 * poolExecutor = new ThreadPoolExecutor( 5, maxpool, 5l,
		 * TimeUnit.SECONDS, (BlockingQueue<Runnable>) new
		 * java.util.concurrent.LinkedBlockingQueue<Runnable>());
		 * 
		 * /* for (int i = 0; i < sizeclient; i++) vSenderBroadcasters.add(new
		 * SenderBroadcaster(delay));
		 */
		// setPriority(Thread.NORM_PRIORITY + 2);
		setPriority(Thread.NORM_PRIORITY + 4);

	}

	public MessageBroadcasterString(int delay) {
		this.delay = delay;
		/*
		 * for (int i = 0; i < sizeclient; i++) vSenderBroadcasters.add(new
		 * SenderBroadcaster(delay));
		 */}

	@Override
	public void run() {

		while (!terminated) {
			try {

				// log.info("wait broadcaster.." + delay + " " + terminated);
				synchronized (this) {
					this.wait(delay);
				}

				// log.info("wait broadcaster 2.."+delay+" "+terminated);

				// int sizesent = 0;
				long startTemp = System.nanoTime();
				Vector<ClientGtwSocket> vc = ((GtwServiceSocket) feedApp
						.getService()).getVclientsocket();

				// Vector<ClientGtwSocket> vtemp = new
				// Vector<ClientGtwSocket>();

				long start = System.nanoTime();

				// synchronized (vc) {

				Vector<ClientGtwSocket> vctemp = new Vector<ClientGtwSocket>();
				vctemp.addAll(vc);

				startTemp = System.nanoTime() - startTemp;

				for (int i = 0; i < vctemp.size(); i++) {
					ClientGtwSocket cgt = vctemp.get(i);

					if (!cgt.isConnected()) {
						// vtemp.add(cgt);
					} else {
						// cgt.sendByteTrade();
						// poolExecutor.execute(cgt);
						cgt.sendMessageString();
					}
				}

				/*
				 * if (vtemp.size() > 0) { synchronized (vc) {
				 * vc.removeAll(vtemp); } }
				 */

				/*
				 * for (ClientGtwSocket cgt : vc) { try { cgt.send(); } catch
				 * (Exception e) { e.printStackTrace(); }
				 * 
				 * }
				 */

				// }

				long diff = System.nanoTime() - start;
				if (diff > this.diff) {
					this.diff = diff;
					delayTime = System.currentTimeMillis();
				}

				if (startTemp > delayClientTemp) {
					delayClientTemp = startTemp;
					sizeClientTemp = vctemp.size();
				}

			} catch (Exception e) {
				e.printStackTrace();
				log.error(Utils.logException(e));
			}

		}
	}

	// log.info("outtt broadcaster...."+terminated); }

	public void setstop() {
		synchronized (this) {
			terminated = true;
			this.notify();
		}
	}

	/*
	 * public long getDiff() { return diff; }
	 */

	public int getDelay() {
		return delay;
	}

	/*
	 * public void login(ClientGtwSocket clientGtwSocket) {
	 * vSenderBroadcasters.get(qidx).getVclients().add(clientGtwSocket); qidx++;
	 * if (qidx >= sizeclient) qidx = 0; }
	 */

	/*
	 * public void start() { for (int i = 0; i < sizeclient; i++)
	 * vSenderBroadcasters.get(i).start(); }
	 */

	public void printInfo() {
		/*
		 * for (int i = 0; i < sizeclient; i++) { SenderBroadcaster sender =
		 * vSenderBroadcasters.get(i); log.info("sender_" + i + "_sizeclient_" +
		 * sender.getVclients().size() + "_" +
		 * TimeUnit.SECONDS.convert(sender.getDiff(), TimeUnit.NANOSECONDS) +
		 * "_delay:" + sender.getDelay()); }
		 */

		// Vector<ClientGtwSocket> v = ((GtwServiceSocket) feedApp.getService())
		// .getVclientsocket();
		// log.info("check.client");
		// for (ClientGtwSocket cg : v)
		// cg.print();

		log.info(getClass()
				+ " size_client_"
				+ ((GtwServiceSocket) feedApp.getService()).getVclientsocket()
						.size()
				+ "_diff:"
				+ TimeUnit.SECONDS.convert(diff, TimeUnit.NANOSECONDS)
				+ ":"
				+ delay
				+ " delay_gen_client:"
				+ TimeUnit.MILLISECONDS.convert(delayClientTemp,
						TimeUnit.NANOSECONDS) + " " + sizeClientTemp
				+ ":delay_time:" + new Date(delayTime));

	}

	public int getSizeclient() {
		return sizeclient;
	}

	public void reset() {
		diff = 0;
		delayClientTemp = 0;
		sizeClientTemp = 0;
		// Vector<ClientGtwSocket> v = ((GtwServiceSocket) feedApp.getService())
		// .getVclientsocket();
		// for (ClientGtwSocket cg : v)
		// cg.reset();
	}

}
