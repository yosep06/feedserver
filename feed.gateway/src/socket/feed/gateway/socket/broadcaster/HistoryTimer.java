package feed.gateway.socket.broadcaster;

import java.util.List;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import feed.gateway.socket.core.HistoryDataClient;
import feed.builder.msg.Message;
import feed.builder.msg.TopGainer;
import feed.builder.msg.Trade;
import feed.builder.msg.TradeMobile;
import feed.builder.msg.TradePrice;
import feed.builder.msg.TradeSummBrokerStock;
import feed.builder.msg.TradeSummStockBroker;
import feed.gateway.broadcaster.Client;
import feed.gateway.consumer.FeedConsumer;

public class HistoryTimer extends Thread {

	private Vector<HistoryDataClient> vdataclient = new Vector<HistoryDataClient>();
	private FeedConsumer consumer;
	private Logger log = LoggerFactory.getLogger(getClass());

	public HistoryTimer(FeedConsumer consumer) {
		super();
		this.consumer = consumer;
	}

	@Override
	public void run() {
		while (true) {
			try {

				if (!vdataclient.isEmpty()) {
					HistoryDataClient hdc = vdataclient.remove(0);
					// log.info("hdc_"+hdc);
					sendOtherSnapShot(hdc.getCls(), hdc.getParam(),
							hdc.getSeqno(), hdc.getC());

				} else {
					synchronized (vdataclient) {
						vdataclient.wait();

					}
				}

			} catch (Exception ex) {
				ex.printStackTrace();
			}

		}
	}

	public void addHistoryClient(HistoryDataClient hdc) {
		synchronized (vdataclient) {
			vdataclient.add(hdc);
			vdataclient.notifyAll();
		}
	}

	public void sendOtherSnapShot(Class cls, String param, int seqno, Client c) {
		List l = null;
		long start = System.nanoTime();

		if (cls.equals(TradePrice.class)) {
			l = consumer.getOtherConsumer().getTPSnapShot(param, seqno);
		} else if (cls.equals(Trade.class)) {
			l = consumer.getOtherConsumer().getTHSnapShot(param, seqno);
		} else if (cls.equals(TradeSummBrokerStock.class)) {
			l = consumer.getOtherConsumer().getTSBSSnapShot(param, seqno);
		} else if (cls.equals(TradeSummStockBroker.class)) {
			l = consumer.getOtherConsumer().getTSSBSnapShot(param, seqno);
		} else if (cls.equals(TradeMobile.class)) {
			l = consumer.getOtherConsumer().getTHSnapShotPaging(param, seqno);
		}else if (cls.equals(TopGainer.class)) {
			System.out.println("historytimer topgainer");
			l = consumer.getOtherConsumer().selectTopGainer(param);
		} else {
			l = consumer.getOtherConsumer().getSnapShot(cls, seqno);
		}
		if (l != null) {
			for (int i = 0; i < l.size(); i++) {
				c.addOther(((Message) l.get(i)).toString());
			}
			long end = System.nanoTime() - start;
			log.info("send_snaphot_" + cls + "_" + param + "_" + l.size() + "_"
					+ TimeUnit.MILLISECONDS.convert(end, TimeUnit.NANOSECONDS)+" "+c.getSessionid());

		}
	}

}
