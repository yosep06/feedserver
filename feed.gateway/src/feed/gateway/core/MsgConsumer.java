package feed.gateway.core;

import java.rmi.Naming;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import feed.builder.core.MsgManager;
import feed.gateway.broadcaster.GtwService;
import feed.provider.core.MsgProducer;

public class MsgConsumer extends MsgManager {
	protected MsgProducer provider;
	protected GtwService service;
	protected Log log = LogFactory.getLog(getClass());	
	
	public MsgConsumer(String url, String type) throws Exception{
		super(url, type);
	}
	
	public MsgConsumer(String url, String type,boolean b) throws Exception{
		super(url, type,b);
	}
	
	public void setService(GtwService service){
		this.service = service;
	}
		
	public boolean connect() throws Exception {
		provider = (MsgProducer) Naming.lookup(url);
		//System.out.println("connect: "+ msg.getSeqno() + ":"+ url);
		if (provider != null){
			provider.subscribe(this, "", "", (int)msg.getSeqno());
		}
		log.info("connection to: "+url+" ready");
		return provider!=null;
	}
	
	public void exit() throws Exception {
		provider.unsubscribe(this);
	}
}
