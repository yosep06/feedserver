package feed.gateway.core;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

public class FileWriter extends Thread {

	private PrintWriter out;
	private String fileName;
	private Vector<String> vmsg = new Vector<String>();
	private boolean isstop = false;
	public static long nincomingmsg = 0L;
	private SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

	public FileWriter(String fileName) throws FileNotFoundException {
		this.fileName = fileName;
		initProperty();
		setPriority(Thread.MIN_PRIORITY + 1);
	}

	private void initProperty() throws FileNotFoundException {

		StringBuffer sb = new StringBuffer("data/");
		sb.append(fileName);
		out = new PrintWriter(sb.toString());
	}

	@Override
	public void run() {
		while (!isstop) {
			if (this.vmsg.size() > 0) {
				String stemp = (String) this.vmsg.remove(0);
				process_msg("[" + sdf.format(new Date()) + "]"
						+ new String(stemp));
			} else {
				try {
					synchronized (this.vmsg) {
						this.vmsg.wait();
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
	}

	private void process_msg(String msg) {

		this.out.println(msg);
		this.out.flush();

	}

	public void setStop() {
		isstop = true;
	}

	public void addMsg(String msg) {
		vmsg.add(msg);
		synchronized (vmsg) {
			vmsg.notifyAll();
		}
	}

}
