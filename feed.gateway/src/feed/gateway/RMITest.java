package feed.gateway;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

interface RemoteOperations extends Remote {
    String remoteOperation() throws RemoteException;
}

public class RMITest implements RemoteOperations {
    private static boolean holdStrongReference = false;
    private static boolean invokeGarbageCollector = true;
    private static int delay = 0;

    private static final String REMOTE_NAME = RemoteOperations.class.getName();

    public static void main(String... args) throws Exception {
        for (String arg : args) {
            if ("-gc".equals(arg)) {
                invokeGarbageCollector = true;
            } else if ("-nogc".equals(arg)) {
                invokeGarbageCollector = false;
            } else if ("-hold".equals(arg)) {
                holdStrongReference = true;
            } else if ("-release".equals(arg)) {
                holdStrongReference = false;
            } else if (arg.startsWith("-delay")) {
                delay = Integer.parseInt(arg.substring("-delay".length()));
            } else {
                System.err.println("usage: javac RMITest.java && java RMITest [-gc] [-nogc] [-hold] [-release] [-delay<seconds>]");
                System.exit(1);
            }
        }
        server();
        if (invokeGarbageCollector) {
            System.gc();
        }
        if (delay > 0) {
            System.err.println("delaying " + delay + " seconds");
            int milliseconds = delay * 1000;
            Thread.sleep(milliseconds);
        }
        client();
        System.exit(0);
    }

    @Override
    public String remoteOperation() {
        return "foo";
    }

    private static RemoteOperations classVariable ;
    private static Remote remote ;
    private static Registry registry ;

    private static void server() throws Exception {
        // This reference is eligible for GC after this method returns
        RemoteOperations methodVariable = new RMITest();
        classVariable = new RMITest();

        //RemoteOperations toBeStubbed = holdStrongReference ? classVariable : methodVariable;
        RemoteOperations toBeStubbed = classVariable ;
        remote = UnicastRemoteObject.exportObject(toBeStubbed, 0);
        registry = LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
        registry.bind(REMOTE_NAME, remote);
    }

    private static void client() throws Exception {
        Registry registry = LocateRegistry.getRegistry();
        Remote remote = registry.lookup(REMOTE_NAME);
        RemoteOperations stub = RemoteOperations.class.cast(remote);
        String message = stub.remoteOperation();
        System.out.println("received: " + message);
    }
}