package feed.gateway;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.jboss.netty.channel.ChannelFuture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eqtrade.netty.ClientSocketNetty;

import feed.builder.msg.Trade;
import feed.builder.msg.TradeSummStockMarketInv;
import feed.gateway.socket.broadcaster.ClientGtwSocket;
import feed.gateway.socket.broadcaster.FeedGatewayAppSocket;
import feed.provider.core.Utils;
import feed.provider.data.FeedMsg;

public final class Console extends Thread {
	private final Logger log = LoggerFactory.getLogger(getClass());
	private FeedGatewayApp apps;
	private static final String C_VERSION = "3.0.0.Release1";
	private boolean stop;

	public Console(FeedGatewayApp apps) {
		this.apps = apps;
		stop = false;
	}

	private void prompt() {
		System.out.println("");
		System.out.print("gateway> ");
	}

	public void showVersion() {
		System.out.println("Version : " + C_VERSION);
	}

	private void showMenu() {
		welcomScreen();
		System.out.println("List of All Commands : ");
		System.out.println("\tCommands must appear first on line and end with enter ");
		System.out.println("");
		System.out.println("");
		System.out.println("help\t\tdisplay this help (menu).");
		System.out.println("version\t\tdisplay server  version.");
		System.out.println("client\t\tdisplay client list");
		System.out.println("ping\t\tping db connection");
		System.out.println("exit\t\texit server application");
		System.out.println("clear.news\t\tclear news map");

		System.out.println("");
	}

	public void setstop() {
		stop = true;
	}

	public void run() {
		welcomScreen();
		prompt();
		BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
		do {
			try {
				String cmd = bufferedreader.readLine();
				if (cmd.toLowerCase().equals("start")) {
					// apps.run();
				} else if (cmd.toLowerCase().equals("version")) {
					showVersion();
				} else if (cmd.toLowerCase().equals("help")) {
					showMenu();
				} else if (cmd.toLowerCase().equals("client")) {
					apps.getService().printClient();
				} else if (cmd.toLowerCase().startsWith("data.builder")) {
					String[] sp = cmd.split(" ");
					int type = Integer.parseInt(sp[1]);
					List l = null;
					if (type == 0) {
						l = apps.getConsumer().getOtherConsumer().getSnapShot(0);
					} else if (type == 1)
						l = apps.getConsumer().getQuoteConsumer().getSnapShot(0);
					else if (type == 2)
						l = apps.getConsumer().getTradeConsumer().getSnapShotForce(0);

					System.out.println("data consumer for type " + type + " " + (l == null ? 0 : l.size()));
					long bytemsg = 0;
					for (int i = 0; i < l.size(); i++)
						bytemsg += ((FeedMsg) l.get(i)).getMsg().getBytes().length;

					System.out.println("length of msg for type " + type + " " + bytemsg);

				} else if (cmd.toLowerCase().startsWith("bench.print")) {
					apps.getConsumer().getTimer().printLatency();
				} else if (cmd.toLowerCase().equals("ping")) {
					// apps.ping();
				} else if (cmd.toLowerCase().equals("exit")) {
					try {
						stop = true;
						apps.stop();
						System.exit(0);
					} catch (Exception ex) {
						ex.printStackTrace();
						System.out.println("error : stopping services");
						log.error("error stopping service", ex);
					}
				} else if (cmd.toLowerCase().equals("emiten")) {
					// File file = new File("emiten/AALI/AALI.zip");
					// FileInputStream fin = new FileInputStream(file);
					// BufferedInputStream bin = new BufferedInputStream(fin);
					// byte[] contents = new byte[4096];
					// int bytesRead = 0;
					// ByteArrayOutputStream bout = new ByteArrayOutputStream();
					// FileOutputStream fos = new FileOutputStream(
					// "data/AALI.zip");
					// while ((bytesRead = bin.read(contents)) != -1) {
					// //fos.write( contents, 0, bytesRead);
					// bout.write(contents, 0, bytesRead);
					// }
					// fos.write(bout.toByteArray());
					// fos.close();
					// bin.close();
				} else if (cmd.toLowerCase().startsWith("th|")) {
					String[] trade = cmd.split("\\|");
					Integer seqno = Integer.parseInt(trade[2]);
					Integer isshow = Integer.parseInt(trade[3]);
					List l = apps.getService().getConsumer().getOtherConsumer()
							.getTHSnapShot(trade[1].trim().toUpperCase(), seqno);

					if (isshow == 1 && l != null) {
						for (int i = 0; i < l.size(); i++) {
							Trade tr = (Trade) l.get(i);
							log.info("th_" + tr.toString());
						}
					}

					log.info("size_data_" + l.size() + "_" + trade[1]);

				} else if (cmd.toLowerCase().equals("t|")) {
					String[] trade = cmd.split("\\|");
					System.out.println(apps.getService().getConsumer().getOtherConsumer()
							.getTHSnapShotLast(trade[1].toUpperCase(), 100));
				} else if (cmd.startsWith("check.socket")) {
					Iterator<Integer> keys = apps.getService().getClient().keySet().iterator();
					while (keys.hasNext()) {
						Integer key = keys.next();
						ClientGtwSocket cgs = (feed.gateway.socket.broadcaster.ClientGtwSocket) apps.getService()
								.getClient().get(key);
						ClientSocketNetty csn = (ClientSocketNetty) cgs.getSocketInterface();
						log.info(cgs.getUserid() + ":" + cgs.getSessionid());

						//ChannelFuture future = csn.get.getChannel().write(Utils.compress(("test.send.data").getBytes()))
						//		.await();

						//log.info("test.send.data:" + future.isCancelled() + ":" + future.isDone() + ":"
						//		+ future.isSuccess());

					}
				} else if (cmd.startsWith("check.scheduler")) {

					((FeedGatewayAppSocket) apps).getMsgBroadcaster().printInfo();

				} else if (cmd.startsWith("reset.scheduler")) {
					((FeedGatewayAppSocket) apps).getMsgBroadcaster().reset();
				}

				else if (cmd.startsWith("market.detail")) {
					apps.getService().getConsumer().getOtherConsumer().getMMSnapShot();
				} else if (cmd.startsWith("news.mobile")) {
					System.out.println(apps.getService().getConsumer().getOtherConsumer().getNWMSnapShot());
				} else if (cmd.startsWith("tmsi")) {
					System.out.println(" 789789 ");
					String[] foreign = cmd.split(" ");
					System.out.println("dd " +
					// apps.getService().getConsumer().getOtherConsumer().getCRMSnapShot().toString()+"
					// \n"+apps.getService().getConsumer().getOtherConsumer().getCRSnapShot().toString()
					// apps.getService().getConsumer().getOtherConsumer().getGIMSnapShot()+"\n"+
					// apps.getService().getConsumer().getOtherConsumer().getCRMSnapShot()
							apps.getService().getConsumer().getOtherConsumer().getFDMSnapshotMobile(foreign[1],
									foreign[2]));
				} else if (cmd.startsWith("stockmarket")) {
					// List l
					// =apps.getService().getConsumer().getOtherConsumer().selectChartIntradayMobile("COMPOSITE");
					List<TradeSummStockMarketInv> l = apps.getService().getConsumer().getOtherConsumer()
							.getSnapShot(TradeSummStockMarketInv.class, 0);
					int i = 0;
					for (TradeSummStockMarketInv object : l) {
						System.out.println(l.get(i).toString());
						i++;
					}
				} else if (cmd.startsWith("limit")) {
					String sp[] = cmd.split("#");
					apps.getService().setlimit(Integer.parseInt(sp[1]));

				} else if (cmd.toLowerCase().startsWith("clear.news")) {
					((FeedGatewayApp) apps).consumer.getOtherConsumer().getNWSnapShot().clear();
					((FeedGatewayApp) apps).consumer.getOtherConsumer().getNWMSnapShot().clear();
				} else {
					// System.out.println("not_found");
					log.info("not_found");
				}
			} catch (Exception exception) {
				exception.printStackTrace();
				System.out.println("bad command, please try again..");
			}
			prompt();
		} while (!stop);
		System.exit(0);
	}

	public void welcomScreen() {
		StringBuffer stringbuffer = new StringBuffer(100);
		stringbuffer.append("\nWelcome to the Feed Gateway. Commands end with enter ");
		stringbuffer.append("\nYour  Module id is FeedGateway version : " + C_VERSION);
		stringbuffer.append("\nCreated by vollux.team@05MAY10 ");
		stringbuffer.append("\n");
		stringbuffer.append("\n Type 'help' for help (menu). ");
		stringbuffer.append("\n");
		System.out.print(stringbuffer);
		System.out.println("");
	}
}