package feed.gateway.consumer;

import java.rmi.RemoteException;
import java.util.List;

import com.db4o.query.Query;

import feed.builder.msg.Trade;
import feed.gateway.core.MsgConsumer;
import feed.provider.data.FeedMsg;


public class TradeConsumer extends MsgConsumer {
	public TradeConsumer(String url) throws Exception{
		super(url, "1");
	}
	
	public TradeConsumer(String url,String type,boolean b) throws Exception{
		super(url, type,b);
	}
	
	public TradeConsumer(String url,String type) throws Exception{
		super(url, type);
	}
	
	public List getSnapShot(int seqno){
		//override running trade dikirim data terakhir
		return null;
	}
	
	public  void newMessage(String message) throws RemoteException {
		try {
			//System.out.println("TradeConsumer: "+message);
		    	String[] data = message.split("\\|");
		    	Trade tr = new Trade();
		    	tr.setContent(data);
		    	//if (tr.getSeqno() > msg.getSeqno()){
			    		msg.setSeqno((long)tr.getSeqno());
			    		db.putMsg(msg);
			        	addSnapShot(tr);
			        	service.rcvTrade(tr);
		    	//}
		} catch (Exception ex){
			System.out.println("error while processing: "+message);
			ex.printStackTrace();
		}
	}	
	
	public List getSnapShotForce(int seqno){
		Query query = db.getDb().getInstance().query();
		query.constrain(FeedMsg.class);
		query.descend("seqno").orderAscending().constrain(new Long(seqno)).greater();
		List o = query.execute();
		return o;
	}
	
}
