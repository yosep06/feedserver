package feed.gateway.consumer;

import java.util.HashMap;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import feed.builder.msg.Queue;
import feed.builder.msg.Quote;
import feed.gateway.broadcaster.GtwService;

public class QueueProcess extends Thread {

	private GtwService service;
	private boolean setstop = false;
	private Vector<String> messages = new Vector<String>();
	private Log log = LogFactory.getLog(getClass());

	public QueueProcess(GtwService service) {
		this.service = service;
	}

	@Override
	public void run() {
		while (!this.setstop) {
			String msg = null;
			try {
			if (messages.isEmpty()) {
				
					synchronized (messages) {
						messages.wait();
					}
			} else {
				msg = messages.remove(0);
				process(msg);
			}
			}catch (Exception e) {
				System.out.println("error Queue proses ="+ msg);
				e.printStackTrace();
				log.error("message "+msg);
				
			}
		}
	}

	private void process(String message) {
		String[] data = message.split("\\|");
		//log.info("proces queue "+data[0]+":"+data[1]+":"+data[2]+":"+data[3]);
		
		HashMap map = null;
		if (data[1].equals("10")) {
			 map = convert("10", message);

		} else if (data[1].equals("11")) {
			 map = convert("11", message);
			//service.rcvQuoteQueue1(map);
		}
		//log.info("convert finished "+map.size());
		service.rcvQuoteQueue1(map);
		
		//log.info("QueueProses finished "+data[0]+":"+data[1]+":"+data[2]+":"+data[3]);
		//log.info("QueueProses finished "+message);

	}

	public void addMessage(String msg) {
		messages.add(msg);
		synchronized (messages) {
			messages.notify();
		}
	}

	public static HashMap convert(String type, String dataBid) {

		// System.out.println("awal databid= "+ dataBid);

		String[] spb = dataBid.split("\\|");

		Quote quote = new Quote();
		quote.setType(spb[1]);
		quote.setHeader(spb[0]);
		quote.setStock(spb[2]);
		quote.setBoard(spb[3]);
		// quote.setBidPriceQueue(sp[5].s);
		// String[] sprice = sp[5].split("\\|");
		// Double iprice = new Double(sprice[0].substring(0,
		// sprice[0].indexOf("[")));
		// quote.setBidPriceQueue(feed.provider.core.Utils.strToDouble(spb[4],0));

		int idx = dataBid.indexOf("[");
		int idxEnd = dataBid.indexOf("<]");
		// System.out.println("sprice "+ sprice[5].indexOf("[") );

		String sQueue = dataBid.substring(idx + 1, idxEnd);

		// System.out.println("databid="+ sQueue + " : idx=" + idx );
		HashMap map = new HashMap();
		// String map = new String();
		String[] array = sQueue.split("!");
		if (array.length > 3) {
			for (int i = 0; i < array.length; i++) {
				// System.out.println("array i "+i +"="+ array[i]);
				String[] sp = array[i].split("\\[");
				if (!sp.equals("")) {
					Double price = new Double(sp[0]);

					String[] queuee = sp[1].split(">");

					// Vector v = new Vector();
					String v = new String();
					for (int j = 0; j < queuee.length; j++) {
						String[] dQueue = queuee[j].split("\\|");
						// System.out.println("dataBid queeue="+ dQueue);
						// System.out.println("q="+ dQueue[4]);
						// Queue q = new Queue(dQueue[0], price, new
						// Double(dQueue[1]),"");

						if (dQueue[0].length()> 3) {

							Queue q = new Queue(dQueue[0], new Double(
									feed.provider.core.Utils.strToDouble(dQueue[4],
											0)), new Double(dQueue[1]), "");
							q.setMatch(new Double(dQueue[2].replace("]", "")));
						}
						
						// v.add(q);
						// System.out.println("array j"+j +"="+
						// dQueue[0]+":"+dQueue[1]+":"+dQueue[2]);
					}
					// System.out.println("CONVERT DATABID = " + price + " VE"+
					// v);
					// 10#"+ newstock +"#"+feed.admin.Utils.strToDouble(newprice, 0)+"#RG
					String key2 = type + "|" + quote.getStock() + "|" + price
							+ "#" + quote.getBoard();
					// String key2 =
					// type+"#"+quote.getStock()+"#"+price+"#"+quote.getBoard();

					// System.out.println(key2);
					// log.info("QuoteConsumer on Convert="+ key2);
					// map.put(price, array[i]);

					map.put(key2, key2 + "|" + array[i]);
				}

			}
		}
		return map;
	}

}
