package feed.gateway.consumer;

import feed.gateway.core.FileConfig;

public class FeedConsumer {
	private TradeConsumer tradeConsumer;
	private QuoteConsumer quoteConsumer;
	private OtherConsumer otherConsumer;
	protected FileConfig config;
	//private QueueConsumer queueConsumer;
	
	public FeedConsumer() throws Exception{
		config = new FileConfig("feed.gateway.config");

	}
	
	public FeedConsumer(boolean usingRMI) throws Exception{
		config = new FileConfig("feed.gateway.config");
		tradeConsumer = new TradeConsumer(config.getProperty("buildertrade"));		
		quoteConsumer = new QuoteConsumer(config.getProperty("builderquote"));		
		otherConsumer = new OtherConsumer(config.getProperty("builderother"));
		//queueConsumer = new QueueConsumer(config.getProperty("builderqueue"));
	}
	
	public TradeConsumer getTradeConsumer(){
		return tradeConsumer;
	}
	
	public QuoteConsumer getQuoteConsumer(){
		return quoteConsumer;
	}
	
	public OtherConsumer getOtherConsumer(){
		return otherConsumer;
	}
	
	/*public QueueConsumer getQueueConsumer(){
		return queueConsumer;
	}*/
	
	public void start() throws Exception{
		tradeConsumer.connect();
		quoteConsumer.connect();
		otherConsumer.connect();
		//queueConsumer.connect();
	}
		
	public void stop(){
		try {
			tradeConsumer.exit();
			quoteConsumer.exit();
			otherConsumer.exit();
			//queueConsumer.exit();
		} catch (Exception ex ){}
	}
}
