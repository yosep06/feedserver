package feed.gateway.consumer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Writer;
import java.rmi.RemoteException;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
//import feed.builder.msg.NewsStock;






import javax.swing.Timer;

import org.apache.commons.logging.LogFactory;

import quickfix.field.CollAction;

import com.db4o.query.Query;
import com.eqtrade.database.AccessQueryLocator;
import com.eqtrade.database.FOData;
import com.eqtrade.database.QueryDataFeed.SQUERY;

import feed.builder.msg.Article;
import feed.builder.msg.Broker;
import feed.builder.msg.BrokerMobile;
import feed.builder.msg.BrokerSummarry;
import feed.builder.msg.ChartIntraday;
import feed.builder.msg.Commodity;
import feed.builder.msg.CorpAction;
import feed.builder.msg.Currency;
import feed.builder.msg.CurrencyMobile;
import feed.builder.msg.Future;
import feed.builder.msg.GlobalIndices;
import feed.builder.msg.GlobalIndicesMobile;
import feed.builder.msg.IPO;
import feed.builder.msg.Indices;
import feed.builder.msg.MarketMobile;
import feed.builder.msg.Message;
import feed.builder.msg.News;
import feed.builder.msg.Rups;
import feed.builder.msg.Stock;
import feed.builder.msg.StockMobile;
import feed.builder.msg.StockSummary;
import feed.builder.msg.StockSummaryMobile;
import feed.builder.msg.TopGainer;
import feed.builder.msg.Trade;
import feed.builder.msg.TradeMobile;
import feed.builder.msg.TradePrice;
import feed.builder.msg.TradeSummBroker;
import feed.builder.msg.TradeSummBrokerStock;
import feed.builder.msg.TradeSummBrokerStockMobile;
import feed.builder.msg.TradeSummInv;
import feed.builder.msg.TradeSummStockBroker;
import feed.builder.msg.TradeSummStockBrokerMobile;
import feed.builder.msg.TradeSummStockInv;
import feed.builder.msg.TradeSummStockInvMobile;
import feed.builder.msg.TradeSummStockMarketInv;
import feed.builder.msg.TradeSummary;
import feed.gateway.broadcaster.GtwService;
import feed.gateway.core.FileConfig;
import feed.gateway.core.MsgConsumer;
import feed.builder.msg.StockNews;
import feed.builder.msg.StockComparisonBySector;
import feed.builder.msg.Sector;

public class OtherConsumer extends MsgConsumer {
	private HashMap indicesMap; // key index
	private HashMap brokerMap; // key brokercode
	private HashMap actionMap; // key actiontype+stock
	private HashMap newsMap; // key no
	private HashMap stockMap; // key stockcode
	private HashMap stockMobileMap; 
	private HashMap ssMap; // key stock+board
	private HashMap tradepriceMap; // key stock+board
	private HashMap tsMap; // key stock+board+broker+inv
	private HashMap brokerMobileMap;
	
	//#Valdhy 20141219
	//--- Mobile Section - Start ---
	private HashMap ssmMap; //key stock+board
	private HashMap mmMap; //key stock+board
	//--- Mobile Section - End ---
	private HashMap tgMap;

	private HashMap tsBroker;
	private HashMap tsBrokerStock;
	private HashMap tsBrokerStockMobile;
	private HashMap tsStockBroker;
	private HashMap tsStockBrokerMobile;
	private HashMap tsStockInv;
	private HashMap tsInv;
	private HashMap tsBrokerSumm;
	private HashMap tsStockMarketInv;
	private HashMap tsStockInvMobile;
	
	// from database relational
	private HashMap articleMap; // key id+date
	private HashMap articleMobileMap;
	private HashMap currencyMap; // key code
	private HashMap currencyMobileMap; // key code
	private HashMap globalIndicesMap; // key code
	private HashMap globalIndicesMobileMap; // key code
	private HashMap rupsMap; // key stock+date
	private HashMap ipoMap; // key name;
	private HashMap commodityMap;
	private HashMap futureMap;
	private HashMap stockNewsMap;
	private HashMap stockNewsStockDetailMap;
	private HashMap comparisonBySectorMap; // StockComparisonBysSector //Key id
	private HashMap SectorMap;

	private HashMap chartIntraday;
	private Connection connArticle = null;
	private Connection connArticleMobile = null;
	private Connection connChartComparison = null;
	private Connection connChartDaily = null;
	private Connection connChartDailyMobile = null;
	private Connection connCurrency = null;
	private Connection connIndices = null;
	private Connection connTradeHistory = null;
	private Connection connCorpAction = null;
	private Connection connCommodity = null;
	private Connection connFuture = null;
	private Connection connRups = null;
	private Connection connIPO = null;
	private Connection connStocknews = null; // StockNews
	private Connection connBySector = null; // StockComparisonBySector
	private Connection connSector = null;
	private Connection connStockSumm = null;
	private Connection connTradeSumm = null;
	private Connection connCompProfile = null;
	private Connection connYearPeriod = null;
	private Connection connLastPrice = null;

	private AccessQueryLocator accessData;
	private Timer timer = null;
	private timerdatabase tm = new timerdatabase();
	private FileConfig config;

	private HashMap tradeHistory;
	private HashMap tradeHistoryMobile;

	private static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	private boolean terminated = false;
	private int bBufLen = 4 * 8192;
	private int counterArticle = 0;

	public void clearDbConnection() {
		connArticle = null;
		connArticleMobile = null;
		connChartComparison = null;
		connChartDaily = null;
		connChartDailyMobile = null;
		connCurrency = null;
		connIndices = null;
		connTradeHistory = null;
		connCorpAction = null;
		connRups = null;
		connIPO = null;
		connCompProfile = null;
		connCommodity = null;
		connFuture = null;
		connStocknews = null;// StockNews
		connBySector = null;// StockComparisonBySector
		connSector = null;
	}

	public void setAccessData(AccessQueryLocator accessData) {
		this.accessData = accessData;
	}

	public void setService(GtwService service) {
		super.setService(service);
		articleMap = new HashMap(500, 5); // key id+date
		articleMobileMap = new HashMap(500, 5);
		currencyMap = new HashMap(500, 5); // key code
		currencyMobileMap = new HashMap(500,5); // key code
		globalIndicesMap = new HashMap(500, 5); // key code
		globalIndicesMobileMap= new HashMap(500, 5); // key code
		rupsMap = new HashMap(500, 5);
		ipoMap = new HashMap(500, 5);
		commodityMap = new HashMap(500, 5);
		futureMap = new HashMap(500, 5);
		stockNewsMap = new HashMap(500, 5);
		stockNewsStockDetailMap = new HashMap(500,5);
		comparisonBySectorMap = new HashMap(500, 5);
		SectorMap = new HashMap(500, 5);
		chartIntraday = new HashMap(500,5);
		
		System.out.println("masuk setService");

		timer = new Timer(1000 * 60 * 1, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectSector();
				selectCurrency();
				selectIndices();
				selectArticle();
				selectCommodity();
				selectFuture();
				selectCorpAction();
				selectIPO();
				selectRups();
				selectStockNews();
				selectBySector();
			}
		});
		timer.setInitialDelay(1000 * 60 * 2);
		timer.stop();
		try {
			config = new FileConfig("feed.gateway.config");
		} catch (Exception ex) {
			System.err.println(ex);
		}
	}

	private ResultSet recSN = null;
	private Statement stSN = null;
	private Clob clobSN;
	private long interval = 0;
	private SimpleDateFormat dt = new SimpleDateFormat("HHmmss");
	
	private void selectStockNews() {
		System.out.println("refresh stocknews");
		try {
			if (!service.ping()) connStocknews = null;
			if (connStocknews == null) connStocknews = service.getDatabase().getConnection();
			if (connStocknews.isClosed()) connStocknews = service.getDatabase().getConnection();
			stSN = connStocknews.createStatement();
//			recSN = stSN.executeQuery("select newsid,news_date,news_stock,news_title,content  from feed_stocknews order by newsid");
			recSN = stSN.executeQuery("select * from FEED_VNEWS");
									
			while (recSN.next()) {
				String key = recSN.getString("newsid");
				StockNews old = (StockNews) stockNewsMap.get(key);
				if (old == null) {
					StockNews ns = new StockNews();
					StockNews snsd = new StockNews();
					ns.setHeader("IDX");
					ns.setType("SN");
					ns.setId(recSN.getInt("newsid") + "");
					ns.setDate(recSN.getString("news_date"));
					ns.setStock(recSN.getString("news_stock"));
					ns.setTitle(recSN.getString("news_title"));
					clobSN = recSN.getClob("content");
					long length = clobSN.length();
					String fileData = clobSN.getSubString(1, (int) length);
					fileData = fileData.replace("\n", "<br/>");
					fileData = fileData.replace("\r\n", "<br/>");
					ns.setContent(fileData);
					stockNewsMap.put(recSN.getString("newsid"), ns);
					service.rcvOther(ns);
					
					snsd.setHeader("IDX");
					snsd.setType("SNSD");
					snsd.setId(recSN.getInt("newsid") + "");
					snsd.setDate(recSN.getString("news_date"));
					snsd.setStock(recSN.getString("news_stock"));
					snsd.setTitle(recSN.getString("news_title"));
					clobSN = recSN.getClob("content");
					long lengthsnsd = clobSN.length();
					String fileDatasnsd = clobSN.getSubString(1, (int) lengthsnsd);
					fileDatasnsd = fileDatasnsd.replace("\n", "<br/>");
					fileDatasnsd = fileDatasnsd.replace("\r\n", "<br/>");
					snsd.setContent(fileDatasnsd);
					stockNewsStockDetailMap.put(recSN.getString("newsid"), snsd);
					service.rcvOther(snsd);

				}
			}		
		} catch (Exception e) {
			e.printStackTrace();
			try {
				connStocknews.close();
			} catch (Exception ex) {
				System.err.println(ex);
			}
		} finally {
			try {
				recSN.close();
			} catch (Exception ex) {
				System.err.println(ex);
			}
			try {
				stSN.close();
			} catch (Exception ex) {
				System.err.println(ex);
			}
		}
	}

	private void selectStockNewsStockDetail() {
		System.out.println("refresh selectStockNewsStockDetail");
		
		try {
			boolean isNull = recSN.getRow() > 0 ? true : false;
			System.out.println("isNull : "+isNull);
			if (!service.ping()) connStocknews = null;
			if (connStocknews == null) connStocknews = service.getDatabase().getConnection();
			if (connStocknews.isClosed()) connStocknews = service.getDatabase().getConnection();
			//stSN = connStocknews.createStatement();
			//recSN = stSN.executeQuery("select newsid,news_date,news_stock,news_title,content  from feed_stocknews order by newsid");
									
			while (recSN.next()) {
				String key = recSN.getString("newsid");
				StockNews old = (StockNews) stockNewsStockDetailMap.get(key);
				if (old == null) {
					StockNews ns = new StockNews();
					ns.setHeader("IDX");
					ns.setType("SNSD");
					ns.setId(recSN.getInt("newsid") + "");
					ns.setDate(recSN.getString("news_date"));
					ns.setStock(recSN.getString("news_stock"));
					ns.setTitle(recSN.getString("news_title"));
					clobSN = recSN.getClob("content");
					long length = clobSN.length();
					String fileData = clobSN.getSubString(1, (int) length);
					fileData = fileData.replace("\n", "<br/>");
					fileData = fileData.replace("\r\n", "<br/>");
					ns.setContent(fileData);
					stockNewsStockDetailMap.put(recSN.getString("newsid"), ns);
					service.rcvOther(ns);
				}
			}		
		} catch (Exception e) {
			e.printStackTrace();
			try {
				connStocknews.close();
			} catch (Exception ex) {
				System.err.println(ex);
			}
		} finally {
			try {
				recSN.close();
			} catch (Exception ex) {
				System.err.println(ex);
			}
			try {
				stSN.close();
			} catch (Exception ex) {
				System.err.println(ex);
			}
		}
	}
	
	public String getBrokerInfo() {
		return config.getProperty("brokerinfo");
	}

	public long readFromClob(Clob clob, Writer out) throws SQLException,
			IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(
				clob.getAsciiStream()));
		int length = -1;
		long read = 0;
		char[] buf = new char[bBufLen];
		while ((length = in.read(buf, 0, bBufLen)) != -1) {
			out.write(buf, 0, length);
			read += length;
		}
		in.close();
		return read;
	}

	public byte[] readFile(String param) {
		try {
			ResultSet rec = null;
			Statement st = null;
			Clob clob;
			File file = null;
			FileOutputStream outStream = null;
			InputStream inStream = null;
			String[] p = param.split("\\|");

			if (!service.ping())
				connIndices = null;
			if (connIndices == null)
				connIndices = service.getDatabase().getConnection();
			if (connIndices.isClosed())
				connIndices = service.getDatabase().getConnection();

			st = connIndices.createStatement();

			rec = st.executeQuery("select name, last, close,high,low, last-close as chg,TO_CHAR((last-close)/close*100,'99D99') as persen,open,urutan from feed_indices_imq order by urutan asc");

			rec = st.executeQuery("select code, content from comp_profile_imq where code='"
					+ p[1] + "'");
			File blobFile = new File(p[1]);

			while (rec.next()) {
				clob = rec.getClob("content");
				long length = clob.length();
				String fileData = clob.getSubString(1, (int) length);

				file = new File(fileData);
				outStream = new FileOutputStream(blobFile);
				inStream = clob.getAsciiStream();
			}
			BufferedInputStream bin = new BufferedInputStream(inStream);
			byte[] contents = new byte[4096];
			int bytesRead = 0;
			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			while ((bytesRead = bin.read(contents)) != -1) {
				bout.write(contents, 0, bytesRead);
			}
			bin.close();
			return bout.toByteArray();
		} catch (Exception ex) {

			ex.printStackTrace();
			// log.error("error "+ex.getMessage());
			ByteArrayOutputStream gagal = new ByteArrayOutputStream();
			gagal.write(0);
			return gagal.toByteArray();
			// return "AAAA";
		}
	}

	public String selectHistory(String param) {
		String[] p = param.split("\\|");

		 log.info("Select History Param : "+p[0]);

		if (p[0].equals("TSB")) {
			return selectTradeHistory(p[0], "%", p[1], p[2], p[3], p[4], p[5]);
		} else if (p[0].equals("TSS")) {
			return selectTradeHistory(p[0], p[1], "%", p[2], p[3], p[4], p[5]);
		} else if (p[0].equals("CHD")) {
			return selectChartDaily(p[1], p[2], p[3]);
		} else if (p[0].equals("CHI")) {
			return selectChartIntraday(p[1]);
		} else if (p[0].equals("CHC")) {
			return selectChartComparison(p[1], p[2], p[3]);
		} else if (p[0].equals("THSI")) {
			return selectTradeSummary(p[1], p[2]);
		} else if (p[0].equals("THSS")) {
			return selectStockSummary(p[1], p[2]);
		} else if (p[0].equals("PH")) {
			return selectPriceHistoryStockDetail(p[1], p[2], p[3], p[4]); // PH = PRICE HISTORY
		} else if (p[0].equals("SSD")) {
			return selectStatisticStockDetail(p[1]); // SSD = STATISTIC STOCK DETAIL
		} else if (p[0].equals("GAMI")) {
			return selectavailablemarketinfo(p[1]); // GAMI = GET AVAILABLE MARKET INFO
		} else if (p[0].equals("GYPMI")) {
			return selectyearperiodmarketinfo(); // GYPMI = GET YEAR PERIOD MARKET INFO
		} else if (p[0].equals("GDMIBD")) {
			return selectmarketinfobydate(p[1]); // GDMIBD = GET DOWNLOAD MARKET INFO BY DATE
		} else if (p[0].equals("MTH")) {
			return getTHSnapShotHistory(p[1], Integer.parseInt(p[2])); // GDMIBD = GET DOWNLOAD MARKET INFO BY DATE
		}else if (p[0].equals("MTHM")) {
			return getTHSnapShotHistoryMobile(p[1], Integer.parseInt(p[2])); // GDMIBD = GET DOWNLOAD MARKET INFO BY DATE
		}else if (p[0].equals("FDM")) { // foreign domestic market
			return getFDMSnapshotMobile(p[1], p[2]); // FDM =(foreign/domestic,sellbuy)
		}else if (p[0].equals("MLOT")) {// setting lot mobile
			System.out.println(config.getProperty("lot")+" 00 ");
			return config.getProperty("lot");
		}else if (p[0].equals("NWM")) {
			HashMap mp = getNWMSnapShot();
//			for (; i.hasNext();) {
			StringBuffer sb = new StringBuffer("");
			Iterator i = mp.values().iterator();
			for (int ca =0 ; ca<20 ; ca++) {
				Article art = (Article) i.next();
				sb.append(art.toString()+"\n");
			}
			return null;
		} else {
			return null;
		}
	}

	private String selectTradeSummary(String from, String to) {
		String query = "select sum(BUYAVG) AS BUYAVG,sum(SELLAVG) AS SELLAVG, sum(BUYVOL) AS BUYVOL ,sum(BUYVAL) AS BUYVAL ,sum(BUYFREQ) AS BUYFREQ, "
				+ "investor ,sum(SELLVOL) AS SELLVOL ,sum(SELLVAL)AS SELLVAL ,sum(SELLFREQ) AS SELLFREQ , stock from FEED_TRADESUMMARY_HIS "
				+ "where transdate between '%s' and '%s' group by investor,stock";
		ResultSet rec = null;
		Statement st = null;
		StringBuffer sb = new StringBuffer("");
		try {
			if (!service.ping())
				connTradeSumm = null;
			if (connTradeSumm == null)
				connTradeSumm = service.getDatabase().getConnection();
			if (connTradeSumm.isClosed())
				connTradeSumm = service.getDatabase().getConnection();

			synchronized (connTradeSumm) {

				st = connTradeSumm.createStatement();
				rec = st.executeQuery(String.format(query, new Object[] { from,
						to }));

				while (rec.next()) {
					TradeSummary ts = new TradeSummary();
					ts.setType("THSI");
					ts.setHeader("IDX");
					ts.setStock(rec.getString("stock"));
					ts.setBroker("ALL");
					ts.setBoard("ALL");
					ts.setInvestor(rec.getString("investor"));
					ts.setBuyvol(rec.getDouble("BUYVOL"));
					ts.setBuyval(rec.getDouble("BUYVAL"));
					ts.setBuyfreq(rec.getDouble("BUYFREQ"));
					ts.setSellvol(rec.getDouble("SELLVOL"));
					ts.setSellval(rec.getDouble("SELLVAL"));
					ts.setSellfreq(rec.getDouble("SELLFREQ"));
					ts.setBuyavg(ts.getBuyavg() / ts.getBuyvol());
					ts.setSellavg(ts.getSellval() / ts.getSellvol());
					sb.append(ts.toString() + "\n");
				}
			}
		} catch (Exception sqle) {
			sqle.printStackTrace();
			try {
				connChartComparison.close();
			} catch (Exception ex) {
			}
		} finally {
			try {
				rec.close();
			} catch (Exception ex) {
			}
			try {
				st.close();
			} catch (Exception ex) {
			}
		}

		return sb.length() == 0 ? null : sb.toString();
	}

	private String selectStockSummary(String from, String to) {
		String query = "SELECT SUM(TRADEVOL) AS TRADEVOL,SUM(TRADEVAL) AS TRADEVAL,SUM(TRADEFREQ) AS TRADEFREQ, STOCK, BOARD from FEED_STOCKSUMMARY_INV where TRANSDATE between  '%s' and '%s' group by STOCK,BOARD";
		ResultSet rec = null;
		Statement st = null;
		StringBuffer sb = new StringBuffer("");
		try {
			if (!service.ping())
				connStockSumm = null;
			if (connStockSumm == null)
				connStockSumm = service.getDatabase().getConnection();
			if (connStockSumm.isClosed())
				connStockSumm = service.getDatabase().getConnection();

			synchronized (connStockSumm) {

				st = connStockSumm.createStatement();
				rec = st.executeQuery(String.format(query, new Object[] { from,
						to }));

				while (rec.next()) {
					StockSummary ts = new StockSummary();
					ts.setType("THSS");
					ts.setHeader("IDX");
					ts.setStock(rec.getString("stock"));
					ts.setBoard(rec.getString("board"));
					ts.setTradeval(rec.getDouble("TRADEVAL"));
					ts.setTradevol(rec.getDouble("TRADEVOL"));
					ts.setTradefreq(rec.getDouble("TRADEFREQ"));
					sb.append(ts.toString() + "\n");
				}
			}

		} catch (Exception sqle) {
			sqle.printStackTrace();
			try {
				connChartComparison.close();
			} catch (Exception ex) {
			}
		} finally {
			try {
				rec.close();
			} catch (Exception ex) {
			}
			try {
				st.close();
			} catch (Exception ex) {
			}
		}

		return sb.length() == 0 ? null : sb.toString();
	}

	private String selectChartComparison(String code, String from, String to) {
		String max = "SELECT A.last as last FROM feed_vchartdaily A,  (SELECT MAX(TRANSDATE) AS TRANSDATE FROM feed_vchartdaily "
				+ "WHERE CODE = '%s' AND TRANSDATE <= '%s')B WHERE CODE = '%s' AND A.TRANSDATE = B.TRANSDATE";
		String query = "select transdate, code, (last-%f)/last*100 as percent from feed_vchartdaily where code='%s' and transdate>='%s' and transdate<='%s'  ";
		String min = "SELECT A.last as last FROM feed_vchartdaily A,  (SELECT MIN(TRANSDATE) AS TRANSDATE FROM feed_vchartdaily "
				+ "WHERE CODE = '%s' AND TRANSDATE >= '%s' and TRANSDATE<='%s')B WHERE CODE = '%s' AND A.TRANSDATE = B.TRANSDATE";
		double start = 0;
		ResultSet rec = null;
		Statement st = null;
		StringBuffer sb = new StringBuffer("");
		try {
			if (!service.ping())
				connChartComparison = null;
			if (connChartComparison == null)
				connChartComparison = service.getDatabase().getConnection();
			if (connChartComparison.isClosed())
				connChartComparison = service.getDatabase().getConnection();

			synchronized (connChartComparison) {

				st = connChartComparison.createStatement();
				rec = st.executeQuery(String.format(max, new Object[] { code,
						from, code }));
				while (rec.next()) {
					start = rec.getDouble("last");
				}
				if (start == 0) {
					rec = st.executeQuery(String.format(min, new Object[] {
							code, from, to, code }));
					while (rec.next()) {
						start = rec.getDouble("last");
					}
				}
				rec = st.executeQuery(String.format(query, new Object[] {
						new Double(start), code, from, to }));
				while (rec.next()) {
					sb.append(rec.getString("transdate")).append("|");
					sb.append(rec.getString("code")).append("|");
					sb.append(rec.getDouble("percent")).append("\n");
				}
			}
		} catch (Exception sqle) {
			sqle.printStackTrace();
			try {
				connChartComparison.close();
			} catch (Exception ex) {
			}
		} finally {
			try {
				rec.close();
			} catch (Exception ex) {
			}
			try {
				st.close();
			} catch (Exception ex) {
			}
		}
		return sb.length() == 0 ? null : sb.toString();
	}

	private String selectChartDaily(String code, String from, String to) {
		String query = "select transdate, code, open, high, low, last, vol from feed_vchartdaily where code='%s' and transdate>='%s' and transdate<='%s'  order by transdate desc  ";
		ResultSet rec = null;
		Statement st = null;
		StringBuffer sb = new StringBuffer("");
		try {
			if (!service.ping())
				connChartDaily = null;
			if (connChartDaily == null)
				connChartDaily = service.getDatabase().getConnection();
			if (connChartDaily.isClosed())
				connChartDaily = service.getDatabase().getConnection();

			synchronized (connChartDaily) {

				st = connChartDaily.createStatement();
				rec = st.executeQuery(String.format(query, new Object[] { code,
						from, to }));
				while (rec.next()) {
					sb.append(rec.getString("transdate")).append("|");
					sb.append(rec.getString("code")).append("|");
					sb.append(rec.getDouble("open")).append("|");
					sb.append(rec.getDouble("high")).append("|");
					sb.append(rec.getDouble("low")).append("|");
					sb.append(rec.getDouble("last")).append("|");
					sb.append(rec.getDouble("vol")).append("\n");
				}
			}
		} catch (Exception sqle) {
			sqle.printStackTrace();
			try {
				connChartDaily.close();
			} catch (Exception ex) {
			}
		} finally {
			try {
				rec.close();
			} catch (Exception ex) {
			}
			try {
				st.close();
			} catch (Exception ex) {
			}
		}
		return sb.length() == 0 ? null : sb.toString();
	}

	private String selectChartIntraday2(String code) {
		StringBuffer sb = new StringBuffer("");
		if (indicesMap.get(code) != null) {
			Query query = db.getDb().getInstance().query();
			query.constrain(Indices.class);
			query.descend("code").constrain(code)
					.and(query.descend("type").constrain("6H"));
			query.descend("seqno").orderAscending();
			List o = query.execute();
			if (o != null) {
				for (int i = 0; i < o.size(); i++) {
					Indices idx = (Indices) o.get(i);
					sb.append(idx.getTranstime()).append("|")
							.append(idx.getCode()).append("|")
							.append(idx.getIndex()).append("\n");
				}
			}
		} else {
			Query query = db.getDb().getInstance().query();
			query.constrain(Trade.class);
			query.descend("stock").constrain(code)
					.and(query.descend("type").constrain("TH"))
					.and(query.descend("board").constrain("RG"));
			query.descend("seqno").orderAscending();
			List o = query.execute();
			if (o != null) {
				for (int i = 0; i < o.size(); i++) {
					Trade th = (Trade) o.get(i);
					sb.append(th.getTradetime()).append("|")
							.append(th.getStock()).append("|")
							.append(th.getPrice()).append("\n");
				}
			}
		}
		return sb.length() == 0 ? null : sb.toString();
	}

	private String selectChartIntraday(String code) {
		String query = "select transtime, code, last from feed_vchartintraday where code='%s' and transdate='%s' ";
		ResultSet rec = null;
		Statement st = null;
		StringBuffer sb = new StringBuffer("");
		try {
			if (!service.ping())
				connChartDaily = null;
			if (connChartDaily == null)
				connChartDaily = service.getDatabase().getConnection();
			if (connChartDaily.isClosed())
				connChartDaily = service.getDatabase().getConnection();

			synchronized (connChartDaily) {

				st = connChartDaily.createStatement();
				rec = st.executeQuery(String.format(query, new Object[] { code,
						format.format(new Date()) }));
				while (rec.next()) {
					sb.append(rec.getString("transtime")).append("|");
					sb.append(rec.getString("code")).append("|");
					sb.append(rec.getDouble("last")).append("\n");
				}
			}
		} catch (Exception sqle) {
			sqle.printStackTrace();
			try {
				connChartDaily.close();
			} catch (Exception ex) {
			}
		} finally {
			try {
				rec.close();
			} catch (Exception ex) {
			}
			try {
				st.close();
			} catch (Exception ex) {
			}
		}
		return sb.length() == 0 ? null : sb.toString();
	}
	
	public ArrayList selectChartIntradayMobile(String code) {
		ArrayList data = new ArrayList();
		String query = "select transtime, code, last from feed_vchartintraday where code='%s' and transdate='%s' and substr(transtime,5,2) = '01' ";
		ResultSet rec = null;
		Statement st = null;
		StringBuffer sb = new StringBuffer("");
		try {
			if (!service.ping())
				connChartDailyMobile = null;
			if (connChartDailyMobile == null)
				connChartDailyMobile = service.getDatabase().getConnection();
			if (connChartDailyMobile.isClosed())
				connChartDailyMobile = service.getDatabase().getConnection();

			synchronized (connChartDailyMobile) {

				st = connChartDailyMobile.createStatement();
				rec = st.executeQuery(String.format(query, new Object[] { "COMPOSITE", format.format(new Date()) }));
				while (rec.next()) {
					ChartIntraday ci = new ChartIntraday();
					ci.setHeader("IDX");
					ci.setType("MHI");					
					ci.setTranstime(rec.getString("transtime"));
					ci.setCode(rec.getString("code"));
					ci.setLast(rec.getDouble("last"));
					chartIntraday.put(rec.getString("code")+rec.getString("transtime"),data);
//					service.rcvOther(ci);
					data.add(ci);
				}
			}
			
			//System.out.println("size : "+data.size());
			
			//System.out.println("data 1 : "+data.get(1));
			
		} catch (Exception sqle) {
			sqle.printStackTrace();
			try {
				connChartDailyMobile.close();
			} catch (Exception ex) {ex.printStackTrace();
			}
		} finally {
			try {
				rec.close();
			} catch (Exception ex) {ex.printStackTrace();
			}
			try {
				st.close();
			} catch (Exception ex) {ex.printStackTrace();
			}
		}
		
		//return sb.length() == 0 ? null : sb.toString();
		return data;
	}
	
	private String selectTradeHistory(String type, String stock, String broker, String from, String to, String inv, String board) {

		String sStock = "select Stock, 'ALL' as broker, 'ALL' as board, 'ALL' as inv, sum(BUYVOL) as buyvol, sum(BUYVAL) as buyval, sum(BUYFREQ) as buyfreq, sum(SELLVOL) as sellvol, "
				+ " sum(SELLVAL) as sellval, sum(SELLFREQ) as sellfreq from feed_tradesummary "
				+ " where transdate >= '"
				+ from
				+ "' and transdate<='"
				+ to
				+ "' "
				+ " and broker = '"
				+ broker
				+ "' and board like '"
				+ board
				+ "' and investor like '"
				+ inv
				+ "' "
				+ " group by stock";
		String sBroker = "select Broker, 'ALL' as stock, 'ALL' as board, 'ALL' as inv, sum(BUYVOL) as buyvol, sum(BUYVAL) as buyval, sum(BUYFREQ) as buyfreq, sum(SELLVOL) as sellvol, "
				+ " sum(SELLVAL) as sellval, sum(SELLFREQ) as sellfreq from feed_tradesummary "
				+ " where transdate >= '"
				+ from
				+ "' and transdate<='"
				+ to
				+ "' "
				+ " and stock = '"
				+ stock
				+ "' and investor like '"
				+ inv + "' " + " group by broker";
		String query = type.equals("TSS") ? sBroker : sStock;
		ResultSet rec = null;
		Statement st = null;
		StringBuffer sb = new StringBuffer("");
		try {
			if (!service.ping())
				connTradeHistory = null;
			if (connTradeHistory == null)
				connTradeHistory = service.getDatabase().getConnection();
			if (connTradeHistory.isClosed())
				connTradeHistory = service.getDatabase().getConnection();

			synchronized (connTradeHistory) {

				st = connTradeHistory.createStatement();
				rec = st.executeQuery(query);
				while (rec.next()) {
					TradeSummary tr = new TradeSummary();
					tr.setHeader("IDX");
					tr.setType("TS");
					tr.setStock(rec.getString("stock"));
					tr.setBroker(rec.getString("broker"));
					tr.setBoard(rec.getString("board"));
					tr.setInvestor(rec.getString("inv"));
					tr.setBuyvol(rec.getDouble("buyvol"));
					tr.setBuyval(rec.getDouble("buyval"));
					tr.setBuyfreq(rec.getDouble("buyfreq"));
					tr.setSellvol(rec.getDouble("sellvol"));
					tr.setSellval(rec.getDouble("sellval"));
					tr.setSellfreq(rec.getDouble("sellfreq"));
					tr.setBuyavg(tr.getBuyval() / tr.getBuyvol());
					tr.setSellavg(tr.getSellval() / tr.getSellvol());
					sb.append(tr.toString() + "\n");
				}
			}
		} catch (Exception sqle) {
			sqle.printStackTrace();
			try {
				connTradeHistory.close();
			} catch (Exception ex) {
			}
		} finally {
			try {
				rec.close();
			} catch (Exception ex) {
			}
			try {
				st.close();
			} catch (Exception ex) {
			}
		}
		return sb.length() == 0 ? null : sb.toString();
		/*GregorianCalendar gc = new GregorianCalendar();
	    java.util.Date d = new Date();
	    String to2 = to;
		try {
			if (to.equals(format.format(new Date()))) {
				d = format.parse(to);
				 gc.setTime(d);
				 int dayBefore = gc.get(Calendar.DAY_OF_YEAR);
				 gc.roll(Calendar.DAY_OF_YEAR, -1);
				 int dayAfter = gc.get(Calendar.DAY_OF_YEAR);
				 if(dayAfter > dayBefore) {
				      gc.roll(Calendar.YEAR, -1);
				 }
				 gc.get(Calendar.DATE);
				 d = gc.getTime();
				 to2 = format.format(d);
			} else {
				
			}
			 
		} catch (ParseException e) {
			//  Auto-generated catch block
			e.printStackTrace();
		}
	   
	String sStock = "select Stock, 'ALL' as broker, 'ALL' as board, 'ALL' as inv, sum(BUYVOL) as buyvol, sum(BUYVAL) as buyval, sum(BUYFREQ) as buyfreq, sum(SELLVOL) as sellvol, "
			+ " sum(SELLVAL) as sellval, sum(SELLFREQ) as sellfreq from feed_tradesummary "
			+ " where transdate >= '"
			+ from
			+ "' and transdate<='"
			+ to2
			+ "' "
			+ " and broker = '"
			+ broker
			+ "' and board like '"
			+ board
			+ "' and investor like '"
			+ inv
			+ "' "
			+ " group by stock";
	String sBroker = "select Broker, 'ALL' as stock, 'ALL' as board, 'ALL' as inv, sum(BUYVOL) as buyvol, sum(BUYVAL) as buyval, sum(BUYFREQ) as buyfreq, sum(SELLVOL) as sellvol, "
			+ " sum(SELLVAL) as sellval, sum(SELLFREQ) as sellfreq from feed_tradesummary "
			+ " where transdate >= '"
			+ from
			+ "' and transdate<='"
			+ to2
			+ "' "
			+ " and stock = '"
			+ stock
			+ "' and investor like '"
			+ inv + "' " + " group by broker";
	String query = type.equals("TSS") ? sBroker : sStock;
	ResultSet rec = null;
	Statement st = null;
	StringBuffer sb = new StringBuffer("");
	try {

		if (from.equals(format.format(new Date()) )  && to.equals(format.format(new Date()) ) ) {
			//hari ini
			if(type.equals("TSS")){
				Iterator i = getTSSBSnapShot(stock,0).iterator();
				while(i.hasNext()){
					TradeSummary tr = (TradeSummary) i.next();
					tr.setType("TS");
						if (tr.getStock().equals(stock)) {
							sb.append(tr.toString()+"\n");
						}
					}
			}else{
				Iterator i = getTSBSSnapShot(broker,0).iterator();
				while(i.hasNext()){
					TradeSummary tr = (TradeSummary) i.next();
					tr.setType("TS");
						if (tr.getStock().equals(stock)) {
							sb.append(tr.toString()+"\n");
						}
					}
			}
		} else if(formatDate.parse(to).equals(new Date())){
			// kemarin sampai hari ini
			if (!service.ping())
				connTradeHistory = null;
			if (connTradeHistory == null)
				connTradeHistory = service.getDatabase().getConnection();
			if (connTradeHistory.isClosed())
				connTradeHistory = service.getDatabase().getConnection();

			synchronized (connTradeHistory) {

				st = connTradeHistory.createStatement();
				rec = st.executeQuery(query);
				while (rec.next()) {
					TradeSummary tr = new TradeSummary();
					TradeSummary ts = type.equals("TSS")?
							(TradeSummary)tsStockBroker.get(rec.getString("stock")+rec.getString("broker"))
							:(TradeSummary)tsBrokerStock.get(rec.getString("broker")+rec.getString("stock"));
					
					tr.setHeader("IDX");
					tr.setType("TS");
					tr.setStock(rec.getString("stock"));
					tr.setBroker(rec.getString("broker"));
					tr.setBoard(rec.getString("board"));
					tr.setInvestor(rec.getString("inv"));
					tr.setBuyvol(rec.getDouble("buyvol")+ts.getBuyvol());
					tr.setBuyval(rec.getDouble("buyval")+ts.getBuyval());
					tr.setBuyfreq(rec.getDouble("buyfreq")+ ts.getBuyfreq());
					tr.setSellvol(rec.getDouble("sellvol")+ ts.getSellvol());
					tr.setSellval(rec.getDouble("sellval")+ ts.getSellval());
					tr.setSellfreq(rec.getDouble("sellfreq")+ ts.getSellfreq());
					tr.setBuyavg(tr.getBuyval() / tr.getBuyvol());
					tr.setSellavg(tr.getSellval() / tr.getSellvol());
					sb.append(tr.toString() + "\n");
				}
			}
		} else{
			// kemarin
			if (!service.ping())
				connTradeHistory = null;
			if (connTradeHistory == null)
				connTradeHistory = service.getDatabase().getConnection();
			if (connTradeHistory.isClosed())
				connTradeHistory = service.getDatabase().getConnection();

			synchronized (connTradeHistory) {

				st = connTradeHistory.createStatement();
				rec = st.executeQuery(query);
				while (rec.next()) {
					TradeSummary tr = new TradeSummary();
					tr.setHeader("IDX");
					tr.setType("TS");
					tr.setStock(rec.getString("stock"));
					tr.setBroker(rec.getString("broker"));
					tr.setBoard(rec.getString("board"));
					tr.setInvestor(rec.getString("inv"));
					tr.setBuyvol(rec.getDouble("buyvol"));
					tr.setBuyval(rec.getDouble("buyval"));
					tr.setBuyfreq(rec.getDouble("buyfreq"));
					tr.setSellvol(rec.getDouble("sellvol"));
					tr.setSellval(rec.getDouble("sellval"));
					tr.setSellfreq(rec.getDouble("sellfreq"));
					tr.setBuyavg(tr.getBuyval() / tr.getBuyvol());
					tr.setSellavg(tr.getSellval() / tr.getSellvol());
					sb.append(tr.toString() + "\n");
				}
			}
		}
		} catch (Exception sqle) {
			sqle.printStackTrace();
			try {
				connTradeHistory.close();
			} catch (Exception ex) {
			}
		} finally {
			try {
				rec.close();
			} catch (Exception ex) {
			}
			try {
				st.close();
			} catch (Exception ex) {
			}
		}
		return sb.length() == 0 ? null : sb.toString();*/
	}
	private String selectPriceHistoryStockDetail(String Stock, String Period, String From, String To) {
		Vector<Object> v = null;
		try {
			FOData fo = new FOData();		
			fo.set_string(Stock, Period, From, To);
			v = accessData.getQueryDataFeed().set_insert(SQUERY.pricehistory.val, fo);
		} catch (Exception e) {
		}	
		boolean isNull = v.toString().replace(",", "").replace("[", "").replace("]", "").length() == 0 ? true : false;
//		System.out.println("PH isNull : "+isNull);
		return v.toString().replace(",", "").replace("[", "").replace("]", "").length() == 0 ? null : v.toString().replace(",", "").replace("[", "").replace("]", "");
	}

	private String selectStatisticStockDetail(String Stock) {
		Vector<Object> v = null;		
		try {
			FOData fo = new FOData();
			fo.set_string(Stock);
			v = accessData.getQueryDataFeed().set_insert(SQUERY.statistic.val,fo);
		} catch (Exception e) {
			System.err.println(e);
			return null;
		}
		boolean isNull = v.toString().replace(",", "").replace("[", "").replace("]", "").length() == 0 ? true : false;
//		System.out.println("STATISTIC isNull : "+isNull);
		return v.toString().replace(",", "").replace("[", "").replace("]", "")
				.length() == 0 ? null : v.toString().replace(",", "")
				.replace("[", "").replace("]", "");
	}

	private String selectavailablemarketinfo(String transdate) {
		Vector<Object> v = null;		
		try {
			FOData fo = new FOData();
			fo.set_string(transdate);
			v = accessData.getQueryDataFeed().set_insert(
					SQUERY.getavailablemarketinfo.val, fo);
		} catch (Exception e) {
		}		
		boolean isNull = v.toString().replace(",", "").replace("[", "").replace("]", "").length() == 0 ? true : false;
//		System.out.println("AVAILABLEMARKETINFO isNull : "+isNull);
		return v.toString().replace(",", "").replace("[", "").replace("]", "")
				.length() == 0 ? null : v.toString().replace(",", "")
				.replace("[", "").replace("]", "");
	}

	private String selectyearperiodmarketinfo() {
		Vector<Object> v = null;
		try {
			v = accessData.getQueryDataFeed().set_insert(
					SQUERY.getyearperiodmarketinfo.val, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		boolean isNull = v.toString().replace(",", "").replace("[", "").replace("]", "").length() == 0 ? true : false;
		System.out.println("YEARPERIODEMARKETINFO isNull : "+isNull);
		return v.toString().replace("[", "").replace("]", "").length() == 0 ? null
				: v.toString().replace("[", "").replace("]", "");
	}

	private String selectmarketinfobydate(String transdate) {
		Vector<Object> v = null;		
		try {
			FOData fo = new FOData();
			fo.set_string(transdate);
			v = accessData.getQueryDataFeed().set_insert(
					SQUERY.getdatamarketinfobydate.val, fo);
		} catch (Exception e) {
			System.err.println(e);
			return null;
		}
		boolean isNull = v.toString().replace(",", "").replace("[", "").replace("]", "").length() == 0 ? true : false;
		return v.toString().replace(",", "").replace("[", "").replace("]", "")
				.length() == 0 ? null : v.toString().replace(" ", "")
				.replace(",", "").replace("[", "").replace("]", "");

	}
	
	public Vector selectTopGainer(String periode){
		Vector<Object> v = null;
		try {
			System.out.println(periode+" periode ");
			if (periode.equalsIgnoreCase("DAY")) {
				List i = getSnapShot(TopGainer.class,0);
				v = new Vector();
				v.addAll(i);
			} else {				
				FOData fo = new FOData();
				fo.set_string(periode);
				v = accessData.getQueryDataFeed().set_insert(SQUERY.topgainer.val, fo);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		System.out.println(v.size()+" select top gainer");
		return v;
		/*List v = null;
		try {
			if (periode.equalsIgnoreCase("DAY")) {
				v = getSnapShot(TopGainer.class,0);
//				v = new Vector();
//				v.addAll(i);
			} else {				
				FOData fo = new FOData();
				fo.set_string(periode);
				v = accessData.getQueryDataFeed().set_insert(SQUERY.topgainer.val, fo);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		System.out.println(v.size()+" select top gainer");
		return v;*/
	}
	private void selectCurrency() {
		ResultSet rec = null;
		Statement st = null;
		DateFormat dt = new SimpleDateFormat("hh:mm:ss");
		System.out.println("refresh currency");
		try {
			if (!service.ping())
				connCurrency = null;
			if (connCurrency == null)
				connCurrency = service.getDatabase().getConnection();
			if (connCurrency.isClosed())
				connCurrency = service.getDatabase().getConnection();
			st = connCurrency.createStatement();
			// rec =
			// st.executeQuery("select code, value, chg from feed_currency order by code");
//			rec = st.executeQuery("SELECT CODE,TIME,LAST,CHG,HIGH,LOW,TIPE FROM FEED_CURRENCY_IMQ order by code");
			rec = st.executeQuery("SELECT * FROM FEED_VCURRENCY_IMQ ");

			while (rec.next()) {
				Currency curr = new Currency();

				curr.setHeader("IDX");
				curr.setType("CR");
				curr.setCode(rec.getString("code"));
				curr.setValue(rec.getDouble("last"));
				curr.setChange(rec.getDouble("chg"));
				curr.setHigh(rec.getDouble("High"));
				curr.setLow(rec.getDouble("low"));
				curr.setTipe(rec.getString("tipe"));
				String temp = rec.getString("time");
				temp = "0" + temp;
				if (temp.length() == 6) {
					temp = temp.substring(0, temp.length());
				} else {
					temp = temp.substring(1, temp.length());
				}

				curr.setTime1(temp.substring(0, 2) + ":" + temp.substring(2, 4)
						+ ":" + temp.substring(4, 6));

				Currency old = (Currency) currencyMap.get(curr.getCode());
				if (old == null) {
					currencyMap.put(curr.getCode(), curr);
				} else {
					old.setValue(curr.getValue());
					old.setChange(curr.getChange());
					old.setHigh(curr.getHigh());
					old.setLow(curr.getLow());
					old.setTime1(curr.getTime1());

				}
				service.rcvOther(curr);
			}
		} catch (Exception sqle) {
			sqle.printStackTrace();
			try {
				connCurrency.close();
			} catch (Exception ex) {
			}
		} finally {
			try {
				rec.close();
			} catch (Exception ex) {
			}
			try {
				st.close();
			} catch (Exception ex) {
			}
		}
	}

	private void selectCorpAction() {
		ResultSet rec = null;
		Statement st = null;
		System.out.println("corpaction");
		try {
			if (!service.ping())
				connCorpAction = null;
			if (connCorpAction == null)
				connCorpAction = service.getDatabase().getConnection();
			if (connCorpAction.isClosed())
				connCorpAction = service.getDatabase().getConnection();
			st = connCorpAction.createStatement();
//			rec = st.executeQuery("select actiontype, stock, amount, ratio1, ratio2, cumdate, exdate, recdate, distdate,tradedate,ex_tradedate from feed_corpaction_imq ");
			rec = st.executeQuery("select * from feed_vcorpaction");
			while (rec.next()) {
				CorpAction action = new CorpAction();
				action.setHeader("IDX");
				action.setType("A");
				action.setActiontype(rec.getString("actiontype"));
				action.setStock(rec.getString("stock"));
				action.setAmount(rec.getDouble("amount"));
				action.setRatio1(rec.getString("ratio1"));
				action.setRatio2(rec.getString("ratio2"));
				action.setTransactiondate(rec.getString("tradedate"));
				action.setLasttransactiondate(rec.getString("ex_tradedate"));

				if (rec.getString("cumdate").length() > 4) {
					action.setCumdate(rec.getString("cumdate").substring(0, 4)
							+ "-" + rec.getString("cumdate").substring(4, 6)
							+ "-" + rec.getString("cumdate").substring(6, 8));
				} else {
					action.setCumdate(rec.getString("cumdate"));
				}
				if (rec.getString("exdate").length() > 4) {
					action.setExdate(rec.getString("exdate").substring(0, 4)
							+ "-" + rec.getString("exdate").substring(4, 6)
							+ "-" + rec.getString("exdate").substring(6, 8));
				} else {
					action.setExdate(rec.getString("exdate"));
				}
				if (rec.getString("recdate").length() > 4) {
					action.setRecordingdate(rec.getString("recdate").substring(
							0, 4)
							+ "-"
							+ rec.getString("recdate").substring(4, 6)
							+ "-" + rec.getString("recdate").substring(6, 8));
				} else {
					action.setRecordingdate(rec.getString("recdate"));
				}
				if (rec.getString("distdate").length() > 4) {
					action.setDistdate(rec.getString("distdate")
							.substring(0, 4)
							+ "-"
							+ rec.getString("distdate").substring(4, 6)
							+ "-"
							+ rec.getString("distdate").substring(6, 8));
				} else {
					action.setDistdate(rec.getString("distdate"));
				}
				if (rec.getString("tradedate").length() > 4) {
					action.setTransactiondate(rec.getString("tradedate")
							.substring(0, 4)
							+ "-"
							+ rec.getString("tradedate").substring(4, 6)
							+ "-"
							+ rec.getString("tradedate").substring(6, 8));
				} else {
					action.setTransactiondate(rec.getString("tradedate"));
				}
				if (rec.getString("ex_tradedate").length() > 4) {
					action.setLasttransactiondate(rec.getString("ex_tradedate")
							.substring(0, 4)
							+ "-"
							+ rec.getString("ex_tradedate").substring(4, 6)
							+ "-"
							+ rec.getString("ex_tradedate").substring(6, 8));
				} else {
					action.setLasttransactiondate(rec.getString("ex_tradedate"));
				}
				// action.setCumdate(rec.getString("cumdate"));
				// action.setExdate(rec.getString("exdate"));
				// action.setRecordingdate(rec.getString("recdate"));
				// action.setDistdate(rec.getString("distdate"));
				CorpAction old = (CorpAction) actionMap.get(action
						.getActiontype()
						+ action.getStock()
						+ action.getCumdate());
//				if (old == null) {
					actionMap.put(action.getActiontype() + action.getStock()
							+ action.getCumdate(), action);
					service.rcvOther(action);
//				}
			}
		} catch (Exception sqle) {
			sqle.printStackTrace();
			try {
				connCorpAction.close();
			} catch (Exception ex) {
			}
		} finally {
			try {
				rec.close();
			} catch (Exception ex) {
			}
			try {
				st.close();
			} catch (Exception ex) {
			}
		}
	}

	private void selectArticle() {
		ResultSet rec = null;
		Statement st = null;
		StringBuffer sb = new StringBuffer("");
		Clob clob;
		try {
			if (!service.ping())
				connArticle = null;
			if (connArticle == null)
				connArticle = service.getDatabase().getConnection();
			if (connArticle.isClosed())
				connArticle = service.getDatabase().getConnection();	
			st = connArticle.createStatement();					
//			rec = st.executeQuery("select id, transdate,transtime, subject, title, content, link from feed_article order by id");
			rec = st.executeQuery("select * from feed_varticle ");
			while (rec.next()) {
				String key = rec.getString("id") + rec.getString("transdate");
				Article old = (Article) articleMap.get(key);
				if (old == null) {
					Article art = new Article();
					art.setHeader("IDX");
					art.setType("NW");
					art.setID(rec.getInt("id") + "");
					String temptime = rec.getString("transtime");
					if (temptime.length() > 4) {
						art.setDate(rec.getString("transdate") + " "
								+ temptime.substring(0, 2) + ":"
								+ temptime.substring(2, 4) + ":"
								+ temptime.substring(4, 6));
					}

					art.setTitle(rec.getString("title"));
					art.setSubject(rec.getString("subject"));
					
					clob = rec.getClob("content");
					long length = clob.length();
					String fileData = clob.getSubString(1, (int) length);
					fileData = fileData.replace("\n", "<br/>");
					fileData = fileData.replace("\r\n", "<br/>");
					art.setContent(fileData);
					articleMap.put(rec.getString("id") + rec.getString("transdate"),art);
					service.rcvOther(art);
				}
				
			}
			
		} catch (Exception sqle) {
			sqle.printStackTrace();
			try {
				connArticle.close();
			} catch (Exception ex) {
			}
		} finally {
			try {
				rec.close();
			} catch (Exception ex) {
			}
			try {
				st.close();
			} catch (Exception ex) {
			}
		}
	}
	
	private void selectArticleMobile() {
		System.out.println("Refresh News Mobile");
		ResultSet rec = null;
		Statement st = null;
		StringBuffer sb = new StringBuffer("");
		Clob clob;
		try {
			if (!service.ping())
				connArticleMobile = null;
			if (connArticleMobile == null)
				connArticleMobile = service.getDatabase().getConnection();
			if (connArticleMobile.isClosed())
				connArticleMobile = service.getDatabase().getConnection();	
			st = connArticleMobile.createStatement();					
			//rec = st.executeQuery("select id, transdate,transtime, subject, title, content, link from feed_article where rownum <= 20 order by id desc");
			rec = st.executeQuery("select * from (select rownum, id, transdate,transtime, subject, title, content, link from feed_article order by id desc) where rownum <= 10 ");
			while (rec.next()) {
				String key = rec.getString("id") + rec.getString("transdate");
				Article old = (Article) articleMobileMap.get(key);
				if (old == null) {
					Article art = new Article();
					art.setHeader("IDX");
					art.setType("NWM");
					art.setID(rec.getInt("id") + "");
					String temptime = rec.getString("transtime");
					if (temptime.length() > 4) {
						art.setDate(rec.getString("transdate") + " "
								+ temptime.substring(0, 2) + ":"
								+ temptime.substring(2, 4) + ":"
								+ temptime.substring(4, 6));
					}

					art.setTitle(rec.getString("title"));
					art.setSubject(rec.getString("subject"));
					
					clob = rec.getClob("content");
					long length = clob.length();
					String fileData = clob.getSubString(1, (int) length);
					fileData = fileData.replace("\n", "<br/>");
					fileData = fileData.replace("\r\n", "<br/>");
					art.setContent(fileData);
					articleMobileMap.put(rec.getString("id") + rec.getString("transdate"),art);
//					service.rcvOther(art);
				}				
			}			
		} catch (Exception sqle) {
			sqle.printStackTrace();
			try {
				connArticleMobile.close();
			} catch (Exception ex) {
			}
		} finally {
			try {
				rec.close();
			} catch (Exception ex) {
			}
			try {
				st.close();
			} catch (Exception ex) {
			}
		}
	}
	
	private void selectRups() {
		ResultSet rec = null;
		Statement st = null;
		System.out.println("rups");
		try {

			if (!service.ping())
				connRups = null;
			if (connRups == null)
				connRups = service.getDatabase().getConnection();
			if (connRups.isClosed())
				connRups = service.getDatabase().getConnection();
			st = connRups.createStatement();
//			rec = st.executeQuery("select stock, transdate,transtime, description from feed_rups order by stock");
			rec = st.executeQuery("select * from feed_vrups");
			while (rec.next()) {
				String key = rec.getString("stock")
						+ rec.getString("transdate");
				Rups old = (Rups) rupsMap.get(key);
				if (old == null) {
					Rups rups = new Rups();
					rups.setHeader("IDX");
					rups.setType("RPS");
					rups.setStock(rec.getString("stock"));
					String temptgl = rec.getString("transdate");

					rups.setTransdate(temptgl.substring(0, 4) + "-"
							+ temptgl.substring(4, 6) + "-"
							+ temptgl.substring(6, 8));
					String temp = rec.getString("transtime");
					temp = "0" + temp;
					if (temp.length() == 6) {
						temp = temp.substring(0, temp.length());
						rups.setTranstime(temp.substring(0, 2) + ":"
								+ temp.substring(2, 4) + ":"
								+ temp.substring(4, 6));
					} else {
						if (temp.length() < 6) {
							rups.setTranstime(temp);
						} else {
							temp = temp.substring(1, temp.length());
							rups.setTranstime(temp.substring(0, 2) + ":"
									+ temp.substring(2, 4) + ":"
									+ temp.substring(4, 6));
						}
					}

					String desc = "";
					if (rec.getString("description") == null) {
						rups.setDescription(" ");
					} else if (rec.getString("description").length() == 0) {
						rups.setDescription(" ");
					} else {
						desc = rec.getString("description");
						desc = desc = desc.replace("\n", "");
						rups.setDescription(desc);
					}
					rupsMap.put(
							rec.getString("stock") + rec.getString("transdate"),
							rups);
					service.rcvOther(rups);
				}
			}
		} catch (Exception sqle) {
			sqle.printStackTrace();
			try {
				connRups.close();
			} catch (Exception ex) {
			}
		} finally {
			try {
				rec.close();
			} catch (Exception ex) {
			}
			try {
				st.close();
			} catch (Exception ex) {
			}
		}
	}

	private void selectIPO() {
		ResultSet rec = null;
		Statement st = null;
		System.out.println("ipo");
		try {
			if (!service.ping())
				connIPO = null;
			if (connIPO == null)
				connIPO = service.getDatabase().getConnection();
			if (connIPO.isClosed())
				connIPO = service.getDatabase().getConnection();
			st = connIPO.createStatement();
//			rec = st.executeQuery("select name, stock, nominal, totalshare, percentshare, effdate, offerstart, offerend, allotment, refunddate, listingdate from feed_ipo  order by name");
			rec = st.executeQuery("select * from feed_vipo");
			while (rec.next()) {
				String key = rec.getString("name");
				IPO old = (IPO) ipoMap.get(key);
				if (old == null) {
					IPO ipo = new IPO();
					ipo.setHeader("IDX");
					ipo.setType("IPO");
					ipo.setName(rec.getString("name"));
					ipo.setStock(rec.getString("stock"));
					ipo.setNominal(rec.getDouble("nominal"));
					ipo.setTotalshare(rec.getDouble("totalshare"));
					ipo.setPercentshare(rec.getDouble("percentshare"));
					ipo.setEffdate(rec.getString("effdate"));
					ipo.setOfferstart(rec.getString("offerstart"));
					ipo.setOfferend(rec.getString("offerend"));
					ipo.setAllotment(rec.getString("allotment"));
					ipo.setRefunddate(rec.getString("refunddate"));
					String temptgl = rec.getString("listingdate");

					if (temptgl.length() > 6) {
						ipo.setListingdate(temptgl.substring(0, 4) + "-"
								+ temptgl.substring(4, 6) + "-"
								+ temptgl.substring(6, 8));
					}

					ipoMap.put(rec.getString("name"), ipo);
					service.rcvOther(ipo);
				}
			}
		} catch (Exception sqle) {
			sqle.printStackTrace();
			try {
				connIPO.close();
			} catch (Exception ex) {
			}
		} finally {
			try {
				rec.close();
			} catch (Exception ex) {
			}
			try {
				st.close();
			} catch (Exception ex) {
			}
		}
	}

	private void selectIndices() {
		ResultSet rec = null;
		Statement st = null;
		System.out.println("refresh indicies");
		try {
			// log.info("masuk select indices");
			if (!service.ping())
				connArticle = null;
			if (connIndices == null)
				connIndices = service.getDatabase().getConnection();
			if (connIndices.isClosed())
				connIndices = service.getDatabase().getConnection();
			st = connIndices.createStatement();
			// rec =
			// st.executeQuery("select code, last, prev, chg, percent, urutan from feed_indices order by urutan asc");
//			rec = st.executeQuery("select name, last, close,high,low, last-close as chg,TO_CHAR((last-close)/close*100,'99D99') as persen,open,urutan from feed_indices_imq order by urutan asc");
			rec = st.executeQuery("select * from FEED_VINDICES_IMQ ");

			while (rec.next()) {

				GlobalIndices idx = new GlobalIndices();
				idx.setHeader("IDX");
				idx.setType("GI");
				idx.setCode(rec.getString("NAME"));
				idx.setLast(rec.getDouble("LAST"));
				idx.setPrev(rec.getDouble("CLOSE"));
				idx.setChange(rec.getDouble("CHG"));
				idx.setPercent(rec.getDouble("PERSEN"));
				idx.setHigh(rec.getDouble("HIGH"));
				idx.setLow(rec.getDouble("LOW"));
				idx.setOpen(rec.getDouble("OPEN"));
				idx.setUrutan(rec.getDouble("URUTAN"));

				GlobalIndices old = (GlobalIndices) globalIndicesMap.get(idx.getCode());
				if (old == null) {
					globalIndicesMap.put(idx.getCode(), idx);
				} else {
					old.setLast(idx.getLast());
					old.setPrev(idx.getPrev());
					old.setChange(idx.getChange());
					old.setPercent(idx.getPercent());
					old.setUrutan(idx.getUrutan());
					old.setHigh(idx.getHigh());
					old.setLow(idx.getLow());
					old.setOpen(idx.getOpen());
				}
				service.rcvOther(idx);
			}
			// log.info("setelah indices");
		} catch (Exception sqle) {
			sqle.printStackTrace();
			try {
				connIndices.close();
			} catch (Exception ex) {
			}
		} finally {
			try {
				rec.close();
			} catch (Exception ex) {
			}
			try {
				st.close();
			} catch (Exception ex) {
			}
		}
	}

	private void selectCommodity() {
		ResultSet rec = null;
		Statement st = null;
		System.out.println("refresh comodity");
		try {
			// log.info("masuk commodity");
			if (!service.ping())
				connCommodity = null;
			if (connCommodity == null)
				connCommodity = service.getDatabase().getConnection();
			if (connCommodity.isClosed())
				connCommodity = service.getDatabase().getConnection();
			st = connCommodity.createStatement();
			// rec =
			// st.executeQuery("select code, last, prev, chg, percent, urutan from feed_indices order by urutan asc");
//			rec = st.executeQuery("select name,detail,market, last, close,high,low, change,TO_CHAR(chg_pct,'99D99') as persen,open from feed_commodity a left join commodity_detail b on a.code=b.code  order by name asc");
			rec = st.executeQuery("select * from feed_vcomodity ");

			while (rec.next()) {

				Commodity idx = new Commodity();
				
				idx.setHeader("IDX");
				idx.setType("COM");
				idx.setCode(rec.getString("NAME"));
				idx.setDetail(rec.getString("DETAIL"));
				idx.setMarket(rec.getString("MARKET"));
				idx.setLast(rec.getDouble("LAST"));
				idx.setClose(rec.getDouble("CLOSE"));
				idx.setChange(rec.getDouble("CHANGE"));
				idx.setPercent(rec.getDouble("PERSEN"));
				idx.setHigh(rec.getDouble("HIGH"));
				idx.setLow(rec.getDouble("LOW"));
				idx.setOpen(rec.getDouble("OPEN"));

				Commodity old = (Commodity) commodityMap.get(idx.getCode());
				if (old == null) {
					commodityMap.put(idx.getCode(), idx);
				} else {
					old.setDetail(idx.getDetail());
					old.setMarket(idx.getMarket());
					old.setLast(idx.getLast());
					old.setClose(idx.getClose());
					old.setChange(idx.getChange());
					old.setPercent(idx.getPercent());
					old.setHigh(idx.getHigh());
					old.setLow(idx.getLow());
					old.setOpen(idx.getOpen());
				}
				service.rcvOther(idx);
			}
			// log.info("setelah commodity");
		} catch (Exception sqle) {
			sqle.printStackTrace();
			try {
				connCommodity.close();
			} catch (Exception ex) {
			}
		} finally {
			try {
				rec.close();
			} catch (Exception ex) {
			}
			try {
				st.close();
			} catch (Exception ex) {
			}
		}
	}

	private void selectSector() {
		ResultSet rec = null;
		Statement st = null;
		try {
			System.out.println("refresh sector");
			if (!service.ping())
				connSector = null;
			if (connSector == null)
				connSector = service.getDatabase().getConnection();
			if (connSector.isClosed())
				connSector = service.getDatabase().getConnection();
			st = connSector.createStatement();
			rec = st.executeQuery("select sector_code, sector_name from mst_sector");
			while (rec.next()) {
				String key = rec.getString("sector_code");
				Sector old = (Sector) SectorMap.get(key);
				if (old == null) {
					Sector s = new Sector();
					s.setHeader("IDX");
					s.setType("SC");
					s.setCode(rec.getDouble("sector_code"));
					s.setName(rec.getString("sector_name"));
					SectorMap.put(rec.getString("sector_code"), s);
					service.rcvOther(s);
					//System.out.println("Sector___"+s);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			try {
				connSector.close();
			} catch (Exception ex) {
			}
		} finally {
			try {
				rec.close();
			} catch (Exception ex) {
			}
			try {
				st.close();
			} catch (Exception ex) {
			}
		}
	}

	private void selectBySector() {
		ResultSet rec = null;
		Statement st = null;
		try {
			if (!service.ping())
				connBySector = null;
			if (connBySector == null)
				connBySector = service.getDatabase().getConnection();
			if (connBySector.isClosed())
				connBySector = service.getDatabase().getConnection();
			st = connBySector.createStatement();

			rec = st.executeQuery("select * from feed_compbysector");
//			rec = st.executeQuery("select p.stockid,p.der,p.roa,p.roe,p.npm,p.opm,p.eps,p.bookvalue,p.sectorcode, s.stock,s.prev "
//					+ "from comp_profile_finance p left join feed_stocksummary s on p.stockid = s.stock order  by stockid asc");
			// System.out.println("Sql________"+rec);
			StockComparisonBySector scbs = new StockComparisonBySector();
			while (rec.next()) {
				String key = rec.getString("stockid");
				StockComparisonBySector old = (StockComparisonBySector) comparisonBySectorMap
						.get(key);
				StockSummary ss ;
				//if (old == null) {
					scbs.setHeader("IDX");
					scbs.setType("CBS");
					scbs.setId(rec.getString("stockid"));
					scbs.setDer(rec.getDouble("der"));
					scbs.setRoa(rec.getDouble("roa"));
					scbs.setRoe(rec.getDouble("roe"));
					scbs.setNpm(rec.getDouble("npm"));
					scbs.setOpm(rec.getDouble("opm"));
					scbs.setEps(rec.getDouble("eps"));
					scbs.setBv(rec.getDouble("bookvalue"));
					ss = (StockSummary) ssMap.get(scbs.getId()+"RG");
					scbs.setSectorcode(rec.getDouble("sectorcode"));
					/* scbs.setLastupdate(rec.getString("lastupdate")); */
					if(ss == null){
					scbs.setClose(rec.getDouble("prev"));
					scbs.setPer(scbs.getClose() / scbs.getEps());
					}else{
					scbs.setClose(ss.getPrev());
					scbs.setPer(ss.getPrev() / scbs.getEps());
					}
					// System.out.println("Per___"+rec.getString("stockid")+rec.getDouble("per"));
					// System.out.println("Prev......."+rec.getString("stockid")+"...."+scbs.getClose()/scbs.getEps());
					// System.out.println("Comparison....."+scbs);
					comparisonBySectorMap.put(rec.getString("stockid"), scbs);
					service.rcvOther(scbs);

				//}

			}
		} catch (Exception sqle) {
			sqle.printStackTrace();
			try {
				connBySector.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} finally {
			try {
				rec.close();
			} catch (Exception ex) {
			}
			try {
				st.close();
			} catch (Exception ex) {
			}
		}
	}

	private void selectFuture() {
		ResultSet rec = null;
		Statement st = null;
		try {

			if (!service.ping())
				connFuture = null;
			if (connFuture == null)
				connFuture = service.getDatabase().getConnection();
			if (connFuture.isClosed())
				connFuture = service.getDatabase().getConnection();
			st = connFuture.createStatement();
			// rec =
			// st.executeQuery("select code, last, prev, chg, percent, urutan from feed_indices order by urutan asc");
//			rec = st.executeQuery("select name,detail,market, last, close,high,low, change,TO_CHAR(chg_pct,'99D99') as persen,open from feed_futures a left join future_detail b on a.code=b.code  order by name asc");
			rec = st.executeQuery("select * from feed_vfuture ");

			while (rec.next()) {

				Future idx = new Future();
				// log.info("Nama "+rec.getString("NAME"));
				// log.info("Last "+rec.getString("LAST"));
				// log.info("Close "+rec.getString("CHG"));

				idx.setHeader("IDX");
				idx.setType("FTR");
				idx.setCode(rec.getString("NAME"));
				idx.setDetail(rec.getString("DETAIL"));
				idx.setMarket(rec.getString("MARKET"));
				idx.setLast(rec.getDouble("LAST"));
				idx.setClose(rec.getDouble("CLOSE"));
				idx.setChange(rec.getDouble("CHANGE"));
				idx.setPercent(rec.getDouble("PERSEN"));
				idx.setHigh(rec.getDouble("HIGH"));
				idx.setLow(rec.getDouble("LOW"));
				idx.setOpen(rec.getDouble("OPEN"));

				Future old = (Future) futureMap.get(idx.getCode());
				if (old == null) {
					futureMap.put(idx.getCode(), idx);
				} else {
					old.setLast(idx.getLast());
					old.setClose(idx.getClose());
					old.setChange(idx.getChange());
					old.setPercent(idx.getPercent());
					old.setHigh(idx.getHigh());
					old.setLow(idx.getLow());
					old.setOpen(idx.getOpen());
				}
				service.rcvOther(idx);
			}
			// log.info("setelah future");
		} catch (Exception sqle) {
			sqle.printStackTrace();
			try {
				connFuture.close();
			} catch (Exception ex) {
			}
		} finally {
			try {
				rec.close();
			} catch (Exception ex) {
			}
			try {
				st.close();
			} catch (Exception ex) {
			}
		}
	}
		
	public OtherConsumer(String url) throws Exception {
		super(url, "0");
	}

	public OtherConsumer(String url, String type, boolean b) throws Exception {
		super(url, type, b);
	}

	public List getSnapShot(Class c, int seqno) {
		Query query = db.getDb().getInstance().query();
		query.constrain(c);
		query.descend("seqno").orderAscending().constrain(new Long(seqno))
				.greater();
		List o = query.execute();
		return o;
	}

	public List getTPSnapShot(final String param, final int seqno) {
		String[] p = param.split("\\#");
		Query query = db.getDb().getInstance().query();
		query.constrain(TradePrice.class);
		query.descend("stock")
				.constrain(p[0])
				.and(query.descend("board").constrain(p[1]))
				.and(query.descend("seqno").orderAscending()
						.constrain(new Long(seqno)).greater());
		List o = query.execute();
		return o;
	}
	
	public List getTHSnapShot(final String param, final int seqno) {		

		Vector<Trade> vtemp = new Vector<Trade>();

		Vector<Trade> v = (Vector<Trade>) tradeHistory.get(param);
		int i = 0;
		if (v != null) {
			for (; i < v.size(); i++) {
				Trade trd = v.get(i);
				if (seqno < trd.getSeqno()) {
					break;
				}
			}
			vtemp.addAll(v.subList(i, v.size()));
		} else
			log.info("v is null " + v + " " + param);
		
		return vtemp;

	}
	
	public List getTSBSSnapShot(final String param, final int seqno) {
		Query query = db.getDb().getInstance().query();
		query.constrain(TradeSummBrokerStock.class);
		query.descend("broker")
				.constrain(param)
				.and(query.descend("seqno").orderAscending()
						.constrain(new Long(seqno)).greater());
		List o = query.execute();
		return o;
	}

	public List getTSSBSnapShot(final String param, final int seqno) {
		Query query = db.getDb().getInstance().query();
		query.constrain(TradeSummStockBroker.class);
		query.descend("stock")
				.constrain(param)
				.and(query.descend("seqno").orderAscending()
						.constrain(new Long(seqno)).greater());
		List o = query.execute();
		return o;
	}
	
	public List getBSSnapShot(final String param, final int seqno) {
		Query query = db.getDb().getInstance().query();
		query.constrain(BrokerSummarry.class);
		query.descend("stock")
				.constrain(param)
				.and(query.descend("seqno").orderAscending()
						.constrain(new Long(seqno)).greater());
		List o = query.execute();
		return o;
	}

	public String getTHSnapShotLast(final String param, final int seqno) {
		Query query = db.getDb().getInstance().query();
		query.constrain(Trade.class);
		// query.descend("stock").constrain(param).and(query.descend("board").constrain("RG")).and(
		query.descend("stock")
				.constrain(param)
				.and(query.descend("board").constrain("RG"))
				.and(query.descend("seqno").orderDescending()
						.constrain(new Long(seqno)).greater());
		return query.execute().toString();
	}

	public HashMap getCRSnapShot() {
		return currencyMap;
	}	
	
	public HashMap getCRMSnapShot() {
		return currencyMobileMap;
	}

	public HashMap getGISnapShot() {
		return globalIndicesMap;
	}
	
	public HashMap getGIMSnapShot() {
		return globalIndicesMobileMap;
	}

	public HashMap getNWSnapShot() {
		return articleMap;
	}

	public HashMap getNWMSnapShot() {
		return articleMobileMap;
	}
	
	public HashMap getCASnapShot() {
		return actionMap;
	}

	public HashMap getRupsSnapShot() {
		return rupsMap;
	}

	public HashMap getIPOSnapShot() {
		return ipoMap;
	}

	public HashMap getCOMSnapShot() {
		return commodityMap;
	}

	public HashMap getFTRSnapShot() {
		return futureMap;
	}

	public HashMap getSNSnapShot() {
		return stockNewsMap;
	}
	
	public HashMap getSNSDSnapShot() {
		return stockNewsStockDetailMap;
	}

	public HashMap getCBSSnapShot() {
		return comparisonBySectorMap;
	}

	public HashMap getSCSnapShot() {
		return SectorMap;
	}

	public HashMap getCHISnapShot(){
		return chartIntraday;
	}
	
	public HashMap getTMSISnapshot(){
		return tsStockMarketInv;
	}
	
	//#Valdhy 20141219
	//--- Mobile Section - Start ---
	public List getSSMSnapShot(final String param, final int seqno) {
		Query query = db.getDb().getInstance().query();
		System.out.println(param +"getSSMSnapshot");
		String[] data = param.split("\\|");
		if (data[2].equals("0")) {
			query.constrain(StockSummaryMobile.class);
			query.descend("board").constrain("RG");
			query.descend(data[1]).orderAscending();
		} else if (data[2].equals("1")) {
			query.constrain(StockSummaryMobile.class);
			query.descend("board").constrain("RG");
			query.descend(data[1]).orderDescending();
		}
		
        
		List o = query.execute().subList(0, 20);		
		return o;
	}
	
	public List getMMSnapShot() {
		Query query = db.getDb().getInstance().query();
		query.constrain(MarketMobile.class);
		query.descend("board").constrain("TOTAL");
		//query.descend(param).orderDescending();
        
		List o = query.execute();		
		return o;
	}
	
	public List getTSBSMSnapShot(final String param, final int seqno) {
		Query query = db.getDb().getInstance().query();
		query.constrain(TradeSummBrokerStockMobile.class);
		query.descend("broker").constrain(param);
        
		List o = query.execute();
		//o.add("##End##");
//		System.err.println("TSBSM : " + o.toString());
		return o;
	}
	
	public List getTSSBMSnapShot(final String param, final int seqno) {
		Query query = db.getDb().getInstance().query();
		query.constrain(TradeSummStockBrokerMobile.class);
		query.descend("stock").constrain(param);
        
		List o = query.execute();
		//List o = new ArrayList(Arrays.asList(query.execute().toString().split(",")) );
		//o.add("##End##");
//		System.err.println("TSSBM : " + o.toString());
		return o;
	}
	//yosep timetrade paging
	public List getTHSnapShotPaging(final String param, final int seqno) {
		Vector<TradeMobile> vtemp = new Vector<TradeMobile>();

		Vector<TradeMobile> v = (Vector<TradeMobile>) tradeHistoryMobile.get(param);
		int i = v.size()-1;
		int j = 0;
		if (v != null) {
			for (; i > 0; i--) {
				TradeMobile trd = v.get(i);
//				v.get(i).setType("THM");
//				trd.setType("THM");
//				System.out.println(trd.getSeqno()+ " getTHSnapShotPaging");
				if (seqno > trd.getSeqno()) {
					break;
				}
				if (j>=100) {
					break;
				}j++;
			}
			vtemp.addAll(v.subList(i, v.size()));
		} else
			log.info("v is null " + v + " " + param);
		

		return vtemp;
	}
	
	public String getTHSnapShotHistory(final String param, final int seqno) {		

		Vector<TradeMobile> vtemp = new Vector<TradeMobile>();
		StringBuffer sb = new StringBuffer("");
		Vector<TradeMobile> v = (Vector<TradeMobile>) tradeHistoryMobile.get(param);
		int i = v.size()-1;
		int j = 0;
		if (v != null) {
			for (; i > 0; i--) {
				TradeMobile trd = v.get(i);
//				trd.setType("THM");
				if (seqno > trd.getSeqno()) {
//					vtemp.add(v.get(i));
					sb.append(trd.toString() + "\n");
//					break;
					j++;
				}
				if (j>=100) {
					break;
				}
			}
//			if (v.size()<=30) {
//				vtemp.addAll(v.subList(i, v.size()));
//			} else {
//				vtemp.addAll(v.subList(0, 30));
//			}
		} else
			log.info("v is null " + v + " " + param);

		
		return sb.length() == 0 ? null : sb.toString();

	}
	// get mobile th 123
	public String getTHSnapShotHistoryMobile(final String param, final int seqno) {		

		Vector<TradeMobile> vtemp = new Vector<TradeMobile>();
		StringBuffer sb = new StringBuffer("");
		Vector<TradeMobile> v = (Vector<TradeMobile>) tradeHistoryMobile.get(param);
		int i = v.size()-1;
		int j = 0;
		if (v != null) {
			for (; i > 0; i--) {
				TradeMobile trd = v.get(i);
//				trd.setType("THM");
				if (seqno > trd.getSeqno()) {
//					vtemp.add(v.get(i));
					sb.append(trd.toString() + "\n");
//					break;
					j++;
				}
				if (j>=100) {
					break;
				}
			}
			if (sb.length() == 0) {
				for (int i2 =0; i2 > 0; i2++) {
					TradeMobile trd = v.get(i2);
	//				trd.setType("THM");
	//				if (seqno < trd.getSeqno()) {
	//					vtemp.add(v.get(i));
						sb.append(trd.toString() + "\n");
	//					break;
						j++;
	//				}
					if (j>=100) {
						break;
					}
				}
			}
		} else{
			log.info("v is null " + v + " " + param);
			
			
		}

		
		return sb.length() == 0 ? null : sb.toString();

	}
	private void selectCurrencyMobile() {
		ResultSet rec = null;
		Statement st = null;
		DateFormat dt = new SimpleDateFormat("hh:mm:ss");
		System.out.println("refresh currency");
		try {
			if (!service.ping())
				connCurrency = null;
			if (connCurrency == null)
				connCurrency = service.getDatabase().getConnection();
			if (connCurrency.isClosed())
				connCurrency = service.getDatabase().getConnection();
			st = connCurrency.createStatement();
			// rec =
			// st.executeQuery("select code, value, chg from feed_currency order by code");
//			rec = st.executeQuery("SELECT CODE,TIME,LAST,CHG,HIGH,LOW,TIPE FROM FEED_CURRENCY_IMQ order by code");
			rec = st.executeQuery("SELECT * FROM FEED_VCURRENCY_IMQ ");
			int queue =0;
			while (rec.next()) {
				CurrencyMobile curr = new CurrencyMobile();

				curr.setHeader("IDX");
				curr.setType("CRM");
				curr.setCode(rec.getString("code"));
				curr.setValue(rec.getDouble("last"));
				curr.setChange(rec.getDouble("chg"));
				curr.setHigh(rec.getDouble("High"));
				curr.setLow(rec.getDouble("low"));
				curr.setTipe(rec.getString("tipe"));
				curr.setQueue(queue+"");
				String temp = rec.getString("time");
				temp = "0" + temp;
				if (temp.length() == 6) {
					temp = temp.substring(0, temp.length());
				} else {
					temp = temp.substring(1, temp.length());
				}

				curr.setTime1(temp.substring(0, 2) + ":" + temp.substring(2, 4)
						+ ":" + temp.substring(4, 6));

				CurrencyMobile old = (CurrencyMobile) currencyMobileMap.get(curr.getCode());
				if (old == null) {
					currencyMobileMap.put(curr.getCode(), curr);
				} else {
					old.setValue(curr.getValue());
					old.setChange(curr.getChange());
					old.setHigh(curr.getHigh());
					old.setLow(curr.getLow());
					old.setTime1(curr.getTime1());

				}
				service.rcvOther(curr);
				queue++;
			}
		} catch (Exception sqle) {
			sqle.printStackTrace();
			try {
				connCurrency.close();
			} catch (Exception ex) {
			}
		} finally {
			try {
				rec.close();
			} catch (Exception ex) {
			}
			try {
				st.close();
			} catch (Exception ex) {
			}
		}
	}
	
	private void selectIndicesMobile() {
		ResultSet rec = null;
		Statement st = null;
		System.out.println("refresh indicies");
		try {
			// log.info("masuk select indices");
			if (!service.ping())
				connArticle = null;
			if (connIndices == null)
				connIndices = service.getDatabase().getConnection();
			if (connIndices.isClosed())
				connIndices = service.getDatabase().getConnection();
			st = connIndices.createStatement();
			// rec =
			// st.executeQuery("select code, last, prev, chg, percent, urutan from feed_indices order by urutan asc");
//			rec = st.executeQuery("select name, last, close,high,low, last-close as chg,TO_CHAR((last-close)/close*100,'99D99') as persen,open,urutan from feed_indices_imq order by urutan asc");
			rec = st.executeQuery("select * from FEED_VINDICES_IMQ ");
			int queue =0;
			while (rec.next()) {

				GlobalIndicesMobile idx = new GlobalIndicesMobile();
				idx.setHeader("IDX");
				idx.setType("GIM");
				idx.setCode(rec.getString("NAME"));
				idx.setLast(rec.getDouble("LAST"));
				idx.setPrev(rec.getDouble("CLOSE"));
				idx.setChange(rec.getDouble("CHG"));
				idx.setPercent(rec.getDouble("PERSEN"));
				idx.setHigh(rec.getDouble("HIGH"));
				idx.setLow(rec.getDouble("LOW"));
				idx.setOpen(rec.getDouble("OPEN"));
				idx.setUrutan(rec.getDouble("URUTAN"));
				idx.setUrut(queue);

//				GlobalIndicesMobile old = (GlobalIndicesMobile) globalIndicesMobileMap.get(idx.getCode());
//				if (old == null) {
					globalIndicesMobileMap.put(idx.getCode(), idx);
//				} else {
//					old.setLast(idx.getLast());
//					old.setPrev(idx.getPrev());
//					old.setChange(idx.getChange());
//					old.setPercent(idx.getPercent());
//					old.setUrutan(idx.getUrutan());
//					old.setHigh(idx.getHigh());
//					old.setLow(idx.getLow());
//					old.setOpen(idx.getOpen());
//				}
				service.rcvOther(idx);
				queue++;
			}
			// log.info("setelah indices");
		} catch (Exception sqle) {
			sqle.printStackTrace();
			try {
				connIndices.close();
			} catch (Exception ex) {
			}
		} finally {
			try {
				rec.close();
			} catch (Exception ex) {
			}
			try {
				st.close();
			} catch (Exception ex) {
			}
		}
	}
	//:TODO FDM 
	public String getFDMSnapshotMobile(String investor,String shortby){
		Query query = db.getDb().getInstance().query();
		query.constrain(TradeSummStockInvMobile.class);
		query.descend("investor").constrain(investor);
		query.descend(shortby).orderDescending();
		List o = query.execute();
		StringBuffer sb = new StringBuffer("");
		for (int i = 0; i < 20; i++) {
			sb.append(o.get(i)+"\n");
		}
		System.out.println(investor+"-"+shortby+"\n"+sb.toString()+"------------------------------------------");
		return sb.toString();
	}
	
	//--- Mobile Section - End ---
		
	protected void loadConfig() {
		log = LogFactory.getLog(getClass());
		super.loadConfig();
		indicesMap = new HashMap(500, 1); // key index
		brokerMap = new HashMap(500, 1); // key brokercode
		actionMap = new HashMap(500, 1); // key actiontype+stock
		newsMap = new HashMap(500, 1); // key no
		stockMap = new HashMap(500, 1); // key stockcode
		ssMap = new HashMap(500, 1); // key stock+board
		tradepriceMap = new HashMap(500, 1); // key stock+board
		tsMap = new HashMap(500, 20); // key stock+board+broker
		
		//#Valdhy 20141219
		//--- Mobile Section - Start ---
		ssmMap = new HashMap(500, 1);
		mmMap = new HashMap(500, 1);
		stockMobileMap = new HashMap(500,1);
		brokerMobileMap = new HashMap(500,1);
		tsBrokerStockMobile = new HashMap(500, 1);
		tsStockBrokerMobile = new HashMap(500, 1);
		//--- Mobile Section - End ---

		tgMap = new HashMap(500,1);
		tsBroker = new HashMap(500, 1);
		tsBrokerStock = new HashMap(500, 1);
		tsStockBroker = new HashMap(500, 1);
		tsStockInv = new HashMap(500, 1);
		tsInv = new HashMap(2, 1);
		tsBrokerSumm = new HashMap(500,1);
		tsStockMarketInv = new HashMap(500,1);
		tsStockInvMobile = new HashMap(500,1);
		tradeHistory = new HashMap(500, 5);
		tradeHistoryMobile = new HashMap(500, 5);

		// List listIndices = db.query(Indices.class);
		Query query = db.getDb().getInstance().query();
		query.constrain(Indices.class);
		query.descend("type").constrain("6");
		query.descend("code").orderAscending();
		List t = query.execute();
		for (int i = 0; i < t.size(); i++) {
			Indices q = (Indices) t.get(i);
			indicesMap.put(q.getCode(), q);
		}
		List listBroker = db.query(Broker.class);
		for (int i = 0; i < listBroker.size(); i++) {
			Broker q = (Broker) listBroker.get(i);
			brokerMap.put(q.getCode(), q);
		}
		List listAction = db.query(CorpAction.class);
		for (int i = 0; i < listAction.size(); i++) {
			CorpAction q = (CorpAction) listAction.get(i);
			actionMap.put(q.getActiontype() + q.getStock() + q.getCumdate(), q);
		}
		List listNews = db.query(News.class);
		for (int i = 0; i < listNews.size(); i++) {
			News q = (News) listNews.get(i);
			newsMap.put(q.getNo(), q);
		}
		List listStock = db.query(Stock.class);
		for (int i = 0; i < listStock.size(); i++) {
			Stock q = (Stock) listStock.get(i);
			stockMap.put(q.getCode(), q);
		}
		List listSS = db.query(StockSummary.class);
		for (int i = 0; i < listSS.size(); i++) {
			StockSummary q = (StockSummary) listSS.get(i);
			ssMap.put(q.getStock() + q.getBoard(), q);
		}
		List listTradeprice = db.query(TradePrice.class);
		for (int i = 0; i < listTradeprice.size(); i++) {
			TradePrice q = (TradePrice) listTradeprice.get(i);
			tradepriceMap.put(q.getStock() + q.getBoard() + q.getPrice(), q);
		}
		List listTS = db.query(TradeSummary.class);
		for (int i = 0; i < listTS.size(); i++) {
			TradeSummary q = (TradeSummary) listTS.get(i);
			tsMap.put(
					q.getStock() + q.getBoard() + q.getBroker()
							+ q.getInvestor(), q);
		}

		List list = db.query(TradeSummBroker.class);
		for (int i = 0; i < list.size(); i++) {
			TradeSummBroker q = (TradeSummBroker) list.get(i);
			tsBroker.put(q.getBroker(), q);
		}
		List listbs = db.query(TradeSummBrokerStock.class);
		for (int i = 0; i < listbs.size(); i++) {
			TradeSummBrokerStock q = (TradeSummBrokerStock) listbs.get(i);
			tsBrokerStock.put(q.getBroker() + q.getStock(), q);
		}
		List listinv = db.query(TradeSummInv.class);
		for (int i = 0; i < listinv.size(); i++) {
			TradeSummInv q = (TradeSummInv) listinv.get(i);
			tsInv.put(q.getInvestor(), q);
		}
		List listsb = db.query(TradeSummStockBroker.class);
		for (int i = 0; i < listsb.size(); i++) {
			TradeSummStockBroker q = (TradeSummStockBroker) listsb.get(i);
			tsStockBroker.put(q.getStock() + q.getBroker(), q);
		}
		List listbs2 = db.query(BrokerSummarry.class);
		for (int i = 0; i < listbs2.size(); i++) {
			BrokerSummarry b = (BrokerSummarry) listbs2.get(i);
			b.setType("BS");
			tsBrokerSumm.put(b.getStock() + b.getBroker(), b);
		}		
		List listinv1 = db.query(TradeSummStockInv.class);
		for (int i = 0; i < listinv1.size(); i++) {
			TradeSummStockInv q = (TradeSummStockInv) listinv1.get(i);
			tsStockInv.put(q.getStock() + q.getInvestor(), q);
		}
		List listminv2 = db.query(TradeSummStockMarketInv.class);
		for (int i = 0; i < listminv2.size(); i++) {
			TradeSummStockMarketInv q = (TradeSummStockMarketInv) listminv2.get(i);
			tsStockMarketInv.put(q.getStock()+q.getBoard() +q.getBroker()+ q.getInvestor(), q);
//			tsStockMarketInv.put(q.getStock() + q.getInvestor()+q.getBoard(), q);
		}
		List listsim = db.query(TradeSummStockInvMobile.class);
		for (int i = 0; i < listsim.size(); i++) {
			TradeSummStockInvMobile q = (TradeSummStockInvMobile) listsim.get(i);
			tsStockInvMobile.put(q.getStock()+q.getInvestor(), q);
		}
		//#Valdhy 20141219
		//--- Mobile Section - Start ---
		List listSSM = db.query(StockSummaryMobile.class);
		for (int i = 0; i < listSSM.size(); i++) {
			StockSummaryMobile q = (StockSummaryMobile) listSSM.get(i);
			ssmMap.put(q.getStock() + q.getBoard(), q);
		}
		
		List listMM = db.query(MarketMobile.class);
		for (int i = 0; i < listMM.size(); i++) {
			MarketMobile q = (MarketMobile) listMM.get(i);
			mmMap.put(q.getStock() + q.getBoard(), q);
		}
		
		List listbsm = db.query(TradeSummBrokerStockMobile.class);
		for (int i = 0; i < listbsm.size(); i++) {
			TradeSummBrokerStockMobile q = (TradeSummBrokerStockMobile) listbsm.get(i);
			tsBrokerStockMobile.put(q.getBroker() + q.getStock(), q);
		}
		
		List listsbm = db.query(TradeSummStockBrokerMobile.class);
		for (int i = 0; i < listsbm.size(); i++) {
			TradeSummStockBrokerMobile q = (TradeSummStockBrokerMobile) listsbm.get(i);
			tsStockBrokerMobile.put(q.getStock() + q.getBroker(), q);
		}
		
		List listStockMobile = db.query(StockMobile.class);
		for (int i = 0; i < listStockMobile.size(); i++) {
			StockMobile q = (StockMobile) listStockMobile.get(i);
			stockMobileMap.put(q.getCode(), q);
		}
		List listBrokerMobile = db.query(BrokerMobile.class);
		for (int i = 0; i < listBrokerMobile.size(); i++) {
			BrokerMobile q = (BrokerMobile) listBrokerMobile .get(i);
			brokerMobileMap.put(q.getCode(), q);
		}
		//--- Mobile Section - End ---
		
		List trades = db.query(Trade.class);
		for (int i = 0; i < trades.size(); i++) {
			Trade tr = (Trade) trades.get(i);
			Vector<Trade> v = (Vector<Trade>) tradeHistory.get(tr.getStock());
			if (v == null) {
				v = new Vector<Trade>();
				tradeHistory.put(tr.getStock(), v);
			}
			v.add(tr);
		}
		log.info("trade_history_size_" + tradeHistory.size());
		List tradesm = db.query(TradeMobile.class);
		for (int i = 0; i < tradesm.size(); i++) {
			TradeMobile trm = (TradeMobile) tradesm.get(i);
			Vector<TradeMobile> v = (Vector<TradeMobile>) tradeHistoryMobile.get(trm.getStock());
			if (v == null) {
				v = new Vector<TradeMobile>();
				tradeHistoryMobile.put(trm.getStock(), v);
			}
				v.add(trm);
		}
		List listTG = db.query(TopGainer.class);
		for (int i = 0; i < listTG.size(); i++) {
			TopGainer q = (TopGainer) listTG.get(i);
			tgMap.put(q.getStock() + q.getBoard(), q);
		}
		log.info("tradeHistoryMobile" + tradeHistoryMobile.size());
	}

	public void newMessage(String message) throws RemoteException {
		try {
			String[] data = message.split("\\|");
			Message dat = null;
			if (data[1].equals("4")) {
				dat = new Broker();
				dat.setContent(data);
				// if (dat.getSeqno()>=msg.getSeqno()){
				Broker b = (Broker) brokerMap.get(((Broker) dat).getCode());
				if (b == null) {
					b = new Broker();
				}
				b.setContent(data);
				brokerMap.put(b.getCode(), b);
				addSnapShot(b);
				
				dat = new BrokerMobile();
				dat.setContent(data);
				// if (dat.getSeqno()>=msg.getSeqno()){
				BrokerMobile bm = (BrokerMobile) brokerMobileMap.get(((BrokerMobile) dat).getCode());
				if (bm == null) {
					bm = new BrokerMobile();
				}
				bm.setContent(data);
				brokerMap.put(bm.getCode(), bm);
				addSnapShot(bm);
				// }
			} else if (data[1].equals("A")) {
				dat = new CorpAction();
				dat.setContent(data);
				// if (dat.getSeqno()>=msg.getSeqno()){
				CorpAction a = (CorpAction) actionMap.get(((CorpAction) dat)
						.getActiontype() + ((CorpAction) dat).getStock());
				if (a == null) {
					a = new CorpAction();
				}
				a.setContent(data);
				actionMap.put(
						a.getActiontype() + a.getStock() + a.getCumdate(), a);
				addSnapShot(a);
				// }
			} else if (data[1].equals("6")) {
				dat = new Indices();
				dat.setContent(data);
				// if (dat.getSeqno()>=msg.getSeqno()){
				Indices i = (Indices) indicesMap.get(((Indices) dat).getCode());
				if (i == null) {
					i = new Indices();
				}
				i.setContent(data);
				indicesMap.put(i.getCode(), i);
				addSnapShot(i);
				if (i.getCode().equalsIgnoreCase("COMPOSITE") 
						&& interval < dt.parse(i.getTranstime()).getTime()) {
					ChartIntraday ci = new ChartIntraday();
					ci.setHeader("IDX");
					ci.setType("MHI");					
					ci.setTranstime(i.getTranstime());//rec.getString("transtime"));
					ci.setCode(i.getCode());//rec.getString("code"));
					ci.setLast(i.getIndex());//rec.getDouble("last"));
					service.rcvOther(ci);
					Calendar c = Calendar.getInstance();
					c.setTime(dt.parse(i.getTranstime()));
					c.add(Calendar.MINUTE,	1);
					interval = c.getTimeInMillis();
				}
				// add index history
				// Indices his = new Indices();
				// his.setContent(data);
				// his.setType("6H");
				// addSnapShot(his);
				// }
			} else if (data[1].equals("9")) {
				/* BEI NEWS*/
				dat = new News();
				dat.setContent(data);
				
				News n = (News) newsMap.get(((News) dat).getNo());
				if (n == null) {
					n = new News();
				}				
				n.setContent(data);
				newsMap.put(n.getNo(), n);
				addSnapShot(n);
				
			} else if (data[1].equals("3")) {
				dat = new Stock();
				dat.setContent(data);
				// if (dat.getSeqno()>=msg.getSeqno()){
				Stock s = (Stock) stockMap.get(((Stock) dat).getCode());
				if (s == null) {
					s = new Stock();
				}
				s.setContent(data);
				stockMap.put(s.getCode(), s);
				addSnapShot(s);
				
				// stock mobile 
				dat = new StockMobile();
				dat.setContent(data);
				// if (dat.getSeqno()>=msg.getSeqno()){
				StockMobile sm = (StockMobile) stockMobileMap.get(((StockMobile) dat).getCode());
				if (sm == null) {
					sm = new StockMobile();
				}
				sm.setContent(data);
				stockMobileMap.put(sm.getCode(), sm);
				addSnapShot(sm);
				// }
			} else if (data[1].equals("5")) {
				dat = new StockSummary();
				dat.setContent(data);
				// if (dat.getSeqno()>=msg.getSeqno()){
				StockSummary ss = (StockSummary) ssMap.get(((StockSummary) dat)
						.getStock() + ((StockSummary) dat).getBoard());
				if (ss == null) {
					ss = new StockSummary();
				}
				ss.setContent(data);
				ssMap.put(ss.getStock() + ss.getBoard(), ss);
				addSnapShot(ss);
				
				//#Valdhy 20141219
				//--- Mobile Section - Start ---
				
				//Stock Summary Mobile
				StockSummaryMobile sm = (StockSummaryMobile) ssmMap.get(((StockSummary) dat)
						.getStock() + ((StockSummary) dat).getBoard());
				if (sm == null) {
					sm = new StockSummaryMobile();
				}
				sm.setContent(data);
				sm.setType("SSM");
				sm.calculate();
			
				ssmMap.put(sm.getStock()+sm.getBoard(), sm);
				addSnapShot(sm);
				
				//Market Mobile
				//System.err.println("MM Start");
				MarketMobile mm = (MarketMobile) mmMap.get("TOTAL");
				if (mm == null){
					//System.err.println("MM mm null");
					mm = new MarketMobile();
					mm.setContent(data);
				} else {
					MarketMobile oldmm = (MarketMobile) mmMap.get(((StockSummary) dat)
							.getStock() + ((StockSummary) dat).getBoard());
					
					if (oldmm == null) {
						//System.err.println("MM oldmm null");
						oldmm = new MarketMobile();
						mm.setValue(new Double(mm.getValue() + ss.getTradeval()));
		                mm.setVolume(new Double(mm.getVolume() + ss.getTradevol()));
		                mm.setFreq(new Double(mm.getFreq() + ss.getTradefreq()));  

					} else {
						//System.err.println("MM oldmm not null");
						mm.setValue(new Double(mm.getValue() - oldmm.getTradeval() + ss.getTradeval()));
		                mm.setVolume(new Double(mm.getVolume() - oldmm.getTradevol() + ss.getTradevol()));
		                mm.setFreq(new Double(mm.getFreq() - oldmm.getTradefreq() + ss.getTradefreq()));
					}
					oldmm.setContent(data);
					mmMap.put(ss.getStock()+ss.getBoard(), oldmm);
				}
				mm.calculate();
				mm.setType("MM");
				mm.setBoard("TOTAL");
				
				addSnapShot(mm);
				mmMap.put("TOTAL", mm);
				service.rcvOther(mm);
				
				//Topgainer 
				TopGainer tg = (TopGainer) tgMap.get(((StockSummary) dat)
						.getStock() + ((StockSummary) dat).getBoard());
				if (tg == null) {
					tg = new TopGainer();
				}
				tg.setContent(data);
				tg.setType("TG");
			
				tgMap.put(tg.getStock()+tg.getBoard(), tg);
				addSnapShot(tg);
				service.rcvOther(tg);
				//--- Mobile Section - End ---
				
			} else if (data[1].equals("TP")) {
				dat = new TradePrice();
				dat.setContent(data);
				// if (dat.getSeqno()>=msg.getSeqno()){
				TradePrice tp = (TradePrice) tradepriceMap
						.get(((TradePrice) dat).getStock()
								+ ((TradePrice) dat).getBoard()
								+ ((TradePrice) dat).getPrice());
				if (tp == null) {
					tp = new TradePrice();
				}
				tp.setContent(data);
				tradepriceMap.put(
						tp.getStock() + tp.getBoard() + tp.getPrice(), tp);
				addSnapShot(tp);
				// }
			} else if (data[1].equals("TS")) {
				dat = new TradeSummary();
				dat.setContent(data);
				// if (dat.getSeqno()>=msg.getSeqno()){
				TradeSummary ts = (TradeSummary) tsMap.get(((TradeSummary) dat)
						.getStock()
						+ ((TradeSummary) dat).getBoard()
						+ ((TradeSummary) dat).getBroker()
						+ ((TradeSummary) dat).getInvestor());
				if (ts == null) {
					ts = new TradeSummary();
				}
				ts.setContent(data);
				tsMap.put(
						ts.getStock() + ts.getBoard() + ts.getBroker()
								+ ts.getInvestor(), ts);
				addSnapShot(ts);
				//IDX|TS|12714|LMAS|RG|CC|F|50.00|50.00|3270800|163540000|227|3272000|163600000|229 ts string
				// }

				TradeSummStockInvMobile fdm = (TradeSummStockInvMobile) tsStockInvMobile
						.get(ts.getStock()
								+ ts.getInvestor());
				if (ts.getBoard().equalsIgnoreCase("RG")){
					if (fdm == null) {
						fdm = new TradeSummStockInvMobile();
						fdm.setContent(data);
						fdm.setBoard("ALL");
						fdm.setBroker("ALL");
						fdm.setType("FDM");
					}else{
						TradeSummStockMarketInv tsmi = (TradeSummStockMarketInv) tsStockMarketInv
								.get(
										ts.getStock()
										+ ts.getBoard()
										+ ts.getBroker()
										+ ts.getInvestor());
						if (tsmi == null) {
							fdm.setBuyvol(new Double(fdm.getBuyvol()  +ts.getBuyvol()));
				            fdm.setBuyval(new Double(fdm.getBuyval()  + ts.getBuyval()));
				            fdm.setBuyfreq(new Double(fdm.getBuyfreq() + ts.getBuyfreq()));                
				            fdm.setSellvol(new Double(fdm.getSellvol() + ts.getSellvol()));
				            fdm.setSellval(new Double(fdm.getSellval() + ts.getSellval()));
				            fdm.setSellfreq(new Double(fdm.getSellfreq() + ts.getSellfreq()));
						} else {
							fdm.setBuyvol(new Double(fdm.getBuyvol() - tsmi.getBuyvol() +ts.getBuyvol()));
				            fdm.setBuyval(new Double(fdm.getBuyval() - tsmi.getBuyval() + ts.getBuyval()));
				            fdm.setBuyfreq(new Double(fdm.getBuyfreq() - tsmi.getBuyfreq() + ts.getBuyfreq()));                
				            fdm.setSellvol(new Double(fdm.getSellvol() - tsmi.getSellvol() + ts.getSellvol()));
				            fdm.setSellval(new Double(fdm.getSellval() - tsmi.getSellval() + ts.getSellval()));
				            fdm.setSellfreq(new Double(fdm.getSellfreq() - tsmi.getSellfreq() + ts.getSellfreq()));    
						}
					}
//				log.info(fdm.toString());
				tsStockInvMobile.put(ts.getStock()+ts.getInvestor(), fdm);
				addSnapShot(fdm);
				}
				
				TradeSummStockMarketInv tsmi = (TradeSummStockMarketInv) tsStockMarketInv
						.get(
								ts.getStock()
								+ ts.getBoard()
								+ ts.getBroker()
								+ ts.getInvestor());
				if (tsmi == null) {
					tsmi = new TradeSummStockMarketInv();
				}
				tsmi.setContent(data);
				tsmi.setType("TSMI");
				tsStockMarketInv.put(tsmi.getStock()
						+ ((TradeSummary) dat).getBoard()
						+ ((TradeSummary) dat).getBroker() + tsmi.getInvestor()
						, tsmi);
				addSnapShot(tsmi);
				service.rcvOther(tsmi);
				
				
			} else if (data[1].equals("TSBB")) {
				dat = new TradeSummBroker();
				dat.setContent(data);
				TradeSummBroker ts = (TradeSummBroker) tsBroker
						.get(((TradeSummBroker) dat).getBroker());
				if (ts == null) {
					ts = new TradeSummBroker();
				}
				ts.setContent(data);
				tsBroker.put(ts.getBroker(), ts);
				addSnapShot(ts);
			} else if (data[1].equals("TSBS")) {
				dat = new TradeSummBrokerStock();
				dat.setContent(data);
				TradeSummBrokerStock ts = (TradeSummBrokerStock) tsBrokerStock
						.get(((TradeSummBrokerStock) dat).getBroker()
								+ ((TradeSummBrokerStock) dat).getStock());
				if (ts == null) {
					ts = new TradeSummBrokerStock();
				}
				ts.setContent(data);
				tsBrokerStock.put(ts.getBroker() + ts.getStock(), ts);
				addSnapShot(ts);
				
				//Mobile
				TradeSummBrokerStockMobile tsm = (TradeSummBrokerStockMobile) tsBrokerStockMobile.get(((TradeSummBrokerStock) dat)
						.getBroker() + ((TradeSummBrokerStock) dat).getStock());
				if (tsm == null) {
					tsm = new TradeSummBrokerStockMobile();
				}
				tsm.setContent(data);
				tsm.setType("TSBSM");
				tsBrokerStockMobile.put(tsm.getBroker()+tsm.getStock(), tsm);
				addSnapShot(tsm);
			} else if (data[1].equals("TSSB")) {
				dat = new TradeSummStockBroker();
				dat.setContent(data);
				TradeSummStockBroker ts = (TradeSummStockBroker) tsStockBroker
						.get(((TradeSummStockBroker) dat).getStock()
								+ ((TradeSummStockBroker) dat).getBroker());
				if (ts == null) {
					ts = new TradeSummStockBroker();
				}
				ts.setContent(data);
				tsStockBroker.put(ts.getStock() + ts.getBroker(), ts);
				addSnapShot(ts);
				
				//broksum
				BrokerSummarry bs = (BrokerSummarry) tsBrokerSumm
						.get(((TradeSummStockBroker) dat).getStock()
								+ ((TradeSummStockBroker) dat).getBroker());
				if (bs == null) {
					bs = new BrokerSummarry();
				}
				bs.setContent(data);
				bs.setType("BS");
				tsBrokerSumm.put(bs.getStock() + bs.getBroker(), bs);
				addSnapShot(bs);
				
				//Mobile
				TradeSummStockBrokerMobile tsm = (TradeSummStockBrokerMobile) tsStockBrokerMobile.get(((TradeSummStockBroker) dat)
						.getStock() + ((TradeSummStockBroker) dat).getBroker());
				if (tsm == null) {
					tsm = new TradeSummStockBrokerMobile();
				}
				tsm.setContent(data);
				tsm.setType("TSSBM");
				tsStockBrokerMobile.put(tsm.getStock()+tsm.getBroker(), tsm);
				addSnapShot(tsm);
				
			} else if (data[1].equals("TSI")) {
				dat = new TradeSummInv();
				dat.setContent(data);
				TradeSummInv ts = (TradeSummInv) tsInv.get(((TradeSummInv) dat)
						.getInvestor());
				if (ts == null) {
					ts = new TradeSummInv();
				}
				ts.setContent(data);
				tsInv.put(ts.getInvestor(), ts);
				addSnapShot(ts);
			} else if (data[1].equals("TSSI")) {
				dat = new TradeSummStockInv();
				dat.setContent(data);
				TradeSummStockInv ts = (TradeSummStockInv) tsStockInv
						.get(((TradeSummStockInv) dat).getStock()
								+ ((TradeSummStockInv) dat).getInvestor());
				if (ts == null) {
					ts = new TradeSummStockInv();
				}
				ts.setContent(data);
				tsStockInv.put(ts.getStock() + ts.getInvestor(), ts);
				addSnapShot(ts);
			} else if (data[1].equals("TH")) {
				/*
				 * dat = new Trade(); dat.setContent(data); if
				 * (dat.getSeqno()>=msg.getSeqno()){ addSnapShot(dat); }
				 */
				dat = new Trade();
				dat.setContent(data);
				// if (dat.getSeqno()>=msg.getSeqno()){

				Trade tr = (Trade) dat;

				Vector<Trade> v = (Vector<Trade>) tradeHistory.get(tr
						.getStock());
				if (v == null) {
					v = new Vector<Trade>();
					tradeHistory.put(tr.getStock(), v);
				}

				v.add(tr);

				addSnapShot(dat);
				//mobile trade paging 
				Message dt = new TradeMobile();
				dt.setContent(data);
				dt.setType("MTH");
				TradeMobile trm = (TradeMobile) dt;

				Vector<TradeMobile> vm = (Vector<TradeMobile>) tradeHistoryMobile.get(trm
						.getStock());
				if (vm == null) {
					vm = new Vector<TradeMobile>();
					tradeHistoryMobile.put(trm.getStock(), vm);
				}

				vm.add(trm);
				service.rcvOther(dt);
				addSnapShot(dt);
			}
			if (dat.getSeqno() >= msg.getSeqno()) {
				msg.setSeqno((long) dat.getSeqno());
				db.putMsg(msg);
			}
			service.rcvOther(dat);
		} catch (Exception ex) {
			System.out.println("error while processing: " + message);
			ex.printStackTrace();
		}
	}

	public boolean connect() throws Exception {
		boolean r = super.connect();
		// timer.start();
		log.info("start");
		tm.start(); // open for baru 04102013
		return r;
	}

	public void exit() throws Exception {
		// timer.stop();
		tm.stop(); // open for baru 04102013
		provider.unsubscribe(this);
	}

	public class timerdatabase extends Thread {
		@Override
		public void run() {
			while (!terminated) {
				log.info("mulai timerdatabase");
				try {
					
					selectArticleMobile();
					selectSector();
					selectCurrency();
					selectCurrencyMobile();
					selectIndices();
					selectIndicesMobile();
					selectArticle();
					selectCommodity();
					selectFuture();
					selectCorpAction();
					selectIPO();
					selectRups();
					selectStockNews();
					//selectStockNewsStockDetail();
					selectBySector();
					synchronized (this) {
						
						this.wait(1000 * 60 * 1); //this.wait(60 * 1000 * 1);  == 1 menit
					}
					log.info("selesai timer");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}