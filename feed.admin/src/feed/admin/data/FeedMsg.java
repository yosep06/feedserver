package feed.admin.data;

public class FeedMsg {
	    private String type;
		private String msg;
		private long seqno;
		private int time;
		
		public FeedMsg(){}
		
		public FeedMsg(String type, String msg, long seqno){
			this.type = type;
			this.msg = msg;
			this.seqno = seqno;
		}
		
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public String getMsg() {
			return msg;
		}
		public void setMsg(String msg) {
			this.msg = msg;
		}
		public long getSeqno() {
			return seqno;
		}
		public void setSeqno(long seqno) {
			this.seqno = seqno;
		}
		
		public int getTime(){
			return time;
		}
		
		public void setTime(int time){
			this.time = time;
		}
}
