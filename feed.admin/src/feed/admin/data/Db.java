package feed.admin.data;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.db4o.Db4o;
import com.db4o.ObjectContainer;

public class Db {
	private ObjectContainer db = null;
	private final static SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
	private final Logger log = LoggerFactory.getLogger(getClass());
	
	public Db(String prefix){
		String session = format.format(new Date());
		log.info("opening database : "+"data/"+prefix+"-feed."+session+".db");
		db=Db4o.openFile("data/"+prefix+"-feed."+session+".db");		
	}
	
	public Db(String prefix, String session){
		log.info("opening database : "+"data/"+prefix+"-feed."+(session.equals("")? "db": session+".db"));
		db=Db4o.openFile("data/"+prefix+"-feed."+(session.equals("")? "db": session+".db"));		
	}
	
	public ObjectContainer getInstance(){
		return db;
	}
	
	public void test(){
		FeedMsg fm = new FeedMsg("0", "ini adalah testing feed message", 0);
		db.store(fm);
	}
	
	public void close(){
		try {
			log.info("closing database");
			db.close();
		} catch (Exception ex){
			ex.printStackTrace();
		}
	}
}
