package feed.admin;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public final class Console extends Thread {
	private final Logger log = LoggerFactory.getLogger(getClass());	
	private FeedAdminApp apps;
	private static final String C_VERSION = "1.0.0.Release1";
	private boolean stop;

	public Console(FeedAdminApp apps) {
		this.apps = apps;
		stop = false;
	}

	private void prompt() {
		System.out.println("");
		System.out.print("admin> ");
	}

	public void showVersion() {
		System.out.println("Version : "+C_VERSION);
	}

	private void showMenu() {
		welcomScreen();
		System.out.println("List of All Commands : ");
		System.out.println("\tCommands must appear first on line and end with enter ");
		System.out.println("");
		System.out.println("");
		System.out.println("help\t\tdisplay this help (menu).");
		System.out.println("version\t\tdisplay server  version.");
		System.out.println("client\t\tdisplay client list");
		System.out.println("gateway\t\tdisplay gateway list");
		System.out.println("kill\t\tkill user session ");
		System.out.println("ping\t\tping db connection");
		System.out.println("exit\t\texit server application");
		System.out.println("");
	}

	public void setstop() {
		stop = true;
	}

	public void run() {
		welcomScreen();
		prompt();
		BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
		do {
			try {
					String cmd = bufferedreader.readLine();
					if (cmd.toLowerCase().equals("start")){
						//apps.run();
					} else if (cmd.toLowerCase().equals("version")){
						showVersion();
					} else if (cmd.toLowerCase().equals("help")){
						showMenu();
					} else if (cmd.toLowerCase().equals("client")){
						apps.printClient();
					} else if (cmd.toLowerCase().equals("gateway")){
						apps.printGateway();
					} else if (cmd.toLowerCase().startsWith("kill")){
						apps.kill(cmd.split(" ")[1]);
					} else if (cmd.toLowerCase().equals("ping")){
						apps.ping();
					} else if (cmd.toLowerCase().equals("exit")){
						try {
							stop = true;
							System.exit(0);
						} catch (Exception ex){							
							ex.printStackTrace();
							System.out.println("error : stopping services");
							log.error("error stopping service", ex);
						}
					} else {
						System.out.println("bad command, please try again..");						
					}
			} catch (Exception exception) {
				System.out.println("bad command, please try again..");
			}
			prompt();
		} while (!stop);
		System.exit(0);
	}

	public void welcomScreen() {
		StringBuffer stringbuffer = new StringBuffer(100);		
		stringbuffer.append("\nWelcome to the Feed Admin. Commands end with enter ");
		stringbuffer.append("\nYour  Module id is FeedAdmin version : "+C_VERSION);
		stringbuffer.append("\nCreated by vollux.team@05MAY10 ");
		stringbuffer.append("\n");
		stringbuffer.append("\n Type 'help' for help (menu). ");
		stringbuffer.append("\n");
		System.out.print(stringbuffer);
		System.out.println("");
	}
}