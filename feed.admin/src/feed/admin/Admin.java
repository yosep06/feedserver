package feed.admin;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Vector;



public interface Admin extends Remote {
	public long register(AdminClient client, String intranetaddr, String internetaddr, int intranet, int internet, int maxconnection) throws RemoteException;
	public boolean unregister(long gtwid) throws RemoteException;
	public long login(long gtwid, String userid, String password) throws RemoteException;
	public boolean logout(long gtwid, long sessionid) throws RemoteException;
	public boolean heartbeat(long gtwid, long sessionid) throws RemoteException;
	public boolean changePwd(long gtwid, long sessionid, String userid, String oldpass, String newpass) throws RemoteException;
	public String getAvailableServer(int internet) throws RemoteException;
	//yosep 10122014 encforement password
	public Vector loginString(long gtwid, String userid, String password) throws RemoteException;
}
