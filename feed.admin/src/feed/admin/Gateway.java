package feed.admin;

public class Gateway {
	private long id;
	private String intranetAddr;
	private String internetAddr;
	private int intranet;
	private int internet;
	private int maxConn;
	private int currConn;
	private AdminClient stub;
	
	public Gateway(long id, String intranetAddr, String internetAddr, int intranet, int internet, int maxConn, AdminClient stub){
		this.id = id;
		this.intranetAddr = intranetAddr;
		this.internetAddr = internetAddr;
		this.intranet = intranet;
		this.internet = internet;
		this.maxConn = maxConn;
		this.currConn = 0;
		this.stub = stub;
	}
	
	public String toString(){
		StringBuffer sb = new StringBuffer("GTW: "+id+" max connection: "+maxConn+" and current connection: "+currConn+"\n");
		sb.append(intranet == 1 ? "available for intranet with addr "+intranetAddr+"\n" : "not available for intranet\n");
		sb.append(internet == 1 ? "available for internet with addr "+internetAddr+"\n" : "not available for internet\n");
		return sb.toString();
	}
	
	public void addClient(){
		currConn++;
	}
	
	public void removeClient(){
		currConn--;
	}
	
	public boolean isAvailable(){
		return currConn+1 <= maxConn;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getIntranetAddr() {
		return intranetAddr;
	}

	public void setInternalAddr(String intranetAddr) {
		this.intranetAddr = intranetAddr;
	}

	public String getInternetAddr() {
		return internetAddr;
	}

	public void setInternetAddr(String internetAddr) {
		this.internetAddr = internetAddr;
	}

	public int getIntranet() {
		return intranet;
	}

	public void setIntranet(int intranet) {
		this.intranet = intranet;
	}

	public int getInternet() {
		return internet;
	}

	public void setInternet(int internet) {
		this.internet = internet;
	}

	public int getMaxConn() {
		return maxConn;
	}

	public void setMaxConn(int maxConn) {
		this.maxConn = maxConn;
	}

	public int getCurrConn() {
		return currConn;
	}

	public void setCurrConn(int currConn) {
		this.currConn = currConn;
	}

	public AdminClient getStub() {
		return stub;
	}

	public void setStub(AdminClient stub) {
		this.stub = stub;
	}
}
