package feed.admin;

import java.util.Date;

public class Client {
	private String userid;
	private String pass;
	private long sessionid;
	private long gatewayid;
	private long lastActivity;
	
	public Client(long gatewayid, long sessionid, String userid, String pass){
		this.gatewayid = gatewayid;
		this.sessionid  = sessionid;
		this.userid = userid;
		this.pass = pass;
		lastActivity = new Date().getTime();
	}
	
	public String toString(){
		return "Client sessionid:"+sessionid+" gatewayid: "+gatewayid+" userid: "+userid + " last activity: "+lastActivity;
	}
	
	public void logActivity(){
		lastActivity = new Date().getTime();		
	}
	
	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public long getSessionid() {
		return sessionid;
	}

	public void setSessionid(long sessionid) {
		this.sessionid = sessionid;
	}

	public long getGatewayid() {
		return gatewayid;
	}

	public void setGatewayid(long gatewayid) {
		this.gatewayid = gatewayid;
	}

	public long getLastActivity() {
		return lastActivity;
	}

	public void setLastActivity(long lastActivity) {
		this.lastActivity = lastActivity;
	}
}
