package feed.admin;

import java.rmi.Remote;
import java.rmi.RemoteException;


public interface AdminClient extends Remote{
	public void heartbeat() throws RemoteException;
	public void kill(long sessionid) throws RemoteException;
}
