package feed.admin;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.PropertyConfigurator;

import com.eqtrade.database.AccessQueryLocator;
import com.eqtrade.database.FOData;
import com.eqtrade.database.QueryData.SQUERY;
import com.eqtrade.utils.properties.FEProperties;

import feed.admin.data.FeedDb;
import feed.admin.data.FeedMsg;

public class FeedAdminApp extends UnicastRemoteObject implements Admin {
	private final Log log = LogFactory.getLog(getClass());

	private static final long serialVersionUID = -5774347629058395431L;
	// private long sessionid=1;
	private long gtwid = 1;
	private static FileConfig config;
	private Vector gatewayList;
	private Vector gatewayIntranet;
	private Vector gatewayInternet;
	private Hashtable clientList;
	private long currGtwIntranet = 0;
	private long currGtwInternet = 0;
	private Database database;
	protected AccessQueryLocator accessData;
	private Connection conn = null;
	private FeedDb db;
	private FeedMsg msg;
	private final static SimpleDateFormat format = new SimpleDateFormat(
			"yyyyMMdd");

	public FeedAdminApp(int port) throws Exception {
		super(port);
		db = new FeedDb("session");
		db.start();
		gatewayList = new Vector(50, 5);
		gatewayIntranet = new Vector(50, 5);
		gatewayInternet = new Vector(50, 5);
		clientList = new Hashtable();
		msg = new FeedMsg();
		msg.setType("SETTING");
		List o = db.getDb().getInstance().queryByExample(msg);
		if (o.size() > 0) {
			msg = (FeedMsg) o.get(0);
			msg.setSeqno(msg.getMsg().equals(format.format(new Date())) ? msg
					.getSeqno() : 1);
			msg.setMsg(format.format(new Date()));
		} else {
			msg.setMsg(format.format(new Date()));
			msg.setSeqno(1);
		}
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				try {
					log.info("shutingdown FeedAdmin application");
					db.putMsg(msg);
					db.doStop();
				} catch (Exception ex) {
				}
			}
		});

	}

	public void printClient() {
		synchronized (clientList) {
			ArrayList keys = new ArrayList();
			keys.addAll(clientList.keySet());
			Collections.sort(keys);
			Iterator it = keys.iterator();
			while (it.hasNext()) {
				Long l = (Long) it.next();
				Client c = (Client) clientList.get(l);
				System.out.println(c.toString());
			}
		}
	}

	public void printGateway() {
		for (int i = 0; i < gatewayList.size(); i++) {
			Gateway g = (Gateway) gatewayList.elementAt(i);
			System.out.println(g.toString());
		}
	}

	public void ping() {
		try {
			String query = "select 1 as correct from tfo_user";
			ResultSet rec = null;
			Statement st = null;
			if (conn == null)
				conn = database.getConnection();
			st = conn.createStatement();
			rec = st.executeQuery(query);
			System.out.println("ping db OK!");
			rec.close();
			st.close();
		} catch (Exception ex) {
			try {
				System.out.println("db ping error: " + ex.getMessage());
				System.out.println("trying to reconnect...");
				conn = null;
				database.start(config.getProperty("database"));
				System.out.println("db back to live");
			} catch (Exception exy) {
				System.out.println(exy.getMessage());
				System.out.println("reconnecting failed, please ping again!");
			}
		}
	}

	public void start() throws Exception {
		Registry registry = LocateRegistry.createRegistry(Integer
				.parseInt(config.getProperty("port", "7000")));
		registry.rebind(config.getProperty("service", "admin"), this);
		database = new Database(); //yosep #23022015
		database.start(config.getProperty("database"));
		log.info("FeedAdmin ready to use on port: "
				+ config.getProperty("port"));
//
//		FEProperties fep = new FEProperties("gtw.properties");
//		accessData = new AccessQueryLocator(fep.get("database"),true);
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				log.info("shuting down application");
			}
		});
	}

	public static void main(String[] args) {
		try {
			config = new FileConfig("feed.admin.config");
			PropertyConfigurator.configure("log.properties");
			FeedAdminApp app = new FeedAdminApp(Integer.parseInt(config
					.getProperty("port", "7000")));
			app.start();
			new Console(app).start();

		} catch (Exception ex) {
			ex.printStackTrace();
			System.exit(-1);
		}
	}

	public String getAvailableServer(int internet) throws RemoteException {
		String addr = null;
		try {
			if (internet == 1) {
				if (currGtwInternet > gatewayInternet.size())
					currGtwInternet = 0;
				while (addr == null && currGtwInternet < gatewayInternet.size()) {
					Gateway g = (Gateway) findGateway(((Long) gatewayInternet
							.elementAt((int) currGtwInternet)).longValue());
					if (g.isAvailable())
						addr = g.getInternetAddr();
					currGtwInternet++;
				}
				;
			} else {
				if (currGtwIntranet > gatewayIntranet.size())
					currGtwIntranet = 0;
				while (addr == null && currGtwIntranet < gatewayIntranet.size()) {
					Gateway g = (Gateway) findGateway(((Long) gatewayIntranet
							.elementAt((int) currGtwIntranet)).longValue());
					if (g.isAvailable())
						addr = g.getIntranetAddr();
					currGtwIntranet++;
				}
				;
			}
		} catch (Exception ex) {
			log.error(Utils.logException(ex));
			ex.printStackTrace();
		}
		return addr;
	}

	public boolean heartbeat(long gtwid, long sessionid) throws RemoteException {
		synchronized (clientList) {
			Client c = (Client) clientList.get(new Long(sessionid));
			if (c != null) {
				c.logActivity();
				return true;
			}
			return false;
		}
	}

	private SimpleDateFormat sessFormat = new SimpleDateFormat("ddMMyyHHmmss");

	public synchronized long login(long gtwid, String userid, String password)
			throws RemoteException {
		ping();
		if (userid.equals("dummy")) {
			long id = msg.getSeqno();
			msg.setSeqno(msg.getSeqno() + 1);
			db.putMsg(msg);

			return id;
		}else if (validUser(userid, password)) {
			// long id = Long.parseLong(sessFormat.format(new Date())) +
			// sessionid++;
			long id = msg.getSeqno();
			msg.setSeqno(msg.getSeqno() + 1);
			db.putMsg(msg);
			Client c = new Client(gtwid, id, userid, password);
			kill(userid);
			clientList.put(new Long(id), c);
			log.info("user : " + userid + " login from gateway " + gtwid
					+ " with sessionid: " + id);
			return id;
		}  else {
			log.error("user :" + userid
					+ " invalid userid or password login from gateway " + gtwid);
			return -1;
		}
	}

	//yosep 10122014 encforement password
	@Override
	public synchronized Vector loginString(long gtwid, String userid, String password)
			throws RemoteException {
//		ping();
		Vector login = new Vector();
		try {
			FOData fodata = new FOData();

			fodata.set_string(userid, password);
			Vector<Object> v = null;
			v = accessData.getQueryData().set_insert(SQUERY.logonfeed.val,
					fodata);
			int psukses = (Integer) v.elementAt(0);
			String pmsg = (String) v.elementAt(1);
			String pnext_action = (String) v.elementAt(2);
			if (psukses == 1) {
				long id = msg.getSeqno();
				msg.setSeqno(msg.getSeqno() + 1);
				db.putMsg(msg);
				Client c = new Client(gtwid, id, userid, password);
				kill(userid);
				clientList.put(new Long(id), c);
				log.info("user : " + userid + " login from gateway " + gtwid
						+ " with sessionid: " + id);
				//seqno | sukses/Tidak | pesan | action client
//				login = id+"|"+psukses+"|"+pmsg+"|"+pnext_action;
				login.addElement(id);
				login.addElement(psukses);
				login.addElement(pmsg);
				login.addElement(pnext_action);
				return login;
				
			}else {
				log.error("user :" + userid
						+ " msg= " +pmsg+ " gtwid= " + gtwid+" sukses= "+psukses);
				//seqno | sukses/Tidak | pesan | action client
//				login = -1+"|"+psukses+"|"+pmsg+"|"+pnext_action;
				long c = new Long(-1);
				login.addElement(c);
				login.addElement(psukses);
				login.addElement(pmsg);
				login.addElement(pnext_action);
				return login;
			}
		} catch (Exception e) {
			// TODO: handle exception
//			login = -1+"|"+-1+"|"+e.getMessage();
			e.printStackTrace();
			login.addElement(-1);
			login.addElement(-1);
			login.addElement(e.getMessage());
			login.addElement(e.getMessage());
		}
		return login;
	}
	private boolean validUser(String userid, String password) {
		// String query =
		// "select 1 as correct from tfo_user  where upper(userid)=upper('%s') and password='%s' and isfeed='1' ";
		String query = "select count(*) as ada from tfo_user  where upper(userid)=upper('%s') and password='%s' and isfeed='1'";

		ResultSet rec = null;
		Statement st = null;
		boolean result = false;
		try {
			if (conn == null)
				conn = database.getConnection();
			st = conn.createStatement();
			String queryStr = String.format(query,
					new Object[] { userid.toLowerCase(), password });

			rec = st.executeQuery(queryStr);
			boolean brec = rec.next();
			int ada = rec.getInt("ada");
			// result = rec.next();
			result = ada > 0 ? true : false;
			log.info("ada:" + queryStr + ":" + ada + ":" + result);
		} catch (Exception sqle) {
			sqle.printStackTrace();
			try {
				conn.close();
			} catch (Exception ex) {
			}
			conn = null;
			try {
				database.start(config.getProperty("database"));
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} finally {
			try {
				rec.close();
			} catch (Exception ex) {
			}
			try {
				st.close();
			} catch (Exception ex) {
			}
		}
		return result;
	}

	private Gateway findGateway(long id) {
		Gateway result = null;
		for (int i = 0; i < gatewayList.size(); i++) {
			if (((Gateway) gatewayList.elementAt(i)).getId() == id) {
				return (Gateway) gatewayList.elementAt(i);
			}
		}
		return result;
	}

	protected void kill(String userid) {
		synchronized (clientList) {
			for (Iterator i = clientList.values().iterator(); i.hasNext();) {
				Client c = (Client) i.next();
				if (c.getUserid().equals(userid)) {
					Gateway g = findGateway(c.getGatewayid());
					if (g != null) {
						try {
							log.info("internal process, kill old userid: "
									+ userid + " on gateway " + g.getId()
									+ " with session: " + c.getSessionid());
							g.removeClient();
							g.getStub().kill(c.getSessionid());
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					}
					clientList.remove(new Long(c.getSessionid()));
					break;
				}
			}
		}
	}

	public boolean logout(long gtwid, long sessionid) throws RemoteException {
		synchronized (clientList) {
			for (Iterator i = clientList.values().iterator(); i.hasNext();) {
				Client c = (Client) i.next();
				if (c.getSessionid() == sessionid) {
					log.info("user " + c.getUserid() + " logout from gateway: "
							+ gtwid);
					Gateway g = (Gateway) findGateway(c.getGatewayid());
					if (g != null) {
						g.removeClient();
					}
					clientList.remove(new Long(c.getSessionid()));
					break;
				}
			}
			return true;
		}
	}

	public boolean changePwd(long gtwid, long sessionid, String userid,
			String oldpass, String newpass) throws RemoteException {
		synchronized (clientList) {
			for (Iterator i = clientList.values().iterator(); i.hasNext();) {
				Client c = (Client) i.next();
				if (c.getSessionid() == sessionid) {
					c.setPass(newpass);
					log.info("user " + c.getUserid()
							+ " has changed her/his password");
				}
			}
			return true;
		}
	}

	public long register(AdminClient client, String intranetaddr,
			String internetaddr, int intranet, int internet, int maxconnection)
			throws RemoteException {
		long id = gtwid++;
		log.info("new register gateway " + id + " " + intranetaddr + " | "
				+ internetaddr + " | " + intranet + " | " + internet + " | "
				+ maxconnection);
		if (intranet == 1)
			gatewayIntranet.addElement(new Long(id));
		if (internet == 1)
			gatewayInternet.addElement(new Long(id));
		Gateway g = new Gateway(id, intranetaddr, internetaddr, intranet,
				internet, maxconnection, client);
		gatewayList.addElement(g);
		return id;
	}

	public boolean unregister(long gtwid) throws RemoteException {
		log.info("gateway " + gtwid + " unregistered from admin");
		Gateway g = findGateway(gtwid);
		if (g != null) {
			if (g.getIntranet() == 1)
				gatewayIntranet.remove(new Long(gtwid));
			if (g.getInternet() == 1)
				gatewayInternet.remove(new Long(gtwid));
			gatewayList.remove(findGateway(gtwid));
			Iterator i = clientList.values().iterator();
			for (; i.hasNext();) {
				Client c = (Client) i.next();
				if (c.getGatewayid() == gtwid)
					clientList.remove(new Long(c.getSessionid()));
			}
			return true;
		}
		return false;
	}

}
