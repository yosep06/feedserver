package feed.admin;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

public  class FileConfig extends Properties {
	private static final long serialVersionUID = -8133947669792923190L;
	private String fname;

	public FileConfig(String s) throws Exception {
		fname = s;
		load();
	}

	public void setFilename(String s) {
		fname = s;
	}

	public String getFilename() {
		return fname;
	}
	
	public boolean save() {
		boolean flag = false;
		try {
			store(new FileOutputStream(fname), "CONFIGURATION HEADER");
			flag = true;
		} catch (Exception exception) {
		}
		return flag;
	}


	public void load() throws Exception{
		try {
				BufferedReader bufferedreader = new BufferedReader(new FileReader(fname));
				do {
					String s;
					if((s = bufferedreader.readLine()) == null) break;
					if(!s.startsWith("#") && !s.trim().equals("")) {
						int i = s.indexOf("=");
						String s1 = s.substring(0, i);
						String s2 = s.substring(i + 1, s.length());
						setProperty(s1.trim().toLowerCase(), s2);
					}
				} while(true);
				try{
					bufferedreader.close();
				}	catch(IOException ioexception) {
				}
		} catch (Exception ex){
			try {
				ex.printStackTrace(new PrintWriter(new FileOutputStream("error.log"), true));
			} catch (Exception exception) {
			}			
			throw ex;
		}
	}

	public String getProperty(String s, String s1) {
		return super.getProperty(s.trim().toLowerCase(), s1);
	}
}