package feed.builder.builder;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import feed.builder.consumer.FeedConsumer;
import feed.builder.core.Database;
import feed.builder.msg.StockSummary;
import feed.provider.core.FileConfig;

public class FeedBuilder {
	protected TradeBuilder tradeBuilder;
	protected QuoteBuilder quoteBuilder;
	protected OtherBuilder otherBuilder;
	protected FileConfig config;
	protected Hashtable stockSummary;
	protected Database database;
	private Logger log = LoggerFactory.getLogger(getClass());
	protected QueueBuilder queueBuilder;
	protected boolean isUsingQueue = false;

	public FeedBuilder(FeedConsumer consumer) throws Exception {
		stockSummary = new Hashtable();
		config = new FileConfig("feed.builder.config");
		// database = new Database();
		log.info("database...");
		// database.start(config.getProperty("database"));

		// selectSS();
		log.info("database started");
		log.info("other builder started");
		otherBuilder = new OtherBuilder(consumer, this, Integer.parseInt(config.getProperty("builderport")));
		log.info("other builder started");

		tradeBuilder = new TradeBuilder(consumer, this, Integer.parseInt(config.getProperty("builderport")));
		log.info("trade builder started");

		quoteBuilder = new QuoteBuilder(consumer, this, Integer.parseInt(config.getProperty("builderport")));
		log.info("quote builder started");

		/*--new multitrade
		 * quoteBuilder = new QuoteMultiThreadBuilder(consumer, this,
				Integer.parseInt(config.getProperty("builderport")), config);
		log.info("quote builder started");*/

		// queueBuilder = new QueueBuilder(consumer, this,
		// Integer.parseInt(config.getProperty("builderport")));
		// log.info("queue builder started");

	}

	public FeedBuilder(FeedConsumer consumer, boolean normi) throws Exception {
		stockSummary = new Hashtable();
		config = new FileConfig("feed.builder.config");

		log.info("other builder started");

		boolean isUsingQueue = Boolean.parseBoolean(config
				.getProperty("usingqueue"));
		log.info("using queue " + isUsingQueue);
		if (isUsingQueue) {

			queueBuilder = new QueueBuilder(consumer, this);
			this.isUsingQueue = isUsingQueue;

		} else {

			database = new Database();
			log.info("database...");
			database.start(config.getProperty("database"));

			// selectSS();
			log.info("database started");

			otherBuilder = new OtherBuilder(consumer, this);
			log.info("other builder started ");

			tradeBuilder = new TradeBuilder(consumer, this);
			log.info("trade builder started");

			quoteBuilder = new QuoteBuilder(consumer, this);
			log.info("quote builder started");
		}

		/*--new multitrade
		 * quoteBuilder = new QuoteMultiThreadBuilder(consumer, this,
				Integer.parseInt(config.getProperty("builderport")), config);
		log.info("quote builder started");*/

		// queueBuilder = new QueueBuilder(consumer, this,
		// Integer.parseInt(config.getProperty("builderport")));
		// log.info("queue builder started");

	}

	protected final static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

	protected void selectSS() {
		String query = "select stock, open, prev, board  from feed_stocksummary where transdate='%s' ";
		ResultSet rec = null;
		Connection conn = null;
		Statement st = null;
		try {
			conn = database.getConnection();
			st = conn.createStatement();
			String queryStr = String.format(query,
					new Object[] { format.format(new Date()) });
			rec = st.executeQuery(queryStr);
			StockSummary ss = null;
			while (rec.next()) {
				ss = new StockSummary();
				ss.setStock(rec.getString("stock"));
				ss.setBoard(rec.getString("board"));
				ss.setOpen(rec.getDouble("open"));
				ss.setPrev(rec.getDouble("prev"));
				updateOpen(ss);
			}
		} catch (Exception sqle) {
			sqle.printStackTrace();
		} finally {
			try {
				rec.close();
			} catch (Exception ex) {
			}
			try {
				st.close();
			} catch (Exception ex) {
			}
			try {
				conn.close();
			} catch (Exception ex) {
			}
		}
	}

	public void updateOpen(StockSummary ss) {
		String keys = ss.getStock() + "#" + ss.getBoard();
		StockSummary old = (StockSummary) stockSummary.get(keys);
		if (old != null) {
			if (old.getOpen() == 0 && ss.getOpen() != 0) {
				old.setOpen(ss.getOpen());
			}
			if (old.getPrev() == 0 && ss.getPrev() != 0) {
				old.setPrev(ss.getPrev());
			}
			stockSummary.put(keys, old);
		} else {
			StockSummary t = new StockSummary();
			t.setStock(ss.getStock());
			t.setBoard(ss.getBoard());
			t.setOpen(ss.getOpen());
			t.setPrev(ss.getPrev());
			stockSummary.put(keys, t);
		}
	}

	public StockSummary getSS(String keys) {
		return (StockSummary) stockSummary.get(keys);
	}

	public String getQuote(String quote, String board) {
		return quoteBuilder.getSnapShot(quote, board);
	}

	public String getTrade(String stock) {
		return otherBuilder.getTHSnapShot(stock, 10000);
	}

	public void printClient() {
		quoteBuilder.printClient();
		tradeBuilder.printClient();
		otherBuilder.printClient();
		// queueBuilder.printClient();
	}

	public void start() throws Exception {
		log.info("start");
		tradeBuilder.connect();
		quoteBuilder.connect();
		otherBuilder.connect();
		Registry registry = LocateRegistry.createRegistry(Integer.parseInt(config.getProperty("builderport")));
		registry.rebind(config.getProperty("tradebuilder"),tradeBuilder.getService());
		registry.rebind(config.getProperty("quotebuilder"),quoteBuilder.getService());
		registry.rebind(config.getProperty("otherbuilder"),otherBuilder.getService());
		// System.out.println("tes");
		log.info("started");
	}

	public void stop() {
		try {
			tradeBuilder.exit();
			quoteBuilder.exit();
			otherBuilder.exit();
			// queueBuilder.exit();
		} catch (Exception ex) {
		}
	}

	public TradeBuilder getTradeBuilder() {
		return tradeBuilder;
	}

	public QuoteBuilder getQuoteBuilder() {
		return quoteBuilder;
	}

	public OtherBuilder getOtherBuilder() {
		return otherBuilder;
	}

	public QueueBuilder getQueueBuilder() {
		return queueBuilder;
	}

}
