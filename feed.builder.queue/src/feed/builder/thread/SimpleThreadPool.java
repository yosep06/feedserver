package feed.builder.thread;

import java.util.AbstractSequentialList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;

import feed.provider.core.FileConfig;

public class SimpleThreadPool implements ThreadPooling {

	private int prio = Thread.NORM_PRIORITY;
	private LinkedList availWorkers = new LinkedList();
	private LinkedList busyWorkers = new LinkedList();
	private final Object nextRunnableLock = new Object();
	private int count = -1;
	private Logger log;
	private boolean handoffPending = false;
	private boolean isShutdown = false;

	private List workers;
	private ThreadGroup threadGroup;
	
	public SimpleThreadPool(FileConfig config) throws Exception{
		this.count = Integer.parseInt(config.get("maxpoolsize").toString());
		this.prio = Thread.NORM_PRIORITY;
		initialize();
	}

	public void initialize() throws Exception {

		if (workers != null && workers.size() > 0) // already initialized...
			return;

		if (count <= 0) {
			throw new Exception("Thread count must be > 0");
		}
		if (prio <= 0 || prio > 9) {
			throw new Exception("Thread priority must be > 0 and <= 9");
		}

		if (isThreadsInheritGroupOfInitializingThread()) {
			threadGroup = Thread.currentThread().getThreadGroup();
		} else {
			// follow the threadGroup tree to the root thread group.
			threadGroup = Thread.currentThread().getThreadGroup();
			ThreadGroup parent = threadGroup;
			while (!parent.getName().equals("main")) {
				threadGroup = parent;
				parent = threadGroup.getParent();
			}
			threadGroup = new ThreadGroup(parent, "SimpleThreadPool");
			if (isMakeThreadsDaemons()) {
				threadGroup.setDaemon(true);
			}
		}

		// create the worker threads and start them
		Iterator workerThreads = createWorkerThreads(count).iterator();
		while (workerThreads.hasNext()) {
			WorkerThread wt = (WorkerThread) workerThreads.next();
			wt.start();
			availWorkers.add(wt);
		}
	}

	public boolean runInThread(Runnable runnable) {
		if (runnable == null) {
			return false;
		}

		synchronized (nextRunnableLock) {

			handoffPending = true;

			// Wait until a worker thread is available
			while ((availWorkers.size() < 1) && !isShutdown) {
				try {
					nextRunnableLock.wait(500);
				} catch (InterruptedException ignore) {
				}
			}

			if (!isShutdown) {
				WorkerThread wt = (WorkerThread) availWorkers.removeFirst();
				busyWorkers.add(wt);
				wt.run(runnable);
			} else {
				// If the thread pool is going down, execute the Runnable
				// within a new additional worker thread (no thread from the
				// pool).
				WorkerThread wt = new WorkerThread(this, threadGroup,
						"WorkerThread-LastJob", prio, isMakeThreadsDaemons(),
						runnable);
				busyWorkers.add(wt);
				workers.add(wt);
				wt.start();
			}
			nextRunnableLock.notifyAll();
			handoffPending = false;
		}

		return true;
	}

	protected List createWorkerThreads(int count) {
		workers = new LinkedList();
		for (int i = 1; i <= count; ++i) {
			WorkerThread wt = new WorkerThread(this, threadGroup,
					getThreadNamePrefix() + "-" + i, getThreadPriority(),
					isMakeThreadsDaemons());
			workers.add(wt);
		}

		return workers;
	}

	private int getThreadPriority() {
		return this.prio;
	}

	private String getThreadNamePrefix() {
		return "WorkerSimplePool";
	}

	private boolean isMakeThreadsDaemons() {
		return true;
	}

	private boolean isThreadsInheritGroupOfInitializingThread() {
		return true;
	}

	class WorkerThread extends Thread {

		// A flag that signals the WorkerThread to terminate.
		private boolean run = true;

		private SimpleThreadPool tp;

		private Runnable runnable = null;

		private boolean runOnce = false;

		/**
		 * <p>
		 * Create a worker thread and start it. Waiting for the next Runnable,
		 * executing it, and waiting for the next Runnable, until the shutdown
		 * flag is set.
		 * </p>
		 */
		WorkerThread(SimpleThreadPool tp, ThreadGroup threadGroup, String name,
				int prio, boolean isDaemon) {

			this(tp, threadGroup, name, prio, isDaemon, null);
		}

		/**
		 * <p>
		 * Create a worker thread, start it, execute the runnable and terminate
		 * the thread (one time execution).
		 * </p>
		 */
		WorkerThread(SimpleThreadPool tp, ThreadGroup threadGroup, String name,
				int prio, boolean isDaemon, Runnable runnable) {

			super(threadGroup, name);
			this.tp = tp;
			this.runnable = runnable;
			if (runnable != null)
				runOnce = true;
			setPriority(prio);
			setDaemon(isDaemon);
		}

		/**
		 * <p>
		 * Signal the thread that it should terminate.
		 * </p>
		 */
		void shutdown() {
			synchronized (this) {
				run = false;
			}
		}

		public void run(Runnable newRunnable) {
			synchronized (this) {
				if (runnable != null) {
					throw new IllegalStateException(
							"Already running a Runnable!");
				}

				runnable = newRunnable;
				this.notifyAll();
			}
		}

		/**
		 * <p>
		 * Loop, executing targets as they are received.
		 * </p>
		 */
		public void run() {
			boolean ran = false;
			boolean shouldRun = false;
			synchronized (this) {
				shouldRun = run;
			}

			while (shouldRun) {
				try {
					synchronized (this) {
						while (runnable == null && run) {
							this.wait(500);
						}
					}

					if (runnable != null) {
						ran = true;
						runnable.run();
					}
				} catch (InterruptedException unblock) {
					// do nothing (loop will terminate if shutdown() was called
					try {
						getLog().error("Worker thread was interrupt()'ed.",
								unblock);
					} catch (Exception e) {
						// ignore to help with a tomcat glitch
					}
				} catch (Throwable exceptionInRunnable) {
					try {
						getLog().error("Error while executing the Runnable: ",
								exceptionInRunnable);
					} catch (Exception e) {
						// ignore to help with a tomcat glitch
					}
				} finally {
					synchronized (this) {
						runnable = null;
					}
					// repair the thread in case the runnable mucked it up...
					if (getPriority() != tp.getThreadPriority()) {
						setPriority(tp.getThreadPriority());
					}

					if (runOnce) {
						synchronized (this) {
							run = false;
						}
						clearFromBusyWorkersList(this);
					} else if (ran) {
						ran = false;
						makeAvailable(this);
					}

				}

				// read value of run within synchronized block to be
				// sure of its value
				synchronized (this) {
					shouldRun = run;
				}
			}

			// if (log.isDebugEnabled())
			try {
				getLog().debug("WorkerThread is shut down.");
			} catch (Exception e) {
				// ignore to help with a tomcat glitch
			}
		}

	}

	public Logger getLog() {
		return log;
	}

	public int blockForAvailableThreads() {
		synchronized (nextRunnableLock) {

			while ((availWorkers.size() < 1 || handoffPending) && !isShutdown) {
				try {
					nextRunnableLock.wait(500);
				} catch (InterruptedException ignore) {
				}
			}

			return availWorkers.size();
		}
	}

	protected void makeAvailable(WorkerThread wt) {
		synchronized (nextRunnableLock) {
			if (!isShutdown) {
				availWorkers.add(wt);
			}
			busyWorkers.remove(wt);
			nextRunnableLock.notifyAll();
		}
	}

	protected void clearFromBusyWorkersList(WorkerThread wt) {
		synchronized (nextRunnableLock) {
			busyWorkers.remove(wt);
			nextRunnableLock.notifyAll();
		}
	}

	@Override
	public void execute(Runnable runnable) {
		runInThread(runnable);
	}
}
