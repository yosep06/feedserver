package feed.builder.msg;

public class News extends Message{
	private String no;
	private String subject;
	private String title;
	private String content;
	
	public News(){}
	
	public String toString(){
		StringBuffer result = new StringBuffer(super.toString()).append(delimiter).append(no).append(delimiter).append(subject);
		result.append(delimiter).append(title);
		result.append(delimiter).append(content);
		return result.toString();
	}
	
	public void setContent(String[] data){
		super.setContent(data);
		no = data[3].trim();
		subject = data[4].trim();
		title = data[5].trim();
		content = data[6].trim();
	}
	
	public void fromProtocol(String[] data){
		super.fromProtocol(data);
		no = data[5].trim();
		subject = data[6].trim();
		title = data[7].trim();
		content = data[8].trim();		
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	
}
