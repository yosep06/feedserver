package feed.builder.msg;

public class StockComparisonBySector extends Message{
	private String id;
	private double der;
	private double roa;
	private double roe;
	private double npm;
	private double opm;
	private double eps;
	private double bv;
	/*private String lastupdate;*/
	private double sectorcode;
	private double close;
	private double per;
	
	public StockComparisonBySector(){
		
	}
	public String toString(){
		StringBuffer result = new StringBuffer(super.toString());
		result.append(delimiter).append(id);
		result.append(delimiter).append(format2.format(der));
		result.append(delimiter).append(format2.format(roa));
		result.append(delimiter).append(format2.format(roe));
		result.append(delimiter).append(format2.format(npm));
		result.append(delimiter).append(format2.format(opm));
		result.append(delimiter).append(format2.format(eps));
		result.append(delimiter).append(format2.format(bv));
		/*result.append(delimiter).append(lastupdate);*/
		result.append(delimiter).append(format.format(sectorcode));
		result.append(delimiter).append(format2.format(close));
		result.append(delimiter).append(format2.format(per));
		return result.toString();
	}
	public void setContent(String[] data){
		super.setContent(data);
		id=data[3].trim();
		der=Double.parseDouble(data[4]);
		roa=Double.parseDouble(data[5]);
		roe=Double.parseDouble(data[6]);
		npm=Double.parseDouble(data[7]);
		opm=Double.parseDouble(data[8]);
		eps= Double.parseDouble(data[9]);
		bv=Double.parseDouble(data[10]);
		/*lastupdate=data[11].trim();*/
		sectorcode=Double.parseDouble(data[11]);	
		close=Double.parseDouble(data[12]);
		per=Double.parseDouble(data[13]);
	}
	public void fromProtocol(String[] data){
		super.fromProtocol(data);
		id=data[5].trim();
		der=Double.parseDouble(data[6]);
		roa=Double.parseDouble(data[7]);
		roe=Double.parseDouble(data[8]);
		npm=Double.parseDouble(data[9]);
		opm=Double.parseDouble(data[10]);
		eps= Double.parseDouble(data[11]);
		bv=Double.parseDouble(data[12]);
		/*lastupdate=data[13].trim();*/
		sectorcode=Double.parseDouble(data[13]);
		close=Double.parseDouble(data[14]);
		per=Double.parseDouble(data[15]);
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Double getDer() {
		return der;
	}
	public void setDer(Double der) {
		this.der = der;
	}
	public Double getRoa() {
		return roa;
	}
	public void setRoa(Double roa) {
		this.roa = roa;
	}
	public Double getRoe() {
		return roe;
	}
	public void setRoe(Double roe) {
		this.roe = roe;
	}
	public Double getNpm() {
		return npm;
	}
	public void setNpm(Double npm) {
		this.npm = npm;
	}
	public Double getOpm() {
		return opm;
	}
	public void setOpm(Double opm) {
		this.opm = opm;
	}
	public Double getEps() {
		return eps;
	}
	public void setEps(Double eps) {
		this.eps = eps;
	}
	public Double getBv() {
		return bv;
	}
	public void setBv(Double bv) {
		this.bv = bv;
	}
	/*public String getLastupdate() {
		return lastupdate;
	}
	public void setLastupdate(String lastupdate) {
		this.lastupdate = lastupdate;
	}*/
	public Double getSectorcode() {
		return sectorcode;
	}
	public void setSectorcode(Double sectorcode) {
		this.sectorcode = sectorcode;
	}
	public Double getClose() {
		return close;
	}
	public void setClose(Double close) {
		this.close = close;
	}
	public Double getPer() {
		return per;
	}
	public void setPer(Double per) {
		this.per = per;
	}
	
	
	
}
