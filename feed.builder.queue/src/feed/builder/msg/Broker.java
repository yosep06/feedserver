package feed.builder.msg;

public class Broker extends Message{
	private String code;
	private String name;
	private String status;
	
	public Broker(){}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String toString(){
		return new StringBuffer(super.toString()).append(delimiter).append(code).append(delimiter).append(name).append(delimiter).append(status).toString();
	}
	
	public void setContent(String[] data){
		super.setContent(data);
		code = data[3].trim();
		name = data[4].trim();
		status = data[5].trim();
	}
	
	public void fromProtocol(String[] data){
		super.fromProtocol(data);
		code = data[5].trim();
		name = data[6].trim();
		status = data[7].trim();
	}
	
}
