package feed.builder.msg;

public class Price implements Comparable{
	private double price;
	private double lot;
	private double freq;
	
	public Price(double price, double lot, double freq){
		this.price = price;
		this.lot = lot;
		this.freq = freq;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getLot() {
		return lot;
	}

	public void setLot(double lot) {
		this.lot = lot;
	}

	public int compareTo(Object arg0) {
		return (int) (this.price - ((Price)arg0).getPrice()) ;
	}

	public double getFreq() {
		return freq;
	}

	public void setFreq(double freq) {
		this.freq = freq;
	}
}
