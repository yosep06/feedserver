package feed.builder.msg;

public class Order
  implements Comparable<Order>
{
  private String orderid;
  private String stockcode;
  private String board;
  private Double price;
  private Double lot;
  private Integer no;
  private String command;
  private String header = "queue";

  public Order(String orderid, String stockcode, String board, Double price, Double lot, Integer no, String command)
  {
    this.orderid = orderid;
    this.stockcode = stockcode;
    this.board = board;
    this.price = price;
    this.lot = lot;
    this.no = no;
    this.command = command;
  }

  public Order()
  {
	  
  }

  public void fromProtocol(String sord)
  {
    String[] sd = sord.split("\\|");
    this.stockcode = sd[0];
    this.board = sd[1];
    this.command = sd[2];
    this.orderid = sd[3];
    this.price = Double.valueOf(Double.parseDouble(sd[4]));
    this.lot = Double.valueOf(Double.parseDouble(sd[5]));
    this.no = Integer.valueOf(Integer.parseInt(sd[6]));
  }

  public String toStringReceiver()
  {
    return this.stockcode + "|" + this.board + "|" + this.command + "|" + this.orderid + "|" + 
      this.price + "|" + this.lot;
  }

  public String getOrderid() {
    return this.orderid;
  }

  public void setOrderid(String orderid) {
    this.orderid = orderid;
  }

  public String getStockcode() {
    return this.stockcode;
  }

  public void setStockcode(String stockcode) {
    this.stockcode = stockcode;
  }

  public String getBoard() {
    return this.board;
  }

  public void setBoard(String board) {
    this.board = board;
  }

  public Double getPrice() {
    return this.price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public Double getLot() {
    return this.lot;
  }

  public void setLot(Double lot) {
    this.lot = lot;
  }

  public Integer getNo() {
    return this.no;
  }

  public void setNo(Integer no) {
    this.no = no;
  }

  public boolean equals(Object obj)
  {
    Order ord = (Order)obj;
    return getOrderid().equals(ord.getOrderid());
  }

  public String getCommand() {
    return this.command;
  }

  public void setCommand(String command) {
    this.command = command;
  }

  public int compareTo(Order o)
  {
    return this.orderid.equals(o.getOrderid()) ? 0 : -1;
  }

  public String toString()
  {
    return this.header + "|" + this.stockcode + "|" + this.board + "|" + this.command + "|" + 
      this.orderid + "|" + this.price + "|" + this.lot + "|" + this.no;
  }
}