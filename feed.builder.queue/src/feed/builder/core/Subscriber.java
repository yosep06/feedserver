package feed.builder.core;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import feed.builder.msg.Message;
import feed.provider.core.MsgListener;
import feed.provider.data.FeedMsg;

public class Subscriber extends Thread{
	private MsgManager manager;
	private MsgListener listener;
	private boolean ready = false;
	private boolean needSnapShot = true;
	private int seqno;
	private Vector qeueu;
	private boolean bstop = false;
	private Logger log = LoggerFactory.getLogger(getClass());
	
	public Subscriber (MsgManager manager, MsgListener listener, int seqno){
		this.manager = manager;
		this.listener = listener;
		this.seqno = seqno;
		this.qeueu = new Vector(20,5);
	}
	
	
	public void setStop(){
		bstop = true;
        synchronized(qeueu){
        	qeueu.notify();
        }      
	}
	
	public void run(){
		while(!bstop){
				String msg = null;
				synchronized (qeueu) {
					if (qeueu.size()>0){
						msg = (String)qeueu.remove(0);
					}
				}
				if (msg!=null){
					newMessage(msg);
					Thread.yield();
				}else {
					synchronized(qeueu){
						try {qeueu.wait();} catch (Exception ex){}
					}
				}
		}
		//sent rest of queue
		while (qeueu.size()>0){
			String msg = (String)qeueu.remove(0);
			if (msg!=null) newMessage(msg);
			Thread.yield();
		}
	}
	
	public int login(){
		sendSnapShot();
		return 1;
	}
	
	private void sendSnapShot(){
		if (needSnapShot && !ready){
			new Thread(new Runnable(){
				public void run() {
					List snapshot = manager.getSnapShot(seqno);
					if (snapshot!=null) {
							for (int i=0;i<snapshot.size();i++){
								if (snapshot.get(i) instanceof FeedMsg){
									FeedMsg msg = (FeedMsg)snapshot.get(i);
									newMessage(msg.getMsg());
								} else {
									Message msg = (Message)snapshot.get(i);
									newMessage(msg.toString());
								}
								try { Thread.sleep(1);}catch (Exception ex){}
							}
					}
					setReady(true);
					Subscriber.this.start();
				}
			}).start();
		} else {
			this.start();
		}
	}
	
	public synchronized boolean isReady(){
		return ready;
	}
	
	public synchronized  void setReady(boolean ready){
		this.ready = ready;
	}
	
	public void addQueue(String message){
		synchronized (qeueu) {
			qeueu.addElement(message);
			qeueu.notify();
		}
	}
	
	private void newMessage(String message){
		try {
			listener.newMessage(message);
		} catch (RemoteException ex){
			qeueu.clear();
			this.setStop();
			manager.clientList.remove(listener);
			//log.error("exeception in subscriber ", ex);
		}
	}
	
	
}
