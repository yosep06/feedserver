package feed.builder.core;

import java.rmi.Naming;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import feed.provider.core.MsgProducer;

public class MsgConsumer extends MsgManager {
	private final Logger log = LoggerFactory.getLogger(getClass());

	protected MsgProducer provider;
	
	public MsgConsumer(String url, String type) throws Exception{
		super(url, type);
	}
		
	public boolean connect() throws Exception {
		provider = (MsgProducer) Naming.lookup(url);
		if (provider != null){
			provider.subscribe(this, "", "", (int)msg.getSeqno());
		}
		//log.info("connection to: "+url+" ready from seqno: "+msg.getSeqno());
		return provider!=null;
	}
	
	public void exit() throws Exception {
		provider.unsubscribe(this);
		this.close();
	}
}
