package feed.gateway.socket.core;

import feed.gateway.broadcaster.Client;

public class HistoryDataClient {
	private Class cls;
	private String param;
	private int seqno;
	private Client c;

	public HistoryDataClient(Class cls, String param, int seqno, Client c) {
		super();
		this.cls = cls;
		this.param = param;
		this.seqno = seqno;
		this.c = c;
	}

	public Class getCls() {
		return cls;
	}

	public void setCls(Class cls) {
		this.cls = cls;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public int getSeqno() {
		return seqno;
	}

	public void setSeqno(int seqno) {
		this.seqno = seqno;
	}

	public Client getC() {
		return c;
	}

	public void setC(Client c) {
		this.c = c;
	}

}