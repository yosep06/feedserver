package feed.gateway.socket.broadcaster;

import java.util.Vector;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import feed.provider.core.Utils;

public class MessageBroadcaster extends Thread {

	private FeedGatewayAppSocket feedApp;
	/*private int delay = 0;
	private boolean terminated = false;*/
	
	private int delay = 0, sizeclient = 6, qidx = 0;
	private boolean terminated = false;	
	private long diff = 0;
	
	private Logger log = LoggerFactory.getLogger(getClass());

	public MessageBroadcaster(FeedGatewayAppSocket feedApp) {
		this.feedApp = feedApp;
		delay = feedApp.getTimerMessageBroadcaster();
	}

	@Override
	public void run() {
		while (!terminated) {
			try {

				synchronized (this) {
					this.wait(delay);
				}

				Vector<ClientGtwSocket> vc = ((GtwServiceSocket) feedApp
						.getService()).getVclientsocket();

				Vector<ClientGtwSocket> vtemp = new Vector<ClientGtwSocket>();
				long start = System.nanoTime();
				synchronized (vc) {

					for (int i = 0; i < vc.size(); i++) {
						ClientGtwSocket cgt = vc.get(i);
						cgt.send();
						if (!cgt.isConnected()) {
							vtemp.add(cgt);
						}
					}

					if(vtemp.size()>0)
					vc.removeAll(vtemp);

					/*for (ClientGtwSocket cgt : vc) {
						try {
							cgt.send();
						} catch (Exception e) {
							e.printStackTrace();
						}

					}*/








				}
				
				long diff = System.nanoTime() - start;
				if (diff > this.diff)
					this.diff = diff;

			} catch (Exception e) {
				e.printStackTrace();
				log.error(Utils.logException(e));
			}

		}
	}

	public void setstop() {
		synchronized (this) {
			terminated = true;
			this.notify();
		}
	}
	
	public int getDelay() {
		return delay;
	}

	
	public void printInfo() {
		
		log.info("size_client_"
				+ ((GtwServiceSocket) feedApp.getService()).getVclientsocket()
						.size() + "_"
				+ TimeUnit.SECONDS.convert(diff, TimeUnit.NANOSECONDS) + " "
				+ delay);
	}

	public int getSizeclient() {
		return sizeclient;
	}

	public void reset() {
		diff = 0;
	}

}
