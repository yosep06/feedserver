package feed.gateway.socket.broadcaster;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import feed.gateway.socket.core.HistoryDataClient;

import com.eqtrade.ClientSocket;
import com.eqtrade.Receiver;
import com.eqtrade.SocketFactory;
import com.eqtrade.SocketInterface;
import com.eqtrade.netty.protocol.CompressionDecoder;
import com.eqtrade.netty.protocol.CompressionEncoder;

import feed.builder.msg.ChartIntraday;
import feed.builder.msg.MarketMobile;
import feed.builder.msg.Message;
import feed.builder.msg.Quote;
import feed.builder.msg.StockSummary;
import feed.builder.msg.StockSummaryMobile;
import feed.builder.msg.TopGainer;
import feed.builder.msg.Trade;
import feed.builder.msg.TradeMobile;
import feed.builder.msg.TradePrice;
import feed.builder.msg.TradeSummBrokerStock;
import feed.builder.msg.TradeSummBrokerStockMobile;
import feed.builder.msg.TradeSummStockBroker;
import feed.builder.msg.TradeSummStockBrokerMobile;
import feed.builder.msg.TradeSummStockMarketInv;
import feed.gateway.broadcaster.Client;
import feed.gateway.broadcaster.GtwService;
import feed.gateway.consumer.FeedConsumer;
import feed.gateway.core.Database;
import feed.gateway.core.FileConfig;

public class GtwServiceSocket extends GtwService implements Receiver {
	private Logger log = LoggerFactory.getLogger(getClass());
	private SocketInterface socketConnector;
	private Long sessionidtest=100000l;
	private HistoryTimer historyTimer;
	private Vector<ClientGtwSocket> vclientsocket = new Vector<ClientGtwSocket>();
	private SimpleDateFormat sdftime = new SimpleDateFormat("HHmmss");
	private Vector<byte[]> vtemp = new Vector<byte[]>();
	
	
	public GtwServiceSocket(FileConfig config, FeedConsumer consumer,Database database) throws Exception {
		super(config, consumer, database);
		socketConnector = SocketFactory.createSocket(config.getProperty("socket.receiver"), this);		
		socketConnector.start();
		
		historyTimer = new HistoryTimer(consumer);
		historyTimer.start();
	}

	public int login(ClientSocket sock, String userid, String pass) throws RemoteException {
		long idc = 0;
		try {
			idc = admin.login(id, userid, pass);
			log.info("user " + userid+ " login to this gateway with sessionid " + idc);
		if (idc != -1) {
			ClientGtwSocket c = new ClientGtwSocket((int) idc, userid, pass,sock);
			client.put(new Integer(c.getSessionid()), c);
			vclientsocket.add(c);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return (int) idc;
	}

	@Override
	public void connected(ClientSocket sock) {
	}

	@Override
	public void disconnect(ClientSocket sock) {
		log.info("client sessionid " + sock.getID() + " disconnected");
		try {
			ClientGtwSocket c = (ClientGtwSocket) client.remove(new Integer(Integer.parseInt(sock.getID())));
			
			if(c != null)
				vclientsocket.remove(c);
				logout(Integer.parseInt(sock.getID()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void rcvTrade(Trade msg) {

		Vector v = (Vector) request.get(msg.getType());
		if (v != null && v.size() != 0) {

			String trade = msg.toString(); 
			
			byte[] bt = CompressionEncoder.compress(trade.getBytes());
			
			Integer currenttime = Integer.parseInt(sdftime
					.format(new Date()));

			if ((currenttime >= 90000 && currenttime <= 90100)
					|| (currenttime >= 160000 && currenttime <= 160100)) {
				Integer tradetime = Integer.parseInt(msg.getTradetime());
				
				if ((currenttime - tradetime) <= 10) {
					
					synchronized (v) {
						for (int i = 0; i < v.size(); i++) {
							Integer sessionid = (Integer) v.elementAt(i);
							Client client = getClient(sessionid.intValue());
							if (client != null)
								client.addTrade(bt);
						}
					}
					
				} else {
					vtemp.add(bt);

					if (vtemp.size() >= 60) {
						synchronized (vtemp) {
							List<byte[]> l = vtemp.subList(
									vtemp.size() - 20, vtemp.size());
							
								
								synchronized (v) {
									for (int i = 0; i < v.size(); i++) {
										Integer sessionid = (Integer) v.elementAt(i);
										Client client = getClient(sessionid.intValue());
										if (client != null)
											client.addListTrade(l);
									}
								}
								
							
							
							vtemp.clear();

						}
					}

				}
				
			}else {
				synchronized (v) {
					for (int i = 0; i < v.size(); i++) {
						Integer sessionid = (Integer) v.elementAt(i);
						Client client = getClient(sessionid.intValue());
						if (client != null)
							client.addTrade(bt);
					}
				}
			}
			
			
			

			
		}

		
	}

	@Override
	public void receive(ClientSocket sock, byte[] bt) {
//		System.out.println("bt : "+bt);
		
		String msg = new String(CompressionDecoder.decompress(bt));
		
//		System.out.println("msg : "+msg);
		
		String[] data = msg.split("\\*");
		String response = "";
		
		//System.out.println("data 1 : "+data[1] + " " + "data 2 : " + data[2]);
		
		try {
			if (msg.startsWith("login")) {
				System.out.println("data 1 : "+data[1] + " " + "data 2 : " + data[2]);
				int idc = login(sock, data[1], data[2]);
				
				if (idc != -1) {
					sock.setID(idc + "");
					sock.validated(true);
				}
				response = data[0] + "|" + idc;
			} else if (msg.startsWith("timestamp")){//yosep request mobile server time
				response = "timestamp|"+sdftime.format(new Date());
			} else if (msg.startsWith("test")) {
				data = msg.split("\\|");
				
				if (data[1].equals("login")) {
					String userid = data[2];
					sessionidtest++;					
					sock.setID(sessionidtest + "");
					sock.validated(true);
					ClientGtwSocket c = new ClientGtwSocket((int) sessionidtest.longValue(), userid, "", sock);
					client.put(new Integer(c.getSessionid()), c);					
					vclientsocket.add(c);	
					response = data[1] + "|"+data[2]+"|" + c.getSessionid();
					log.info("user test " + userid+ " login to this gateway with sessionid "+ sessionidtest);
				}

			} else {
				int sessionid = Integer.parseInt(data[1]);
				if (msg.startsWith("subscribe")) {
					subscribe(sessionid, data[2]);
				} else if (msg.startsWith("unsubscribe")) {
					unsubscribe(sessionid, data[2]);
				} else if (msg.startsWith("heartbeat")) {
					boolean istrue = heartbeat(sessionid);
				} else if (msg.startsWith("history")) {
					String hist = getHistoryString(sessionid, data[2]);
					response = data[0] + "|" + hist;
				} else if (msg.startsWith("history.file")) {
					String hist = getHistoryString(sessionid, data[2]);
					response = data[0] + "|" + hist;
				} else if (msg.startsWith("brokerinfo")) {
					String hist = getHistoryString(sessionid, data[2]);
					response = data[0] + "|" + hist;					
//				} else if(msg.startsWith("yearinfo")) {
//					String hist = getHistoryString(sessionid, data[2]);
//					response = data[0] + "|" + hist;		
				} else if (msg.startsWith("date")) {
					String sdate = getDate(); 
					response = data[0] + "|" + sdate;
				} else if (msg.startsWith("time")) {
					long time = getTime();
					response = data[0] + "|" + time;
				} else if (msg.startsWith("chgPassword")) {
					boolean ischange = chgPassword(sessionid, data[2], data[3],
							data[4]);
					response = data[0] + "|" + ischange;
				} else if (msg.startsWith("logout")) {
					logout(sessionid);
				} else if (msg.startsWith("queue")) {
					byte[] bt2 = getQueue(sessionid, data[2]);
					if (bt2 != null)
						sock.sendMessage(bt2);
				} else {
					log.error("cannot process this message " + msg);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();			
			if (ex.getMessage().contains("expired") ) {
				data[0] = "heartbeat";
			}
			response = data[0] + "|" + ex.getMessage();
		}
		if (!response.isEmpty()) {
			sock.sendMessage(CompressionEncoder.compress(response.getBytes()));
		}
	}

	public String getHistoryString(int sessionid, String msg)
			throws RemoteException {
		Client c = (Client) client.get(new Integer(sessionid));
		if (c != null) {
			if (msg.startsWith("FILE")) {
				byte[] result = consumer.getOtherConsumer().readFile(msg);
				return new String(result);
			} else if (msg.startsWith("BROKER")) {
				return consumer.getOtherConsumer().getBrokerInfo();
//			} else if (msg.startsWith("GETYEAR")) {
//				return consumer.getOtherConsumer().getYearMarketInfo();
			} else {
				String result = consumer.getOtherConsumer().selectHistory(msg);
				return result;
			}
		} else {
			throw new RemoteException("session expired or killed");
		}

	}

	public boolean heartbeat(int sessionid) throws RemoteException {
		Client c = (Client) client.get(new Integer(sessionid));
		if (c != null) {
			c.kick();
			return true;
		} else {
			throw new RemoteException("session expired or killed");
		}
	}

	@Override
	public void kill(long arg0) throws RemoteException {
		ClientGtwSocket c = (ClientGtwSocket) client.remove(new Integer(
				(int) arg0));
		if (c != null) {
			vclientsocket.remove(c);

			c.logout();
			log.warn("user " + c.getUserid() + " with sessionid " + arg0
					+ " has been killed by server");
			unsubscribeAll((int) arg0);
			c.kick();
			clientKill.put(new Integer((int) arg0), c);
			c.getSocketInterface().sendMessage(
					CompressionEncoder.compress(new String(
							"kill|session expired or killed").getBytes()));
		}
	}
	
	public Vector<ClientGtwSocket> getVclientsocket() {
		return vclientsocket;
	}

	@Override
	public void printClient() {
		ArrayList keys = new ArrayList();
		keys.addAll(client.keySet());
		Collections.sort(keys);
		Iterator it = keys.iterator();
		while (it.hasNext()) {
			Integer l = (Integer) it.next();
			Client c = (Client) client.get(l);
			System.out.println(c.toString());
		}
		System.out.println();
		System.out.println("Total Users :" + keys.size());
		System.out.println("Total Users socket :" + vclientsocket.size());
		
	}
	
	/*@Override
	public void rcvTrade(Trade msg) {
		Vector v = (Vector) request.get(msg.getType());
		if (v != null && v.size() != 0) {			
			String trade = msg.toString();
			byte[] bt = CompressionEncoder.compress(trade.getBytes());
			synchronized (v) {
				for (int i = 0; i < v.size(); i++) {
					Integer sessionid = (Integer) v.elementAt(i);
					Client client = getClient(sessionid.intValue());
					if (client != null)
						client.addTrade(bt);
				}
			}
		}
	}*/

	@Override
	public void rcvQuote(Quote msg) {
		Vector v = (Vector) request.get(msg.getType() + "|" + msg.getStock()
				+ "#" + msg.getBoard());
		String quote = "";
		if (v != null && v.size() != 0) {
			quote = msg.toString();
			//log.info("quote broadcast "+msg.getSeqno()+" "+v.size());
			byte[] bt = CompressionEncoder.compress(quote.getBytes());
			
			synchronized (v) {
				for (int i = 0; i < v.size(); i++) {
					Integer sessionid = (Integer) v.elementAt(i);
					Client client = getClient(sessionid.intValue());
					if (client != null)
						client.addQuote(bt);
				}
			}
		}
	}
	
	@Override
	public void sendOtherSnapShot(Class cls, String param, int seqno, Client c) {		
		List l = null;
		long start = System.nanoTime();
		
		if (cls.equals(TradePrice.class)) {
			l = consumer.getOtherConsumer().getTPSnapShot(param, seqno);
		} else if (cls.equals(Trade.class)) {
			historyTimer.addHistoryClient(new HistoryDataClient(cls, param, seqno,
					c));
		} else if (cls.equals(TradeSummBrokerStock.class)) {
			l = consumer.getOtherConsumer().getTSBSSnapShot(param, seqno);
		} else if (cls.equals(TradeSummStockBroker.class)) {
			l = consumer.getOtherConsumer().getTSSBSnapShot(param, seqno);
        } 
		
		//#Valdhy 20141219
		//--- Mobile Section - Start ---
		//Stock Summary Mobile
		else if (cls.equals(StockSummaryMobile.class)) {
			System.out.println("Stock Summary Mobile somplak");
			l = consumer.getOtherConsumer().getSSMSnapShot(param, seqno);
		} 
		//Market Mobile
		else if (cls.equals(MarketMobile.class)) {
			System.out.println("Market Mobile somplak");
			l = consumer.getOtherConsumer().getMMSnapShot();
		} 
		//History Intraday Mobile
		else if (cls.equals(ChartIntraday.class)) {
			System.out.println("History Intraday Mobile somplak");
			l = consumer.getOtherConsumer().selectChartIntradayMobile("COMPOSITE");
		} 
		//Trade Broker by Stock Mobile
		else if (cls.equals(TradeSummBrokerStockMobile.class)) {
			System.out.println("Trade Broker by Stock Mobile somplak");
			l = consumer.getOtherConsumer().getTSBSMSnapShot(param, seqno);
		} 
		//Trade Stock by Broker Mobile
		else if (cls.equals(TradeSummStockBrokerMobile.class)) {
			System.out.println("Trade Stock by Broker Mobile somplak");
			l = consumer.getOtherConsumer().getTSSBMSnapShot(param, seqno);
		} 
		else if (cls.equals(TradeMobile.class)){
			historyTimer.addHistoryClient(new HistoryDataClient(cls, param, seqno, c));
		}	
		else if (cls.equals(TopGainer.class)){
			historyTimer.addHistoryClient(new HistoryDataClient(cls, param, seqno, c));
		}		
		
		//--- Mobile Section - End ---
		
		else {
			l = consumer.getOtherConsumer().getSnapShot(cls, seqno);
		}
		if (l != null) {
			for (int i = 0; i < l.size(); i++) {
				c.addOther(((Message) l.get(i)).toString());
			}
			
			if (cls.equals(TradeSummBrokerStockMobile.class) || cls.equals(TradeSummStockBrokerMobile.class))
			{	
				c.addOther("##End##");
			}
			
			else if(cls.equals(ChartIntraday.class)){
				ChartIntraday ci = new ChartIntraday();
				ci.setHeader("IDX");
				ci.setType("MHI");
				ci.setTranstime("##End##");
				ci.setCode("##End##");
				c.addOther(ci.toString());
			}
			
			long end = System.nanoTime() - start;
			log.info("send_snaphot_" + cls + "_" + param + "_" + l.size() + "_"
					+ TimeUnit.MILLISECONDS.convert(end, TimeUnit.NANOSECONDS));
		}
	}

	@Override
	public void rcvOther(Message msg) {
		//System.out.println("rcvOther GtwServiceSocket");
		String req;
		if (msg instanceof TradePrice) {
			req = msg.getType() + "|" + ((TradePrice) msg).getStock() + "#"
					+ ((TradePrice) msg).getBoard();
		} else if (msg instanceof Trade) {
			req = msg.getType() + "|" + ((Trade) msg).getStock();
		} else if (msg instanceof TradeSummBrokerStock) {
			req = msg.getType() + "|"
					+ ((TradeSummBrokerStock) msg).getBroker();
		} else if (msg instanceof TradeSummStockBroker) {
			req = msg.getType() + "|" + ((TradeSummStockBroker) msg).getStock();
		} else if (msg instanceof TradeMobile){
			req = msg.getType() + "|" + ((TradeMobile)msg).getStock();
		} else {		
			req = msg.getType();
		}
		Vector v = (Vector) request.get(req);
		if (v != null && v.size() != 0) {
			String other = msg.toString();
			byte[] bt = CompressionEncoder.compress(other.getBytes());
			
			synchronized (v) {
				for (int i = 0; i < v.size(); i++) {
					Integer sessionid = (Integer) v.elementAt(i);
					Client client = getClient(sessionid.intValue());
					if (client != null)
						client.addOther(bt);
				}
			}
		}
	}

	@Override
	public void receive(ClientSocket arg0, Object arg1) {
		// TODO Auto-generated method stub
		
	}
}