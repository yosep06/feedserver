package feed.gateway.socket.broadcaster;

import java.util.Vector;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eqtrade.netty.protocol.CompressionEncoder;

import feed.gateway.broadcaster.Client;

public class ClientGtwSocket extends Client {
	private com.eqtrade.ClientSocket socketInterface;
	private Logger log = LoggerFactory.getLogger(getClass());
	private Long delayMsg = 0l, delaySend = 0l, sizeMsgCompress1 = 0l,
			delayMsgByte = 0l, sizeMsgNotCompress1 = 0l, sizeMsgCompress2 = 0l,
			sizeMsgNotCompress2 = 0l, sizeMsgByte = 0l;

	public ClientGtwSocket(int sessionid, String userid, String pass, com.eqtrade.ClientSocket s) {
		super(sessionid, userid, pass);
		this.socketInterface = s;		
	}

	public byte[] getMessage() {
		kick();
		StringBuffer temp = new StringBuffer();
		synchronized (vtrade) {			
			int start = vtrade.size() > 30 ? vtrade.size() - 30 : 0;
			for (int i = 0; vtrade.size() > start && i < 30; i++) {
				temp.append(vtrade.remove(start).toString().concat("\n"));
			}
			vtrade.clear();
		}
		synchronized (vquote) {
			for (int i = 0; vquote.size() > 0 && i < 50; i++) {
				temp.append(vquote.remove(0).toString().concat("\n"));
			}
		}
		/*synchronized (vother) {		
			int start = vother.size() > 30 ? vother.size() - 30 : 0;
			for (int i=0; vother.size()>start && i<30;i++) {	
				temp.append(vother.remove(start).toString().concat("\n"));
			}			
		}*/
		synchronized (vother) {			
			for (int i=0; vother.size()>0 && i<30;i++) {	
				temp.append(vother.remove(0).toString().concat("\n"));
			}			
		}	
		synchronized (vqueue) {
			for (int i = 0; vqueue.size() > 0 && i < 50; i++) {
				temp.append(vqueue.remove(0).toString().concat("\n"));
			}
		}

		return (temp.length() > 0) ? CompressionEncoder.compress(temp.toString().getBytes()) : null;
	}
	
	public byte[] getMessageString() {
		kick();
		StringBuffer temp = new StringBuffer();
		synchronized (vtrade) {			
			int start = vtrade.size() > 30 ? vtrade.size() - 30 : 0;
			for (int i = 0; vtrade.size() > start && i < 30; i++) {
				temp.append(vtrade.remove(start).toString().concat("\n"));
			}
			vtrade.clear();
		}
		synchronized (vquote) {
			for (int i = 0; vquote.size() > 0 && i < 50; i++) {
				temp.append(vquote.remove(0).toString().concat("\n"));
			}
		}
		/*synchronized (vother) {		
			int start = vother.size() > 30 ? vother.size() - 30 : 0;
			for (int i=0; vother.size()>start && i<30;i++) {	
				temp.append(vother.remove(start).toString().concat("\n"));
			}			
		}*/
		synchronized (vother) {			
			for (int i=0; vother.size()>0 && i<30;i++) {					
				temp.append(vother.remove(0).toString().concat("\n"));
			}			
		}	
		synchronized (vqueue) {
			for (int i = 0; vqueue.size() > 0 && i < 50; i++) {
				temp.append(vqueue.remove(0).toString().concat("\n"));
			}
		}

		return (temp.length() > 0) ? temp.toString().getBytes() : null;
	}
	
	public byte[] getMessageOther() {
		kick();
		StringBuffer temp = new StringBuffer();
		/*synchronized (vother) {		
			int start = vother.size() > 30 ? vother.size() - 30 : 0;
			for (int i=0; vother.size()>start && i<30;i++) {	
				temp.append(vother.remove(start).toString().concat("\n"));
			}			
		}*/
		synchronized (vother) {			
			for (int i=0; vother.size()>0 && i<30;i++) {				
				temp.append(vother.remove(0).toString().concat("\n"));
			}
		}
		return (temp.length() > 0) ? CompressionEncoder.compress(temp.toString().getBytes()) : null;
	}

	public void send() {
		if (isConnected()) {	
			long start = System.nanoTime();

			byte[] btmsg = getMessageString();
			byte[] bt = btmsg != null ? CompressionEncoder.compress(btmsg) : null;

			long endGen = System.nanoTime() - start;

			long startSend = System.nanoTime();

			if (bt != null) socketInterface.sendMessage(bt);

			long endSend = System.nanoTime() - startSend;

			if (endGen > delayMsg) {
				delayMsg = endGen;
				sizeMsgCompress1 = new Long(bt != null ? bt.length : 0);
				sizeMsgNotCompress1 = new Long(btmsg != null ? btmsg.length : 0);
			}

			if (endSend > delaySend) {
				delaySend = endSend;
				sizeMsgCompress2 = new Long(bt != null ? bt.length : 0l);
				sizeMsgNotCompress2 = new Long(btmsg != null ? btmsg.length : 0l);
			}

			long startbt = System.nanoTime();
			Vector vdata = getMessageWithByte();

			if (vdata.size() > 0)
				socketInterface.sendMessage(vdata);

			long dMsgByte = System.nanoTime() - startbt;

			if (dMsgByte > delayMsgByte) {
				delayMsgByte = dMsgByte;
				sizeMsgByte = new Long(vdata.size());
			}
		} else if (!isConnected()) {
			
		}
	}

	private Vector getMessageWithByte() {
		Vector vdata = new Vector();
		synchronized (vtradeByte) {			
			int start = vtradeByte.size() > 30 ? vtradeByte.size() - 30 : 0;
			for (int i = 0; vtradeByte.size() > start && i < 30; i++) {
				vdata.add(vtradeByte.remove(start));
			}
			vtradeByte.clear();
		}
		
		synchronized (vquoteByte) {
			for (int i = 0; vquoteByte.size() > 0 && i < 50; i++) {
				vdata.add(vquoteByte.remove(0));
			}
		}
		/*synchronized (votherByte) {		
			int start = votherByte.size() > 30 ? votherByte.size() - 30 : 0;
			for (int i=0; votherByte.size()>start && i<30;i++) {	
				vdata.add(votherByte.remove(start));
			}			
		}*/
		synchronized (votherByte) {			
			for (int i=0; votherByte.size()>0 && i<30;i++) {				
				vdata.add(votherByte.remove(0));
			}			
		}
		return vdata;
	}
	
	public boolean isConnected() {
		return socketInterface.isConnected();
	}

	public com.eqtrade.ClientSocket getSocketInterface() {
		return socketInterface;
	}
	
	@Override
	public boolean equals(Object obj) {
		ClientGtwSocket ccomp = (ClientGtwSocket) obj;

		if (this.getSessionid() == ccomp.getSessionid()) return true;

		return false;
	}
	
	public void print() {
		long delayMsgsec = TimeUnit.MILLISECONDS.convert(delayMsg,TimeUnit.NANOSECONDS);
		long delaySendSec = TimeUnit.MILLISECONDS.convert(delaySend,TimeUnit.NANOSECONDS);
		long delayMsgByte2 = TimeUnit.MILLISECONDS.convert(delayMsgByte,TimeUnit.NANOSECONDS);

		if (delayMsgsec > 0 || delaySendSec > 0 || delayMsgByte2 > 0)
			log.info("delay_genMsg_" + delayMsgsec + "_delaySend_"
					+ delaySendSec + "_sizeMsgNotCompress_"
					+ sizeMsgNotCompress1 + "_" + sizeMsgNotCompress2
					+ "_sizeMsgCompress_" + sizeMsgCompress1 + "_"
					+ sizeMsgCompress2 + "_" + getSessionid() + "_"
					+ delayMsgByte2+"_"+sizeMsgByte);
	}

	public void reset() {
		delayMsg = 0l;
		delaySend = 0l;
		sizeMsgCompress1 = 0l;
		sizeMsgCompress2 = 0l;
		sizeMsgNotCompress1 = 0l;
		sizeMsgNotCompress2 = 0l;
		delayMsgByte = 0l;
		sizeMsgByte = 0l;
	}
}