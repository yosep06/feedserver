package feed.gateway.socket.broadcaster;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Timer;

import feed.gateway.Console;
import feed.gateway.FeedGatewayApp;
import feed.gateway.FeedGatewayApp.AgingTimerTask;
import feed.gateway.broadcaster.GtwService;
import feed.gateway.core.Database;
import feed.gateway.core.FileConfig;
import feed.gateway.socket.consumer.FeedConsumerSocket;

public class FeedGatewayAppSocket extends FeedGatewayApp {

	private int timerMessageBroadcaster = 100;
	private MessageBroadcaster msgBroadcaster;

	public void start() throws Exception {
		config = new FileConfig("feed.gateway.config");

		timerMessageBroadcaster = Integer.parseInt((String) config.getProperty("msgbroadcastertimer"));

		database = new Database();
		database.start(config.getProperty("database"));

		log.info("FeedGateway service start : "+ config.getProperty("gatewayservice"));
		// consumer = new FeedConsumer();
		consumer = new FeedConsumerSocket();
		service = new GtwServiceSocket(config, consumer, database);
		consumer.getTradeConsumer().setService(service);
		consumer.getQuoteConsumer().setService(service);
		consumer.getOtherConsumer().setService(service);
		consumer.getOtherConsumer().setAccessData(service.getaccessData());
		consumer.start();

		Timer timer = new Timer(false);
		timer.schedule(new AgingTimerTask(), 60000, 60000);

		// log.info("FeedGateway service: "+config.getProperty("gatewayservice")+" available on port "+config.getProperty("gatewayport"));

		console = new Console(this);
		console.start();
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				try {
					log.info("shutingdown FeedGateway application");
					service.stop();
				} catch (Exception ex) {
				}
			}
		});

		msgBroadcaster = new MessageBroadcaster(this);
		msgBroadcaster.start();

	}

	public void stop() {
		consumer.stop();
		database.setStop();
	}

	public int getTimerMessageBroadcaster() {
		return timerMessageBroadcaster;
	}
	
	public MessageBroadcaster getMsgBroadcaster() {
		return msgBroadcaster;
	}
}