package feed.gateway.socket.consumer;

import com.eqtrade.SocketInterface;

import feed.gateway.consumer.OtherConsumer.timerdatabase;
import feed.gateway.socket.core.ProcessorConsumerThread;

public class OtherConsumerSocket extends feed.gateway.consumer.OtherConsumer {

	private SocketInterface socketConnector;
	private ProcessorConsumerThread processorManager;

	timerdatabase tm = new timerdatabase();
	
	public OtherConsumerSocket(SocketInterface socket) throws Exception {
		super("","0",false);
		processorManager = new ProcessorConsumerThread(this);
		this.socketConnector = socket;
		processorManager.start();

	}

	@Override
	public boolean connect() throws Exception {
		tm.start();
		//log.info("start timer");
		this.socketConnector.sendMessage(("subscribe " + types+" "+msg.getSeqno()).getBytes());
		return true;
	}

	@Override
	public void exit() throws Exception {
		tm.stop();
		this.socketConnector.sendMessage(("unsubscribe " + types).getBytes());
		processorManager.setStop();

	}

	public void addMsg(String msg) {
		processorManager.addMsg(msg);
	}

}
