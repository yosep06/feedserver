package feed.gateway.socket.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eqtrade.ClientSocket;
import com.eqtrade.Receiver;
import com.eqtrade.SocketFactory;
import com.eqtrade.SocketInterface;
import com.eqtrade.TimeCollector;

import feed.gateway.consumer.FeedConsumer;
import feed.gateway.consumer.OtherConsumer;
import feed.gateway.consumer.OtherConsumerMobile;
import feed.gateway.consumer.QuoteConsumer;
import feed.gateway.consumer.TradeConsumer;

public class FeedConsumerSocket extends FeedConsumer implements Receiver {
	private SocketInterface socketConsumer;
	private OtherConsumerSocket otherConsumer;
	private OtherConsumerMobile otherConsumerMobile;
	private TradeConsumerSocket tradeConsumer;
	private QuoteConsumerSocket quoteConsumer;
	private Logger log = LoggerFactory.getLogger(getClass());
	private boolean isstart = false;
	private TimeCollector timer = new TimeCollector("bench.consumer");

	public FeedConsumerSocket() throws Exception {
		super();
		socketConsumer = SocketFactory.createSocket(
				config.getProperty("socket.consumer"), this);
		socketConsumer.start();

		tradeConsumer = new TradeConsumerSocket(socketConsumer);
		otherConsumer = new OtherConsumerSocket(socketConsumer);
		quoteConsumer = new QuoteConsumerSocket(socketConsumer);
		otherConsumerMobile = new OtherConsumerMobile(null, null,otherConsumer);
	}

	@Override
	public void connected(ClientSocket sock) {
		sock.validated(true);
	}

	@Override
	public void disconnect(ClientSocket sock) {
		stop();
	}

	@Override
	public void receive(ClientSocket sock, byte[] bt) {
		String msg = new String(bt);
		//log.info("receive "+msg);
		/*if(!isstart){
			isstart = true;
			timer.savedLatency(0, System.nanoTime(), msg);
		}*/

		String[] sp = msg.split("\\!");
		if (sp[0].equals("0")) {
			otherConsumer.addMsg(sp[1]);
			otherConsumerMobile.addMsg(sp[1]);
		} else if (sp[0].equals("1")) {
			quoteConsumer.addMsg(sp[1]);
		} else if (sp[0].equals("2")) {
			//log.info("trade "+msg);
			tradeConsumer.addMsg(sp[1]);
		}
	}

	@Override
	public void start() throws Exception {
		quoteConsumer.connect();
		otherConsumer.connect();
		tradeConsumer.connect();
//		otherConsumerMobile.connect();
	}

	@Override
	public void stop() {
		try {
			quoteConsumer.exit();
			otherConsumer.exit();
			tradeConsumer.exit();
//			otherConsumerMobile.exit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public TradeConsumer getTradeConsumer() {
		// TODO Auto-generated method stub
		return tradeConsumer;
	}

	@Override
	public QuoteConsumer getQuoteConsumer() {
		// TODO Auto-generated method stub
		return quoteConsumer;
	}

	@Override
	public OtherConsumer getOtherConsumer() {
		// TODO Auto-generated method stub
		return otherConsumer;
	}
	
	public OtherConsumerMobile getOtherConsumerMobile(){
		return otherConsumerMobile;
	}

	public TimeCollector getTimer() {
		return timer;
	}

	@Override
	public void receive(ClientSocket arg0, Object arg1) {
		// TODO Auto-generated method stub
		
	}
	
	
	
	

}
