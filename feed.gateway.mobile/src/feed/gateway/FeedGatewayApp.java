package feed.gateway;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Enumeration;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.PropertyConfigurator;

import feed.gateway.broadcaster.Client;
import feed.gateway.broadcaster.GtwService;
import feed.gateway.consumer.FeedConsumer;
import feed.gateway.core.Database;
import feed.gateway.core.FileConfig;
import feed.gateway.socket.broadcaster.FeedGatewayAppSocket;
import feed.gateway.socket.consumer.FeedConsumerSocket;


public class FeedGatewayApp {
	protected Console console;
	protected FeedConsumerSocket consumer;
	protected GtwService service;
	protected FileConfig config;
	protected final Log log = LogFactory.getLog(getClass());	
	protected Database database;
	
	
	public GtwService getService(){
		return service;
	}
	
	public void start() throws Exception{
		//System.out.println("start from super class..");
		config = new FileConfig("feed.gateway.config");
		database = new Database();
		database.start(config.getProperty("database"));
		
		//log.info("FeedGateway service start : "+config.getProperty("gatewayservice"));
		//consumer = new FeedConsumer();
		consumer = new FeedConsumerSocket();
		service = new GtwService(config, consumer, database, Integer.parseInt(config.getProperty("gatewayinternal")));		
		consumer.getTradeConsumer().setService(service);
		consumer.getQuoteConsumer().setService(service);
		consumer.getOtherConsumer().setService(service);
		//System.out.println(service.getaccessData()+" 132132");
		consumer.getOtherConsumer().setAccessData(service.getaccessData());
		consumer.start();
	    Registry registry = LocateRegistry.createRegistry(Integer.parseInt(config.getProperty("gatewayport")));
		registry.rebind(config.getProperty("gatewayservice", "feed"), service);
		
		Timer timer = new Timer(false);
		timer.schedule(new AgingTimerTask(), 60000, 60000);

		//log.info("FeedGateway service: "+config.getProperty("gatewayservice")+" available on port "+config.getProperty("gatewayport"));
		
		
		console = new Console(this);
		console.start();
		 Runtime.getRuntime().addShutdownHook(new Thread() {
			    public void run() { 
			    	try {
				    	log.info("shutingdown FeedGateway application");
				    	service.stop();
			    	} catch (Exception ex){}
			    }
		 });
	}
	
	public void stop(){
		consumer.stop();
		database.setStop();
	}
	
	/*public void ping(){
		service.ping();
	}*/
	
	public class AgingTimerTask extends TimerTask {
		private long lastRun = System.currentTimeMillis();
		
		public void run() {
			try {
				   log.info("scheduler : checking for invalid sessions");
					Vector vtemp = new Vector(100,1);
					long now = System.currentTimeMillis();
					long delta = 30000; //now - lastRun;
					lastRun = now;
					Enumeration e = service.getClient().keys();
					while (e.hasMoreElements()){
						Integer key = (Integer)e.nextElement();
						Client c = (Client)service.getClient().get(key);
						c.age(delta);
						if (c.isExpired()){ 
							vtemp.addElement(key);
						}
					}
					for (int i=0; i<vtemp.size(); i++){
						Integer key = (Integer)vtemp.elementAt(i);
						log.info("sessionid "+key+" is expired");
						service.logout(key.intValue());
					}
					service.cleaningSessionKill();
			} catch (Exception ex){
				ex.printStackTrace();
			}
		}
	}
	
	
	
	public FeedConsumerSocket getConsumer() {
		return consumer;
	}

	public static void main(String[] args) {
		try {
			PropertyConfigurator.configure("log.properties");
			System.out.println("mobile");
			new FeedGatewayAppSocket().start();
		} catch (Exception ex){
			ex.printStackTrace();
			System.exit(-1);
		}
	}

}
