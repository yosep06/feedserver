package feed.gateway.consumer;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import feed.builder.msg.BrokerMobile;
import feed.builder.msg.ChartIntraday;
import feed.builder.msg.Indices;
import feed.builder.msg.MarketMobile;
import feed.builder.msg.Message;
import feed.builder.msg.StockMobile;
import feed.builder.msg.StockSummary;
import feed.builder.msg.StockSummaryMobile;
import feed.builder.msg.TopGainer;
import feed.builder.msg.TradeMobile;
import feed.builder.msg.TradeSummBrokerStock;
import feed.builder.msg.TradeSummBrokerStockMobile;
import feed.builder.msg.TradeSummStockBroker;
import feed.builder.msg.TradeSummStockBrokerMobile;
import feed.builder.msg.TradeSummStockInvMobile;
import feed.builder.msg.TradeSummStockMarketInv;
import feed.builder.msg.TradeSummary;
import feed.gateway.core.MsgConsumer;
import feed.gateway.socket.core.ProcessorConsumerThread;

public class OtherConsumerMobile extends MsgConsumer{
	private HashMap brokerMobileMap;
	private HashMap tsBrokerStockMobile;
	private HashMap tsStockBrokerMobile;
	private HashMap articleMobileMap;
	private HashMap globalIndicesMobileMap; // key code
	private HashMap stockMobileMap; 
	//#Valdhy 20141219
	//--- Mobile Section - Start ---
	private HashMap ssmMap; //key stock+board
	private HashMap mmMap; //key stock+board
	//--- Mobile Section - End ---
	private HashMap tgMap;
	private HashMap tradeHistoryMobile;
	private HashMap tsStockMarketInv;
	private HashMap tsStockInvMobile;
	
	private ProcessorConsumerThread processorManager;
	private long interval = 0;
	private SimpleDateFormat dt = new SimpleDateFormat("HHmmss");
	private OtherConsumer other ;
	
	public OtherConsumerMobile(String url, String type,OtherConsumer other) throws Exception {
		super(url, "4");
		this.other = other;
		processorManager = new ProcessorConsumerThread(this);
		// TODO Auto-generated constructor stub
	}
	
	
	protected void loadconfig(){

		tgMap = new HashMap(500,1);
		ssmMap = new HashMap(500, 1);
		mmMap = new HashMap(500, 1);
		stockMobileMap = new HashMap(500,1);
		brokerMobileMap = new HashMap(500,1);
		tsBrokerStockMobile = new HashMap(500, 1);
		tsStockBrokerMobile = new HashMap(500, 1);
		tradeHistoryMobile = new HashMap(500, 5);
		tsStockInvMobile = new HashMap(500,1);
		tsStockMarketInv = new HashMap(500,1);
		
		List listSSM = db.query(StockSummaryMobile.class);
		for (int i = 0; i < listSSM.size(); i++) {
			StockSummaryMobile q = (StockSummaryMobile) listSSM.get(i);
			ssmMap.put(q.getStock() + q.getBoard(), q);
		}

		List listBrokerMobile = db.query(BrokerMobile.class);
		for (int i = 0; i < listBrokerMobile.size(); i++) {
			BrokerMobile q = (BrokerMobile) listBrokerMobile .get(i);
			brokerMobileMap.put(q.getCode(), q);
		}
		
		List listStockMobile = db.query(StockMobile.class);
		for (int i = 0; i < listStockMobile.size(); i++) {
			StockMobile q = (StockMobile) listStockMobile.get(i);
			stockMobileMap.put(q.getCode(), q);
		}
		
		List listMM = db.query(MarketMobile.class);
		for (int i = 0; i < listMM.size(); i++) {
			MarketMobile q = (MarketMobile) listMM.get(i);
			mmMap.put(q.getStock() + q.getBoard(), q);
		}

		List listsbm = db.query(TradeSummStockBrokerMobile.class);
		for (int i = 0; i < listsbm.size(); i++) {
			TradeSummStockBrokerMobile q = (TradeSummStockBrokerMobile) listsbm.get(i);
			tsStockBrokerMobile.put(q.getStock() + q.getBroker(), q);
		}
		
		List listminv2 = db.query(TradeSummStockMarketInv.class);
		for (int i = 0; i < listminv2.size(); i++) {
			TradeSummStockMarketInv q = (TradeSummStockMarketInv) listminv2.get(i);
			tsStockMarketInv.put(q.getStock()+q.getBoard() +q.getBroker()+ q.getInvestor(), q);
//			tsStockMarketInv.put(q.getStock() + q.getInvestor()+q.getBoard(), q);
		}// 17012017
		
		List tradesm = db.query(TradeMobile.class);
		for (int i = 0; i < tradesm.size(); i++) {
			TradeMobile trm = (TradeMobile) tradesm.get(i);
			Vector<TradeMobile> v = (Vector<TradeMobile>) tradeHistoryMobile.get(trm.getStock());
			if (v == null) {
				v = new Vector<TradeMobile>();
				tradeHistoryMobile.put(trm.getStock(), v);
			}
				v.add(trm);
		}
		List listbsm = db.query(TradeSummBrokerStockMobile.class);
		for (int i = 0; i < listbsm.size(); i++) {
			TradeSummBrokerStockMobile q = (TradeSummBrokerStockMobile) listbsm.get(i);
			tsBrokerStockMobile.put(q.getBroker() + q.getStock(), q);
		}
		
		List listTG = db.query(TopGainer.class);
		for (int i = 0; i < listTG.size(); i++) {
			TopGainer q = (TopGainer) listTG.get(i);
			tgMap.put(q.getStock() + q.getBoard(), q);
		}
	}
	public void newMwssage(String message) throws RemoteException {
		try {
			String[] data = message.split("\\|");
			Message dat = null;
			if (data[1].equals("4")) {
				dat = new BrokerMobile();
				dat.setContent(data);
				// if (dat.getSeqno()>=msg.getSeqno()){
				BrokerMobile bm = (BrokerMobile) brokerMobileMap.get(((BrokerMobile) dat).getCode());
				if (bm == null) {
					bm = new BrokerMobile();
				}
				bm.setContent(data);
				brokerMobileMap.put(bm.getCode(), bm);
				addSnapShot(bm);
			} else if (data[1].equals("6")) {
				dat = new Indices();
				dat.setContent(data);
				// if (dat.getSeqno()>=msg.getSeqno()){
				Indices i = (Indices) other.getIndices().get(((Indices) dat).getCode());
				if (i == null) {
					i = new Indices();	
				}
				i.setContent(data);
				// 17012017
				if (i.getCode().equalsIgnoreCase("COMPOSITE") // 17012017
						&& interval < dt.parse(i.getTranstime()).getTime()) {
					ChartIntraday ci = new ChartIntraday();
					ci.setHeader("IDX");
					ci.setType("MHI");					
					ci.setTranstime(i.getTranstime());//rec.getString("transtime"));
					ci.setCode(i.getCode());//rec.getString("code"));
					ci.setLast(i.getIndex());//rec.getDouble("last"));
					service.rcvOther(ci);
					Calendar c = Calendar.getInstance();
					c.setTime(dt.parse(i.getTranstime()));
					c.add(Calendar.MINUTE,	1);
					interval = c.getTimeInMillis();
				}// 17012017 *
			} else if (data[1].equals("3")) {
				// stock mobile 
				dat = new StockMobile();
				dat.setContent(data);
				// if (dat.getSeqno()>=msg.getSeqno()){
				StockMobile sm = (StockMobile) stockMobileMap.get(((StockMobile) dat).getCode());
				if (sm == null) {
					sm = new StockMobile();
				}
				sm.setContent(data);
				stockMobileMap.put(sm.getCode(), sm);
				addSnapShot(sm);
			} else if (data[1].equals("5")) {
				dat = new StockSummary();
				dat.setContent(data);
				// if (dat.getSeqno()>=msg.getSeqno()){
				StockSummary ss = (StockSummary) other.getssMap().get(((StockSummary) dat)
						.getStock() + ((StockSummary) dat).getBoard());
				if (ss == null) {
					ss = new StockSummary();
				}
				//#Valdhy 20141219
				//--- Mobile Section - Start ---
				
				//Stock Summary Mobile
				StockSummaryMobile sm = (StockSummaryMobile) ssmMap.get(((StockSummary) dat)
						.getStock() + ((StockSummary) dat).getBoard());
				if (sm == null) {
					sm = new StockSummaryMobile();
				}
				sm.setContent(data);
				sm.setType("SSM");
				sm.calculate();
			
				ssmMap.put(sm.getStock()+sm.getBoard(), sm);
				addSnapShot(sm);
				
				//Market Mobile
				//System.err.println("MM Start");
				MarketMobile mm = (MarketMobile) mmMap.get("TOTAL");
				if (mm == null){
					//System.err.println("MM mm null");
					mm = new MarketMobile();
					mm.setContent(data);
				} else {
					MarketMobile oldmm = (MarketMobile) mmMap.get(((StockSummary) dat)
							.getStock() + ((StockSummary) dat).getBoard());
					
					if (oldmm == null) {
						//System.err.println("MM oldmm null");
						oldmm = new MarketMobile();
						mm.setValue(new Double(mm.getValue() + ss.getTradeval()));
		                mm.setVolume(new Double(mm.getVolume() + ss.getTradevol()));
		                mm.setFreq(new Double(mm.getFreq() + ss.getTradefreq()));  

					} else {
						//System.err.println("MM oldmm not null");
						mm.setValue(new Double(mm.getValue() - oldmm.getTradeval() + ss.getTradeval()));
		                mm.setVolume(new Double(mm.getVolume() - oldmm.getTradevol() + ss.getTradevol()));
		                mm.setFreq(new Double(mm.getFreq() - oldmm.getTradefreq() + ss.getTradefreq()));
					}
					oldmm.setContent(data);
					mmMap.put(ss.getStock()+ss.getBoard(), oldmm);
				}
				mm.calculate();
				mm.setType("MM");
				mm.setBoard("TOTAL");
				
				addSnapShot(mm);
				mmMap.put("TOTAL", mm);
				service.rcvOther(mm);
				
				//Topgainer 
				TopGainer tg = (TopGainer) tgMap.get(((StockSummary) dat)
						.getStock() + ((StockSummary) dat).getBoard());
				if (tg == null) {
					tg = new TopGainer();
				}
				tg.setContent(data);
				tg.setType("TG");
			
				tgMap.put(tg.getStock()+tg.getBoard(), tg);
				addSnapShot(tg);
				service.rcvOther(tg);
				//--- Mobile Section - End ---
			} else if (data[1].equals("TS")) {
				dat = new TradeSummary();
				dat.setContent(data);
				TradeSummary ts = new TradeSummary();
				ts.setContent(data);
				// 17012017 
				TradeSummStockInvMobile fdm = (TradeSummStockInvMobile) tsStockInvMobile
						.get(ts.getStock()
								+ ts.getInvestor());
				if (ts.getBoard().equalsIgnoreCase("RG")){
					if (fdm == null) {
						fdm = new TradeSummStockInvMobile();
						fdm.setContent(data);
						fdm.setBoard("ALL");
						fdm.setBroker("ALL");
						fdm.setType("FDM");
					}else{
						TradeSummStockMarketInv tsmi = (TradeSummStockMarketInv) tsStockMarketInv
								.get(
										ts.getStock()
										+ ts.getBoard()
										+ ts.getBroker()
										+ ts.getInvestor());
						if (tsmi == null) {
							fdm.setBuyvol(new Double(fdm.getBuyvol()  +ts.getBuyvol()));
				            fdm.setBuyval(new Double(fdm.getBuyval()  + ts.getBuyval()));
				            fdm.setBuyfreq(new Double(fdm.getBuyfreq() + ts.getBuyfreq()));                
				            fdm.setSellvol(new Double(fdm.getSellvol() + ts.getSellvol()));
				            fdm.setSellval(new Double(fdm.getSellval() + ts.getSellval()));
				            fdm.setSellfreq(new Double(fdm.getSellfreq() + ts.getSellfreq()));
						} else {
							fdm.setBuyvol(new Double(fdm.getBuyvol() - tsmi.getBuyvol() +ts.getBuyvol()));
				            fdm.setBuyval(new Double(fdm.getBuyval() - tsmi.getBuyval() + ts.getBuyval()));
				            fdm.setBuyfreq(new Double(fdm.getBuyfreq() - tsmi.getBuyfreq() + ts.getBuyfreq()));                
				            fdm.setSellvol(new Double(fdm.getSellvol() - tsmi.getSellvol() + ts.getSellvol()));
				            fdm.setSellval(new Double(fdm.getSellval() - tsmi.getSellval() + ts.getSellval()));
				            fdm.setSellfreq(new Double(fdm.getSellfreq() - tsmi.getSellfreq() + ts.getSellfreq()));    
						}
					}
//				log.info(fdm.toString());
				tsStockInvMobile.put(ts.getStock()+ts.getInvestor(), fdm);
				addSnapShot(fdm);
				}
				// 17012017 *				
				TradeSummStockMarketInv tsmi = (TradeSummStockMarketInv) tsStockMarketInv
						.get(
								ts.getStock()
								+ ts.getBoard()
								+ ts.getBroker()
								+ ts.getInvestor());
				if (tsmi == null) {
					tsmi = new TradeSummStockMarketInv();
				}
				tsmi.setContent(data);
				tsmi.setType("TSMI");
				tsStockMarketInv.put(tsmi.getStock()
						+ ((TradeSummary) dat).getBoard()
						+ ((TradeSummary) dat).getBroker() + tsmi.getInvestor()
						, tsmi);
				addSnapShot(tsmi);
				service.rcvOther(tsmi);
			} else if (data[1].equals("TSBS")) {
				//Mobile
				TradeSummBrokerStockMobile tsm = (TradeSummBrokerStockMobile) tsBrokerStockMobile.get(((TradeSummBrokerStock) dat)
						.getBroker() + ((TradeSummBrokerStock) dat).getStock());
				if (tsm == null) {
					tsm = new TradeSummBrokerStockMobile();
				}
				tsm.setContent(data);
				tsm.setType("TSBSM");
				tsBrokerStockMobile.put(tsm.getBroker()+tsm.getStock(), tsm);
				addSnapShot(tsm);
			} else if (data[1].equals("TSSB")) {

				//Mobile
				TradeSummStockBrokerMobile tsm = (TradeSummStockBrokerMobile) tsStockBrokerMobile.get(((TradeSummStockBroker) dat)
						.getStock() + ((TradeSummStockBroker) dat).getBroker());
				if (tsm == null) {
					tsm = new TradeSummStockBrokerMobile();
				}
				tsm.setContent(data);
				tsm.setType("TSSBM");
				tsStockBrokerMobile.put(tsm.getStock()+tsm.getBroker(), tsm);
				addSnapShot(tsm);
			} else if (data[1].equals("")) {

				//mobile trade paging 
				Message dt = new TradeMobile();
				dt.setContent(data);
				dt.setType("MTH");
				TradeMobile trm = (TradeMobile) dt;

				Vector<TradeMobile> vm = (Vector<TradeMobile>) tradeHistoryMobile.get(trm
						.getStock());
				if (vm == null) {
					vm = new Vector<TradeMobile>();
					tradeHistoryMobile.put(trm.getStock(), vm);
				}

				vm.add(trm);
				service.rcvOther(dt);
				addSnapShot(dt);
			} 
//			if (dat.getSeqno() >= msg.getSeqno()) {
//				msg.setSeqno((long) dat.getSeqno());
//				db.putMsg(msg);
//			}
//			service.rcvOther(dat);
		} catch (Exception ex) {
			System.out.println("error while processing: " + message);
			ex.printStackTrace();
		}
		
	}

	public void addMsg(String msg) {
		processorManager.addMsg(msg);
	}
}
