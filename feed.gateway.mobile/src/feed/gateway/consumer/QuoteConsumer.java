package feed.gateway.consumer;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.LogFactory;

import com.db4o.query.Query;

import feed.builder.msg.Quote;
import feed.builder.msg.Queue;
import feed.gateway.core.MsgConsumer;

public class QuoteConsumer extends MsgConsumer {
	private HashMap quoteMap;
	//private QueueProcess queueProces;


	public QuoteConsumer(String url) throws Exception{
		super(url, "2");
		
	}
	
	public QuoteConsumer(String url,String type) throws Exception{
		super(url, type);
	}
	
	public QuoteConsumer(String url,String type,boolean b) throws Exception{
		super(url, type,b);
	}
	
	
	
	
	@Override
	public boolean connect() throws Exception {	
		boolean isconn = super.connect();
		
		/*queueProces = new QueueProcess(service);
		queueProces.start();
		*/
		return isconn;
	}



	public List getSnapShot(int seqno){
		Query query = db.getDb().getInstance().query();
		query.constrain(Quote.class);
		query.descend("seqno").orderAscending().constrain(new Long(seqno)).greater();
		List o = query.execute();
		return o;
	}
	
	public Quote getSnapShot(String key){
		log.info("QuoteConsumer getSnapShot" + key);
		return (Quote)quoteMap.get(key);
	}
	
	protected void loadConfig(){
		log = LogFactory.getLog(getClass());	
		super.loadConfig();
		quoteMap = new HashMap(500,10);
		//List t = db.query(Quote.class);
		Query query = db.getDb().getInstance().query();
		query.constrain(Quote.class);
		query.descend("stock").orderAscending();
		List t = query.execute();
		for (int i=0; i<t.size();i++){
			Quote q = (Quote)t.get(i);
			q.unpack();
			quoteMap.put(q.getStock()+"#"+q.getBoard(), q);
		}
	
	
	}
	

	
	public  void newMessage(String message) throws RemoteException {
		try {
		    	String[] data = message.split("\\|");
		    	//log.info("newMessage on QuoteConsumer ="+message + " data="+ data[1]);
		    	//System.out.println("new msg in QuoteConsumer= " + message);
		    	//if(data[1].equals("10")){		
				//Quote quote = new Quote();
	    			//quote.setContentQueueBid(message);
		    		//log.info("QuoteConsumer on NewMessage="+ message);
		//    		HashMap map = convert("10",message);
		  //  		service.rcvQuoteQueue1(map);
		    		//queueProces.addMessage(message);


		    		//ini codingnya yedi
		    		/*Quote quote = new Quote();
			    	//quote.setContentQueueBid(message);
		    		HashMap map = convert("10",message);
			    	service.rcvQuoteQueue1(map);*/


				 
		    		/*Quote queue = new Quote();
			    	queue.setContentQueueBid(message);
			    	String key = data[1]+"|"+queue.getStock()+"|"+queue.getBidPriceQueue() +"|"+queue.getBoard();
			    	log.info("new msg "+ data[1] +" in QuoteConsumer= " + key);
			    	msg.setSeqno((long)queue.getSeqno());
				    db.putMsg(msg);
				    log.info("new put "+ msg +" in QuoteConsumer= " + queue);
				    service.rcvQuoteQueue(queue);			
					
				    Quote q  = (Quote)quoteMap.get(key);  
				    if (q==null) {
				      		q = new Quote();
				    } 
			        q.setContentQueueBid(message);
			        q.pack();
				    quoteMap.put(key, q);
				    addSnapShot(q);*/
		    		
		    	//} else if(data[1].equals("11")){	
				//Quote quote = new Quote();
	    			//quote.setContentQueueBid(message);
		    //		HashMap map = convert("11",message);
			//	service.rcvQuoteQueue1(map);
		    		//queueProces.addMessage(message);

		    		/*Quote quote = new Quote();
			    	//quote.setContentQueueOffer(message);	
			    	//service.rcvQuoteQueue(quote);
		    		//HashMap map = convert("11",message);
			    	//service.rcvQuoteQueue1(map);
		    		Quote queue = new Quote();
			    	queue.setContentQueueBid(message);
			    	//String key = data[1]+"|"+queue.getStock()+"|"+queue.getBoard();
			    	String key = "10|"+queue.getStock()+"|"+queue.getBidPriceQueue() +"|"+queue.getBoard();
			    	log.info("new msg in QuoteConsumer 11 = " + key);
			    	msg.setSeqno((long)queue.getSeqno());
				    db.putMsg(msg);
				    service.rcvQuoteQueue(queue);			        	
				    Quote q  = (Quote)quoteMap.get(key);  
				    if (q==null) {
				      		q = new Quote();
				    } 
			        q.setContentQueueBid(message);
			        q.pack();
				    quoteMap.put(key, q);
				    addSnapShot(q);*/
		    		
		    	//} else {
		    	
		    	//System.out.println("new msg in QuoteConsumer= " + message);
		    	
		    	Quote quote = new Quote();
		    	quote.setContent(message);
		    	String key = quote.getStock()+"#"+quote.getBoard();
		    	
		    	
		    	//System.out.println("new msg in QuoteConsumer= " + key);
		    	//if (quote.getSeqno() > msg.getSeqno()){
			    		msg.setSeqno((long)quote.getSeqno());
			    		db.putMsg(msg);
			        	service.rcvQuote(quote);			        	
			        	Quote q  = (Quote)quoteMap.get(key);  
			        	if (q==null) {
			        		q = new Quote();
			        	} 
		        		q.setContent(message);
		        		q.pack();
			        	quoteMap.put(key, q);
			        	addSnapShot(q);
			        	
			        	
			        	
		    	//}
		    	//}
		} catch (Exception ex){
			//System.out.println("error while processing: "+message);
			log.info("error message in QuoteConsumer:"+message);
			ex.printStackTrace();
		}
	}		

}
