package feed.gateway.broadcaster;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.eqtrade.database.AccessQueryLocator;
import com.eqtrade.netty.protocol.CompressionEncoder;
import com.eqtrade.utils.properties.FEProperties;

import feed.admin.Admin;
import feed.admin.AdminClient;
import feed.builder.msg.Article;
import feed.builder.msg.Broker;
import feed.builder.msg.BrokerMobile;
import feed.builder.msg.BrokerSummarry;
import feed.builder.msg.ChartIntraday;
import feed.builder.msg.Commodity;
import feed.builder.msg.CorpAction;
import feed.builder.msg.Currency;
import feed.builder.msg.CurrencyMobile;
import feed.builder.msg.Future;
import feed.builder.msg.GlobalIndices;
import feed.builder.msg.GlobalIndicesMobile;
import feed.builder.msg.IPO;
import feed.builder.msg.Indices;
import feed.builder.msg.MarketMobile;
import feed.builder.msg.Message;
import feed.builder.msg.News;
import feed.builder.msg.Quote;
import feed.builder.msg.Rups;
import feed.builder.msg.Sector;
import feed.builder.msg.Stock;
import feed.builder.msg.StockComparisonBySector;
import feed.builder.msg.StockMobile;
import feed.builder.msg.StockNews;
import feed.builder.msg.StockSummary;
import feed.builder.msg.StockSummaryMobile;
import feed.builder.msg.TopGainer;
import feed.builder.msg.Trade;
import feed.builder.msg.TradeMobile;
import feed.builder.msg.TradePrice;
import feed.builder.msg.TradeSummBroker;
import feed.builder.msg.TradeSummBrokerStock;
import feed.builder.msg.TradeSummBrokerStockMobile;
import feed.builder.msg.TradeSummInv;
import feed.builder.msg.TradeSummStockBroker;
import feed.builder.msg.TradeSummStockBrokerMobile;
import feed.builder.msg.TradeSummStockInv;
import feed.builder.msg.TradeSummStockMarketInv;
import feed.builder.msg.TradeSummary;
import feed.gateway.consumer.FeedConsumer;
import feed.gateway.core.Database;
import feed.gateway.core.FileConfig;
import feed.provider.core.Utils;


public class GtwService extends UnicastRemoteObject implements Gateway,
		AdminClient {
	private static final long serialVersionUID = 1L;
	protected Hashtable client;
	protected Hashtable clientKill;
	protected Hashtable request;
	protected Admin admin;
	protected long id = 0;
	protected FeedConsumer consumer;
	private final Log log = LogFactory.getLog(getClass());
	protected Database database;
	private HashMap mapQuoteQueue = new HashMap();

	public Database getDatabase() {
		return database;
	}

	public Hashtable getClient() {
		return client;
	}

	private Connection conn = null;

	public boolean ping() {
		boolean result = true;
		try {
			String query = "select 1 as correct from FEED_TRADESUMMARY_HIS";
			ResultSet rec = null;
			Statement st = null;
			if (conn == null)
				conn = database.getConnection();
			st = conn.createStatement();
			rec = st.executeQuery(query);
			rec.close();
			st.close();
		} catch (Exception ex) {
			try {
				ex.printStackTrace();
				result = false;
				log.info("db ping error: " + ex.getMessage());
				log.info("trying to reconnect...");
				conn = null;
				database.start(config.getProperty("database"));
				log.info("db back to live");
			} catch (Exception exy) {
				exy.printStackTrace();
				log.info(exy.getMessage());
				log.info("reconnecting failed, please ping again!");
			}
		}
		return result;
	}

	private FileConfig config;

	public GtwService(FileConfig config, FeedConsumer consumer,
			Database database, int port) throws Exception {
		super(port);
		this.config = config;
		this.database = database;
		this.consumer = consumer;
		client = new Hashtable(100, 5);
		clientKill = new Hashtable(100, 5);
		request = new Hashtable(100, 5);
		admin = (Admin) Naming.lookup(config.getProperty("adminurl"));
		id = admin.register(this, config.getProperty("intranetaddr"),
				config.getProperty("internetaddr"),
				Integer.parseInt(config.getProperty("intranet")),
				Integer.parseInt(config.getProperty("internet")),
				Integer.parseInt(config.getProperty("maxconnection")));
		if (id == -1)
			throw new Exception("Cannot connect to admin server");
		log.info("registering this gateway to FeedAdmin@"
				+ config.getProperty("adminurl"));
		initTimer();
		
	}

	protected AccessQueryLocator accessData;
	
	public void initDatabase() {	
		try {
			FEProperties fep = new FEProperties("gtw.properties");
			accessData = new AccessQueryLocator(fep.get("database"),false);
			log.info("init database finished");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public GtwService(FileConfig config, FeedConsumer consumer, Database database) throws Exception {
		this.config = config;
		this.database = database;
		this.consumer = consumer;
		client = new Hashtable(100, 5);
		clientKill = new Hashtable(100, 5);
		request = new Hashtable(100, 5);
		admin = (Admin) Naming.lookup(config.getProperty("adminurl"));
		id = admin.register(this, config.getProperty("intranetaddr"),
				config.getProperty("internetaddr"),
				Integer.parseInt(config.getProperty("intranet")),
				Integer.parseInt(config.getProperty("internet")),
				Integer.parseInt(config.getProperty("maxconnection")));
		if (id == -1)
			throw new Exception("Cannot connect to admin server");
		log.info("registering this gateway to FeedAdmin@" + config.getProperty("adminurl"));	
		initTimer();
		log.info("init database started");
		initDatabase();
		
	}

	private void initTimer() {
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				String result = consumer.getOtherConsumer().selectHistory("CHI" + "|" + "COMPOSITE");
			}
		}, 2000, 7200000);

	}

	public void stop() throws Exception {
		admin.unregister(id);
	}

	public Client getClient(int sessionid) {
		return (Client) client.get(new Integer(sessionid));
	}

	SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");

	public String getDate() throws RemoteException {
		return format.format(new Date());
	}

	public long getTime() throws RemoteException {
		return new Date().getTime();
	}

	public boolean chgPassword(int sessionid, String userid, String pass,
		String newPass) throws RemoteException {
		Client c = (Client) client.get(new Integer(sessionid));
		if (c != null) {
			c.setPass(newPass);
			log.info("user " + c.getUserid() + " request change password");
			return true;
		} else {
			throw new RemoteException("session expired or killed");
		}
	}

	public byte[] getMessages(int sessionid) throws RemoteException {
		Client c = (Client) client.get(new Integer(sessionid));
		if (c != null) {
			return c.getMessage();
		} else {
			Client cKill = (Client) clientKill.get(new Integer(sessionid));
			if (cKill != null) {
				clientKill.remove(sessionid);
				throw new RemoteException("session killed");
			}
			throw new RemoteException("session expired or killed");
		}
	}

	public boolean heartbeat(int sessionid) throws RemoteException {
		Client c = (Client) client.get(new Integer(sessionid));
		if (c != null) {
			return true;
		} else {
			throw new RemoteException("session expired or killed");
		}
	}

	public byte[] getHistory(int sessionid, String msg) throws RemoteException {
		Client c = (Client) client.get(new Integer(sessionid));
		if (c != null) {
			if (msg.startsWith("FILE")) {
				byte[] result = consumer.getOtherConsumer().readFile(msg);
				return result;
			} else if (msg.startsWith("BROKER")) {
				return Utils.compress(consumer.getOtherConsumer().getBrokerInfo().getBytes());
			} else {
				String result = consumer.getOtherConsumer().selectHistory(msg);
				return result != null ? Utils.compress(result.getBytes()) : null;
			}
		} else {
			throw new RemoteException("session expired or killed");
		}

	}

	public int login(String userid, String pass) throws RemoteException {
		long idc = admin.login(id, userid, pass);
		if (idc != -1) {
			log.info("user " + userid + " login to this gateway with sessionid " + idc);
			Client c = new Client((int) idc, userid, pass);
			client.put(new Integer(c.getSessionid()), c);
		}
		return (int) idc;
	}

	public void printClient() {
		ArrayList keys = new ArrayList();
		keys.addAll(client.keySet());
		Collections.sort(keys);
		Iterator it = keys.iterator();
		while (it.hasNext()) {
			Integer l = (Integer) it.next();
			Client c = (Client) client.get(l);
			System.out.println(c.toString());
		}
		System.out.println();
		System.out.println("Total Users :" + keys.size());
	}

	public void logout(int sessionid) throws RemoteException {
		Client c = (Client) client.remove(new Integer(sessionid));
		if (c != null) {
			admin.logout(id, c.getSessionid());
			c.logout();
			log.info("user " + c.getUserid() + " logout from this gateway with sessionid " + c.getSessionid());
			unsubscribeAll(sessionid);
		}
	}

	public void subscribe(int sessionid, String msg) throws RemoteException {
		Client c = (Client) client.get(new Integer(sessionid));
		if (c != null) {
			subscribeTo(msg, sessionid);
			log.info("user " + c.getUserid() + " subscribe to " + msg);
		} else {

			Client cKill = (Client) clientKill.get(new Integer(sessionid));
			if (cKill != null) {
				clientKill.remove(sessionid);
				throw new RemoteException("session killed");
			}

			throw new RemoteException("session expired");
		}
	}

	public void unsubscribe(int sessionid, String msg) throws RemoteException {
		Client c = (Client) client.get(new Integer(sessionid));
		if (c != null) {
			unsubscribeFrom(msg, sessionid);
			log.info("user " + c.getUserid() + " unsubscribe from " + msg);
		} else {

			Client cKill = (Client) clientKill.get(new Integer(sessionid));
			if (cKill != null) {
				clientKill.remove(sessionid);
				throw new RemoteException("session killed");
			}

			throw new RemoteException("session expired");
		}
	}

	// broadcaster--------------------------------------------------------------------------------------------------//
	public void sendOtherSnapShot(Class cls, String param, int seqno, Client c) {
		List l = null;
		long start = System.nanoTime();
		if (cls.equals(TradePrice.class)) {
			l = consumer.getOtherConsumer().getTPSnapShot(param, seqno);
		} else if (cls.equals(Trade.class)) {
			l = consumer.getOtherConsumer().getTHSnapShot(param, seqno);
		} else if (cls.equals(TradeSummBrokerStock.class)) {
			l = consumer.getOtherConsumer().getTSBSSnapShot(param, seqno);
		} else if (cls.equals(TradeSummStockBroker.class)) {
			l = consumer.getOtherConsumer().getTSSBSnapShot(param, seqno);
		} else if (cls.equals(BrokerSummarry.class)) {
			l = consumer.getOtherConsumer().getBSSnapShot(param, seqno);
		} else {
			l = consumer.getOtherConsumer().getSnapShot(cls, seqno);
		}
		if (l != null) {
			for (int i = 0; i < l.size(); i++) {
				c.addOther(((Message) l.get(i)).toString());
			}
			
			long end = System.nanoTime() - start;
			log.info("send_snaphot_" + cls + "_" + l.size() + "_"
					+ TimeUnit.MILLISECONDS.convert(end, TimeUnit.NANOSECONDS));

		}
	}

	public void setlimit(Integer lm){
		this.lm = lm;
	}
	Integer lm=20;
	public void subscribeTo(String msg, int sessionid) {
		//log.info("subscribeTo : " + msg + " sesid=" + sessionid);
		String[] data = msg.split("\\|");
		String req = data[0] + (data.length > 2 ? "|" + data[1] : "");
		if (data[0].equals("10") || data[0].equals("11")) {
			req = msg;
		}
		String param = data.length > 2 ? data[1] : "";
		Vector v = (Vector) request.get(req);
		if (v == null) {
			v = new Vector(10, 5);
			v.addElement(new Integer(sessionid));
			request.put(req, v);
		} else {
			synchronized (v) {
				v.addElement(new Integer(sessionid));
			}
		}
		Client c = (Client) client.get(new Integer(sessionid));
		if (data[0].equals("1")) {
			Quote q = consumer.getQuoteConsumer().getSnapShot(data[1]);
			if (q != null)
				c.addQuote(q.toString());
		} else if (data[0].equals("2")) {
		} else if (data[0].equals("3")) {
			sendOtherSnapShot(Stock.class, param, Integer.parseInt(data[1]), c);
		} else if (data[0].equals("4")) {
			sendOtherSnapShot(Broker.class, param, Integer.parseInt(data[1]), c);
		} else if (data[0].equals("5")) {
			sendOtherSnapShot(StockSummary.class, param,
					Integer.parseInt(data[1]), c);
			// 17012017
			log.info("ready SS "+sessionid+" subscribe M");
			try {
				subscribe(sessionid, "M|0");
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} else if (data[0].equals("6")) {
			sendOtherSnapShot(Indices.class, param, Integer.parseInt(data[1]),
					c);
		} else if (data[0].equals("9")) {
			sendOtherSnapShot(News.class, param, Integer.parseInt(data[1]), c);
		} else if (data[0].equals("TP")) {
			sendOtherSnapShot(TradePrice.class, param,
					Integer.parseInt(data[2]), c);
		} else if (data[0].equals("TS")) {
			sendOtherSnapShot(TradeSummary.class, param,
					Integer.parseInt(data[1]), c);
		} else if (data[0].equals("TSBB")) {
			sendOtherSnapShot(TradeSummBroker.class, param,
					Integer.parseInt(data[1]), c);
		} else if (data[0].equals("TSI")) {
			sendOtherSnapShot(TradeSummInv.class, param,
					Integer.parseInt(data[1]), c);
		} else if (data[0].equals("TSSI")) {
			sendOtherSnapShot(TradeSummStockInv.class, param,
					Integer.parseInt(data[1]), c);
		} else if (data[0].equals("TSMI")) {
			/*HashMap mp = consumer.getOtherConsumer().getTMSISnapshot();
			Iterator i = mp.values().iterator();
			for (; i.hasNext();) {
				TradeSummStockMarketInv cur = (TradeSummStockMarketInv) i.next();
				c.addOther(cur.toString());
			}*/
			sendOtherSnapShot(TradeSummStockMarketInv.class, param,
					Integer.parseInt(data[1]), c);
		} else if (data[0].equals("TSBS")) {
			sendOtherSnapShot(TradeSummBrokerStock.class, param,
					Integer.parseInt(data[2]), c);
		} else if (data[0].equals("TSSB")) {
			sendOtherSnapShot(TradeSummStockBroker.class, param,Integer.parseInt(data[2]), c);
		} else if (data[0].equals("BS")) {
			sendOtherSnapShot(BrokerSummarry.class, param,Integer.parseInt(data[2]), c);
		} else if (data[0].equals("TH")) {			
			sendOtherSnapShot(Trade.class, param, Integer.parseInt(data[2]), c);			
		} /*else if (data[0].equals("NW")) {
			System.out.println("NW gtwService ; "+data[2]);
			sendOtherSnapShot(Article.class, param, Integer.parseInt(data[2]), c);
		} */else if (data[0].equals("CR")) {
			HashMap mp = consumer.getOtherConsumer().getCRSnapShot();
			Iterator i = mp.values().iterator();
			for (; i.hasNext();) {
				Currency cur = (Currency) i.next();
				c.addOther(cur.toString());
			}
		}else if (data[0].equals("CRM")) {
			HashMap mp = consumer.getOtherConsumer().getCRMSnapShot();
			Iterator i = mp.values().iterator();
			for (; i.hasNext();) {
				CurrencyMobile cur = (CurrencyMobile) i.next();
				c.addOther(cur.toString());
			}
		} else if (data[0].equals("GI")) {
			HashMap mp = consumer.getOtherConsumer().getGISnapShot();
			Iterator i = mp.values().iterator();
			for (; i.hasNext();) {
				GlobalIndices idx = (GlobalIndices) i.next();
				c.addOther(idx.toString());
			}
		}else if (data[0].equals("GIM")) {
			HashMap mp = consumer.getOtherConsumer().getGIMSnapShot();
			Iterator i = mp.values().iterator();
			for (; i.hasNext();) {
				GlobalIndicesMobile idx = (GlobalIndicesMobile) i.next();
				c.addOther(idx.toString());
			}
		} else if (data[0].equals("NWM")) {
//			HashMap mp = consumer.getOtherConsumer().getNWMSnapShot();
			// 17012017
//			for (; i.hasNext();) {
//			Iterator i = mp.values().iterator();
//			Article art = (Article) i.next();
//			c.addOther(art.toString());
//			for (int ca =0 ; ca<lm ; ca++) {
//				Article art = (Article) i.next();
//				c.addOther(art.toString());

//			}
//			for (; i.hasNext();) {
//				Article art = (Article) i.next();
//				rcvOther(art);

//			}
		} else if (data[0].equals("NW")) {
			//log.info("request NW");
			HashMap mp = consumer.getOtherConsumer().getNWSnapShot();
			Iterator i = mp.values().iterator();
			
			//Vector<Article> vArt = new Vector<Article>();
			//StringBuffer sbf = new StringBuffer();
			for (; i.hasNext();) {
				Article art = (Article) i.next();
				c.addOther(art.toString());
				//vArt.add(art);
				//sbf.append(art.toString()).append("\n");
			}
			/*byte[] btr = sbf.toString().getBytes();
			byte[] bt = CompressionEncoder.compress(btr);
			//log.info("sbf : " + sbf.toString());
			log.info("request NW done "+bt.length+" "+btr.length);
			c.addOther(bt);*/
			
		} else if (data[0].equals("A")) {
			HashMap mp = consumer.getOtherConsumer().getCASnapShot();
			Iterator i = mp.values().iterator();
			for (; i.hasNext();) {
				CorpAction ca = (CorpAction) i.next();
				c.addOther(ca.toString());
			}
		} else if (data[0].equals("IPO")) {
			HashMap mp = consumer.getOtherConsumer().getIPOSnapShot();
			Iterator i = mp.values().iterator();
			for (; i.hasNext();) {
				IPO ipo = (IPO) i.next();
				c.addOther(ipo.toString());
			}
		} else if (data[0].equals("RPS")) {
			HashMap mp = consumer.getOtherConsumer().getRupsSnapShot();
			Iterator i = mp.values().iterator();
			for (; i.hasNext();) {
				Rups rups = (Rups) i.next();
				//log.info("get RPS:" + rups.toString());
				c.addOther(rups.toString());
			}
			//log.info("get RPS size:" + mp.size());
		} else if (data[0].equals("10") || data[0].equals("11")) {
			Quote q = consumer.getQuoteConsumer().getSnapShot(data[1]);
			// Queue qu = consumer.getQuoteConsumer().getSnapShot(data[1]);
			//log.info("Gtwservice = " + msg);
			String sdata = (String) mapQuoteQueue.get(msg);
			//log.info("Gtwservice = data[0]=" + data[0] +" data1="+ data[1] + " data "+ sdata);
			if (sdata == null)
				//log.info(mapQuoteQueue.toString());

			if (sdata != null) {
				// log.info(sdata);
				c.addQuoteQueue(sdata);
			}
		} else if (data[0].equals("COM")){
			HashMap mp = consumer.getOtherConsumer().getCOMSnapShot();
			Iterator i = mp.values().iterator();
			for (; i.hasNext();){
				Commodity idx = (Commodity)i.next();
				c.addOther(idx.toString());
				//log.info("subscribe com"+idx.toString());
			}
		} else if(data[0].equals("SN")){
			HashMap mp =consumer.getOtherConsumer().getSNSnapShot();
			Iterator i = mp.values().iterator();
			for(;i.hasNext();){
				StockNews sn = (StockNews)i.next();
				c.addOther(sn.toString());
				//log.info("Subscribe Stock News"+sn.toString());
			}
		} else if(data[0].equals("SNSD")){
			HashMap mp =consumer.getOtherConsumer().getSNSDSnapShot();
			Iterator i = mp.values().iterator();
			for(;i.hasNext();){
				StockNews sn = (StockNews)i.next();
				c.addOther(sn.toString());
			}
		} else if(data[0].equals("CBS")){
			HashMap mp = consumer.getOtherConsumer().getCBSSnapShot();
			Iterator i = mp.values().iterator();
			for(;i.hasNext();){
				StockComparisonBySector scbs = (StockComparisonBySector)i.next();
				c.addOther(scbs.toString());
				//log.info("Subscribe Stock Comparison By Sector"+scbs.toString());
			}
		}
		else if(data[0].equals("SC")){
			HashMap mp = consumer.getOtherConsumer().getSCSnapShot();
			Iterator i = mp.values().iterator();
			for(;i.hasNext();){
				Sector s = (Sector)i.next();
				c.addOther(s.toString());
				
			}
		} else if (data[0].equals("FTR")){
			HashMap mp = consumer.getOtherConsumer().getFTRSnapShot();
			Iterator i = mp.values().iterator();
			for (; i.hasNext();){
				Future idx = (Future)i.next();
				c.addOther(idx.toString());
				//log.info("subscribe cm"+idx.toString());
			}
		} else if (data[0].equals("TG")){
			/*Vector mp = consumer.getOtherConsumer().selectTopGainer(data[1]);
			Iterator i = mp.iterator();
			for (; i.hasNext(); ){
				TopGainer st = (TopGainer) i.next();
//				st.setType("TG");
				c.addOther(st.toString());
			}*/
			sendOtherSnapShot(TopGainer.class, data[1], Integer.parseInt(data[2]), c);
		} else if (data[0].equals("SM")) {
			sendOtherSnapShot(Stock.class, param, Integer.parseInt(data[1]), c);
		} 
		
		//#Valdhy 20141219
		//--- Mobile Section - Start ---
		//Stock Summary Mobile
		else if (data[0].equals("SSM")) {
			System.out.println(data[0]+" Stock Summary Mobile");			
			sendOtherSnapShot(StockSummaryMobile.class, msg, Integer.parseInt(data[2]), c);
		}
		//Market Mobile
		else if (data[0].equals("MM")) {
			System.out.println(data[0]+" Market Mobile");			
			sendOtherSnapShot(MarketMobile.class, param, Integer.parseInt(data[1]), c);
		}
		//History Intraday Mobile
		else if (data[0].equals("MHI")) {
			System.out.println(data[0]+" History Intraday Mobile");			
			sendOtherSnapShot(ChartIntraday.class, param, Integer.parseInt(data[1]), c);
		}
		//Trade Stock by Broker Mobile
		else if (data[0].equals("TSBSM")) {
			System.out.println(data[0]+" Trade Stock by Broker Mobile");			
			sendOtherSnapShot(TradeSummBrokerStockMobile.class, data[1], Integer.parseInt(data[2]), c);
		}
		//Trade Broker by Stock Mobile
		else if (data[0].equals("TSSBM")) {
			System.out.println(data[0]+" Trade Broker by Stock Mobile");			
			sendOtherSnapShot(TradeSummStockBrokerMobile.class, data[1], Integer.parseInt(data[2]), c);
		}
		else if (data[0].equals("MTH")){
			sendOtherSnapShot(TradeMobile.class, data[1], Integer.parseInt(data[2]), c);
		}// stock mobile 
		else if (data[0].equals("3M")){
			sendOtherSnapShot(StockMobile.class, data[1], Integer.parseInt(data[2]), c);
		}
		//broker mobile 
		else if (data[0].equals("4M")){
			sendOtherSnapShot(BrokerMobile.class, data[1], Integer.parseInt(data[2]), c);
		}
		//--- Mobile Section - End ---

	}

	public void unsubscribeFrom(String msg, int sessionid) {
		Vector v = (Vector) request.get(msg);
		if (v != null) {
			v.remove(new Integer(sessionid));
		}
	}

	public void unsubscribeAll(int sessionid) {
		synchronized (request) {
			for (Iterator i = request.values().iterator(); i.hasNext();) {
				Vector v = (Vector) i.next();
				if (v != null)
					v.remove(new Integer(sessionid));
			}
		}
	}

	public void rcvTrade(Trade msg) {
		// System.out.println("receive: "+msg.toString());
		Vector v = (Vector) request.get(msg.getType());
		if (v != null && v.size() != 0) {
			
			String trade = msg.toString();
			//log.info("trade reply "+msg.getSeqno()+" "+v.size());
			synchronized (v) {
				for (int i = 0; i < v.size(); i++) {
					Integer sessionid = (Integer) v.elementAt(i);
					Client client = getClient(sessionid.intValue());
					if (client != null)
						client.addTrade(trade);
				}
			}
		}
	}

	public void rcvQuote(Quote msg) {
		Vector v = (Vector) request.get(msg.getType() + "|" + msg.getStock()
				+ "#" + msg.getBoard());
		String quote = "";
		if (v != null && v.size() != 0) {
			quote = msg.toString();
			//log.info("quote broadcast "+msg.getSeqno()+" "+v.size());
			synchronized (v) {
				for (int i = 0; i < v.size(); i++) {
					Integer sessionid = (Integer) v.elementAt(i);
					Client client = getClient(sessionid.intValue());
					if (client != null)
						client.addQuote(quote);
				}
			}
		}
		//log.info("receive quote finished "+quote);
	}

	/*public void rcvQuoteQueue(Quote msg) {
		log.info("rcvQuoteQueue on GtwService=" + msg);
		// Vector v = (Vector) request.get(msg.getType() + "|" + msg.getStock()
		// + "|" + msg.getBidPriceQueue()
		// + "|" + msg.getBoard());
		Vector v = (Vector) request.get(msg.getType() + "#" + msg.getStock()
				+ "#" + msg.getBidPriceQueue() + "#" + msg.getBoard());
		if (v != null && v.size() != 0) {
			String queue = msg.toStringBidOrder();
			synchronized (v) {
				for (int i = 0; i < v.size(); i++) {
					Integer sessionid = (Integer) v.elementAt(i);
					Client client = getClient(sessionid.intValue());
					if (client != null)
						client.addQuoteQueue(queue);
				}
			}
		}
	}*/

	public void rcvQuoteQueue1(HashMap map) {

		Iterator keys = map.keySet().iterator();
		// System.out.println(keys);
		// log.info("rcvQuoteQueue1 key2: "+keys.toString());
		while (keys.hasNext()) {
			// Double key =(Double) keys.next();
			String key2 = (String) keys.next();
			String isidata = (String) map.get(key2);

			// System.out.println(key +" ="+ isidata );
			mapQuoteQueue.put(key2, isidata);

			Vector v = (Vector) request.get(key2);

			if (v != null && v.size() != 0) {
				// String queue = msg.toStringBidOrder();
				synchronized (v) {
					for (int i = 0; i < v.size(); i++) {
						Integer sessionid = (Integer) v.elementAt(i);
						Client client = getClient(sessionid.intValue());
						// log.info("rcvQuoteQueue1 : "+ key2 + " vector "+ v +
						// "cl:"+ client);
						if (client != null)
							client.addQuoteQueue(isidata);
					}
				}
				//log.info("rcvQuoteQueue1 finished "+key2);	
			}
		}
		//log.info("rcvQuoteQueue1 finished "+keys);
	}

	public void rcvOther(Message msg) {
		//System.out.println("rcvOther GtwService");
		String req;
		if (msg instanceof TradePrice) {
			req = msg.getType() + "|" + ((TradePrice) msg).getStock() + "#" + ((TradePrice) msg).getBoard();
		} else if (msg instanceof Trade) {
			req = msg.getType() + "|" + ((Trade) msg).getStock();
		} else if (msg instanceof TradeSummBrokerStock) {
			req = msg.getType() + "|" + ((TradeSummBrokerStock) msg).getBroker();
		} else if (msg instanceof TradeSummStockBroker) {
			req = msg.getType() + "|" + ((TradeSummStockBroker) msg).getStock();
		} else {
			req = msg.getType();
		}
		Vector v = (Vector) request.get(req);
		if (v != null && v.size() != 0) {
			String other = msg.toString();
			synchronized (v) {
				for (int i = 0; i < v.size(); i++) {
					Integer sessionid = (Integer) v.elementAt(i);
					Client client = getClient(sessionid.intValue());
					if (client != null)
						client.addOther(other);
					
					//test yosep 
					if (msg instanceof Indices) {
						System.out.println(sessionid.intValue()+" rcvOther Indices");
					}
				}
			}
		}
	}

	public void heartbeat() throws RemoteException {
	}

	public void kill(long arg0) throws RemoteException {
		Client c = (Client) client.remove(new Integer((int) arg0));
		if (c != null) {
			c.logout();
			/*log.warn("user " + c.getUserid() + " with sessionid " + arg0
					+ " has been killed by server");*/
			unsubscribeAll((int) arg0);
			c.kick();
			clientKill.put(new Integer((int) arg0), c);
		}
	}

	public void cleaningSessionKill() {
		Enumeration en = clientKill.keys();
		long delta = 30000;
		Vector vtemp = new Vector(100, 1);
		while (en.hasMoreElements()) {
			Integer key = (Integer) en.nextElement();
			Client c = (Client) clientKill.get(key);
			c.age(delta);
			/*log.info("clientKill:" + key + ":" + c.getUserid() + ":"
					+ c.isExpired());*/
			if (c.isExpired())
				vtemp.addElement(key);
		}
		for (int i = 0; i < vtemp.size(); i++) {
			Integer key = (Integer) vtemp.elementAt(i);
			clientKill.remove(key);
		}
	}

	public byte[] getQueue(int sesid, String key) throws RemoteException {

		//log.info("getQueue on Gtwservice key =" + key);
		String queue = (String) mapQuoteQueue.get(key);

		return (queue != null) ? Utils.compress(queue.toString().getBytes())
				: null;
	}
	public FeedConsumer getConsumer(){
		return consumer;
	}
	public AccessQueryLocator getaccessData(){
		return this.accessData;
	}
}
