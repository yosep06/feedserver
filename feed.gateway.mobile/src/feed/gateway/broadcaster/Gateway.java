package feed.gateway.broadcaster;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Gateway extends Remote{
	public int login(String userid, String pass) throws RemoteException;
	public void logout(int sessionid) throws RemoteException;
	public boolean chgPassword(int sessionid, String userid, String pass, String newPass) throws RemoteException;
	public void subscribe(int sessionid, String msg) throws RemoteException;
	public void unsubscribe(int sessionid, String msg) throws RemoteException;
	public boolean  heartbeat(int sessionid) throws RemoteException;
	public byte[] getMessages(int sessionid) throws RemoteException;
	public String getDate() throws RemoteException;
	public long getTime() throws RemoteException;
	public byte[] getQueue(int sesid,String key) throws RemoteException;
	public byte[] getHistory(int sessionid, String msg) throws RemoteException;
}
