package feed.gateway.broadcaster;

import java.util.List;
import java.util.Vector;

import org.jfree.util.Log;

import feed.provider.core.Utils;

public class Client {
	protected int sessionid;
	protected String userid;
	protected String pass;
	
	protected Vector vtrade,vtradeByte;
	protected Vector vquote,vquoteByte;
	protected Vector vother, votherByte,vstocksummtemp;
	// protected long live = 720*1000;
	protected long live = 60 * 2 * 1000;
	protected volatile long timeToLive = live;
	protected Vector vqueue;

	
	public Client(int sessionid, String userid, String pass){
		this.sessionid = sessionid;
		this.userid = userid;
		this.pass = pass;
		vtrade = new Vector(100,5);
		vquote = new Vector(100,5);
		vother = new Vector(100,5);
		vqueue = new Vector(100,5);
		
		vtradeByte = new Vector(100,5);
		vquoteByte = new Vector(100,5);
		votherByte = new Vector(100,5);
		vstocksummtemp = new Vector(100,5);
	}
	
	public void age(long aDeltaMillis) {
		timeToLive -= aDeltaMillis;
	}

	public boolean isExpired() {
		return timeToLive <= 0;
	}

	public void kick() {
		timeToLive = live;
	}

	
	public String toString(){
		return "client sessionid: "+sessionid+" userid "+userid+" total queue("+vtrade.size()+", "+vquote.size()+", "+vother.size()+")";		
	}
	
	public int getSessionid(){
		return sessionid;
	}
	
	public String getUserid(){
		return userid;
	}
	
	public String getPass(){
		return pass;		
	}
	
	public void setPass(String pass){
		this.pass = pass;
	}
	
	public byte[] getMessage(){
		kick();
		StringBuffer temp = new StringBuffer();
		synchronized(vtrade){				
			 int start = vtrade.size() >30 ? vtrade.size() - 30 : 0;
             for (int i=0; vtrade.size()>start && i<30;i++){
                 temp.append(vtrade.remove(start).toString().concat("\n"));
             }
             vtrade.clear();
		}
		synchronized(vquote){
			for (int i=0; vquote.size()>0 && i<50;i++){
				temp.append(vquote.remove(0).toString().concat("\n"));
			}
		}
		/*synchronized(vother) {
			int start = vother.size() >30 ? vother.size() - 30 : 0;
			for (int i=0; vother.size()>start && i<30;i++) {	
				temp.append(vother.remove(start).toString().concat("\n"));
			}			
		}*/
		synchronized(vother) {
			for (int i=0; vother.size()>0 && i<30;i++) {	
				temp.append(vother.remove(0).toString().concat("\n"));
			}			System.out.println("other "+temp);
		}
		synchronized(vqueue){
			for (int i=0; vqueue.size()>0 && i<50;i++){
				temp.append(vqueue.remove(0).toString().concat("\n"));
			}
		}
		
		return  (temp.length() > 0) ? Utils.compress(temp.toString().getBytes()) : null;
	}
	
	public void logout(){
		vtrade.clear();
		vquote.clear();
		vother.clear();
		vqueue.clear();
	}
	
	public void addTrade(String trade){
		synchronized(vtrade){
			vtrade.addElement(trade);
		}
	}
	
	public void addQuote(String quote){
		synchronized(vquote){
			vquote.addElement(quote);
		}		
	}
	
	public void addOther(String other){
		synchronized(vother){
			vother.addElement(other);
		}
	}

	public void addQuoteQueue(String queue) {
		synchronized(vqueue){
			vqueue.addElement(queue);
		}		
	}

	public void addTrade(byte[] trade){
		synchronized(vtradeByte){
			vtradeByte.addElement(trade);
		}
	}
	
	public void addListTrade(List<byte[]> trade) {
		synchronized (vtradeByte) {
			vtradeByte.addAll(trade);
		}
	}
	public void addQuote(byte[] quote){
		synchronized(vquoteByte){
			vquoteByte.addElement(quote);
		}		
	}
	
	public void addOther(byte[] other){
		synchronized(votherByte){
			votherByte.addElement(other);
		}
	}
	
	public void addStockSumm(String other) {
		synchronized (vstocksummtemp) {
			vstocksummtemp.addElement(other);
		}
	}
	
}
